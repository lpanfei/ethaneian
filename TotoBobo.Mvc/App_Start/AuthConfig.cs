﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using System.Web.Security;

namespace TotoBobo.Mvc
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
             WebSecurity.InitializeDatabaseConnection("DefaultConnection", "User", "UserId", "UserName", autoCreateTables: true);
            if (!WebSecurity.UserExists("admin"))
            {
                WebSecurity.CreateUserAndAccount("admin", "admin", new { Email = "admin@totobobo.myinfinitechsg.com" }, false);
            }

            SimpleRoleProvider provider = Roles.Provider as SimpleRoleProvider;
            if (provider != null)
            {
                if (!provider.RoleExists(ConstantHelper.AdminRole))
                {
                    provider.CreateRole(ConstantHelper.AdminRole);
                }
                if (!provider.RoleExists(ConstantHelper.QAAdminRole))
                {
                    provider.CreateRole(ConstantHelper.QAAdminRole);
                }
                if (!provider.RoleExists(ConstantHelper.SalesRole))
                {
                    provider.CreateRole(ConstantHelper.SalesRole);
                }
                if (!provider.RoleExists(ConstantHelper.CustomerRole))
                {
                    provider.CreateRole(ConstantHelper.CustomerRole);
                }
                if (!provider.IsUserInRole("admin", ConstantHelper.AdminRole))
                {
                    provider.AddUsersToRoles(new[] { "admin" }, new[] { ConstantHelper.AdminRole });
                }
            }
        }
    }
}