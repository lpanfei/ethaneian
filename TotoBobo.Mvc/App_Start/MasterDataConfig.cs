﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc
{
    public static class MasterDataConfig
    {
        public static void InitializeMasterData()
        {
            string country = System.Web.Configuration.WebConfigurationManager.AppSettings["AppliedCountry"];
            List<Country> countries = Country.Select(new Country.Parms() { ByName = true, Name = country });
            if (countries == null || !countries.Any())
            {
                Country.Insert(new Country() { Name = country, IsEnabled = true, Id = 0 });
            }
        }
    }
}