﻿using System.Web;
using System.Web.Optimization;

namespace TotoBobo.Mvc
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/flexslider.css",
                "~/Content/fancySelect.css",
                "~/Content/animate.css",
                "~/Content/style.css",
                 "~/Scripts/admin/datatables/datatables.css"
                ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            bundles.Add(new ScriptBundle("~/bundles/totobobo").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/superfish.js",
                "~/Scripts/jquery.sticky.js",
                "~/Scripts/parallax.js",
                "~/Scripts/jquery.flexslider.js",
                "~/Scripts/jquery.jcarousel.js",
                "~/Scripts/fancySelect.js",
                "~/Scripts/animate.js",
                 "~/Scripts/admin/datatables/jquery.dataTables.js",
                "~/Scripts/myscript.js"));
            bundles.Add(new ScriptBundle("~/bundles/totobobo/admin").Include(
                "~/Scripts/admin/jquery.js",
                "~/Scripts/admin/bootstrap.js",
                "~/Scripts/admin/app.js",
                 "~/Scripts/admin/slimscroll/jquery.slimscroll.js",
                 "~/Scripts/admin/sortable/jquery.sortable.js",
                  "~/Scripts/admin/app.plugin.js",
                  "~/Scripts/admin/datatables/jquery.dataTables.js",
                  "~/Scripts/admin/wysiwyg/jquery.hotkeys.js",
                  "~/Scripts/admin/wysiwyg/bootstrap-wysiwyg.js",
                  "~/Scripts/admin/wysiwyg/demo.js"
                ));
            bundles.Add(new StyleBundle("~/Content/admin/css").Include(
                "~/Content/admin/bootstrap.css",
                 "~/Content/admin/animate.css",
                "~/Content/admin/font-awesome.css",
                "~/Content/admin/icon.css",
                "~/Content/admin/font.css",
                 "~/Content/admin/app.css",
                  "~/Scripts/admin/datatables/datatables.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/totobobo/nestable").Include(
                "~/Scripts/admin/nestable/jquery.nestable.js",
                "~/Scripts/admin/nestable/demo.js"
                ));
            bundles.Add(new StyleBundle("~/Content/css/nestable").Include(
                 "~/Scripts/admin/nestable/nestable.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/totobobo/upload").Include(
                "~/Scripts/admin/upload/jquery.fineuploader.js"
                ));
            bundles.Add(new StyleBundle("~/Content/css/upload").Include(
                 "~/Scripts/admin/upload/fineuploader.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/totobobo/datepicker").Include(
                "~/Scripts/admin/datepicker/bootstrap-datepicker.js"
                ));
            bundles.Add(new StyleBundle("~/Content/css/datepicker").Include(
                 "~/Scripts/admin/datepicker/datepicker.css"
                ));
            bundles.Add(new StyleBundle("~/bundles/scripts/productview").Include(
                 "~/Scripts/jquery.tinysort.js",
                 "~/Scripts/jquery.simplePagination.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/totobobo/chart").Include(
                "~/Scripts/admin/charts/sparkline/jquery.sparkline.js",
                "~/Scripts/admin/charts/easypiechart/jquery.easy-pie-chart.js",
                 "~/Scripts/admin/charts/flot/jquery.flot.js",
                "~/Scripts/admin/charts/flot/jquery.flot.tooltip.js",
                 "~/Scripts/admin/charts/flot/jquery.flot.resize.js",
                "~/Scripts/admin/charts/flot/jquery.flot.orderBars.js",
                 "~/Scripts/admin/charts/flot/jquery.flot.pie.js",
                 "~/Scripts/admin/charts/flot/jquery.flot.grow.js",
                 "~/Scripts/admin/charts/flot/demo.js"
                ));
        }
    }
}