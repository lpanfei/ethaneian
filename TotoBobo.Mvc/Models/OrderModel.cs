﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;
using System.ComponentModel.DataAnnotations;
using TotoBobo.Mvc.Helper;

namespace TotoBobo.Mvc.Models
{
    public class UserOrderModel
    {
        public List<OrderModel> Orders { get; set; }
    }
    public class OrderModel
    {
        public int Id { get; set; }
        public decimal Total { get; set; }
        public decimal DeliveryFee { get; set; }
        public UserAddress ShippingAddress { get; set; }
        public ShippingAddressModel NewAddress { get; set; }
        public List<UserAddress> Addresses { get; set; }
        public string Status { get; set; }
        public string OrderNo { get; set; }
        public string Consignee { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<OrderItemModel> Items { get; set; }
        public DeliveryMethod DeliveryMethod { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public User CreatedBy { get; set; }
        public decimal TaxRate { get; set; }
        public string Guid { get; set; }
        public decimal RepeatCustomerDiscount { get; set; }
        public decimal RecordedInDiscount { get; set; }
        public decimal Discount
        {
            get
            {
                return Decimal.Round(RepeatCustomerDiscount * LastTotal / 100, 2);
            }
        }
        public decimal LastTotal
        {
            get;
            set;
        }
        public decimal Tax
        {
            get
            {
                return Decimal.Round(TaxRate * Total / 100, 2);
            }
        }
    }

    public class OrderItemModel
    {
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public string Picture { get; set; }
        public int Inventory { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Weight { get; set; }
        public List<ProductTag> Tags { get; set; }
        public bool IsHamper { get; set; }
        public List<Product> Products { get; set; }
        public int OrderItemId { get; set; }
        public string ProductType
        {
            get
            {
                if (IsHamper)
                    return "hamper";
                else
                    return "product";
            }
        }
    }

    public class PayModel
    {
        public string CreditType { get; set; }
        [Required]
        [Display(Name = "Card Number")]
        [CreditCard]
        public string CardNumber { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "3 Digits Security Pin")]
        [StringLength(3,MinimumLength=3,ErrorMessage="Please input only 3 digits")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Please numbers only for price.")]
        public string SecurityPin { get; set; }

        [Required]
        [Display(Name = "Expire Month")]
        [Range(1,12)]
        [RegularExpression(@"^\d+$", ErrorMessage = "Please numbers only for price.")]
        public int ExpireMonth { get; set; }

        [Required]
        [Display(Name = "Expire Year")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Please numbers only for price.")]
        [Range(1000,9999)]
        public int ExpireYear { get; set; }
        public Order Order { get; set; }
    }

    public class OrderCancelModel
    {
        public int OrderId { get; set; }

        [Display(Name="Remark")]
        public string Remark { get; set; }
    }
}