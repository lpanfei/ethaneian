﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Uri { get; set; }
        public string Uri2 { get; set; }
        public List<string> Pictures { get; set; }
        [Display(Name="Qty")]
        public int Quantity { get; set; }
        [System.Web.Mvc.AllowHtml]
        public string Description { get; set; }
        public int Inventory { get; set; }
        public bool IsFeatured { get; set; }
        public string ProductType { get; set; }
        public string CountryName { get; set; }
        public string CountryPicture { get; set; }
        public List<ProductPicture> ProductPictures { get; set; }
        public bool IsOutOfStock
        {
            get
            {
                return Inventory == 0;
            }
        }
        public bool IsSale
        {
            get;
            set;
        }
        public List<ProductTag> Tags { get; set; }
        public bool HasTag
        {
            get
            {
                return Tags != null && Tags.Any();
            }
        }
        public List<ProductSetItem> ProductSetItems { get; set; }
    }

    public class ProductCatModel
    {
        public List<CategoryModel> Categories { get; set; }
        public List<ProductModel> Products { get; set; }
        public List<Brand> Brands { get; set; }
        public string TopBanner { get; set; }
        public string Banner1 { get; set; }
        public string Banner1Link { get; set; }
        public int CategoryId { get; set; }
        public int BrandId { get; set; }
    }

    public class ProductSaleModel
    {
        public List<CategoryModel> Categories { get; set; }
        public List<ProductModel> Products { get; set; }
        public string TopBanner { get; set; }
        public string Banner1 { get; set; }
        public string Banner1Link { get; set; }
    }

    public class ProductDetailModel
    {
        public List<CategoryModel> Categories { get; set; }
        public ProductModel Product { get; set; }
        public List<ProductTag> Tags { get; set; }
        public List<ProductModel> Recommends { get; set; }
        public List<ProductModel> Relates { get; set; }
        public List<ProductSetItem> Items { get; set; }
        public List<ProductModel> Accessories { get; set; }
        public string ProductType { get; set; }
        public string TopBanner { get; set; }
        public string Banner1 { get; set; }
        public string Banner1Link { get; set; }
    }

    public class GiftSetModel
    {
        public List<CategoryModel> Categories { get; set; }
        public List<ProductModel> Products { get; set; }
        public string TopBanner { get; set; }
        public string Banner1 { get; set; }
        public string Banner1Link { get; set; }
    }
}