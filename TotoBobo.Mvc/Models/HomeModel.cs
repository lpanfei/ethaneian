﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;
using System.ComponentModel.DataAnnotations;

namespace TotoBobo.Mvc.Models
{
    public class HomeModel
    {
        public List<HomeBanner> HomeBanners { get; set; }
        public List<FeatureProductRow> FeaturedProductRows { get; set; }
        public List<ProductModel> NewArraivals { get; set; }
        public List<Brand> Brands { get; set; }
    }

    public class FeatureProductRow
    {
        public List<ProductModel> FeaturedProducts { get; set; }
    }

    public class FAQModel
    {
        public List<Question> Questions { get; set; }
        public Question Question { get; set; }
        public List<Doctor> Doctors { get; set; }

        [Required]
        [Display(Name="Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }


        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "")]
        public string Keywords { get; set; }

        [Required]
        [Display(Name="Type")]
        public int TypeId { get; set; }

        [Display(Name = "Doctor")]
        public int DoctorId { get; set; }
    }

    public class ReplyModel
    {
        public int QuestionId { get; set; }
        public int RepliedTo { get; set; }

        [Display(Name="Body")]
        [Required]
        public string Body { get; set; }
    }
}