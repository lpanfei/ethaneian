﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;
using System.ComponentModel.DataAnnotations;

namespace TotoBobo.Mvc.Models
{
    public class CartModel
    {
        public List<CartItemModel> Items { get; set; }
        public int Quantity
        {
            get
            {
                return Items.Sum(item => item.Quantity);
            }
        }
        public decimal Total
        {
            get
            {
                return Decimal.Round(Items.Sum(item => item.Price * item.Quantity), 2);
            }
        }
        public decimal TaxRate { get; set; }
        public decimal Tax
        {
            get
            {
                return Decimal.Round(TaxRate * Total/100, 2);
            }
        }

        public decimal RepeatCustomerDiscount { get; set; }
        public decimal Discount
        {
            get
            {
                return Decimal.Round(RepeatCustomerDiscount * LastTotal / 100, 2);
            }
        }

        public decimal LastTotal { get; set; }
    }


    public class CartItemModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string Url { get; set; }
        public decimal Weight { get; set; }
        public int Inventory { get; set; }
        public decimal SubTotal
        {
            get
            {
                return Decimal.Round(Price * Quantity, 2);
            }
        }
        public List<ProductTag> Tags { get; set; }
        public bool IsHamper { get; set; }
        public List<ProductModel> Products { get; set; }
    }

    public class CheckoutModel
    {
        public List<UserAddress> Addresses { get; set; }
        public int ShippingAddressId { get; set; }
        public ShippingAddressModel NewAddress { get; set; }
        public int StepNo { get; set; }
        public UserAddress SelectedAddress { get; set; }
        public CartModel Cart { get; set; }
        public DeliveryMethod Delivery { get; set; }
        public PaymentMethod Payment { get; set; }
        public decimal ShippingFee { get; set; }
        public int PaymentId { get; set; }
        public int DeliveryId { get; set; }
        public decimal TaxRate { get; set; }
        public decimal Tax
        {
            get
            {
                if (Cart == null)
                    return 0;
                return Decimal.Round(TaxRate * Cart.Total / 100, 2);
            }
        }
    }

    
}