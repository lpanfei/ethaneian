﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Controllers;
using System.Web.Mvc;

namespace TotoBobo.Mvc.Models
{
    public class AdminModel
    {
        public List<AssignRoleModel> Members { get; set; }

        public List<RoleModel> Roles { get; set; }

        public SearchRoleMode SearchModel { get; set; }
    }

    public class SearchRoleMode
    {
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }
    }

    public class AssignRoleModel
    {
        public int UserId { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Roles")]
        public List<RoleCheckBoxModel> Roles { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }
    }

    public class RoleCheckBoxModel
    {
        public string RoleName { get; set; }
        public bool IsChecked { get; set; }
    }

    public class RoleModel
    {
        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }
    }

    //
    public class SearchUserModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
    }

    public class ManageUserModel
    {
        public SearchUserModel SearchModel { get; set; }
        public List<UserModel> Users { get; set; }
    }

    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }

    public class EditUserModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }        

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    //

    public class SearchCategoryModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
    }

    public class ManageCategoryModel
    {
        public SearchCategoryModel SearchModel { get; set; }
        public List<CategoryModel> Categories { get; set; }
    }

    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CategoryModel> Children { get; set; }
    }

    public class EditCategoryModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Parent")]
        public int ParentId { get; set; }
    }

    public class SearchProductModel
    {
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
    }

    public class EditProductModel
    {
        public int Id { get; set; }
        public string GUID { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Required]
        [Display(Name = "Brand")]
        public int BrandId { get; set; }

        [Required]
        [Display(Name = "Country")]
        public int CountryId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        [System.Web.Mvc.AllowHtml]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Price($)")]
        [DataType(DataType.Currency)]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for price.")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Weight")]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for weight.")]
        public decimal Weight { get; set; }

        [Display(Name = "Pictures")]
        public List<ProductPicture> Pictures { get; set; }
        
        public List<ProductTag> Tags { get; set; }

         [Display(Name = "Related Products")]
        public List<RelatedProduct> RelatedItems { get; set; }

        [Display(Name="Is Recommended")]
        public bool IsRecommended { get; set; }

        [Display(Name = "Accessories")]
        public List<Accessory> Accessories { get; set; }

        public string AccessoryIds { get; set; }
       
        public string RelatedIds { get; set; }

        [System.Web.Mvc.AllowHtml]
        public string TagXml { get; set; }
    }

    public class ManageProductModel
    {
        public List<Product> Products { get; set; }

        public SearchProductModel SearchModel { get; set; }

        public List<Product> Sales { get; set; }
    }

    public class ManageOrderModel
    {
        public List<OrderModel> Orders { get; set; }
        [Display(Name="Order Status")]
        public string Status { get; set; }
        [Display(Name="Order No.")]
        public string OrderNo { get; set; }
    }

    public class StockProductModel
    {
        public int Id { get; set; }

        [Display(Name="Name")]
        public string Name { get; set; }

        [Display(Name = "Current Quantity")]
        public int CurrentQuantity { get; set; }

        [Required]
        [Display(Name = "Stock Quantity")]
        [Range(1,Int32.MaxValue,ErrorMessage="Please input numbers larger than 0")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Please numbers only for price.")]
        public int StockQuantity { get; set; }

        public List<StockHistory> Histories { get; set; }
    }

    public class ManageStateModel
    {
        public Country Country { get; set; }
        public List<StateModel> States { get; set; }
    }

    public class StateModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public int CountryId { get; set; }
        public List<CityModel> Cities { get; set; }
    }

    public class CityModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
    }

    public class FeeModel
    {
        public int DeliveryMethodId { get; set; }
        [Display(Name="Delivery Method")]
        public string DeliveryMethod { get; set; }
        public int StateId { get; set; }
        [Display(Name = "State")]
        public string StateName { get; set; }

        [Required]
        [Display(Name="Fee per Kg")]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for price.")]
        public decimal Fee { get; set; }
    }

    public class ManageFeeModel
    {
        public int DeliveryMethodId { get; set; }
        public string DeliveryMethod { get; set; }
        public List<FeeModel> Fees { get; set; }
    }

    public class ManagePaymentModel
    {
        public List<PaymentModel> Payments { get; set; }
    }

    public class PaymentModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name="Name")]
        public string Name { get; set; }

        [Display(Name = "Logo")]
        public string Logo { get; set; }
        public string GUID { get; set; }
        public string Status { get; set; }
        [Display(Name="Is Enabled")]
        public bool IsEnabled { get; set; }
    }

    public class ManageDeliveryModel
    {
        public List<DeliveryModel> Deliveries { get; set; }
    }

    public class DeliveryModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Logo")]
        public string Logo { get; set; }
        public string GUID { get; set; }
        public string Status { get; set; }
        [Display(Name = "Is Enabled")]
        public bool IsEnabled { get; set; }
    }

    public class ManageBrandModel
    {
        public List<BrandModel> Brands { get; set; }
    }

    public class BrandModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Logo")]
        public string Logo { get; set; }
        public string GUID { get; set; }
    }

    public class ManagePromotionModel
    {
        public List<FeaturedProductModel> FeaturedProducts { get; set; }
        public List<FeaturedProductModel> NonFeaturedProducts { get; set; }
        public List<FeaturedProductModel> RecommendProducts { get; set; }
        public List<FeaturedProductModel> NotRecommendProducts { get; set; }
    }

    public class FeaturedProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Uri { get; set; }
    }

    public class SettingModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Exchange Rate ( 1 dollar = ? IDR)")]
        [DataType(DataType.Currency)]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for exchange rate.")]
        public decimal ExchangeRate { get; set; }

        [Required]
        [Display(Name = "Tax Rate (%)")]
        [DataType(DataType.Currency)]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for exchange rate.")]
        public decimal TaxRate { get; set; }

        [Required]
        [Display(Name = "Gift Set Discount (%)")]
        [DataType(DataType.Currency)]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for gift set discount.")]
        public decimal GiftSetDiscount { get; set; }

        [Required]
        [Display(Name = "Repeat Customer Discount (%)")]
        [DataType(DataType.Currency)]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for repeat customer discount.")]
        public decimal RepeatCustomerDiscount { get; set; }
    }

    public class ManageBannerModel
    {
        public List<HomeBanner> HomeBanners { get; set; }
        public List<CategoryBannerModel> CategoryBanners { get; set; }
        public List<SaleBanner> SaleBanners { get; set; }
        public List<PopupBannerModel> PopupBanners { set; get; }
    }

    public class PopupBannerModel
    {
        [Display(Name = "Banner (600 x 600) ")]
        public string Banner { get; set; }
        public string GUID { get; set; }
    }

    public class HomeBannerModel
    {
        public int Id { get; set; }

        [Display(Name="Top Banner (1600 x 700) ")]
        public string TopBanner { get; set; }

        [Display(Name = "Banner 1 (570 x 180) ")]
        public string Banner1 { get; set; }

        [Display(Name = "Banner 2 (270 x 180)")]
        public string Banner2 { get; set; }

        [Display(Name = "Banner 3 (270 x 180)")]
        public string Banner3 { get; set; }

        [Display(Name = "Top Banner Link")]
        public string TopBannerLink { get; set; }

        [Display(Name = "Banner 1 Link")]
        public string Banner1Link { get; set; }

        [Display(Name = "Banner 2 Link")]
        public string Banner2Link { get; set; }

        [Display(Name = "Banner 3 Link")]
        public string Banner3Link { get; set; }

        public string GUID { get; set; }
    }

    public class CategoryBannerModel
    {
        public int Id { get; set; }

        [Display(Name="Category")]
        public string Name { get; set; }

        [Display(Name = "Top Banner (1600 x 360) ")]
        public string TopBanner { get; set; }

        [Display(Name = "Banner 1 (870 x 370) ")]
        public string Banner1 { get; set; }

        [Display(Name = "Banner 1 Link")]
        public string Banner1Link { get; set; }
        public string GUID { get; set; }
    }

    public class UpdateOrderModel
    {
        public int Id { get; set; }
        [Display(Name = "Order No.")]
        public string OrderNo { get; set; }

        [Display(Name="Current Status")]
        public string CurrentStatus { get; set; }

        [Display(Name = "New Status")]
        [Required]
        public string NewStatus { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Display(Name = "Shipping No. (required)")]
        public string ShippingNo { get; set; }

        public OrderModel Order { get; set; }
    }

    public class NewsletterModel
    {
        public int Id { get; set; }
        [Display(Name="Title")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Body")]
        [Required]
        [System.Web.Mvc.AllowHtml]
        public string Body { get; set; }

        [Display(Name = "SentType")]
        [Required]
        public string SentType { get; set; }

        [Display(Name = "StartDate ( MM/dd/yyyy )")]
        [Required]
        public string StartDate { get; set; }

        public bool IsEnabled { get; set; }
        public string ProductIds { get; set; }
        public List<NewsletterProductModel> Products { get; set; }
    }

    public class NewsletterProductModel
    {
        public bool IsChecked { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Picture { get; set; }
    }

    public class ManageNewsModel
    {
        public List<NewsletterModel> Newsletters { get; set; }
    }

    public class OrderStatisModel
    {
        public int Orders { get; set; }
        public int Products { get; set; }
        public int Customers { get; set; }
    }

    public class ManageSaleModel
    {
        public int Id { get; set; }
        public int ProductId {get;set;}
        [Display(Name="Price")]
        public decimal ProductPrice { get; set; }
        [Required]
        [Display(Name="Discount")]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for discount.")]
        public decimal Discount { get; set; }

        [Required]
        [Display(Name = "From Date ( MM/dd/yyyy )")]
        public string FromDate { get; set; }

        [Required]
        [Display(Name = "End Date ( MM/dd/yyyy )")]
        public string EndDate { get; set; }
        public bool ByValue { get; set; }

        public string ProductName { get; set; }
        public List<ProductDiscount> Discounts { get; set; }
    }

    public class QuestionTypeModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Name")]
        public string Name { get; set; }
    }

    public class QuestionModel
    {
        public int Id { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Answer")]
        [System.Web.Mvc.AllowHtml]
        [Required]
        public string Answer { get; set; }

        [Display(Name = "Email")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }

        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Display(Name = "Question Type")]
        public string QTType { get; set; }
    }

    public class ManageQTModel
    {
        public List<QuestionType> Types { get; set; }
    }

    public class ManageQuestionModel
    {
        public List<Question> Questions { get; set; }
    }

    public class ManageHamperModel
    {
        public List<ProductSet> ProductSets { get; set; }
    }

    public class EditHamperModel
    {
        public int Id { get; set; }
        public string GUID { get; set; }

        [Required]
        [Display(Name = "Max Optional Selection")]
        public int OptionalCount { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        [System.Web.Mvc.AllowHtml]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Price($)")]
        [DataType(DataType.Currency)]
        [RegularExpression(@"^[0-9,]+(\.\d{0,3})?$", ErrorMessage = "Numbers only for price.")]
        public decimal Price { get; set; }

        public List<ProductSetPicture> Pictures { get; set; }

        public List<ProductSetItem> Items { get; set; }

        public List<Product> Products { get; set; }

        public string MandatoryIds { get; set; }
        public string OptionalIds { get; set; }
    }

    public class ManageDoctorModel
    {
        public List<Doctor> Doctors { get; set; }
    }

    public class ManageCountryModel
    {
        public List<Country> Countries { get; set; }
    }

    public class EditCountryModel
    {
        public int Id { get; set; }
        public string GUID { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name="Picture")]
        public string Picture { get; set; }
    }


    public class EditDoctorModel
    {
        public int Id { get; set; }
        public string GUID { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Url")]
        public string Url { get; set; }

        [Display(Name = "Avator")]
        public string Avator { get; set; }
    }
}