﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{ 
    public static class CaptchaRepository
    {
        public static Captcha Get(string guid)
        {
            List<Captcha> list = Captcha.Select(new Captcha.Parms() { Guid=guid});
            if (list != null && list.Any())
                return list.FirstOrDefault();
            return null;
        }

        public static bool Validate(string guid, string text)
        {
            Captcha c = Get(guid);
            return c != null && c.Text.Equals(text, StringComparison.InvariantCultureIgnoreCase);
        }

        public static void Create(string guid, string captcha)
        {
            Captcha.Insert(new Captcha() { Guid = guid, Text = captcha });
        }

        public static void Delete(string guid)
        {
            Captcha.Delete(new Captcha() { Guid = guid });
        }
    }
}