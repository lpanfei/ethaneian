﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public class ProductRepository
    {
        public static List<Product> GetAllProducts()
        {
            return Product.Select(new Product.Parms() { AllProducts=true});
        }

        public static List<Product> GetActives()
        {
            return Product.Select(new Product.Parms() { ActiveSales=true });
        }

        public static void Hide(int id, bool isHidden)
        {
            Product.Hide(id, isHidden);
        }

        public static List<Product> GetProducts(int bid, int cid)
        {
            return Product.Select(new Product.Parms() { ProductList=true,BrandId=bid,CategoryId=cid});
        }

        public static Product GetProductById(int id)
        {
            List<Product> list = Product.Select(new Product.Parms { ById=true,Id=id});
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static Product GetProductByName(string name)
        {
            List<Product> list = Product.Select(new Product.Parms { ByName=true,Name=name });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static List<Product> GetRandomRelatedItems(Product product)
        {
            List<Product> sameCategoryProductList = new List<Product>();
            List<Product> productList = ProductRepository.GetAllProducts();
            if (productList != null && productList.Count > 0)
            {
                foreach (Product p in productList)
                {
                    if (p.Category.Id == product.Category.Id && p.Id != product.Id)
                    {
                        sameCategoryProductList.Add(p);
                    }
                }
            }

            Random r = new Random();
            while (sameCategoryProductList.Count > 8)
            {
                int index = r.Next(sameCategoryProductList.Count);
                sameCategoryProductList.RemoveAt(index);
            }

            return sameCategoryProductList;
        }

        public static List<Product> SearchProduct(string name)
        {
            return null;
        }

        public static void AddProduct(Product product)
        {
            Product.Insert(product);
        }

        public static void DeleteProductById(int id)
        {
            Product.Delete(new Product() { Id=id});
        }

        public static void UpdateProduct(Product product)
        {
            Product.Update(product);
        }

        public static List<StockHistory> GetStockHistories(int productId)
        {
            return StockHistory.Select(new StockHistory.Parms() { ProductId=productId});
        }

        public static void StockToInventory(int productid, int quantity,string username)
        {
            StockHistory.Insert(new StockHistory() { ProductId = productid, Quantity = quantity, CreatedBy = new User() { UserName=username} });
        }

        public static List<Product> GetProductsByCategory(int categoryId)
        {
            return Product.Select(new Product.Parms() { CategoryId=categoryId,ByCategory=true});
        }

        public static void Feature(int id)
        {
            Product.Feature(id, true);
        }

        public static void UnFeature(int id)
        {
            Product.Feature(id, false);
        }

        public static void Recommend(int id)
        {
            Product.Recommend(id, true);
        }

        public static void UnRecommend(int id)
        {
            Product.Recommend(id, false);
        }

        public static List<Product> GetRecommends()
        {
            return Product.Select(new Product.Parms() { Recommended=true});
        }

        public static List<ProductSet> GetHampers()
        {
            return ProductSet.Select(new ProductSet.Parms() { ByAll=true});
        }

        public static ProductSet GetHamper(int id)
        {
            List<ProductSet> list = ProductSet.Select(new ProductSet.Parms { ById = true, Id = id });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static ProductSet GetHamper(string name)
        {
            List<ProductSet> list = ProductSet.Select(new ProductSet.Parms { ByName=true,Name=name });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static void UpdateHamper(ProductSet hamper)
        {
            ProductSet.Update(hamper);
        }

        public static void DeleteHamper(int id)
        {
            ProductSet.Delete(new ProductSet() { Id = id });
        }
    }
}