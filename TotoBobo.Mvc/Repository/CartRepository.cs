﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;
using System.Xml.Linq;
using TotoBobo.Mvc.Models;

namespace TotoBobo.Mvc.Repository
{
    public static class CartRepository
    {
        public static void AddToCart(CartItem item)
        {
            CartItem.Update(item);
        }

        public static void RemoveCartItem(int id)
        {
            CartItem.RemoveItem(id);
        }

        public static void ClearCart(string sessionId)
        {
            CartItem.Delete(new CartItem() { SessionId = sessionId });
        }

        public static CartModel GetCart(string sessionId)
        {
            CartModel model = new CartModel() { Items = new List<CartItemModel>() };
            Setting setting = SettingRepository.GetSetting();
            if (setting != null)
            {
                model.TaxRate = setting.TaxRate;
                model.RepeatCustomerDiscount = setting.RepeatCustomerDiscount;
            }
            List<Order> orders=OrderRepository.GetUserOrder(HttpContext.Current.User.Identity.Name);
            if (orders == null || !orders.Any())
            {
                model.RepeatCustomerDiscount = 0;
            }

            if (orders.Any(o => o.Status == OrderStatus.Paid))
            {
                model.LastTotal = orders.Where(o => o.Status == OrderStatus.Paid).OrderByDescending(o => o.CreatedDate).FirstOrDefault().Total;
            }
            
            List<CartItem> items = CartItem.Select(new CartItem.Parms() { SessionId=sessionId});
          
            if (items != null)
            {
                List<Product> actives = ProductRepository.GetActives();
                foreach (var item in items)
                {
                    /*
                    if (item.Tags != null && item.Tags.Count > 0)
                    {
                        List<ProductTagValue> values = item.Tags[0].Values;
                        if (values != null && values.Count > 0)
                        {
                            item.Price = values[0].Price;
                        }
                    }
                    */

                    if (item.IsHamper)
                    {
                        CartItemModel cartitem = new CartItemModel() { Id = item.Id, ProductId = item.ProductId, Quantity = item.Quantity, Name = item.ProductSet.Name, Price = Decimal.Round(item.Price, 2),Inventory=item.ProductSet.Items.Where(i=>i.IsMandatory).Select(p=>p.Product.Quantity).Min(),IsHamper=true,Products=new List<ProductModel>(),Tags=item.Tags};

                        cartitem.Price = 0;

                        if (item.Tags != null)
                        {
                            foreach (ProductTag pt in item.Tags)
                            {
                                if (!cartitem.Products.Any(p => p.Id == pt.ProductId))
                                {
                                    ProductSetItem psi = item.ProductSet.Items.FirstOrDefault(i => i.ProductId == pt.ProductId);
                                    if (psi != null)
                                    {
                                        ProductModel pm = new ProductModel();
                                        pm.Name = psi.Product.Name;
                                        pm.Id = psi.ProductId;
                                        pm.Price = psi.Product.Price;

                                        if (!string.IsNullOrEmpty(pt.Key))
                                        {
                                            pm.Tags = item.Tags.Where(p => p.ProductId == pt.ProductId).ToList();
                                        }                                        
                                        cartitem.Products.Add(pm);

                                        if (pm.HasTag)
                                        {
                                            cartitem.Price += pm.Tags[0].Values[0].Price;
                                        }
                                        else
                                        {
                                            cartitem.Price += pm.Price;
                                        }
                                    }
                                }
                            }
                        }

                        cartitem.Price = Decimal.Round((cartitem.Price - setting.GiftSetDiscount * cartitem.Price / 100), 2);

                        if (item.ProductSet.Pictures != null && item.ProductSet.Pictures.Any())
                        {
                            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                            string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/" + uploadfolder);
                            ProductSetPicture defPic = item.ProductSet.Pictures.FirstOrDefault(p => p.IsDefault);
                            if (defPic == null)
                                defPic = item.ProductSet.Pictures.FirstOrDefault();
                            cartitem.Url = url + "/" + defPic.Picture;
                        }
                        else
                        {
                            cartitem.Url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                        }
                        model.Items.Add(cartitem);
                    }
                    else
                    {
                        if (item.Tags != null && item.Tags.Count > 0)
                        {
                            List<ProductTagValue> values = item.Tags[0].Values;
                            if (values != null && values.Count > 0)
                            {
                                item.Price = values[0].Price;
                            }
                        }

                        CartItemModel cartitem = new CartItemModel() { Id = item.Id, ProductId = item.ProductId, Quantity = item.Quantity, Name = item.Product.Name, Price = Decimal.Round(item.Price, 2), Weight = item.Product.Weight, Tags = item.Tags, Inventory = item.Product.Quantity };
                        if (actives != null && actives.Any(p => p.Id == item.ProductId))
                        {
                            cartitem.Price = decimal.Round(item.Price - actives.FirstOrDefault(p => p.Id == item.ProductId).DiscountAmount, 2);
                        }
                        if (item.Product.Pictures != null && item.Product.Pictures.Any())
                        {
                            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                            string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/" + uploadfolder);
                            ProductPicture defPic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                            if (defPic == null)
                                defPic = item.Product.Pictures.FirstOrDefault();
                            cartitem.Url = url + "/" + defPic.Picture;
                        }
                        else
                        {
                            cartitem.Url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                        }
                        model.Items.Add(cartitem);
                    }       
                }
            }
            return model;
        }

        public static void ChangeQty(int id, int qty)
        {
            CartItem.ChangeQty(id, qty);
        }
    }
}