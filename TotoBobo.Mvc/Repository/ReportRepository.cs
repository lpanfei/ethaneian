﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class ReportRepository
    {
        public static List<PieChart> GetOrderStatusChart()
        {
            return PieChart.Select(new PieChart.Parms() { ChartType=PieChartType.OrderStatusChart});
        }

        public static List<PieChart> GetBestSellerChart()
        {
            return PieChart.Select(new PieChart.Parms() { ChartType = PieChartType.BestSellerChart });
        }
    }
}