﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class SettingRepository
    {
        public static Setting GetSetting()
        {
            List<Setting> settings = Setting.Select(new Setting.Parms() { });
            if (settings != null && settings.Any())
                return settings.FirstOrDefault();
            return new Setting() { Id=0};
        }

        public static Setting Update(Setting setting)
        {
            return Setting.Insert(setting);
        }

        public static SaleBanner GetSaleBanner()
        {
            List<SaleBanner> settings = SaleBanner.Select(new SaleBanner.Parms() { });
            if (settings != null && settings.Any())
                return settings.FirstOrDefault();
            return new SaleBanner() { Id = 0 };
        }

        public static SaleBanner UpdateSaleBanner(SaleBanner setting)
        {
            return SaleBanner.Insert(setting);
        }
    }

    
}