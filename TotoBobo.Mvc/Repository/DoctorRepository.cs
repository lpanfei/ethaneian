﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class DoctorRepository
    {
        public static List<Doctor> GetDoctors()
        {
            return Doctor.Select(new Doctor.Parms() { });
        }

        public static Doctor GetDoctor(int id)
        {
            List<Doctor> doctors=Doctor.Select(new Doctor.Parms() { Id=id,ById=true});
            if (doctors != null && doctors.Any())
                return doctors.FirstOrDefault();
            return null;
        }

        public static void Update(Doctor doctor)
        {
            Doctor.Update(doctor);
        }

        public static void Delete(int id)
        {
            Doctor.Delete(new Doctor() { Id=id});
        }
    }
}