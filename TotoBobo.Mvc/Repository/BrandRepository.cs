﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class BrandRepository
    {
        public static List<Brand> GetBrands()
        {
            return Brand.Select(new Brand.Parms() { });
        }

        public static Brand GetBrand(int id)
        {
            List<Brand> list = Brand.Select(new Brand.Parms() { Id = id, ById = true });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static Brand GetBrand(string name)
        {
            List<Brand> list = Brand.Select(new Brand.Parms() { Name = name, ByName = true });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static void UpdateBrand(Brand payment)
        {
            Brand.Update(payment);
        }
        public static void AddBrand(Brand payment)
        {
            Brand.Insert(payment);
        }
        public static void DeleteBrand(int id)
        {
            Brand.Delete(new Brand() { Id=id});
        }
    }
}