﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class MasterDataRepository
    {
        public static void UpdateCountry(Country country)
        {
            Country.Update(country);
        }

        public static void DeleteCountry(int id)
        {
            Country.Delete(new Country() { Id=id});
        }

        public static List<Country> GetCountries()
        {
            return Country.Select(new Country.Parms() { });
        }

        public static Country GetCountry(int id)
        {
            List<Country> countries = Country.Select(new Country.Parms() {ById=true,Id=id });
            if (countries != null && countries.Any())
            {
                return countries.FirstOrDefault();
            }
            return null;
        }

        public static Country GetCountry(string name)
        {
            List<Country> countries = Country.Select(new Country.Parms() { ByName = true, Name = name });
            if (countries != null && countries.Any())
            {
                return countries.FirstOrDefault();
            }
            return null;
        }

        public static Country GetEnabledCountry()
        {
            List<Country> countries = Country.Select(new Country.Parms() { });
            if (countries != null && countries.Any(c=>c.IsEnabled))
            {
                return countries.FirstOrDefault(c=>c.IsEnabled);
            }
            return null;
        }

        public static List<State> GetStatesInCountry(int countryid)
        {
            return State.Select(new State.Parms() { ByCountry=true,CountryId=countryid});
        }

        public static State GetState(int id)
        {
            List<State> list= State.Select(new State.Parms() { ById=true,Id=id});
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static State GetState(string name)
        {
            List<State> list = State.Select(new State.Parms() { ByName = true, Name=name });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static void UpdateState(State state)
        {
            State.Update(state);
        }
        public static void AddState(State state)
        {
            State.Insert(state);
        }

        public static void DeleteState(int id)
        {
            State.Delete(new State() { Id=id});
        }

        public static City GetCity(int id)
        {
            List<City> list = City.Select(new City.Parms() { ById = true, Id = id });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static City GetCity(string name)
        {
            List<City> list = City.Select(new City.Parms() { ByName = true, Name = name });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static void UpdateCity(City city)
        {
            City.Update(city);
        }
        public static void AddCity(City city)
        {
            City.Insert(city);
        }

        public static void DeleteCity(int id)
        {
            City.Delete(new City() { Id = id });
        }

        public static List<PaymentMethod> GetPaymentMethods()
        {
            return PaymentMethod.Select(new PaymentMethod.Parms() { });
        }

        public static PaymentMethod GetPaymentMethod(int id)
        {
            List<PaymentMethod> list = PaymentMethod.Select(new PaymentMethod.Parms() { Id=id, ById=true});
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static PaymentMethod GetPaymentMethod(string name)
        {
            List<PaymentMethod> list = PaymentMethod.Select(new PaymentMethod.Parms() { Name=name, ByName=true });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static void UpdatePaymentMethod(PaymentMethod payment)
        {
            PaymentMethod.Update(payment);
        }
        public static void AddPaymentMethod(PaymentMethod payment)
        {
            PaymentMethod.Insert(payment);
        }

        public static List<DeliveryMethod> GetDeliveryMethods()
        {
            return DeliveryMethod.Select(new DeliveryMethod.Parms() { });
        }

        public static DeliveryMethod GetDeliveryMethod(int id)
        {
            List<DeliveryMethod> list = DeliveryMethod.Select(new DeliveryMethod.Parms() { Id = id, ById = true });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static DeliveryMethod GetDeliveryMethod(string name)
        {
            List<DeliveryMethod> list = DeliveryMethod.Select(new DeliveryMethod.Parms() { Name = name, ByName = true });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static void UpdateDeliveryMethod(DeliveryMethod delivery)
        {
            DeliveryMethod.Update(delivery);
        }
        public static void AddDeliveryMethod(DeliveryMethod delivery)
        {
            DeliveryMethod.Insert(delivery);
        }

        public static List<DeliveryFee> GetDeliveryFee(int deliveryMethodId)
        {
            return DeliveryFee.Select(new DeliveryFee.Parms() { ByDeliveryMethod=true,DeliveryMethodId=deliveryMethodId});
        }

        public static DeliveryFee GetDeliveryFee(int deliveryMethodId,int stateId)
        {
            List<DeliveryFee> list = DeliveryFee.Select(new DeliveryFee.Parms() { ByState = true, DeliveryMethodId = deliveryMethodId,StateId=stateId });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static void UpdateDeliveryFee(DeliveryFee fee)
        {
            DeliveryFee.Update(fee);
        }

        public static void AddDeliveryFee(DeliveryFee fee)
        {
            DeliveryFee.Insert(fee);
        }
    }
}