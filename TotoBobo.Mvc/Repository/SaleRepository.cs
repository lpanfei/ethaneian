﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class SaleRepository
    {
        public static List<ProductDiscount> Get(int productId)
        {
            return ProductDiscount.Select(new ProductDiscount.Parms() { ProductId=productId,ByProduct=true});
        }

        public static void Create(ProductDiscount pd)
        {
            ProductDiscount.Insert(pd);
        }

        public static void Delete(int id, string userName)
        {
            ProductDiscount.Delete(new ProductDiscount() { Id = id, CreatedBy = new User() { UserName=userName} });
        }
    }
}