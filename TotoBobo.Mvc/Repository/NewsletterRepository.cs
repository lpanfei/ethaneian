﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class NewsletterRepository
    {
        public static List<Newsletter> GetNewsletters()
        {
            return Newsletter.Select(new Newsletter.Parms() { });
        }

        public static Newsletter GetNewsletter(int id)
        {
            List<Newsletter> list = Newsletter.Select(new Newsletter.Parms() { Id = id, ById = true });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static void UpdateNewsletter(Newsletter payment)
        {
            Newsletter.Update(payment);
        }
        public static void AddNewsletter(Newsletter payment)
        {
            Newsletter.Insert(payment);
        }
        public static void DeleteNewsletter(int id)
        {
            Newsletter.Delete(new Newsletter() { Id = id });
        }
    }
}