﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class UserRepository
    {
        public static User DeleteUser(User user)
        {
            return User.Delete(user);
        }

        public static User GetUserByEmail(string email)
        {
            List<User> list = User.Select(new User.Parms() { Email = email });
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static List<UserAddress> GetAddressesForUser(string username)
        {
            return UserAddress.Select(new UserAddress.Parms() { UserName=username});
        }
        public static UserAddress UpdateUserAddress(UserAddress address)
        {
           return UserAddress.Update(address);
        }
        public static void DeleteUserAddress(UserAddress address)
        {
            UserAddress.Delete(address);
        }
        public static void MarkAddressAsDefault(int addressId)
        {
            UserAddress.MarkDefault(addressId);
        }
        public static List<User> GetAllUsers()
        {
            return User.Select(new User.Parms() { AllUsers=true});
        }
        public static User GetUserById(int id)
        {
            List<User> list = User.Select(new User.Parms() { ById=true,UserId=id});
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }
        public static User UpdateEmail(User user)
        {
            return User.Update(user);
        }
    }
}