﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class BannerRepository
    {
        public static void CreateHomeBanner(HomeBanner banner)
        {
            HomeBanner.Insert(banner);
        }

        public static void UpdateHomeBanner(HomeBanner banner)
        {
            HomeBanner.Update(banner);
        }

        public static List<HomeBanner> GetHomeBanners()
        {
            return HomeBanner.Select(new HomeBanner.Parms() { });
        }

        public static HomeBanner GetHomeBanner(int id)
        {
            List<HomeBanner> banners = HomeBanner.Select(new HomeBanner.Parms() { Id=id,ById=true});
            if (banners != null && banners.Any())
                return banners.FirstOrDefault();
            return null;
        }

        public static void DeleteHomeBanner(int id)
        {
            HomeBanner.Delete(new HomeBanner() { Id=id});
        }
    }
}