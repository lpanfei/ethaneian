﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class OrderRepository
    {
        public static int AddOrder(Order order)
        {
            return Order.Insert(order).Id;
        }

        public static List<Order> GetUserOrder(string username)
        {
            return Order.Select(new Order.Parms() { ByUser=true,UserName=username});
        }

        public static Order GetOrder(int id)
        {
            List<Order> orders = Order.Select(new Order.Parms() { ById=true,Id=id });
            if (orders != null && orders.Any())
                return orders.FirstOrDefault();
            return null;
        }

        public static Order GetOrder(string guid)
        {
            List<Order> orders = Order.Select(new Order.Parms() { ByGuid=true,Guid=guid});
            if (orders != null && orders.Any())
                return orders.FirstOrDefault();
            return null;
        }

        public static void UpdateOrderStatus(OrderHistory history)
        {
            OrderHistory.Insert(history);
        }

        public static void Pay(int orderId, string paymentId, string paymentGuid, OrderStatus status)
        {
            Order.Pay(orderId, paymentId, paymentGuid,status);
        }

        public static List<Order> GetAllOrders()
        {
            return Order.Select(new Order.Parms() { AllOrders=true});
        }
    }
}