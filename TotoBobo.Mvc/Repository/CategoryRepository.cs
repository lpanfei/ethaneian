﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public class CategoryRepository
    {
        public static List<Category> GetAllCategories()
        {
            return Category.Select(new Category.Parms() { AllCategories=true});
        }

        public static Category GetCategoryById(int id)
        {
            List<Category> list=Category.Select(new Category.Parms() { ById=true,Id=id});
            if (list != null && list.Any())
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public static List<Category> SearchCategories(string name)
        {
            return Category.Select(new Category.Parms() { ByName = true, Name=name});
        }

        public static void AddCategory(Category category)
        {
            Category.Insert(category);
        }

        public static void DeleteCategoryById(int id)
        {
            Category.Delete(new Category() { Id=id});
        }

        public static void UpdateCategory(Category category)
        {
            Category.Update(category);
        }

        public static void CreateBanner(Category category)
        {
            Category.CreateBanner(category);
        }
    }
}