﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;

namespace TotoBobo.Mvc.Repository
{
    public static class QuestionRepository
    {
        public static List<QuestionType> GetTypes()
        {
            return QuestionType.Select(new QuestionType.Parms() { ById=false});
        }

        public static QuestionType GetQuestionType(int id)
        {
            List<QuestionType> list = QuestionType.Select(new QuestionType.Parms() { ById = true,Id=id });
            if (list != null && list.Any())
                return list.FirstOrDefault();
            return null;
        }

        public static void AddType(QuestionType type)
        {
            QuestionType.Update(type);
        }

        public static void DeleteType(int id)
        {
            QuestionType.Delete(new QuestionType() { Id=id});
        }

        public static List<Question> GetQuestions()
        {
            return Question.Select(new Question.Parms() { ById=false});
        }

        public static void AddQuestion(Question question)
        {
            Question.Insert(question);
        }

        public static void DeleteQuestion(int id)
        {
            Question.Delete(new Question() { Id = id });
        }

        public static Question GetQuestion(int id)
        {
            List<Question> list = Question.Select(new Question.Parms() { ById = true, Id = id });
            if (list != null && list.Any())
                return list.FirstOrDefault();
            return null;
        }

        public static void Reply(Answer answer)
        {
            Answer.Update(answer);
        }

        public static Answer GetAnswer(int id)
        {
            List<Answer> list = Answer.Select(new Answer.Parms() {  Id = id });
            if (list != null && list.Any())
                return list.FirstOrDefault();
            return null;
        }
    }
}