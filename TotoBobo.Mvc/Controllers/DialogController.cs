﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TotoBobo.Mvc.Models;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Repository;
using TotoBobo.Mvc.Helper;

namespace TotoBobo.Mvc.Controllers
{
    public class DialogController : Controller
    {
        //
        // GET: /Dialog/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductQV()
        {
            ProductModel model = new ProductModel() { Pictures = new List<string>() {
                HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png")
            } };
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        if (HttpContext.Request.QueryString["type"] == "hamper")
                        {
                            model.ProductType = "hamper";
                            ProductSet product = ProductRepository.GetHamper(id);
                            if (product != null)
                            {
                                model.Id = product.Id;
                                model.Name = product.Name;
                                model.Price = Decimal.Round(product.Price, 2);
                                model.Quantity = 1;
                                model.Inventory = product.Items.Where(p => p.IsMandatory).Select(m => m.Product.Quantity).Min();
                                model.ProductSetItems = product.Items;
                                if (product.Pictures != null && product.Pictures.Any())
                                {
                                    model.Pictures = new List<string>();
                                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                    string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
                                    //if (product.Pictures.Count > 4)
                                    //{
                                    //    ProductSetPicture defPic = product.Pictures.FirstOrDefault(p => p.IsDefault);
                                    //    if (defPic == null)
                                    //        defPic = product.Pictures.FirstOrDefault();
                                    //    model.Pictures.Add(url + "/" + defPic.Picture);
                                    //    foreach (ProductSetPicture pp in product.Pictures.Where(p => !p.IsDefault).Take(3))
                                    //    {
                                    //        model.Pictures.Add(url + "/" + pp.Picture);
                                    //    }
                                    //}
                                    //else
                                    //{
                                        foreach (ProductSetPicture pp in product.Pictures)
                                        {
                                            model.Pictures.Add(url + "/" + pp.Picture);
                                        }
                                    //}
                                }
                            }
                        }
                        else
                        {
                            Product product = ProductRepository.GetProductById(id);
                            if (product != null)
                            {
                                model.Id = product.Id;
                                model.Name = product.Name;
                                model.Price = Decimal.Round(product.Price - product.DiscountAmount, 2);
                                model.Quantity = 1;
                                model.Inventory = product.Quantity;
                                model.Tags = product.Tags;
                                model.CountryName = product.Country != null ? product.Country.Name : "";
                                model.CountryPicture = product.Country != null ? product.Country.Picture : "";
                                if (!string.IsNullOrEmpty(model.CountryPicture))
                                {
                                    model.CountryPicture = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + model.CountryPicture;
                                }
                                if (product.Pictures != null && product.Pictures.Any())
                                {
                                    model.Pictures = new List<string>();
                                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                    string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
                                    if (product.Pictures.Count > 4)
                                    {
                                        ProductPicture defPic = product.Pictures.FirstOrDefault(p => p.IsDefault);
                                        if (defPic == null)
                                            defPic = product.Pictures.FirstOrDefault();
                                        model.Pictures.Add(url + "/" + defPic.Picture);
                                        foreach (ProductPicture pp in product.Pictures.Where(p => !p.IsDefault).Take(3))
                                        {
                                            model.Pictures.Add(url + "/" + pp.Picture);
                                        }
                                    }
                                    else
                                    {
                                        foreach (ProductPicture pp in product.Pictures)
                                        {
                                            model.Pictures.Add(url + "/" + pp.Picture);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return View(model);
        }

        public ActionResult CartSummary()
        {
            return View(GetCart());
        }

        public ActionResult CartDetail()
        {
            return View(GetCart());
        }

        public ActionResult CascadeCity()
        {
            List<string> model=new List<string>();
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    State state = MasterDataRepository.GetState(id);
                    if (state != null&&state.Cities != null)
                    {
                        state.Cities.ForEach(c => model.Add(c.Name));
                    }
                }
            }
            
            return View(model.ToArray());
        }

        [Authorize]
        public ActionResult CancelOrder()
        {
            OrderCancelModel model = new OrderCancelModel();
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    model.OrderId = id;
                }
            }

            return View(model);
        }

        [Authorize]
        public ActionResult OrderDetail()
        {
            OrderModel model = null;
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                var obj = Session[RouteData.Values["id"].ToString()];
                if (obj != null)
                {
                    model = obj as OrderModel;
                }
            }
            if (model == null)
                model = new OrderModel() { Items = new List<OrderItemModel>() };
            return View(model);
        }

        // [Authorize(Roles = "QAAdmin")]
        public ActionResult Reply()
        {
            int questionId = 0;
            int.TryParse(RouteData.Values["id"].ToString(), out questionId);
            int repliedTo = 0;
            int.TryParse(HttpContext.Request.QueryString["rep"], out repliedTo);

            return View(new ReplyModel() { QuestionId=questionId,RepliedTo=repliedTo});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Reply(ReplyModel model)
        {
            if (ModelState.IsValid)
            {
                Answer item = new Answer()
                {
                    Id = 0,
                    IsAdminReply = false,
                    Body = model.Body,
                    QuestionId = model.QuestionId,
                    RepliedTo = new Answer() { Id = model.RepliedTo,RepliedDate=DateTime.Now },
                    RepliedBy = new User() { UserName=User.Identity.Name},
                    RepliedDate=DateTime.Now,
                    IsDoctorReply=System.Web.Security.Roles.IsUserInRole(User.Identity.Name,ConstantHelper.QAAdminRole)
                };
                QuestionRepository.Reply(item);
            }

            return RedirectToAction("FAQDetail","Home", new { id=model.QuestionId});
        }

        #region helper
        public CartModel GetCart()
        {
            //CartModel model = new CartModel() { Items=new List<CartItemModel>()};
            //Setting setting = SettingRepository.GetSetting();
            //if (setting != null)
            //    model.TaxRate = setting.TaxRate;
            //HttpCookie existing = Request.Cookies[CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID)];
            //string cart = "";
            //if (existing != null)
            //{
            //    cart = CartDataProtector.DecryptStringFromBytes_Aes(existing.Value);
            //    string[] items = cart.Split('|');
            //    Dictionary<int, int> dicItems = new Dictionary<int, int>();
            //    List<Product> list = ProductRepository.GetAllProducts();
            //    List<Product> actives = ProductRepository.GetActives();
            //    if (list == null)
            //        list = new List<Product>();
            //    foreach (string item in items)
            //    {
            //        if (string.IsNullOrEmpty(item))
            //            continue;
            //        string[] pair = item.Split(',');
            //        if (pair.Length > 1)
            //        {
            //            int pid = 0;
            //            int quantity = 0;
                        
            //            if (int.TryParse(pair[0], out pid) && int.TryParse(pair[1], out quantity) && pid > 0 && quantity > 0)
            //            {
            //                Product product = list.FirstOrDefault(l => l.Id == pid);
            //                if (product != null)
            //                {
            //                    CartItemModel cartitem = new CartItemModel() { ProductId = pid, Quantity = quantity,Name=product.Name,Price=Decimal.Round(product.Price,2),Inventory=product.Quantity };
            //                    if (actives != null && actives.Any(p => p.Id == pid))
            //                    {
            //                        cartitem.Price = decimal.Round(product.Price - actives.FirstOrDefault(p => p.Id == pid).DiscountAmount, 2);
            //                    }
            //                    if (product.Pictures != null && product.Pictures.Any())
            //                    {
            //                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            //                        string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
            //                        ProductPicture defPic = product.Pictures.FirstOrDefault(p => p.IsDefault);
            //                        if (defPic == null)
            //                            defPic = product.Pictures.FirstOrDefault();
            //                        cartitem.Url = url + "/" + defPic.Picture;
            //                    }
            //                    else
            //                    {
            //                        cartitem.Url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
            //                    }
            //                    model.Items.Add(cartitem);
            //                }
            //            }
            //        }
            //    }
            //}
            return CartRepository.GetCart(Session.SessionID);
        }
        #endregion
    }
}
