﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using TotoBobo.Mvc.Repository;
using TotoBobo.Mvc.Models;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Helper;
using System.IO;
using System.Xml.Linq;
using System.Configuration;

namespace TotoBobo.Mvc.Controllers
{
    [Authorize(Roles="Admin,Sales")]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            OrderStatisModel model = new OrderStatisModel();
            
            List<Order> orders = OrderRepository.GetAllOrders();
            List<int> customers = new List<int>();
            if (orders != null && orders.Any())
            {
                model.Orders = orders.Count(o => o.Status != OrderStatus.Cancelled);
                model.Products = orders.Sum(o => o.Items.Sum(i => i.Quantity));
                foreach (Order order in orders)
                {
                    if (!customers.Any(c => c == order.CreatedBy.UserId))
                    {
                        customers.Add(order.CreatedBy.UserId);
                    }
                }
                model.Customers = customers.Count;
            }
            return View(model);
        }

        public ActionResult ManageRole()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text="All",Value="All"});
            AdminModel model = new AdminModel();
            model.Members = new List<AssignRoleModel>();
            model.SearchModel = new SearchRoleMode();
            //model.CurrentMember = new AssignRoleModel();
            model.Roles = new List<RoleModel>();
            foreach (string role in Roles.GetAllRoles())
            {
                items.Add(new SelectListItem() { Text=role,Value=role});
                model.Roles.Add(new RoleModel() { Role=role});
            }
            ViewData["Role"] = items;
            
            foreach (Data.Objects.User user in UserRepository.GetAllUsers())
            {
                string role = ConstantHelper.CustomerRole;
                if (user.Roles != null&&user.Roles.Any())
                {
                    role = "";
                    foreach (TotoBobo.Data.Objects.UserRole ur in user.Roles)
                    {
                        role = role + ur.RoleName + ",";
                    }
                    if (role.Length > 0)
                        role=role.Substring(0, role.Length - 1);
                }
               
                model.Members.Add(new AssignRoleModel() { UserId=user.UserId,UserName=user.UserName,Role=role});
            }

            return View(model);
        }

        public ActionResult ManageUser()
        {
            ManageUserModel model = new ManageUserModel();
            model.Users = new List<UserModel>();
            model.SearchModel = new SearchUserModel();

            foreach (Data.Objects.User user in UserRepository.GetAllUsers())
            {
                model.Users.Add(new UserModel() { Id = user.UserId, Name = user.UserName });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult SearchUser(SearchUserModel searchmodel)
        {
            ManageUserModel model = new ManageUserModel();
            model.Users = new List<UserModel>();
            model.SearchModel = new SearchUserModel();
            
            List<User> users = UserRepository.GetAllUsers();
            
            if (searchmodel.Name == null)
            {
                searchmodel.Name = "";
            }

            if (users != null && users.Any(c => c.UserName.ToLower().Contains(searchmodel.Name.ToLower())))
            {
                List<User> userModelList = users.Where(c => c.UserName.ToLower().Contains(searchmodel.Name.ToLower())).ToList();

                foreach (Data.Objects.User user in userModelList)
                {
                    model.Users.Add(new UserModel() { Id = user.UserId, Name = user.UserName });
                }
            }
            return View("ManageUser", model);
        }
        
        public ActionResult EditUser()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {                                        
                    User user = UserRepository.GetUserById(id);
                    if (user != null)
                    {                        
                        ViewBag.SubTitle = "Edit User";                        
                        EditUserModel model = new EditUserModel()
                        {
                            Id = user.UserId,
                            Name = user.UserName,
                        };
                        return View(model);
                    }
                }
            }

            return RedirectToAction("ManageUser");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(EditUserModel model)
        {            
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ViewBag.SubTitle = "Edit User";                    
                }
            }
            
            if (ModelState.IsValid)
            {
                if (RouteData.Values.Any(v => v.Key == "id"))
                {
                    int id = 0;
                    if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                    {
                        if (model.Id != id)
                        {
                            ModelState.AddModelError("", "Form is injected!");
                            return View(model);
                        }

                        User user = UserRepository.GetUserById(id);

                        bool changePasswordSucceeded;
                        try
                        {
                            string token = WebSecurity.GeneratePasswordResetToken(user.UserName);
                            changePasswordSucceeded = WebSecurity.ResetPassword(token, model.Password);
                        }
                        catch (Exception)
                        {
                            changePasswordSucceeded = false;
                        }

                        if (changePasswordSucceeded)
                        {
                            ModelState.AddModelError("", "Success");
                            return View(model);
                        }
                        else
                        {
                            ModelState.AddModelError("", "The user has not been confirmed yet or The new password is invalid.");
                            return View(model);
                        }                        
                    }
                    else
                    {
                        ModelState.AddModelError("", "Form is injected!");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Form is injected!");
                }

            }

            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUser()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    UserRepository.DeleteUser(new User() { UserId = id });
                }
            }
            return RedirectToAction("ManageUser");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchRole(SearchRoleMode searchmodel)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "All", Value = "All" });
            AdminModel model = new AdminModel();
            model.Members = new List<AssignRoleModel>();
            model.SearchModel = searchmodel;
            //model.CurrentMember = new AssignRoleModel();
            model.Roles = new List<RoleModel>();
            foreach (string role in Roles.GetAllRoles())
            {
                items.Add(new SelectListItem() { Text = role, Value = role });
                model.Roles.Add(new RoleModel() { Role = role });
            }
            ViewData["Role"] = items;

            foreach (Data.Objects.User user in UserRepository.GetAllUsers())
            {
                if (!string.IsNullOrEmpty(searchmodel.UserName) && !user.UserName.ToLower().Contains(searchmodel.UserName.ToLower()))
                {
                    continue;
                }
                
                string role = ConstantHelper.CustomerRole;
                if (user.Roles != null && user.Roles.Any())
                {
                    role = "";
                    foreach (TotoBobo.Data.Objects.UserRole ur in user.Roles)
                    {
                        role = role + ur.RoleName + ",";
                    }
                    if (role.Length > 0)
                        role = role.Substring(0, role.Length - 1);
                }

                if (searchmodel.Role != "All" && !role.Contains(searchmodel.Role))
                {
                    continue;
                }

                model.Members.Add(new AssignRoleModel() { UserId = user.UserId, UserName = user.UserName, Role = role });
            }
            return View("ManageRole", model);
        }

        public ActionResult SearchRole()
        {
            return RedirectToAction("ManageRole");
        }

        public ActionResult AssignRole()
        {
            if (RouteData.Values.Any(v => v.Key.Equals("id", StringComparison.InvariantCultureIgnoreCase)))
            {
                int id = 0;
                if(int.TryParse(RouteData.Values["id"].ToString(),out id))
                {
                    Data.Objects.User user = UserRepository.GetUserById(id);
                    if (user != null)
                    {
                        AssignRoleModel model = new AssignRoleModel()
                        {
                            UserId=id,
                            UserName=user.UserName,
                            Roles=new List<RoleCheckBoxModel>()
                        };
                        foreach (string role in Roles.GetAllRoles())
                        {
                            model.Roles.Add(new RoleCheckBoxModel() { IsChecked=false,RoleName=role });
                        }
                        if (user.Roles != null && user.Roles.Any())
                        {
                            foreach (TotoBobo.Data.Objects.UserRole ur in user.Roles)
                            {
                                model.Roles.FirstOrDefault(r => r.RoleName.Equals(ur.RoleName, StringComparison.InvariantCultureIgnoreCase)).IsChecked = true;
                            }
                        }
                        else
                        {
                            model.Roles.FirstOrDefault(r => r.RoleName.Equals(ConstantHelper.CustomerRole, StringComparison.InvariantCultureIgnoreCase)).IsChecked = true;
                        }
                        return View(model);
                    }
                }
            }
            return RedirectToAction("ManageRole");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignRole(AssignRoleModel model)
        {
            List<string> addroles=new List<string>();
            List<string> deleteroles = new List<string>();
            string[] existing=Roles.Provider.GetRolesForUser(model.UserName);
            foreach (var checkbox in model.Roles)
            {
                if (checkbox.IsChecked&&!existing.Any(r=>r==checkbox.RoleName))
                {
                    addroles.Add(checkbox.RoleName);
                }
                else if (!checkbox.IsChecked && existing.Any(r => r == checkbox.RoleName))
                {
                    deleteroles.Add(checkbox.RoleName);
                }
            }
            if (deleteroles.Any())
            {
                Roles.Provider.RemoveUsersFromRoles(new string[] { model.UserName }, deleteroles.ToArray());
            }
            if (addroles.Any())
            {
                Roles.Provider.AddUsersToRoles(new string[] { model.UserName }, addroles.ToArray());
            }
            return RedirectToAction("ManageRole");
        }

        public ActionResult ManageProduct()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "All", Value = "0" });
            List<Category> categories = CategoryRepository.GetAllCategories();
            if (categories != null && categories.Any())
            {
                categories = categories.GetHiarachy();
                foreach (Category cat in categories)
                {
                    items.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString() });
                }
            }
            ViewData["CategoryId"] = items;
            ManageProductModel model = new ManageProductModel();
            model.SearchModel = new SearchProductModel();
            model.Products = ProductRepository.GetAllProducts();
            model.Sales = ProductRepository.GetActives();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchProduct(SearchProductModel searchmodel)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "All", Value = "0" });
            List<Category> categories = CategoryRepository.GetAllCategories();
            if (categories != null && categories.Any())
            {
                categories = categories.GetHiarachy();
                foreach (Category cat in categories)
                {
                    items.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString() });
                }
            }
            ViewData["CategoryId"] = items;
            ManageProductModel model = new ManageProductModel();
            model.SearchModel =searchmodel;
            List<Product> products = ProductRepository.GetAllProducts();
            IEnumerable<Product> list = products;
            if (products != null)
            {
                if (searchmodel.CategoryId != 0)
                {
                    list = products.Where(p => p.Category.Id == searchmodel.CategoryId);
                }
                if (list != null && !string.IsNullOrEmpty(searchmodel.Name))
                {
                    list = list.Where(l => l.Name.ToLower().Contains(searchmodel.Name.ToLower()));
                }
            }
            if (list == null)
                list = new List<Product>();
            model.Products = list.ToList();
            model.Sales = ProductRepository.GetActives();
            return View("ManageProduct", model);
        }

        public ActionResult SearchProduct()
        {
            return RedirectToAction("ManageProduct");
        }

        public ActionResult EditProduct()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if(int.TryParse(RouteData.Values["id"].ToString(),out id))
                {
                   
                    List<SelectListItem> items = new List<SelectListItem>();
                    List<SelectListItem> branditems = new List<SelectListItem>();
                    List<Category> categories = CategoryRepository.GetAllCategories();
                    List<Brand> brands = BrandRepository.GetBrands();
                    if (brands == null)
                        brands = new List<Brand>();
                    if (categories != null && categories.Any())
                    {
                        categories = categories.GetHiarachy();
                    }
                    if (categories == null)
                        categories = new List<Category>();
                    List<SelectListItem> citems = new List<SelectListItem>();
                    List<Country> countries = MasterDataRepository.GetCountries();
                    foreach (Country country in countries)
                    {
                        citems.Add(new SelectListItem() { Text = country.Name, Value = country.Id.ToString()});
                    }
                    ViewData["CountryId"] = citems;
                    string guid = Guid.NewGuid().ToString();
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), guid);
                    Directory.CreateDirectory(directory);
                    using (var file = System.IO.File.Create(Path.Combine(directory, "title.xml")))
                    {
                        System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(file, System.Text.Encoding.UTF8);
                        writer.WriteStartDocument(true);
                        writer.Formatting = System.Xml.Formatting.Indented;
                        writer.Indentation = 2;
                        writer.WriteStartElement("Titles");
                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                        writer.Close();
                        file.Close();
                    }
                    
                    if (id > 0)
                    {
                        Product product = ProductRepository.GetProductById(id);
                        if (product != null)
                        {
                            EditProductModel model = new EditProductModel()
                            {
                                Id=product.Id,
                                Description=product.Description,
                                Name=product.Name,
                                Price=Decimal.Round(product.Price,2),
                                CategoryId=product.Category.Id,
                                Pictures=product.Pictures,
                                GUID = guid,
                                Weight=Decimal.Round(product.Weight,2),
                                Tags=product.Tags,
                                RelatedItems = RelatedProduct.GetRelates(id,product.RelatedItems),
                                Accessories=Accessory.GetAccessories(id,product.Accessories),
                                RelatedIds="",
                                AccessoryIds=""
                            };
                            if (product.Country != null)
                            {
                                SelectListItem item = citems.FirstOrDefault(c => c.Value == product.Country.Id.ToString());
                                if (item != null)
                                    item.Selected = true;
                            }
                            if (product.RelatedItems != null)
                            {
                                foreach (Product ri in product.RelatedItems)
                                {
                                    model.RelatedIds += ri.Id.ToString() + ",";
                                }
                            }
                            if (model.RelatedIds.Length > 0)
                                model.RelatedIds=model.RelatedIds.Substring(0, model.RelatedIds.Length - 1);

                            if (product.Accessories != null)
                            {
                                foreach (Product ri in product.Accessories)
                                {
                                    model.AccessoryIds += ri.Id.ToString() + ",";
                                }
                            }
                            if (model.AccessoryIds.Length > 0)
                                model.AccessoryIds = model.AccessoryIds.Substring(0, model.AccessoryIds.Length - 1);

                            if (product.Pictures != null)
                            {
                                XDocument doc = XDocument.Load(Path.Combine(directory, "title.xml"));
                                foreach (ProductPicture pp in product.Pictures)
                                {
                                    if(System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder),pp.Picture)))
                                    {
                                        if (pp.IsDefault)
                                        {
                                            string extension = Path.GetExtension(pp.Picture);
                                            string name = pp.Picture.Replace(extension, "-default") + extension;
                                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), pp.Picture), Path.Combine(directory, name), true);
                                        }
                                        else
                                        {
                                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), pp.Picture), Path.Combine(directory, pp.Picture), true);
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(pp.Title))
                                    {
                                        string extension = Path.GetExtension(pp.Picture);
                                        string name = pp.Picture.Replace(extension, "");
                                        XElement newele = new XElement("Title", pp.Title);
                                        newele.SetAttributeValue("pic", name);
                                        doc.Root.Add(newele);
                                    }
                                }
                                doc.Save(Path.Combine(directory, "title.xml"));
                            }
                            foreach (Category cat in categories)
                            {
                                if (product.Category.Id == cat.Id)
                                {
                                    items.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString(),Selected=true });
                                }
                                else
                                {
                                    items.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString() });
                                }
                            }
                            foreach (Brand brand in brands)
                            {
                                if (product.Brand!=null&&product.Brand.Id == brand.Id)
                                {
                                    branditems.Add(new SelectListItem() { Text = brand.Name, Value = brand.Id.ToString(), Selected = true });
                                }
                                else
                                {
                                    branditems.Add(new SelectListItem() { Text = brand.Name, Value = brand.Id.ToString() });
                                }
                            }
                            ViewBag.SubTitle = "Edit Product";
                            ViewData["CategoryId"] = items;
                            ViewData["BrandId"] = branditems;
                            return View(model);
                        }
                    }
                    else
                    {
                        foreach (Category cat in categories)
                        {
                            items.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString() });
                        }
                        foreach (Brand brand in brands)
                        {
                            branditems.Add(new SelectListItem() { Text = brand.Name, Value = brand.Id.ToString() });
                        }
                        ViewBag.SubTitle = "Add Product";
                        ViewData["CategoryId"] = items;
                        ViewData["BrandId"] = branditems;
                        citems.FirstOrDefault().Selected = true;
                        return View(new EditProductModel() { 
                            Id = 0, 
                            Pictures = new List<ProductPicture>(), 
                            GUID = guid, 
                            Tags = new List<ProductTag>(),
                            RelatedItems = RelatedProduct.GetRelates(0,null),
                            Accessories=Accessory.GetAccessories(0,null),
                            });
                    }
                    
                }
            }
            return RedirectToAction("ManageProduct");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProduct(EditProductModel model)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            List<Category> categories = CategoryRepository.GetAllCategories();
            if (categories != null && categories.Any())
            {
                categories = categories.GetHiarachy();
                foreach (Category cat in categories)
                {
                    if (cat.Id == model.CategoryId)
                    {
                        items.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString(),Selected=true });
                    }
                    else
                    {
                        items.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString() });
                    }
                }
            }
            List<SelectListItem> citems = new List<SelectListItem>();
            List<Country> countries = MasterDataRepository.GetCountries();
            foreach (Country country in countries)
            {
                citems.Add(new SelectListItem() { Text = country.Name, Value = country.Id.ToString(), Selected = country.Id == model.CountryId });
            }
            ViewData["CountryId"] = citems;
            ViewData["CategoryId"] = items;
            List<SelectListItem> branditems = new List<SelectListItem>();
            List<Brand> brands = BrandRepository.GetBrands();
            if (brands != null && brands.Any())
            {
                foreach (Brand brand in brands)
                {
                    if (brand.Id == model.BrandId)
                    {
                        branditems.Add(new SelectListItem() { Text = brand.Name, Value = brand.Id.ToString(),Selected=true });
                    }
                    else
                    {
                        branditems.Add(new SelectListItem() { Text = brand.Name, Value = brand.Id.ToString() });
                    }
                }
            }
            ViewData["BrandId"] = branditems;
            ViewBag.SubTitle = "Add Product";
            //model.TagXml = "<Tags><ProductTag><Key>Size</Key><Values><ProductTagValue><Value>XL</Value></ProductTagValue><ProductTagValue><Value>L</Value></ProductTagValue></Values></ProductTag></Tags>";
            model.Tags = new List<ProductTag>();
            try
            {
                if (!string.IsNullOrEmpty(model.TagXml))
                {
                    var regex = new System.Text.RegularExpressions.Regex("&(?!quot;|apos;|amp;|lt;|gt;#x?.*?;)");
                    model.TagXml = regex.Replace(model.TagXml, "&amp;");

                    XDocument doc = XDocument.Parse(model.TagXml);
                    var tags = doc.Root.Elements("ProductTag");
                    if (tags != null)
                    {
                        foreach (XElement ele in tags)
                        {
                            var key = ele.Element("Key").Value;
                            var valuesEle = ele.Element("Values");
                            ProductTag tag = new ProductTag() { Key=key,Values=new List<ProductTagValue>()};
                            var values = valuesEle.Elements("ProductTagValue");
                            if (values != null)
                            {
                                foreach (XElement vele in values)
                                {
                                    tag.Values.Add(new ProductTagValue() { Value=vele.Element("Value").Value,Price=Convert.ToDecimal(vele.Element("Price").Value)});
                                }
                            }
                            model.Tags.Add(tag);
                        }
                    }
                }
            }
            catch { }

            List<Product> list = new List<Product>();
            if (!string.IsNullOrEmpty(model.RelatedIds))
            {
                string[] ids = model.RelatedIds.Split(',');
                foreach (string id in ids)
                {
                    int pid = 0;
                    if (int.TryParse(id, out pid) && pid > 0)
                    {
                        list.Add(new Product() { Id = pid });
                    }
                }
            }

            List<Product> list2 = new List<Product>();
            if (!string.IsNullOrEmpty(model.AccessoryIds))
            {
                string[] ids = model.AccessoryIds.Split(',');
                foreach (string id in ids)
                {
                    int pid = 0;
                    if (int.TryParse(id, out pid) && pid > 0)
                    {
                        list2.Add(new Product() { Id = pid });
                    }
                }
            }
            if (ModelState.IsValid)
            {
                if (RouteData.Values.Any(v => v.Key == "id"))
                {
                    int id = 0;
                    if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                    {
                        Product eprod = ProductRepository.GetProductByName(model.Name);
                        if (eprod != null&&eprod.Id!=model.Id)
                        {
                            ModelState.AddModelError("Name", "Name already exists, please input another one");
                        }
                        else
                        {
                            if (id > 0)
                            {
                                ViewBag.SubTitle = "Edit Product";
                            }
                            if (id == model.Id)
                            {
                                Product p = new Product()
                                {
                                    Id = model.Id,
                                    Name = model.Name,
                                    Description = model.Description,
                                    Price = model.Price,
                                    Pictures = new List<ProductPicture>(),
                                    CreatedBy = new User() { UserName = User.Identity.Name },
                                    LastModifiedBy = new User() { UserName = User.Identity.Name },
                                    Category = new Category() { Id = model.CategoryId },
                                    Weight = model.Weight,
                                    Brand = new Brand() { Id=model.BrandId},
                                    Country=new Country(){Id=model.CountryId},
                                    Tags=model.Tags,
                                    RelatedItems=list,
                                    Accessories=list2
                                };

                                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);

                                string uri = Path.Combine(directory, "title.xml");
                                XDocument doc = XDocument.Load(uri);
                                Dictionary<string, string> titles = new Dictionary<string, string>();
                                foreach (var ele in doc.Root.Elements())
                                {
                                    titles.Add(ele.Attribute("pic").Value.Replace("-default", ""), ele.Value);
                                }

                                DirectoryInfo di = new DirectoryInfo(directory);
                                if (model.Id > 0)
                                {
                                    Product product = ProductRepository.GetProductById(model.Id);
                                    if (product != null && product.Pictures != null)
                                    {
                                        foreach (ProductPicture pp in product.Pictures)
                                        {
                                            Picture.Delete(Path.Combine(Server.MapPath("~/" + uploadfolder), pp.Picture));
                                        }
                                    }
                                }
                                foreach (FileInfo fi in di.EnumerateFiles())
                                {
                                    if (fi.Name.EndsWith(".xml"))
                                        continue;
                                    if (fi.Name.Contains("-default"))
                                    {
                                        var pp = new ProductPicture() { ProductId = model.Id, Picture = fi.Name.Replace("-default", ""), IsDefault = true };
                                        if (titles.Any(t => pp.Picture.ToLower().Contains(t.Key.ToLower())))
                                        {
                                            pp.Title = titles.FirstOrDefault(t => pp.Picture.ToLower().Contains(t.Key.ToLower())).Value;
                                        }
                                        p.Pictures.Add(pp);
                                        Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name.Replace("-default", "")), true);
                                    }
                                    else
                                    {
                                        var pp = new ProductPicture() { ProductId = model.Id, Picture = fi.Name, IsDefault = false };
                                        if (titles.Any(t => pp.Picture.ToLower().Contains(t.Key.ToLower())))
                                        {
                                            pp.Title = titles.FirstOrDefault(t => pp.Picture.ToLower().Contains(t.Key.ToLower())).Value;
                                        }
                                        p.Pictures.Add(pp);
                                        Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name), true);
                                    }
                                }
                                Picture.DeleteDirectory(directory, true);

                                if (id > 0)
                                    ProductRepository.UpdateProduct(p);
                                else
                                    ProductRepository.AddProduct(p);
                                return RedirectToAction("ManageProduct");

                            }
                            else
                            {
                                ModelState.AddModelError("", "Form is injected!");
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Form is injected!");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Form is injected!");
                }
                
            }
            
            model.RelatedItems = RelatedProduct.GetRelates(model.Id,list);
            model.Accessories = Accessory.GetAccessories(model.Id, list2);
            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteProduct()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ProductRepository.DeleteProductById(id);
                }
            }
            return RedirectToAction("ManageProduct");
        }

        public ActionResult StockProduct()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id)&&id>0)
                {
                    Product product = ProductRepository.GetProductById(id);
                    if (product != null)
                    {
                        StockProductModel model = new StockProductModel()
                        {
                            Id=id,
                            Name=product.Name,
                            CurrentQuantity=product.Quantity,
                            Histories=ProductRepository.GetStockHistories(id)
                        };
                        return View(model);
                    }
                }
            }
            return RedirectToAction("ManageProduct");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StockProduct(StockProductModel model)
        {
            model.Histories = new List<StockHistory>();
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                {
                    model.Histories = ProductRepository.GetStockHistories(id);
                }
            }
            if (ModelState.IsValid)
            {
                if (RouteData.Values.Any(v => v.Key == "id"))
                {
                    int id = 0;
                    if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                    {
                        if (id == model.Id && ProductRepository.GetProductById(id) != null)
                        {
                            ProductRepository.StockToInventory(id, model.StockQuantity, User.Identity.Name);
                            return RedirectToAction("ManageProduct");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Form is injected");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Form is injected");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Form is injected");
                }
            }
            return View(model);
        }

        public ActionResult CancelProduct()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManageProduct");
        }

        public ActionResult CancelHamper()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManageHamper");
        }

        public ActionResult ManageCategory()
        {
            ManageCategoryModel model = new ManageCategoryModel();
            model.SearchModel = new SearchCategoryModel();
            model.Categories = new List<CategoryModel>();
            List<Category> categories = CategoryRepository.GetAllCategories();
            if (categories != null&&categories.Any())
            {
                model.Categories = categories.GetParentHiarachy();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchCategory(SearchCategoryModel searchmodel)
        {
            ManageCategoryModel model = new ManageCategoryModel();
            model.SearchModel = new SearchCategoryModel();
            model.Categories = new List<CategoryModel>();
            List<Category> categories = CategoryRepository.GetAllCategories();
            if (searchmodel.Name == null)
                searchmodel.Name = "";
            if (categories != null && categories.Any(c=>c.Name.ToLower().Contains(searchmodel.Name.ToLower())))
            {
                model.Categories = categories.Where(c => c.Name.ToLower().Contains(searchmodel.Name.ToLower())).ToList().GetParentHiarachy();
            }
            return View("ManageCategory", model);
        }

        public ActionResult SearchCategory()
        {
            return RedirectToAction("ManageCategory");
        }

        public ActionResult EditCategory()
        {
            List<SelectListItem> parents = new List<SelectListItem>();
            parents.Add(new SelectListItem() { Text="None",Value="0"});
            
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id == 0)
                    {
                        ViewBag.SubTitle = "Add Category";
                        List<Category> categories = CategoryRepository.GetAllCategories();
                        if (categories != null)
                        {
                            categories = categories.GetHiarachy();
                            foreach (Category cat in categories)
                            {
                                parents.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString() });
                            }
                        }
                        ViewData["ParentId"] = parents;
                        EditCategoryModel model = new EditCategoryModel()
                        {
                            Id =0,
                            Name = "",
                            ParentId = 0
                        };
                        return View(model);
                    }
                    else
                    {
                        
                        Category cat = CategoryRepository.GetCategoryById(id);
                        if (cat != null)
                        {
                            List<Category> categories = CategoryRepository.GetAllCategories();
                            if (categories != null)
                            {
                                categories = categories.GetHiarachy();
                                foreach (Category cat2 in categories)
                                {
                                    if (cat2.Id != id&&cat2.ParentId!=id)
                                    {
                                        if (cat.ParentId == cat2.Id)
                                        {
                                            parents.Add(new SelectListItem() { Text = cat2.Name, Value = cat2.Id.ToString(),Selected=true });
                                        }
                                        else
                                        {
                                            parents.Add(new SelectListItem() { Text = cat2.Name, Value = cat2.Id.ToString() });
                                        }
                                    }
                                }
                            }
                            ViewBag.SubTitle = "Edit Category";
                            ViewData["ParentId"] = parents;
                            EditCategoryModel model = new EditCategoryModel()
                            {
                                Id=cat.Id,
                                Name=cat.Name,
                                ParentId=cat.ParentId
                            };
                            
                            return View(model);
                        }
                    }
                }
            }
            
            return RedirectToAction("ManageCategory");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategory(EditCategoryModel model)
        {
            List<SelectListItem> parents = new List<SelectListItem>();
            parents.Add(new SelectListItem() { Text = "None", Value = "0" });
            List<Category> categories = CategoryRepository.GetAllCategories();
            if (categories != null)
            {
                List<Category> allcategories = categories.GetHiarachy();
                foreach (Category cat in allcategories)
                {
                    if (cat.Id == model.ParentId)
                    {
                        parents.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString(), Selected = true });
                    }
                    else
                    {
                        parents.Add(new SelectListItem() { Text = cat.Name, Value = cat.Id.ToString() });
                    }
                }
            }

            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        ViewBag.SubTitle = "Edit Category";
                        
                    }
                    else
                    {
                        ViewBag.SubTitle = "Add Category";
                        
                    }
                }
            }
            ViewData["ParentId"] = parents;
            
            if (ModelState.IsValid)
            {
                //List<Category> categories = CategoryRepository.GetAllCategories();
                if (RouteData.Values.Any(v => v.Key == "id"))
                {
                    int id = 0;
                    if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                    {
                        if (model.Id != id)
                        {
                            ModelState.AddModelError("", "Form is injected!");
                            return View(model);
                        }
                        if (categories.Any(cat2 => cat2.Name.Equals(model.Name, StringComparison.InvariantCultureIgnoreCase)&&cat2.Id==model.Id))
                        {
                            ModelState.AddModelError("Name", "Category name already exists");
                            return View(model);
                        }
                        Category cat = new Category() { Id = id, Name = model.Name, ParentId = model.ParentId, CreatedBy = new User() { UserName = User.Identity.Name }, LastModifiedBy = new User() { UserName = User.Identity.Name },CreatedDate=DateTime.Now,LastModifiedDate=DateTime.Now };
                        try
                        {
                            CategoryRepository.UpdateCategory(cat);
                            return RedirectToAction("ManageCategory");
                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError("", ex.Message);
                        }
                        
                    }
                    else
                    {
                        ModelState.AddModelError("", "Form is injected!");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Form is injected!");
                }
                
            }
            
            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCategory()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    CategoryRepository.DeleteCategoryById(id);
                }
            }
            return RedirectToAction("ManageCategory");
        }

        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase qqfile)
        {
            string guid = Request.Form["guid"];
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, "/"+uploadfolder+"/"+guid);
            int width = 0;
            int height = 0;
            switch (Request.Form["type"])
            {
                case "Product":
                    int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["ProductImageWidth"], out width);
                    int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["ProductImageHeight"], out height);
                    break;
                case "Brand":
                    width = 200;
                    height = 92;
                    break;
                case "Country":
                    width = 30;
                    height = 30;
                    break;
                
            }
            System.Drawing.Image image = System.Drawing.Image.FromStream(qqfile.InputStream);
            if ((width>0&&height>0)&&(image.Width != width || image.Height != height))
            {
                return Json(new { success = false, error = "Please upload images with " + width.ToString() + "px width and " + height.ToString() + "px height!", preventRetry = true });
            }

            if (qqfile != null && qqfile.ContentLength > 0)
            {
                string fileName = Request.Form["qquuid"] + Path.GetExtension(qqfile.FileName);
                var path = Path.Combine(Server.MapPath("~/" + uploadfolder + "/" + guid), fileName);
                try
                {
                    qqfile.SaveAs(@path);
                    return Json(new { success = true, thumbnailUrl = url + "/" + fileName }, "text/plain");
                }
                catch
                {
                    return Json(new { success = false }, "text/plain");
                } 
            }
            else
            {
                // this works for Firefox, Chrome
                var filename = Request["qqfile"];
                if (!string.IsNullOrEmpty(filename))
                {
                    string newFileName = Request.Form["qquuid"] + Path.GetExtension(qqfile.FileName);
                    filename = Path.Combine(Server.MapPath("~/" + uploadfolder + "/" + guid), newFileName);
                    try
                    {
                        using (var output = System.IO.File.Create(filename))
                        {
                            Request.InputStream.CopyTo(output);
                        }
                        return Json(new { success = true, thumbnailUrl = url + "/" + newFileName }, "text/plain");
                    }
                    catch
                    {
                        return Json(new { success = false }, "text/plain");
                    }
                }
            }
            return Json(new { success = false }, "text/plain");
        }

       

        [HttpDelete]
        public ActionResult DeletePicture()
        {
            string guid = Request.QueryString["guid"];
            Response.StatusCode = 200;
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uuid = RouteData.Values["id"].ToString();
                if (uuid.Length>32)
                {
                    uuid = uuid.Substring(0, 32);
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Server.MapPath("~/" + uploadfolder+"/"+guid);
                    foreach(string filename in Directory.EnumerateFiles(directory))
                    {
                        if (filename.ToLower().Contains(uuid.ToLower()))
                        {
                            try
                            {
                                System.IO.File.Delete(filename);
                            }
                            catch
                            {
                                Response.StatusCode = 500;
                            }
                            break;
                        }
                    }
                }

            }
            return Json(new { success = true }, "text/plain");
        }

        public ActionResult GetPictures()
        {
            string guid = Request.QueryString["guid"];
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), guid);
            string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder+"/"+guid);

            DirectoryInfo di = new DirectoryInfo(directory);
            List<Picture> pictures = new List<Picture>();
            foreach (FileInfo fi in di.EnumerateFiles())
            {
                if (fi.Name.EndsWith(".xml"))
                    continue;
                pictures.Add(new Picture() { 
                    uuid=fi.Name.Replace(Path.GetExtension(fi.Name),""),
                    name=fi.Name,
                    size=fi.Length,
                    thumbnailUrl = url + "/"+fi.Name 
                });
            }
            Response.StatusCode = 200;
            return Json(pictures,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddTitle(string id, string uuid, string guid,string title)
        {
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string uri = Path.Combine(Server.MapPath("~/" + uploadfolder), guid,"title.xml");
            XDocument doc = XDocument.Load(uri);
            XElement ele = doc.Root.Elements().FirstOrDefault(x => x.Attribute("pic")!=null&&x.Attribute("pic").Value == uuid);
            if (ele != null)
            {
                ele.Value = title;
                doc.Save(uri);
                return Json(new { success = true, id = id }, "text/plain");
            }
            else
            {
                XElement newele = new XElement("Title", title);
                newele.SetAttributeValue("pic", uuid);
                doc.Root.Add(newele);
                doc.Save(uri);
                return Json(new { success = true, id = id }, "text/plain");
            }
            //return Json(new { success = false }, "text/plain");
        }

        public ActionResult GetTitles()
        {
            string guid = Request.QueryString["guid"];
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string uri = Path.Combine(Server.MapPath("~/" + uploadfolder), guid, "title.xml");
            XDocument doc = XDocument.Load(uri);


            List<PictureTitle> pictures = new List<PictureTitle>();
            foreach (var ele in doc.Root.Elements())
            {
               
                pictures.Add(new PictureTitle()
                {
                    key=ele.Attribute("pic").Value,
                    value=ele.Value
                });
            }
            Response.StatusCode = 200;
            return Json(pictures, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MarkAsDefault(string id, string uuid, string guid)
        {
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), guid);
            DirectoryInfo di = new DirectoryInfo(directory);
            foreach (FileInfo fi in di.EnumerateFiles())
            {
                if (fi.Name.ToLower().Contains(uuid.ToLower()))
                {
                    try
                    {
                        string extension = Path.GetExtension(fi.Name);
                        System.IO.File.Move(Path.Combine(directory, fi.Name), Path.Combine(directory, fi.Name.Replace(extension,"-default")+extension));
                    }
                    catch { return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet); }
                }
                else if (fi.Name.Contains("-default"))
                {
                    try
                    {
                        System.IO.File.Move(Path.Combine(directory, fi.Name), Path.Combine(directory, fi.Name.Replace("-default","")));
                    }
                    catch { return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet); }
                }
            }

            return Json(new { success = true, id = id }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ManageState()
        {
            ManageStateModel model = new ManageStateModel() { States=new List<StateModel>()};
            Country country = MasterDataRepository.GetEnabledCountry();
            if (country != null)
            {
                model.Country = country;
                List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
                if (states != null && states.Any())
                {
                    foreach (State state in states)
                    {
                        model.States.Add(new StateModel() 
                            { 
                                Id=state.Id,
                                Name=state.Name,
                                CountryId=country.Id
                            }
                        );
                    }
                }
            }
            return View(model);
        }

        public ActionResult EditState()
        {
            Country country = MasterDataRepository.GetEnabledCountry();
            if (country!=null&&RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ViewBag.SubTitle = id == 0 ? "Add State" : "Edit State";
                    if (id == 0)
                    {
                        StateModel model = new StateModel() { Id = id,Name="",CountryId=country.Id };
                        return View(model);
                    }
                    else
                    {
                        State state = MasterDataRepository.GetState(id);
                        if (state != null)
                        {
                            StateModel model = new StateModel() { Id = id, Name = state.Name, CountryId = country.Id };
                            return View(model);
                        }
                    }
                }
            }
            return RedirectToAction("ManageState");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditState(StateModel model)
        {
            ViewBag.SubTitle = model.Id == 0 ? "Add State" : "Edit State";
            if (ModelState.IsValid)
            {
                State state = MasterDataRepository.GetState(model.Name);
                if (state != null&&state.Id!=model.Id)
                {
                    if (state.IsEnabled)
                    {
                        ModelState.AddModelError("Name", "Name already exists, please choose a different one");
                    }
                    else
                    {
                        MasterDataRepository.UpdateState(state);
                        return RedirectToAction("ManageState");
                    }
                }
                else
                {
                    State item = new State() { Id = model.Id, Country = new Country() { Id=model.CountryId},Name=model.Name,IsEnabled=true };
                    if (model.Id > 0)
                    {
                        MasterDataRepository.UpdateState(item);
                    }
                    else
                    {
                        MasterDataRepository.AddState(item);
                    }
                    return RedirectToAction("ManageState");
                }
            }
            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteState()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    MasterDataRepository.DeleteState(id);
                }
            }
            return RedirectToAction("ManageState");
        }

        public ActionResult ManageCities()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        State state = MasterDataRepository.GetState(id);
                        if (state != null)
                        {
                            return View(state);
                        }
                    }
                }
            }
            return RedirectToAction("ManageState");
        }

        public ActionResult EditCity()
        {
            if (RouteData.Values.Any(v => v.Key == "id")&&Request.QueryString.AllKeys.Contains("sid"))
            {
                int id = 0;
                int sid = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && int.TryParse(Request.QueryString["sid"], out sid))
                {
                    if (sid > 0)
                    {
                        State state = MasterDataRepository.GetState(sid);
                        if (state != null)
                        {
                            ViewBag.SubTitle = id == 0 ? "Add City" : "Edit City";
                            if (id == 0)
                            {
                                CityModel model = new CityModel() { Id = id, Name = "", StateId=sid,StateName=state.Name };
                                return View(model);
                            }
                            else
                            {
                                City city = MasterDataRepository.GetCity(id);
                                if (city != null && city.State.Id == sid)
                                {
                                    CityModel model = new CityModel() { Id = id, StateId = sid, Name = city.Name,StateName=state.Name };
                                    return View(model);
                                }
                                else
                                {
                                    return RedirectToAction("ManageCities", new { id=sid});
                                }
                            }
                        }
                    }
                }
            }
            return RedirectToAction("ManageState");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCity(CityModel model)
        {
            ViewBag.SubTitle = model.Id == 0 ? "Add City" : "Edit City";
            if (ModelState.IsValid)
            {
                City city = MasterDataRepository.GetCity(model.Name);
                if (city != null && city.Id != model.Id)
                {
                    if (city.IsEnabled)
                    {
                        ModelState.AddModelError("Name", "Name already exists, please choose a different one");
                    }
                    else
                    {
                        city.State = new State() { Id=model.StateId};
                        MasterDataRepository.UpdateCity(city);
                        return RedirectToAction("ManageCities", new { id = model.StateId });
                    }
                }
                else
                {
                    City item = new City() { Id = model.Id, State = new State() { Id = model.StateId }, Name = model.Name, IsEnabled = true };
                    if (model.Id > 0)
                    {
                        MasterDataRepository.UpdateCity(item);
                    }
                    else
                    {
                        MasterDataRepository.AddCity(item);
                    }
                    return RedirectToAction("ManageCities", new { id = model.StateId });
                }
            }
            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCity()
        {
            if (RouteData.Values.Any(v => v.Key == "id") && Request.QueryString.AllKeys.Contains("sid"))
            {
                int id = 0;
                int sid = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && int.TryParse(Request.QueryString["sid"], out sid))
                {
                    if (sid > 0&&id>0)
                    {
                        State state = MasterDataRepository.GetState(sid);
                        if (state != null)
                        {
                            City city = MasterDataRepository.GetCity(id);
                            if (city != null && city.State.Id == sid)
                            {
                                MasterDataRepository.DeleteCity(id);
                                return RedirectToAction("ManageCities", new { id = sid });
                            }
                        }
                    }
                }
            }
            return RedirectToAction("ManageState");
        }

        public ActionResult ManageBrand()
        {
            ManageBrandModel model = new ManageBrandModel() { Brands=new List<BrandModel>()};
            List<Brand> brands = BrandRepository.GetBrands();
            if (brands != null && brands.Any())
            {
                string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/";
                
                foreach (Brand pm in brands)
                {
                    model.Brands.Add(new BrandModel() { Id = pm.Id, Name = pm.Name, Logo = url + pm.Logo });
                }
            }

            return View(model);
        }

        public ActionResult EditBrand()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ViewBag.SubTitle = id == 0 ? "Add Brand" : "Edit Brand";
                    if (id == 0)
                    {
                        BrandModel model = new BrandModel() { Id = id, Name = "", Logo = "", GUID = Guid.NewGuid().ToString() };
                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                        Directory.CreateDirectory(todirectory);
                        return View(model);
                    }
                    else
                    {
                        Brand brand = BrandRepository.GetBrand(id);
                        if (brand != null)
                        {
                            BrandModel model = new BrandModel() { Id = id, Name = brand.Name, Logo = brand.Logo, GUID = Guid.NewGuid().ToString() };

                            if (!string.IsNullOrEmpty(model.Logo))
                            {
                                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                string directory = Server.MapPath("~/" + uploadfolder);
                                string name = model.Logo.Replace(uploadfolder + "/", "");
                                string path = Path.Combine(directory, name);
                                string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                                Directory.CreateDirectory(todirectory);
                                if (System.IO.File.Exists(path))
                                {
                                    Picture.Copy(path, Path.Combine(todirectory, name), true);
                                }
                            }
                            return View(model);
                        }
                    }
                }
            }
            return RedirectToAction("ManageBrand");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBrand(BrandModel model)
        {
            ViewBag.SubTitle = model.Id == 0 ? "Add Brand" : "Edit Brand";
            if (ModelState.IsValid)
            {
                Brand pm = BrandRepository.GetBrand(model.Name);
                if (pm != null && pm.Id != model.Id)
                {
                    ModelState.AddModelError("Name", "Name already exists, please choose a different one");
                }
                else
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                    DirectoryInfo di = new DirectoryInfo(directory);
                    FileInfo[] files = di.GetFiles();

                    if (files == null || !files.Any())
                    {
                        ModelState.AddModelError("Logo", "Logo is required");
                    }
                    else
                    {
                        foreach (FileInfo fi in di.EnumerateFiles())
                        {
                            Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name), true);
                            model.Logo = uploadfolder + "/" + fi.Name;
                        }
                        Picture.DeleteDirectory(directory, true);
                        Brand item = new Brand() { Id = model.Id, Name = model.Name, Logo = model.Logo };
                        BrandRepository.UpdateBrand(item);
                        return RedirectToAction("ManageBrand");
                    }
                }
            }
            return View(model);
        }

        public ActionResult CancelBrand()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManageBrand");
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteBrand()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    BrandRepository.DeleteBrand(id);
                }
            }
            return RedirectToAction("ManageBrand");
        }

        public ActionResult ManagePayment()
        {
            ManagePaymentModel model = new ManagePaymentModel() { Payments=new List<PaymentModel>()};
            List<PaymentMethod> payments = MasterDataRepository.GetPaymentMethods();
            if (payments != null && payments.Any())
            {
                string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/");
                foreach (PaymentMethod pm in payments)
                {
                    model.Payments.Add(new PaymentModel() { Id=pm.Id,Name=pm.Name,Logo=url+pm.Logo,Status=pm.IsEnabled?"Enabled":"Disabled"});
                }
            }
            return View(model);
        }

        public ActionResult EditPayment()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ViewBag.SubTitle = id == 0 ? "Add Payment Method" : "Edit Payment Method";
                    if (id == 0)
                    {
                        PaymentModel model = new PaymentModel() { Id = id, Name = "", Logo="",GUID=Guid.NewGuid().ToString(),IsEnabled=true };
                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                        Directory.CreateDirectory(todirectory);
                        return View(model);
                    }
                    else
                    {
                        PaymentMethod payment = MasterDataRepository.GetPaymentMethod(id);
                        if (payment != null)
                        {
                            PaymentModel model = new PaymentModel() { Id = id, Name = payment.Name, IsEnabled=payment.IsEnabled,Logo=payment.Logo,GUID=Guid.NewGuid().ToString() };

                            if (!string.IsNullOrEmpty(model.Logo))
                            {
                                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                string directory = Server.MapPath("~/" + uploadfolder);
                                string name = model.Logo.Replace(uploadfolder + "/", "");
                                string path = Path.Combine(directory, name);
                                string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                                Directory.CreateDirectory(todirectory);
                                if (System.IO.File.Exists(path))
                                {
                                    Picture.Copy(path, Path.Combine(todirectory, name), true);
                                }
                            }
                            return View(model);
                        }
                    }
                }
            }
            return RedirectToAction("ManagePayment");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPayment(PaymentModel model)
        {
            ViewBag.SubTitle = model.Id == 0 ? "Add Payment" : "Edit Payment";
            if (ModelState.IsValid)
            {
                PaymentMethod pm = MasterDataRepository.GetPaymentMethod(model.Name);
                if (pm != null && pm.Id != model.Id)
                {
                    ModelState.AddModelError("Name", "Name already exists, please choose a different one");
                }
                else
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                    DirectoryInfo di = new DirectoryInfo(directory);
                    FileInfo[] files=di.GetFiles();
                    
                    if (files==null||!files.Any())
                    {
                        ModelState.AddModelError("Logo", "Logo is required");
                    }
                    else
                    {
                        foreach (FileInfo fi in di.EnumerateFiles())
                        {
                            Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name), true);
                            model.Logo = uploadfolder + "/" + fi.Name;
                        }
                        Picture.DeleteDirectory(directory, true);
                        PaymentMethod item = new PaymentMethod() { Id = model.Id, Name = model.Name, Logo = model.Logo, IsEnabled = model.IsEnabled };
                        MasterDataRepository.UpdatePaymentMethod(item);
                        return RedirectToAction("ManagePayment");
                    }
                }
            }
            return View(model);
        }

        public ActionResult CancelPayment()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManagePayment");
        }

        public ActionResult ManageDelivery()
        {
            ManageDeliveryModel model = new ManageDeliveryModel() { Deliveries=new List<DeliveryModel>() };
            List<DeliveryMethod> deliveries = MasterDataRepository.GetDeliveryMethods();
            if (deliveries != null && deliveries.Any())
            {
                string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/");
                foreach (DeliveryMethod dm in deliveries)
                {
                    model.Deliveries.Add(new DeliveryModel() { Id = dm.Id, Name = dm.Name, Logo = url + dm.Logo, Status = dm.IsEnabled ? "Enabled" : "Disabled" });
                }
            }
            return View(model);
        }

        public ActionResult EditDelivery()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ViewBag.SubTitle = id == 0 ? "Add Delivery Method" : "Edit Delivery Method";
                    if (id == 0)
                    {
                        DeliveryModel model = new DeliveryModel() { Id = id, Name = "", Logo = "", GUID = Guid.NewGuid().ToString(), IsEnabled = true };
                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                        Directory.CreateDirectory(todirectory);
                        return View(model);
                    }
                    else
                    {
                        DeliveryMethod delivery = MasterDataRepository.GetDeliveryMethod(id);
                        if (delivery != null)
                        {
                            DeliveryModel model = new DeliveryModel() { Id = id, Name = delivery.Name, IsEnabled = delivery.IsEnabled, Logo = delivery.Logo, GUID = Guid.NewGuid().ToString() };

                            if (!string.IsNullOrEmpty(model.Logo))
                            {
                                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                string directory = Server.MapPath("~/" + uploadfolder);
                                string name = model.Logo.Replace(uploadfolder + "/", "");
                                string path = Path.Combine(directory, name);
                                string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                                Directory.CreateDirectory(todirectory);
                                if (System.IO.File.Exists(path))
                                {
                                    Picture.Copy(path, Path.Combine(todirectory, name), true);
                                }
                            }
                            return View(model);
                        }
                    }
                }
            }
            return RedirectToAction("ManageDelivery");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDelivery(DeliveryModel model)
        {
            ViewBag.SubTitle = model.Id == 0 ? "Add Delivery" : "Edit Delivery";
            if (ModelState.IsValid)
            {
                DeliveryMethod pm = MasterDataRepository.GetDeliveryMethod(model.Name);
                if (pm != null && pm.Id != model.Id)
                {
                    ModelState.AddModelError("Name", "Name already exists, please choose a different one");
                }
                else
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                    DirectoryInfo di = new DirectoryInfo(directory);
                    FileInfo[] files = di.GetFiles();

                    if (files == null || !files.Any())
                    {
                        ModelState.AddModelError("Logo", "Logo is required");
                    }
                    else
                    {
                        foreach (FileInfo fi in di.EnumerateFiles())
                        {
                            Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name), true);
                            model.Logo = uploadfolder + "/" + fi.Name;
                        }
                        Picture.DeleteDirectory(directory, true);
                        DeliveryMethod item = new DeliveryMethod() { Id = model.Id, Name = model.Name, Logo = model.Logo, IsEnabled = model.IsEnabled };
                        MasterDataRepository.UpdateDeliveryMethod(item);
                        return RedirectToAction("ManageDelivery");
                    }
                }
            }
            return View(model);
        }

        public ActionResult CancelDelivery()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManageDelivery");
        }

        public ActionResult ManageFee()
        {
            Country country = MasterDataRepository.GetEnabledCountry();
            if (country != null && RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    DeliveryMethod dm = MasterDataRepository.GetDeliveryMethod(id);
                    if (dm != null)
                    {
                        ManageFeeModel model = new ManageFeeModel() { DeliveryMethodId=dm.Id,DeliveryMethod=dm.Name,Fees=new List<FeeModel>()};
                        List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
                        List<DeliveryFee> fees = MasterDataRepository.GetDeliveryFee(dm.Id);
                        foreach (State state in states)
                        {
                            decimal fee = 0;
                            if (fees.Any(f => f.State.Id == state.Id))
                            {
                                fee = fees.FirstOrDefault(f => f.State.Id == state.Id).Fee;
                            }
                            model.Fees.Add(new FeeModel() { Fee=Decimal.Round(fee,2),StateId=state.Id,DeliveryMethodId=dm.Id,StateName=state.Name});
                        }
                        return View(model);
                    }
                }
            }
            return RedirectToAction("ManageDelivery");
        }

        public ActionResult EditFee()
        {
            if (RouteData.Values.Any(v => v.Key == "id") && Request.QueryString.AllKeys.Contains("dmid"))
            {
                int id = 0;
                int dmid = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && int.TryParse(Request.QueryString["dmid"], out dmid))
                {
                    if (dmid > 0 && id > 0)
                    {
                        DeliveryMethod dm = MasterDataRepository.GetDeliveryMethod(dmid);
                        State state = MasterDataRepository.GetState(id);
                        DeliveryFee fee = MasterDataRepository.GetDeliveryFee(dmid, id);
                        if (dm != null && state != null)
                        {
                            FeeModel model = new FeeModel() { StateId = state.Id, Fee = 0, DeliveryMethod = dm.Name, DeliveryMethodId = dm.Id, StateName = state.Name };
                            if (fee != null)
                                model.Fee = Decimal.Round(fee.Fee,2);
                            return View(model);
                        }
                    }
                }
            }
            return RedirectToAction("ManageDelivery");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFee(FeeModel model)
        {
            if (ModelState.IsValid)
            {
                MasterDataRepository.UpdateDeliveryFee(new DeliveryFee() { Fee = model.Fee, State = new State() { Id = model.StateId }, DeliveryMethod = new DeliveryMethod() {Id=model.DeliveryMethodId } });
                return RedirectToAction("ManageFee", new { id=model.DeliveryMethodId});
            }
            return View(model);
        }

        public ActionResult ManagePromotion()
        {
            ManagePromotionModel model = new ManagePromotionModel() {FeaturedProducts=new List<FeaturedProductModel>(),NonFeaturedProducts=new List<FeaturedProductModel>(),RecommendProducts=new List<FeaturedProductModel>(),NotRecommendProducts=new List<FeaturedProductModel>() };
            List<Product> products= ProductRepository.GetAllProducts();
            if (products != null && products.Any())
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
                foreach (Product prod in products)
                {
                    FeaturedProductModel fpm = new FeaturedProductModel() { Id = prod.Id, Name = prod.Name, Category = prod.Category.Name };
                    if (prod.Pictures != null && prod.Pictures.Any())
                    {

                        ProductPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                        if (defPic == null)
                            defPic = prod.Pictures.FirstOrDefault();
                        fpm.Uri = url + "/" + defPic.Picture;
                    }
                    else
                    {
                        fpm.Uri = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                    }
                    if (prod.IsFeatured)
                    {
                        model.FeaturedProducts.Add(fpm);
                    }
                    else
                    {
                        model.NonFeaturedProducts.Add(fpm);
                    }

                    if (prod.IsRecommended)
                    {
                        model.RecommendProducts.Add(fpm);
                    }
                    else
                    {
                        model.NotRecommendProducts.Add(fpm);
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Feature()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        ProductRepository.Feature(id);
                    }
                }
            }
            return RedirectToAction("ManagePromotion");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UnFeature()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        ProductRepository.UnFeature(id);
                    }
                }
            }
            return RedirectToAction("ManagePromotion");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Recommend()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        ProductRepository.Recommend(id);
                    }
                }
            }
            return RedirectToAction("ManagePromotion");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UnRecommend()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        ProductRepository.UnRecommend(id);
                    }
                }
            }
            return RedirectToAction("ManagePromotion");
        }

        public ActionResult ManageSetting()
        {
            SettingModel model = new SettingModel();
            Setting setting = SettingRepository.GetSetting();
            model.Id = setting.Id;
            model.ExchangeRate = Decimal.Round(setting.ExchangeRate,2);
            model.TaxRate = Decimal.Round(setting.TaxRate, 2);
            model.GiftSetDiscount = Decimal.Round(setting.GiftSetDiscount, 2);
            model.RepeatCustomerDiscount = Decimal.Round(setting.RepeatCustomerDiscount, 2);
            ViewBag.Message = "";
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageSetting(SettingModel model)
        {
            ViewBag.Message = "";
            if (ModelState.IsValid)
            {
                Setting existing = SettingRepository.GetSetting();
                Setting setting = SettingRepository.Update(new Setting() { Id = existing.Id, ExchangeRate = model.ExchangeRate,TaxRate=model.TaxRate,GiftSetDiscount=model.GiftSetDiscount,RepeatCustomerDiscount=model.RepeatCustomerDiscount });
                //model.Id = setting.Id;
                ViewBag.Message = "Saved Successfully";
                return View(new SettingModel() { Id=setting.Id,ExchangeRate=setting.ExchangeRate});
            }
            return View(model); 
        }

        public ActionResult ManageBanner()
        {
            ManageBannerModel model = new ManageBannerModel() { HomeBanners=new List<HomeBanner>(),CategoryBanners=new List<CategoryBannerModel>(),SaleBanners=new List<SaleBanner>()};
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);

            model.PopupBanners = new List<PopupBannerModel>();
            string popupBanner = System.Web.Configuration.WebConfigurationManager.AppSettings["PopupBanner"];
            string popupBannerGUID = System.Web.Configuration.WebConfigurationManager.AppSettings["PopupBannerGUID"];
            if (string.IsNullOrWhiteSpace(popupBanner) == false)
            {
                model.PopupBanners.Add(new PopupBannerModel() { Banner = url + "/" + popupBannerGUID + "/" + popupBanner });
            }

            List<HomeBanner> hbs = BannerRepository.GetHomeBanners();
            if (hbs != null && hbs.Any())
            {
                foreach (HomeBanner hb in hbs)
                {
                    hb.TopBanner = url + "/" + hb.TopBanner;
                    hb.Banner1 = url + "/" + hb.Banner1;
                    hb.Banner2 = url + "/" + hb.Banner2;
                    hb.Banner3 = url + "/" + hb.Banner3;
                    model.HomeBanners.Add(hb);
                }
            }
            List<Category> cats = CategoryRepository.GetAllCategories();
            if (cats != null && cats.Any())
            {
                foreach (Category cat in cats)
                {
                    if (cat.ParentId == 0)
                    {
                        CategoryBannerModel cbm = new CategoryBannerModel() { Id=cat.Id,Name=cat.Name};
                        cbm.TopBanner = url + "/" + cat.TopBanner;
                        cbm.Banner1 = url + "/" + cat.Banner1;
                        cbm.Banner1Link =  cat.Banner1Link;
                        model.CategoryBanners.Add(cbm);
                    }
                }
            }
            SaleBanner sb = SettingRepository.GetSaleBanner();
            if (sb != null)
            {
                sb.TopBanner = url + "/" + sb.TopBanner;
                sb.Banner1 = url + "/" + sb.Banner1;
                model.SaleBanners.Add(sb);
            }
            return View(model);
        }

        public ActionResult EditHomeBanner()
        {
            if (!RouteData.Values.Any(v => v.Key == "id"))
                return RedirectToAction("ManageBanner");
            HomeBannerModel model = new HomeBannerModel() { GUID=Guid.NewGuid().ToString()};
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
            Directory.CreateDirectory(directory);
            Directory.CreateDirectory(Path.Combine(directory,"1600x700"));
            Directory.CreateDirectory(Path.Combine(directory,"570x180"));
            Directory.CreateDirectory(Path.Combine(directory,"270x180x1"));
            Directory.CreateDirectory(Path.Combine(directory, "270x180x2"));
            ViewBag.SubTitle = "Add Home Banner";
            
            int id = 0;
            if (int.TryParse(RouteData.Values["id"].ToString(), out id))
            {
                if (id > 0)
                {
                    HomeBanner hb = BannerRepository.GetHomeBanner(id);
                    if (hb != null)
                    {
                        ViewBag.SubTitle = "Edit Home Banner";
                        model.Id = hb.Id;
                        model.TopBanner = hb.TopBanner;
                        model.TopBannerLink = hb.TopBannerLink;
                        model.Banner1 = hb.Banner1;
                        model.Banner1Link = hb.Banner1Link;
                        model.Banner2 = hb.Banner2;
                        model.Banner2Link = hb.Banner2Link;
                        model.Banner3 = hb.Banner3;
                        model.Banner3Link = hb.Banner3Link;
                        if (!string.IsNullOrEmpty(hb.TopBanner) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.TopBanner)))
                        {
                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.TopBanner), Path.Combine(directory,"1600x700", hb.TopBanner), true);
                        }
                        if (!string.IsNullOrEmpty(hb.Banner1) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner1)))
                        {
                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner1), Path.Combine(directory,"570x180", hb.Banner1), true);
                        }
                        if (!string.IsNullOrEmpty(hb.Banner2) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner2)))
                        {
                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner2), Path.Combine(directory,"270x180x1", hb.Banner2), true);
                        }
                        if (!string.IsNullOrEmpty(hb.Banner3) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner3)))
                        {
                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner3), Path.Combine(directory,"270x180x2", hb.Banner3), true);
                        }
                    }
                }
            }
            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditHomeBanner(HomeBannerModel model)
        {
            ViewBag.SubTitle = "Add Home Banner";
            if(model.Id>0)
                ViewBag.SubTitle = "Edit Home Banner";
            if (ModelState.IsValid)
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                HomeBanner hb = new HomeBanner()
                {
                    Id=model.Id,
                    Banner1Link=model.Banner1Link,
                    Banner2Link=model.Banner2Link,
                    Banner3Link=model.Banner3Link,
                    TopBannerLink=model.TopBannerLink
                };
                var topfiles = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "1600x700")).GetFiles();
                if (topfiles.Count() == 0)
                {
                    ModelState.AddModelError("", "Top Banner is required!");
                    return View(model);
                }
                else
                {
                    hb.TopBanner = topfiles.FirstOrDefault().Name;
                }

                var banner1files = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "570x180")).GetFiles();
                if (banner1files.Count() == 0)
                {
                    ModelState.AddModelError("", "Banner 1 is required!");
                    return View(model);
                }
                else
                {
                    hb.Banner1 = banner1files.FirstOrDefault().Name;
                }

                var banner2files = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "270x180x1")).GetFiles();
                if (banner2files.Count() == 0)
                {
                    ModelState.AddModelError("", "Banner 2 is required!");
                    return View(model);
                }
                else
                {
                    hb.Banner2 = banner2files.FirstOrDefault().Name;
                }

                var banner3files = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "270x180x2")).GetFiles();
                if (banner3files.Count() == 0)
                {
                    ModelState.AddModelError("", "Banner 3 is required!");
                    return View(model);
                }
                else
                {
                    hb.Banner3 = banner3files.FirstOrDefault().Name;
                }

                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "1600x700"), hb.TopBanner), Path.Combine(Server.MapPath("~/" + uploadfolder), hb.TopBanner), true);
                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "570x180"), hb.Banner1), Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner1), true);
                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "270x180x1"), hb.Banner2), Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner2), true);
                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "270x180x2"), hb.Banner3), Path.Combine(Server.MapPath("~/" + uploadfolder), hb.Banner3), true);
                Picture.DeleteDirectory(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID), true);

                if (hb.Id > 0)
                {
                    BannerRepository.UpdateHomeBanner(hb);
                }
                else
                {
                    BannerRepository.CreateHomeBanner(hb);
                }

                
                return RedirectToAction("ManageBanner");
                
            }
            return View(model);
        }

        public ActionResult EditPopupBanner()
        {
            PopupBannerModel model = new PopupBannerModel() { GUID = Guid.NewGuid().ToString() };

            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
            Directory.CreateDirectory(directory);
            string popupBanner = System.Web.Configuration.WebConfigurationManager.AppSettings["PopupBanner"];

            ViewBag.SubTitle = "Add Popup Banner";

            if (string.IsNullOrWhiteSpace(popupBanner) == false)
            {                
                ViewBag.SubTitle = "Edit Popup Banner";

                model.GUID = System.Web.Configuration.WebConfigurationManager.AppSettings["PopupBannerGUID"];
                model.Banner = popupBanner;
            }

            return View(model);
        }

        [HttpDelete]
        public ActionResult DeletePopupBanner()
        {
            SetAppKey("PopupBannerGUID", string.Empty);
            SetAppKey("PopupBanner", string.Empty);

            return RedirectToAction("ManageBanner");
        }

        private void SetAppKey(string keyName, string keyValue)  
        {
            System.Configuration.Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(System.Web.HttpContext.Current.Request.ApplicationPath);
            AppSettingsSection appSection = (AppSettingsSection)config.GetSection("appSettings");
            if (appSection.Settings[keyName] == null)
            {
                appSection.Settings.Add(new KeyValueConfigurationElement(keyName, keyValue));
            }
            else
            {
                appSection.Settings[keyName].Value = keyValue;
            }
            config.Save();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPopupBanner(PopupBannerModel model)
        {
            string popupBanner = System.Web.Configuration.WebConfigurationManager.AppSettings["PopupBanner"];

            ViewBag.SubTitle = "Add Popup Banner";
            if (string.IsNullOrWhiteSpace(popupBanner) == false)
            {
                ViewBag.SubTitle = "Edit Popup Banner";
            }

            if (ModelState.IsValid)
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                var files = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID)).GetFiles();
                if (files.Count() == 0)
                {
                    ModelState.AddModelError("", "Popup Banner is required!");
                    return View(model);
                }
                else
                {
                    popupBanner = files.FirstOrDefault().Name;

                    SetAppKey("PopupBannerGUID", model.GUID);
                    SetAppKey("PopupBanner", popupBanner);
                }                

                return RedirectToAction("ManageBanner");

            }
            return View(model);
        }

        [HttpPost]
        public ActionResult UploadBanner(HttpPostedFileBase qqfile)
        {
            if (Request.Form["type"] == null)
            {
                return Json(new { success = false, error = "Invalid Request!", preventRetry = true });
            }
            string subdirectory = "";
            int width = 0;
            int height = 0;
            switch (Request.Form["type"])
            {
                case "HomeTopBanner":
                    subdirectory = "1600x700";
                    width = 1600;
                    height = 700;
                    break;
                case "HomeBanner1":
                    subdirectory = "570x180";
                    width = 570;
                    height = 180;
                    break;
                case "HomeBanner2":
                    subdirectory = "270x180x1";
                    width = 270;
                    height = 180;
                    break;
                case "HomeBanner3":
                    subdirectory = "270x180x2";
                    width = 270;
                    height = 180;
                    break;
                case "CategoryTopBanner":
                    subdirectory = "1600x360";
                    width = 1600;
                    height = 360;
                    break;
                case "CategoryBanner1":
                    subdirectory = "870x370";
                    width = 870;
                    height = 370;
                    break;
                case "PopupBanner":
                    subdirectory = "";
                    width = 600;
                    height = 600;
                    break;
            }

            System.Drawing.Image image = System.Drawing.Image.FromStream(qqfile.InputStream);
            if (image.Width != width || image.Height != height)
            {
                return Json(new { success = false, error = "Please upload images with "+width.ToString()+"px width and "+height.ToString()+"px height!", preventRetry = true });
            }

            string guid = Request.Form["guid"];
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, "/" + uploadfolder + "/" + guid+"/"+subdirectory);

            if (qqfile != null && qqfile.ContentLength > 0)
            {
                string fileName = Request.Form["qquuid"] + Path.GetExtension(qqfile.FileName);
                var path = Path.Combine(Server.MapPath("~/" + uploadfolder + "/" + guid + "/" + subdirectory), fileName);
                try
                {
                    qqfile.SaveAs(@path);
                    return Json(new { success = true, thumbnailUrl = url + "/" + fileName }, "text/plain");
                }
                catch
                {
                    return Json(new { success = false }, "text/plain");
                }
            }
            else
            {
                // this works for Firefox, Chrome
                var filename = Request["qqfile"];
                if (!string.IsNullOrEmpty(filename))
                {
                    string newFileName = Request.Form["qquuid"] + Path.GetExtension(qqfile.FileName);
                    filename = Path.Combine(Server.MapPath("~/" + uploadfolder + "/" + guid + "/" + subdirectory), newFileName);
                    try
                    {
                        using (var output = System.IO.File.Create(filename))
                        {
                            Request.InputStream.CopyTo(output);
                        }
                        return Json(new { success = true, thumbnailUrl = url + "/" + newFileName }, "text/plain");
                    }
                    catch
                    {
                        return Json(new { success = false }, "text/plain");
                    }
                }
            }
            return Json(new { success = false }, "text/plain");
        }

        [HttpDelete]
        public ActionResult DeleteBanner()
        {
            if (Request.QueryString["type"] == null)
            {
                return Json(new { success = false, error = "Invalid Request!", preventRetry = true });
            }
            string subdirectory = "";
            switch (Request.QueryString["type"])
            {
                case "HomeTopBanner":
                    subdirectory = "1600x700";
                    break;
                case "HomeBanner1":
                    subdirectory = "570x180";
                    break;
                case "HomeBanner2":
                    subdirectory = "270x180x1";
                    break;
                case "HomeBanner3":
                    subdirectory = "270x180x2";
                    break;
                case "CategoryTopBanner":
                    subdirectory = "1600x360";
                    break;
                case "CategoryBanner1":
                    subdirectory = "870x370";
                    break;
                case "PopupBanner":
                    subdirectory = "";
                    break;
            }
            string guid = Request.QueryString["guid"];
            Response.StatusCode = 200;
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uuid = RouteData.Values["id"].ToString();
                if (uuid.Length > 32)
                {
                    uuid = uuid.Substring(0, 32);
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Server.MapPath("~/" + uploadfolder + "/" + guid+"/"+subdirectory);
                    foreach (string filename in Directory.EnumerateFiles(directory))
                    {
                        if (filename.ToLower().Contains(uuid.ToLower()))
                        {
                            try
                            {
                                System.IO.File.Delete(filename);
                            }
                            catch
                            {
                                Response.StatusCode = 500;
                            }
                            break;
                        }
                    }
                }

            }
            return Json(new { success = true }, "text/plain");
        }

        public ActionResult GetBanner()
        {
            if (Request.QueryString["type"] == null)
            {
                return Json(new { success = false, error = "Invalid Request!", preventRetry = true });
            }
            string subdirectory = "";
            switch (Request.QueryString["type"])
            {
                case "HomeTopBanner":
                    subdirectory = "1600x700";
                    break;
                case "HomeBanner1":
                    subdirectory = "570x180";
                    break;
                case "HomeBanner2":
                    subdirectory = "270x180x1";
                    break;
                case "HomeBanner3":
                    subdirectory = "270x180x2";
                    break;
                case "CategoryTopBanner":
                    subdirectory = "1600x360";
                    break;
                case "CategoryBanner1":
                    subdirectory = "870x370";
                    break;
                case "PopupBanner":
                    subdirectory = "";
                    break;
            }
            string guid = Request.QueryString["guid"];
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), guid,subdirectory);

            string url = "";
            if (string.IsNullOrWhiteSpace(subdirectory) == false)
            {
                url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder + "/" + guid + "/" + subdirectory);
            }
            else
            {
                url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder + "/" + guid);
            }

            DirectoryInfo di = new DirectoryInfo(directory);
            List<Picture> pictures = new List<Picture>();
            foreach (FileInfo fi in di.EnumerateFiles())
            {
                pictures.Add(new Picture()
                {
                    uuid = fi.Name.Replace(Path.GetExtension(fi.Name), ""),
                    name = fi.Name,
                    size = fi.Length,
                    thumbnailUrl = url + "/" + fi.Name
                });
            }
            Response.StatusCode = 200;
            return Json(pictures, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CancelBanner()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManageBanner");
        }

        [HttpDelete]
        public ActionResult DeleteHomeBanner()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id)&&id>0)
                {
                    BannerRepository.DeleteHomeBanner(id);
                }
            }
            return RedirectToAction("ManageBanner");
        }

        public ActionResult EditCategoryBanner()
        {
            if (!RouteData.Values.Any(v => v.Key == "id"))
                return RedirectToAction("ManageBanner");
            CategoryBannerModel model = new CategoryBannerModel() { GUID = Guid.NewGuid().ToString() };

            int id = 0;
            if (int.TryParse(RouteData.Values["id"].ToString(), out id))
            {
                if (id > 0)
                {

                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                    Directory.CreateDirectory(directory);
                    Directory.CreateDirectory(Path.Combine(directory, "1600x360"));
                    Directory.CreateDirectory(Path.Combine(directory, "870x370"));
                    ViewBag.SubTitle = "Edit Category Banner";
                    Category cat = CategoryRepository.GetCategoryById(id);
                    if (cat != null)
                    {
                        model.Id = cat.Id;
                        model.TopBanner = cat.TopBanner;
                        model.Banner1 = cat.Banner1;
                        model.Banner1Link = cat.Banner1Link;
                        model.Name = cat.Name;
                        if (!string.IsNullOrEmpty(cat.TopBanner) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.TopBanner)))
                        {
                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.TopBanner), Path.Combine(directory, "1600x360", cat.TopBanner), true);
                        }
                        if (!string.IsNullOrEmpty(cat.Banner1) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.Banner1)))
                        {
                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.Banner1), Path.Combine(directory, "870x370", cat.Banner1), true);
                        }
                    }
                }
                else
                {
                    return RedirectToAction("ManageBanner");
                }
            }
            else
            {
                return RedirectToAction("ManageBanner");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategoryBanner(CategoryBannerModel model)
        {
            ViewBag.SubTitle = "Edit Category Banner";
           
            if (ModelState.IsValid)
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                Category category=new Category()
                {
                    Id = model.Id,
                    Banner1Link = string.IsNullOrEmpty(model.Banner1Link)?"":model.Banner1Link
                };
                var topfiles = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "1600x360")).GetFiles();
                if (topfiles.Count() == 0)
                {
                    ModelState.AddModelError("", "Top Banner is required!");
                    return View(model);
                }
                else
                {
                    category.TopBanner = topfiles.FirstOrDefault().Name;
                }

                var banner1files = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "870x370")).GetFiles();
                if (banner1files.Count() == 0)
                {
                    category.Banner1 = string.Empty;
                    //ModelState.AddModelError("", "Banner 1 is required!");
                    //return View(model);
                }
                else
                {
                    category.Banner1 = banner1files.FirstOrDefault().Name;
                }



                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "1600x360"), category.TopBanner), Path.Combine(Server.MapPath("~/" + uploadfolder), category.TopBanner), true);
                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "870x370"), category.Banner1), Path.Combine(Server.MapPath("~/" + uploadfolder), category.Banner1), true);
                Picture.DeleteDirectory(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID), true);

                CategoryRepository.CreateBanner(category);

                return RedirectToAction("ManageBanner");

            }
            return View(model);
        }

        public ActionResult EditSaleBanner()
        {
            if (!RouteData.Values.Any(v => v.Key == "id"))
                return RedirectToAction("ManageBanner");
            CategoryBannerModel model = new CategoryBannerModel() { GUID = Guid.NewGuid().ToString() };

            int id = 0;
            if (int.TryParse(RouteData.Values["id"].ToString(), out id))
            {
                SaleBanner cat = SettingRepository.GetSaleBanner();
                if (cat != null&&cat.Id==id)
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                    Directory.CreateDirectory(directory);
                    Directory.CreateDirectory(Path.Combine(directory, "1600x360"));
                    Directory.CreateDirectory(Path.Combine(directory, "870x370"));
                    ViewBag.SubTitle = "Edit Gift Set Banner";
                    model.Id = cat.Id;
                    model.TopBanner = cat.TopBanner;
                    model.Banner1 = cat.Banner1;
                    model.Banner1Link = cat.Banner1Link;
                    if (!string.IsNullOrEmpty(cat.TopBanner) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.TopBanner)))
                    {
                        Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.TopBanner), Path.Combine(directory, "1600x360", cat.TopBanner), true);
                    }
                    if (!string.IsNullOrEmpty(cat.Banner1) && System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.Banner1)))
                    {
                        Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), cat.Banner1), Path.Combine(directory, "870x370", cat.Banner1), true);
                    }
                } 
            }
            else
            {
                return RedirectToAction("ManageBanner");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSaleBanner(CategoryBannerModel model)
        {
            ViewBag.SubTitle = "Edit Sale Banner";

            if (ModelState.IsValid)
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                SaleBanner category = new SaleBanner()
                {
                    Id = model.Id,
                    Banner1Link = string.IsNullOrEmpty(model.Banner1Link) ? "" : model.Banner1Link
                };
                var topfiles = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "1600x360")).GetFiles();
                if (topfiles.Count() == 0)
                {
                    ModelState.AddModelError("", "Top Banner is required!");
                    return View(model);
                }
                else
                {
                    category.TopBanner = topfiles.FirstOrDefault().Name;
                }

                var banner1files = new DirectoryInfo(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "870x370")).GetFiles();
                if (banner1files.Count() == 0)
                {
                    category.Banner1 = string.Empty;
                    //ModelState.AddModelError("", "Banner 1 is required!");
                    //return View(model);
                }
                else
                {
                    category.Banner1 = banner1files.FirstOrDefault().Name;
                }



                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "1600x360"), category.TopBanner), Path.Combine(Server.MapPath("~/" + uploadfolder), category.TopBanner), true);
                Picture.Copy(Path.Combine(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID, "870x370"), category.Banner1), Path.Combine(Server.MapPath("~/" + uploadfolder), category.Banner1), true);
                Picture.DeleteDirectory(Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID), true);

                SettingRepository.UpdateSaleBanner(category);

                return RedirectToAction("ManageBanner");

            }
            return View(model);
        }

        public ActionResult ManageNews()
        {
            List<Newsletter> newsletters = NewsletterRepository.GetNewsletters();
            ManageNewsModel model = new ManageNewsModel() { Newsletters=new List<NewsletterModel>()};
            //List<Product> products = ProductRepository.GetAllProducts();
            //if (products != null)
            //    products = new List<Product>();
            if (newsletters != null && newsletters.Any())
            {
                foreach (var item in newsletters)
                {
                    NewsletterModel nm = new NewsletterModel()
                    {
                        IsEnabled=item.IsEnabled,
                        StartDate=item.StartDate.ToShortDateString(),
                        Id=item.Id,
                        Body=item.Body,
                        Title=item.Title,
                        SentType=item.SentType.ToString()
                    };
                    //,
                    //    ProductIds="",
                    //    Products=products
                    //if (nm.Products != null && nm.Products.Any())
                    //{
                    //    foreach (var product in nm.Products)
                    //    {
                    //        nm.ProductIds += product.Id.ToString() + ",";
                    //    }
                    //}
                    //if (nm.ProductIds.Length > 0)
                    //    nm.ProductIds = nm.ProductIds.Substring(0, nm.ProductIds.Length - 1);
                    model.Newsletters.Add(nm);
                }
            }
            return View(model);
        }

        public ActionResult EditNews()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ViewBag.SubTitle = id == 0 ? "Add Newsletter" : "Edit Newsletter";
                    if (id == 0)
                    {
                        List<Product> products = ProductRepository.GetAllProducts();
                        ViewData["SentType"] = new List<SelectListItem>()
                        {
                            new SelectListItem(){Text="Daily", Value="Daily"},
                            new SelectListItem(){Text="Weekly", Value="Weekly"},
                            new SelectListItem(){Text="Monthly", Value="Monthly"}
                        };
                        NewsletterModel model = new NewsletterModel() { Id=0,IsEnabled=true,StartDate=DateTime.Now.ToString("MM/dd/yyyy"),SentType="Daily",Products=new List<NewsletterProductModel>(),ProductIds=""};
                        if (products != null && products.Any())
                        {
                            foreach (Product prod in products)
                            {
                                NewsletterProductModel npm = new NewsletterProductModel()
                                {
                                    IsChecked=false,
                                    ProductId=prod.Id,
                                    ProductName=prod.Name
                                };
                                string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority;
                                if (prod.Pictures.Any())
                                {
                                    var defpic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                                    if (defpic == null)
                                        defpic = prod.Pictures.FirstOrDefault();
                                    npm.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defpic.Picture;
                                }
                                else
                                {
                                    npm.Picture = uri + "/images/assets/not_available-generic.png";
                                }
                                model.Products.Add(npm);
                            }
                        }
                        return View(model);
                    }
                    else
                    {
                        Newsletter newsletter = NewsletterRepository.GetNewsletter(id);
                        if (newsletter != null)
                        {
                            List<Product> products = ProductRepository.GetAllProducts();
                            ViewData["SentType"] = new List<SelectListItem>()
                            {
                                new SelectListItem(){Text="Daily", Value="Daily",Selected=newsletter.SentType.ToString()=="Daily"},
                                new SelectListItem(){Text="Weekly", Value="Weekly",Selected=newsletter.SentType.ToString()=="Weekly"},
                                new SelectListItem(){Text="Monthly", Value="Monthly",Selected=newsletter.SentType.ToString()=="Monthly"}
                            };
                            NewsletterModel model = new NewsletterModel() { Id = newsletter.Id, IsEnabled = newsletter.IsEnabled, StartDate = newsletter.StartDate.ToString("MM/dd/yyyy"), SentType = newsletter.SentType.ToString(), Body = newsletter.Body, Title = newsletter.Title, Products = new List<NewsletterProductModel>(), ProductIds = "" };
                            if (products != null && products.Any())
                            {
                                foreach (Product prod in products)
                                {
                                    bool ischecked = false;
                                    if (newsletter.Products != null && newsletter.Products.Any(p => p.Id == prod.Id))
                                    {
                                        ischecked = true;
                                        model.ProductIds += prod.Id.ToString() + ",";
                                    }
                                    NewsletterProductModel npm = new NewsletterProductModel()
                                    {
                                        IsChecked = ischecked,
                                        ProductId = prod.Id,
                                        ProductName = prod.Name
                                    };
                                    string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority;
                                    if (prod.Pictures.Any())
                                    {
                                        var defpic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                                        if (defpic == null)
                                            defpic = prod.Pictures.FirstOrDefault();
                                        npm.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defpic.Picture;
                                    }
                                    else
                                    {
                                        npm.Picture = uri + "/images/assets/not_available-generic.png";
                                    }
                                    model.Products.Add(npm);
                                }
                            }
                            if (model.ProductIds.Length > 0)
                                model.ProductIds = model.ProductIds.Substring(0, model.ProductIds.Length - 1);
                            return View(model);
                        }
                    }

                }
            }
            return RedirectToAction("ManageNews");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditNews(NewsletterModel model)
        {
            DateTime date = DateTime.Now;
            if (!DateTime.TryParseExact(model.StartDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date))
            {
                ModelState.AddModelError("StartDate", "Please input a valid start date in format: MM/dd/yyyy");
            }
            if (ModelState.IsValid)
            {
                NewsletterType type = NewsletterType.Daily;
                switch (model.SentType)
                {
                    case "Daily":
                        break;
                    case "Monthly":
                        type = NewsletterType.Monthly;
                        break;
                    case "Weekly":
                        type = NewsletterType.Weekly;
                        break;
                }
                Newsletter newsletter = new Newsletter()
                {
                    Id=model.Id,
                    IsEnabled=model.IsEnabled,
                    StartDate=date,
                    Body=model.Body,
                    Title=model.Title,
                    SentType=type,
                    Products=new List<Product>()
                };
                string[] pids = { };
                if (!string.IsNullOrEmpty(model.ProductIds))
                {
                     pids= model.ProductIds.Split(',');
                }
                foreach(string pid in pids)
                {
                    int id = 0;
                    if (int.TryParse(pid, out id))
                    {
                        if (id > 0)
                            newsletter.Products.Add(new Product() { Id=id});
                    }
                }
                NewsletterRepository.AddNewsletter(newsletter);
                return RedirectToAction("ManageNews");

            }
            ViewData["SentType"] = new List<SelectListItem>()
            {
                new SelectListItem(){Text="Daily", Value="Daily",Selected=model.SentType=="Daily"},
                new SelectListItem(){Text="Weekly", Value="Weekly",Selected=model.SentType=="Weekly"},
                new SelectListItem(){Text="Monthly", Value="Monthly",Selected=model.SentType=="Monthly"}
            };

            model.Products = new List<NewsletterProductModel>();
            List<Product> products = ProductRepository.GetAllProducts();
            string[] ids = { };
            if (!string.IsNullOrEmpty(model.ProductIds))
            {
                 ids= model.ProductIds.Split(',');
            }
            
            if (products != null && products.Any())
            {
                foreach (Product prod in products)
                {
                    NewsletterProductModel npm = new NewsletterProductModel()
                    {
                        IsChecked = ids.Contains(prod.Id.ToString()),
                        ProductId = prod.Id,
                        ProductName = prod.Name
                    };
                    string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority;
                    if (prod.Pictures.Any())
                    {
                        var defpic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                        if (defpic == null)
                            defpic = prod.Pictures.FirstOrDefault();
                        npm.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defpic.Picture;
                    }
                    else
                    {
                        npm.Picture = uri + "/images/assets/not_available-generic.png";
                    }
                    model.Products.Add(npm);
                }
            }
            
            return View(model);
        }

        public ActionResult ManageOrder()
        {

            ViewData["Status"] = GetOrderStatus(true);
            ManageOrderModel model = new ManageOrderModel() { Orders=new List<OrderModel>()};
            List<Order> orders = OrderRepository.GetAllOrders();
            Setting setting = SettingRepository.GetSetting();
            decimal taxRate = 0;
            if (setting != null)
                taxRate = setting.TaxRate;
            if (orders != null && orders.Any())
            {
                foreach (Order order in orders)
                {
                    OrderModel om = new OrderModel()
                    {
                        Id = order.Id,
                        Total = Decimal.Round(order.Total, 2),
                        ShippingAddress = order.ShippingAddress,
                        Status = order.Status.ToString(),
                        DeliveryFee = Decimal.Round(order.DeliveryFee, 2),
                        OrderNo = ConstantHelper.GetOrderNumber(order.Id),
                        Items = new List<OrderItemModel>(),
                        NewAddress = new ShippingAddressModel(),
                        Guid = Guid.NewGuid().ToString(),
                        PaymentMethod = order.PaymentMethod,
                        DeliveryMethod = order.DeliveryMethod,
                        CreatedBy=order.CreatedBy,
                        CreatedDate=order.CreatedDate,
                        TaxRate=taxRate
                    };
                    string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority ;
                    foreach (var item in order.Items)
                    {
                        if (item.IsHamper)
                        {
                            OrderItemModel oim = new OrderItemModel()
                            {
                                ProductId = item.ProductSet.Id,
                                ProductName = item.ProductSet.Name,
                                Price = Decimal.Round(item.UnitPrice, 2),
                                Quantity = item.Quantity,
                                SubTotal = Decimal.Round(item.SubTotal, 2),
                                Weight = item.UnitWeight
                            };

                            if (item.ProductSet.Pictures != null && item.ProductSet.Pictures.Any())
                            {

                                ProductSetPicture defPic = item.ProductSet.Pictures.FirstOrDefault(p => p.IsDefault);
                                if (defPic == null)
                                    defPic = item.ProductSet.Pictures.FirstOrDefault();
                                oim.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defPic.Picture;

                            }
                            else
                            {
                                oim.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                            }
                            om.Items.Add(oim);
                        }
                        else
                        {
                            var orderitem = new OrderItemModel()
                            {
                                Inventory = item.Product.Quantity,
                                ProductId = item.Product.Id,
                                Price = Decimal.Round(item.UnitPrice, 2),
                                ProductName = item.Product.Name,
                                Quantity = item.Quantity,
                                SubTotal = Decimal.Round(item.SubTotal, 2),
                                Weight = item.UnitWeight
                            };
                            if (item.Product.Pictures.Any())
                            {
                                var defpic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                                if (defpic == null)
                                    defpic = item.Product.Pictures.FirstOrDefault();
                                orderitem.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defpic.Picture;
                            }
                            else
                            {
                                orderitem.Picture = uri + "/images/assets/not_available-generic.png";
                            }
                            om.Items.Add(orderitem);
                        }
                    }
                    model.Orders.Add(om);
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageOrder(ManageOrderModel input)
        {
            List<SelectListItem> items = GetOrderStatus(true);
            ViewData["Status"] = items;
            items.FirstOrDefault(item => item.Value == input.Status).Selected = true;
            ManageOrderModel model = new ManageOrderModel() { Orders = new List<OrderModel>() };
            List<Order> orders = null;
            List<Order> allorders = OrderRepository.GetAllOrders();
            if (!string.IsNullOrEmpty(input.OrderNo))
            {
                var result = allorders.Where(o => ConstantHelper.GetOrderNumber(o.Id) == input.OrderNo);
                if (result != null)
                    orders = result.ToList();
            }
            else
            {
                orders = allorders;
            }

            if (input.Status != "All"&&orders!=null)
            {
                var result = orders.Where(o => o.Status.ToString() == input.Status);
                if (result != null)
                    orders = result.ToList();
            }
           
            if (orders != null && orders.Any())
            {
                Setting setting = SettingRepository.GetSetting();
                decimal taxRate = 0;
                if (setting != null)
                    taxRate = setting.TaxRate;

                foreach (Order order in orders)
                {
                    OrderModel om = new OrderModel()
                    {
                        Id = order.Id,
                        Total = Decimal.Round(order.Total, 2),
                        ShippingAddress = order.ShippingAddress,
                        Status = order.Status.ToString(),
                        DeliveryFee = Decimal.Round(order.DeliveryFee, 2),
                        OrderNo = ConstantHelper.GetOrderNumber(order.Id),
                        Items = new List<OrderItemModel>(),
                        NewAddress = new ShippingAddressModel(),
                        Guid = Guid.NewGuid().ToString(),
                        PaymentMethod = order.PaymentMethod,
                        DeliveryMethod = order.DeliveryMethod,
                        CreatedBy = order.CreatedBy,
                        CreatedDate = order.CreatedDate,
                        TaxRate = taxRate
                    };
                    string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority ;
                    foreach (var item in order.Items)
                    {
                        var orderitem = new OrderItemModel()
                        {
                            Inventory = item.Product.Quantity,
                            ProductId = item.Product.Id,
                            Price = Decimal.Round(item.UnitPrice, 2),
                            ProductName = item.Product.Name,
                            Quantity = item.Quantity,
                            SubTotal = Decimal.Round(item.SubTotal, 2),
                            Weight = item.UnitWeight
                        };
                        if (item.Product.Pictures.Any())
                        {
                            var defpic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                            if (defpic == null)
                                defpic = item.Product.Pictures.FirstOrDefault();
                            orderitem.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defpic.Picture;
                        }
                        else
                        {
                            orderitem.Picture = uri + "/images/assets/not_available-generic.png";
                        }
                        om.Items.Add(orderitem);
                    }
                    model.Orders.Add(om);
                }
            }
            return View(model);
        }

        public ActionResult UpdateOrder()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                {
                    Order order = OrderRepository.GetOrder(id);
                    if (order != null&&(order.Status==OrderStatus.Paid||order.Status==OrderStatus.Sent||order.Status==OrderStatus.Preparing))
                    {
                        UpdateOrderModel uom = new UpdateOrderModel()
                        {
                            Id=order.Id,
                            CurrentStatus=order.Status.ToString(),
                            OrderNo=ConstantHelper.GetOrderNumber(id),
                            ShippingNo=order.ShippingNo
                        };
                        OrderModel model = new OrderModel()
                        {
                            Id = order.Id,
                            Total = Decimal.Round(order.Total, 2),
                            ShippingAddress = order.ShippingAddress,
                            Status = order.Status.ToString(),
                            DeliveryFee = Decimal.Round(order.DeliveryFee, 2),
                            OrderNo = ConstantHelper.GetOrderNumber(id),
                            Items = new List<OrderItemModel>(),
                            NewAddress = new ShippingAddressModel(),
                            Guid = Guid.NewGuid().ToString(),
                            PaymentMethod = order.PaymentMethod,
                            DeliveryMethod = order.DeliveryMethod
                        };
                        uom.Order = model;
                        Setting setting = SettingRepository.GetSetting();
                        if (setting != null)
                        {
                            model.TaxRate = setting.TaxRate;
                        }
                        string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        foreach (var item in order.Items)
                        {
                            var orderitem = new OrderItemModel()
                            {
                                Inventory = item.Product.Quantity,
                                ProductId = item.Product.Id,
                                Price = Decimal.Round(item.UnitPrice, 2),
                                ProductName = item.Product.Name,
                                Quantity = item.Quantity,
                                SubTotal = Decimal.Round(item.SubTotal, 2),
                                Weight = item.UnitWeight
                            };
                            if (item.Product.Pictures.Any())
                            {
                                var defpic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                                if (defpic == null)
                                    defpic = item.Product.Pictures.FirstOrDefault();
                                orderitem.Picture = uri + "/" + defpic.Picture;
                            }
                            else
                            {
                                orderitem.Picture = uri + "/images/assets/not_available-generic.png";
                            }
                            model.Items.Add(orderitem);
                        }
                        switch (order.Status)
                        {
                            case OrderStatus.Paid:
                                ViewData["NewStatus"] = new List<SelectListItem>() 
                                { 
                                    new SelectListItem(){Text="Preparing",Value="Preparing",Selected=true}
                                };
                                break;
                            case OrderStatus.Preparing:
                                ViewData["NewStatus"] = new List<SelectListItem>() 
                                { 
                                    new SelectListItem(){Text="Sent",Value="Sent",Selected=true}
                                };
                                break;
                            case OrderStatus.Sent:
                                ViewData["NewStatus"] = new List<SelectListItem>() 
                                { 
                                    new SelectListItem(){Text="Delivered",Value="Delivered",Selected=true}
                                };
                                break;
                        }
                        return View(uom);
                    }
                }
            }
            return RedirectToAction("ManageOrder");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateOrder(UpdateOrderModel input)
        {
            if (ModelState.IsValid)
            {
                OrderHistory oh = new OrderHistory()
                {
                    OrderId = input.Id,
                    CreatedBy = new User() { UserName=User.Identity.Name},
                    NewStatus=ConvertToEnum(input.NewStatus),
                    OldStatus=ConvertToEnum(input.CurrentStatus),
                    Remark=input.Remark,
                    ShippingNo=input.ShippingNo
                };
                OrderRepository.UpdateOrderStatus(oh);
                if (input.NewStatus != input.CurrentStatus)
                {
                    Order order2 = OrderRepository.GetOrder(input.Id);
                    if (order2 != null && order2.CreatedBy != null && string.IsNullOrWhiteSpace(order2.CreatedBy.Email) == false)
                    {
                        MailManager.Send(ConstantHelper.RepliedTo, order2.CreatedBy.Email, "The status of your order is changed", "Dear customer, the status of your order " + ConstantHelper.GetOrderNumber(input.Id) + " is changed to " + input.NewStatus, true);
                    }
                }
                return RedirectToAction("ManageOrder");
            }
            ViewData["NewStatus"] = new List<SelectListItem>();
            Order order = OrderRepository.GetOrder(input.Id);
            if (order != null && (order.Status == OrderStatus.Paid || order.Status == OrderStatus.Sent || order.Status == OrderStatus.Preparing))
            {
                OrderModel model = new OrderModel()
                {
                    Id = order.Id,
                    Total = Decimal.Round(order.Total, 2),
                    ShippingAddress = order.ShippingAddress,
                    Status = order.Status.ToString(),
                    DeliveryFee = Decimal.Round(order.DeliveryFee, 2),
                    OrderNo = ConstantHelper.GetOrderNumber(order.Id),
                    Items = new List<OrderItemModel>(),
                    NewAddress = new ShippingAddressModel(),
                    Guid = Guid.NewGuid().ToString(),
                    PaymentMethod = order.PaymentMethod,
                    DeliveryMethod = order.DeliveryMethod
                };
                input.Order = model;
                Setting setting = SettingRepository.GetSetting();
                if (setting != null)
                {
                    model.TaxRate = setting.TaxRate;
                }
                string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                foreach (var item in order.Items)
                {
                    var orderitem = new OrderItemModel()
                    {
                        Inventory = item.Product.Quantity,
                        ProductId = item.Product.Id,
                        Price = Decimal.Round(item.UnitPrice, 2),
                        ProductName = item.Product.Name,
                        Quantity = item.Quantity,
                        SubTotal = Decimal.Round(item.SubTotal, 2),
                        Weight = item.UnitWeight
                    };
                    if (item.Product.Pictures.Any())
                    {
                        var defpic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                        if (defpic == null)
                            defpic = item.Product.Pictures.FirstOrDefault();
                        orderitem.Picture = uri + "/" + defpic.Picture;
                    }
                    else
                    {
                        orderitem.Picture = uri + "/images/assets/not_available-generic.png";
                    }
                    model.Items.Add(orderitem);
                }
                switch (order.Status)
                {
                    case OrderStatus.Paid:
                        ViewData["NewStatus"] = new List<SelectListItem>() 
                                { 
                                    new SelectListItem(){Text="Preparing",Value="Preparing",Selected=true}
                                };
                        break;
                    case OrderStatus.Preparing:
                        ViewData["NewStatus"] = new List<SelectListItem>() 
                                { 
                                    new SelectListItem(){Text="Sent",Value="Sent",Selected=true}
                                };
                        break;
                    case OrderStatus.Sent:
                        ViewData["NewStatus"] = new List<SelectListItem>() 
                                { 
                                    new SelectListItem(){Text="Delivered",Value="Delivered",Selected=true}
                                };
                        break;
                }
            }
            return View(input);
        }

        public ActionResult OrderDetail()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                {
                    Order order = OrderRepository.GetOrder(id);
                    if (order != null )
                    {
                        UpdateOrderModel uom = new UpdateOrderModel()
                        {
                            Id = order.Id,
                            CurrentStatus = order.Status.ToString(),
                            OrderNo = ConstantHelper.GetOrderNumber(id),
                            ShippingNo = order.ShippingNo
                        };
                        OrderModel model = new OrderModel()
                        {
                            Id = order.Id,
                            Total = Decimal.Round(order.Total, 2),
                            ShippingAddress = order.ShippingAddress,
                            Status = order.Status.ToString(),
                            DeliveryFee = Decimal.Round(order.DeliveryFee, 2),
                            OrderNo = ConstantHelper.GetOrderNumber(id),
                            Items = new List<OrderItemModel>(),
                            NewAddress = new ShippingAddressModel(),
                            Guid = Guid.NewGuid().ToString(),
                            PaymentMethod = order.PaymentMethod,
                            DeliveryMethod = order.DeliveryMethod
                        };
                        uom.Order = model;
                        Setting setting = SettingRepository.GetSetting();
                        if (setting != null)
                        {
                            model.TaxRate = setting.TaxRate;
                        }
                        string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority;
                        foreach (var item in order.Items)
                        {
                            if (item.IsHamper)
                            {
                                OrderItemModel oim = new OrderItemModel()
                                {
                                    ProductId = item.ProductSet.Id,
                                    ProductName = item.ProductSet.Name,
                                    Price = Decimal.Round(item.UnitPrice, 2),
                                    Quantity = item.Quantity,
                                    SubTotal = Decimal.Round(item.SubTotal, 2),
                                    Weight = item.UnitWeight
                                };

                                if (item.ProductSet.Pictures != null && item.ProductSet.Pictures.Any())
                                {

                                    ProductSetPicture defPic = item.ProductSet.Pictures.FirstOrDefault(p => p.IsDefault);
                                    if (defPic == null)
                                        defPic = item.ProductSet.Pictures.FirstOrDefault();
                                    oim.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defPic.Picture;

                                }
                                else
                                {
                                    oim.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                                }
                                model.Items.Add(oim);
                            }
                            else
                            {
                                var orderitem = new OrderItemModel()
                                {
                                    Inventory = item.Product.Quantity,
                                    ProductId = item.Product.Id,
                                    Price = Decimal.Round(item.UnitPrice, 2),
                                    ProductName = item.Product.Name,
                                    Quantity = item.Quantity,
                                    SubTotal = Decimal.Round(item.SubTotal, 2),
                                    Weight = item.UnitWeight
                                };
                                if (item.Product.Pictures.Any())
                                {
                                    var defpic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                                    if (defpic == null)
                                        defpic = item.Product.Pictures.FirstOrDefault();
                                    orderitem.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defpic.Picture;
                                }
                                else
                                {
                                    orderitem.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                                }
                                model.Items.Add(orderitem);
                            }
                        }
                        
                        return View(uom);
                    }
                }
            }
            return RedirectToAction("ManageOrder");
        }

        public ActionResult ManageSale()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                {
                    Product product = ProductRepository.GetProductById(id);
                    if (product != null)
                    {
                        ManageSaleModel model = new ManageSaleModel()
                        {
                            ProductId=product.Id,
                            ProductName=product.Name,
                            Id=0,
                            ByValue=true,
                            ProductPrice=Decimal.Round(product.Price,2)
                        };
                        List<ProductDiscount> list = SaleRepository.Get(product.Id);
                        model.Discounts = list;
                        if (model.Discounts == null)
                            model.Discounts = new List<ProductDiscount>();
                        return View(model);
                    }
                }
            }
            return RedirectToAction("ManageProduct");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageSale(ManageSaleModel model)
        {
            DateTime fromdate = DateTime.Now;
            if (!DateTime.TryParseExact(model.FromDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromdate))
            {
                ModelState.AddModelError("FromDate", "Please input a valid from date in format: MM/dd/yyyy");
            }
            DateTime enddate = DateTime.Now;
            if (!DateTime.TryParseExact(model.EndDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out enddate))
            {
                ModelState.AddModelError("EndDate", "Please input a valid end date in format: MM/dd/yyyy");
            }
            if (model.Discount <= 0||model.Discount>model.ProductPrice)
            {
                ModelState.AddModelError("Discount", "Discount value should be larger than zero and less than product price");
            }
            if (fromdate > enddate)
            {
                ModelState.AddModelError("FromDate", "From date should be less than end date");
            }
            List<ProductDiscount> list = SaleRepository.Get(model.ProductId);
            model.Discounts = list;
            if (model.Discounts == null)
                model.Discounts = new List<ProductDiscount>();
            if (ModelState.IsValid)
            {
                if (list != null && list.Any(l => (l.FromDate >= fromdate && l.FromDate <= enddate) || (fromdate >= l.FromDate && fromdate <= l.EndDate)))
                {
                    ModelState.AddModelError("FromDate", "There is already sales setting for the period. Please chooose a different from date and end date.");
                    return View(model);
                }
                ProductDiscount pd = new ProductDiscount() { ByValue = true, CreatedBy = new User() { UserName = User.Identity.Name }, LastModifiedBy = new User() { UserName = User.Identity.Name },Discount=model.Discount,FromDate=fromdate,EndDate=enddate,ProductId=model.ProductId,Id=0,Description="" };
                SaleRepository.Create(pd);
                return RedirectToAction("ManageProduct");
            }
            
            
            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteSale()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                {
                    SaleRepository.Delete(id, User.Identity.Name);
                    if (Request.QueryString["pid"] != null)
                    {
                        return RedirectToAction("ManageSale", new { id = Request.QueryString["pid"] });
                    }
                }
            }
            return RedirectToAction("ManageProduct");
        }

        public ActionResult ManageQT()
        {
            ManageQTModel model = new ManageQTModel();
            model.Types = QuestionRepository.GetTypes();
            if (model.Types == null)
                model.Types = new List<QuestionType>();
            return View(model);
        }

        public ActionResult EditQT()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) )
                {
                    QuestionTypeModel model = new QuestionTypeModel() { Id=id};
                    QuestionType qt=QuestionRepository.GetQuestionType(id);
                    if (id == 0 || (id > 0 && qt != null))
                    {
                        model.Name = qt == null ? "" : qt.Name;
                        return View(model);
                    }
                }
            }

            return RedirectToAction("ManageQT");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditQT(QuestionTypeModel model)
        {
            if (ModelState.IsValid)
            {
                List<QuestionType> list = QuestionRepository.GetTypes();
                if (list != null && list.Any(l => l.Id != model.Id && l.Name.Equals(model.Name, StringComparison.InvariantCultureIgnoreCase)))
                {
                    ModelState.AddModelError("Name", "Name already exists!");
                    return View(model);
                }
                QuestionType qt = new QuestionType() { Id=model.Id,Name=model.Name};
                QuestionRepository.AddType(qt);
                return RedirectToAction("ManageQT");
            }

            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteQT()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    QuestionRepository.DeleteType(id);
                }
            }
            return RedirectToAction("ManageQT");
        }

        public ActionResult ManageQuestion()
        {
            ManageQuestionModel model = new ManageQuestionModel();
            model.Questions = QuestionRepository.GetQuestions();
            return View(model);
        }

        public ActionResult EditQuestion()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id)&&id>0)
                {
                    
                    Question question = QuestionRepository.GetQuestion(id);
                    if (question!=null)
                    {
                        QuestionModel model = new QuestionModel() 
                        { 
                            Id = id,
                            Title=question.Title,
                            Description=question.Description,
                            Email=question.Email,
                            QTType=question.QuestionType.Name
                        };
                        return View(model);
                    }
                }
            }
            return RedirectToAction("ManageQuestion");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditQuestion(QuestionModel model)
        {
            if (ModelState.IsValid)
            {
                QuestionRepository.AddQuestion(new Question() { Id = model.Id, Answer = model.Answer, LastModifiedBy = new User() { UserName = User.Identity.Name } });
                string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/Home/FAQDetail/" + model.Id.ToString();
                MailManager.Send(ConstantHelper.RepliedTo, model.Email, "Your question is replied", "Hello,<br/> Your question --" + model.Title + " -- has been replied. Please click below link or copy & paste it in your browser to view the answer: <br/>" + url + "<br/> Best Regards,<br/>TotoBobo", true);
                return RedirectToAction("ManageQuestion");
            }
            return View(model);
        }

        public ActionResult ManageHamper()
        {
            ManageHamperModel model = new ManageHamperModel();
            model.ProductSets = ProductRepository.GetHampers();
            
            return View(model);
        }

        public ActionResult EditHamper()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {

                    string guid = Guid.NewGuid().ToString();
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority+"/"+uploadfolder;
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), guid);
                    Directory.CreateDirectory(directory);
                    if (id > 0)
                    {
                        ProductSet product = ProductRepository.GetHamper(id);
                        if (product != null)
                        {
                            
                            EditHamperModel model = new EditHamperModel()
                            {
                                Id = product.Id,
                                Description = product.Description,
                                Name = product.Name,
                                Price = Decimal.Round(product.Price, 2),
                                Pictures = product.Pictures,
                                GUID = guid,
                                OptionalCount=product.OptionalCount,
                                Products = ProductRepository.GetAllProducts(),
                                Items=product.Items
                            };

                            if (model.Items != null)
                            {
                                if (model.Items.Any(m => m.IsMandatory))
                                {
                                    model.MandatoryIds = string.Join(",", model.Items.Where(m => m.IsMandatory).Select(i => i.ProductId));
                                }
                                else
                                {
                                    model.MandatoryIds = "";
                                }

                                if (model.Items.Any(m => !m.IsMandatory))
                                {
                                    model.OptionalIds = string.Join(",", model.Items.Where(m => !m.IsMandatory).Select(i => i.ProductId));
                                }
                                else
                                {
                                    model.OptionalIds = "";
                                }
                            }
                            else
                            {
                                model.Items = new List<ProductSetItem>();
                                model.OptionalIds = "";
                                model.MandatoryIds = "";
                            }
                            
                            if (product.Pictures != null)
                            {
                                foreach (ProductSetPicture pp in product.Pictures)
                                {
                                    if (System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), pp.Picture)))
                                    {
                                        if (pp.IsDefault)
                                        {
                                            string extension = Path.GetExtension(pp.Picture);
                                            string name = pp.Picture.Replace(extension, "-default") + extension;
                                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), pp.Picture), Path.Combine(directory, name), true);
                                        }
                                        else
                                        {
                                            Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), pp.Picture), Path.Combine(directory, pp.Picture), true);
                                        }
                                    }
                                }
                            }
                            
                            ViewBag.SubTitle = "Edit Hamper";
                            return View(model);
                        }
                    }
                    else
                    {
                        ViewBag.SubTitle = "Add Hamper";
                        return View(new EditHamperModel()
                        {
                            Id = 0,
                            Pictures = new List<ProductSetPicture>(),
                            GUID = guid,
                            Products = ProductRepository.GetAllProducts(),
                            Items=new List<ProductSetItem>(),
                            OptionalIds="",
                            MandatoryIds=""
                        });
                    }

                }
            }
            return RedirectToAction("ManageHamper");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditHamper(EditHamperModel model)
        {
           
            ViewBag.SubTitle = "Add Hamper";
            //model.TagXml = "<Tags><ProductTag><Key>Size</Key><Values><ProductTagValue><Value>XL</Value></ProductTagValue><ProductTagValue><Value>L</Value></ProductTagValue></Values></ProductTag></Tags>";
            model.Products = ProductRepository.GetAllProducts();
            model.Items = new List<ProductSetItem>();
            if (!string.IsNullOrEmpty(model.MandatoryIds))
            {
                foreach (string ids in model.MandatoryIds.Split(','))
                {
                    int id = 0;
                    if (int.TryParse(ids, out id) && id > 0&&model.Products.Any(p=>p.Id==id))
                    {
                        model.Items.Add(new ProductSetItem() { ProductId=id,ProductSetId=model.Id,IsMandatory=true});
                    }
                }
            }
            if (!string.IsNullOrEmpty(model.OptionalIds))
            {
                foreach (string ids in model.OptionalIds.Split(','))
                {
                    int id = 0;
                    if (int.TryParse(ids, out id) && id > 0 && model.Products.Any(p => p.Id == id))
                    {
                        model.Items.Add(new ProductSetItem() { ProductId = id, ProductSetId = model.Id, IsMandatory = false });
                    }
                }
            }
            if (ModelState.IsValid)
            {
                if (RouteData.Values.Any(v => v.Key == "id"))
                {
                    int id = 0;
                    if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                    {
                        ProductSet eprod = ProductRepository.GetHamper(model.Name);
                        if (eprod != null && eprod.Id != model.Id)
                        {
                            ModelState.AddModelError("Name", "Name already exists, please input another one");
                        }
                        else
                        {
                            if (id > 0)
                            {
                                ViewBag.SubTitle = "Edit Hamper";
                            }
                            if (id == model.Id)
                            {
                                ProductSet p = new ProductSet()
                                {
                                    Id = model.Id,
                                    Name = model.Name,
                                    Description = model.Description,
                                    Price = model.Price,
                                    Pictures = new List<ProductSetPicture>(),
                                    CreatedBy = new User() { UserName = User.Identity.Name },
                                    LastModifiedBy = new User() { UserName = User.Identity.Name },
                                    Items=model.Items,
                                    OptionalCount=model.OptionalCount,
                                };

                                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                                DirectoryInfo di = new DirectoryInfo(directory);
                                if (model.Id > 0)
                                {
                                    ProductSet product = ProductRepository.GetHamper(model.Id);
                                    if (product != null && product.Pictures != null)
                                    {
                                        foreach (ProductSetPicture pp in product.Pictures)
                                        {
                                            Picture.Delete(Path.Combine(Server.MapPath("~/" + uploadfolder), pp.Picture));
                                        }
                                    }
                                }
                                foreach (FileInfo fi in di.EnumerateFiles())
                                {
                                    if (fi.Name.Contains("-default"))
                                    {
                                        p.Pictures.Add(new ProductSetPicture() { ProductSetId = model.Id, Picture = fi.Name.Replace("-default", ""), IsDefault = true });
                                        Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name.Replace("-default", "")), true);
                                    }
                                    else
                                    {
                                        p.Pictures.Add(new ProductSetPicture() { ProductSetId = model.Id, Picture = fi.Name, IsDefault = false });
                                        Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name), true);
                                    }
                                }
                                Picture.DeleteDirectory(directory, true);
                                ProductRepository.UpdateHamper(p);
                                return RedirectToAction("ManageHamper");

                            }
                            else
                            {
                                ModelState.AddModelError("", "Form is injected!");
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Form is injected!");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Form is injected!");
                }

            }

           
            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteHamper()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    ProductRepository.DeleteHamper(id);
                }
            }
            return RedirectToAction("ManageHamper");
        }

        public ActionResult ManageDoctor()
        {
            return View(new ManageDoctorModel() { Doctors=DoctorRepository.GetDoctors()});
        }

        public ActionResult EditDoctor()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        Doctor doctor = DoctorRepository.GetDoctor(id);                        
                        if (doctor != null)
                        {
                            User doctorUser = UserRepository.GetUserById(doctor.UserId);

                            string guid = Guid.NewGuid().ToString();
                            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                            string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), guid);
                            Directory.CreateDirectory(directory);
                            EditDoctorModel model = new EditDoctorModel()
                            {
                                Id = doctor.Id,
                                Name=doctor.Name,
                                CompanyName=doctor.CompanyName,
                                GUID=guid,
                                Address=doctor.Address,
                                Title=doctor.Title,
                                Url=doctor.Url,
                                Email = doctorUser == null ? "" : doctorUser.Email,
                            };
                            
                            if (System.IO.File.Exists(Path.Combine(Server.MapPath("~/" + uploadfolder), doctor.Avator)))
                            {
                                Picture.Copy(Path.Combine(Server.MapPath("~/" + uploadfolder), doctor.Avator), Path.Combine(directory, doctor.Avator), true);
                            }
                                
                            ViewBag.SubTitle = "Edit Doctor";

                            return View(model);
                        }
                    }
                    else
                    {
                        ViewBag.SubTitle = "Add Doctor";
                        string guid = Guid.NewGuid().ToString();
                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), guid);
                        Directory.CreateDirectory(directory);
                        return View(new EditDoctorModel()
                        {
                            Id = 0,
                            GUID = guid
                        });
                    }

                }
            }
            return RedirectToAction("ManageDoctor");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDoctor(EditDoctorModel model)
        {
            ViewBag.SubTitle = model.Id>0? "Edit Doctor":"Add Doctor";
            
            if ((model.Id==0&&ModelState.IsValid)||(model.Id>0))
            {
                try
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                    DirectoryInfo di = new DirectoryInfo(directory);
                    FileInfo[] files = di.GetFiles();

                    if (files == null || !files.Any())
                    {
                        ModelState.AddModelError("Avator", "Avator is required");
                        return View(model);
                    }
                    else
                    {
                        int userId=0;
                        if (model.Id == 0)
                        {
                            WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { Email = model.Email }, false);
                            SimpleRoleProvider provider = Roles.Provider as SimpleRoleProvider;
                            if (provider != null)
                            {
                                provider.AddUsersToRoles(new[] { model.UserName }, new[] { ConstantHelper.QAAdminRole });
                            }
                            userId = WebSecurity.GetUserId(model.UserName);
                        }
                        else
                        {
                            bool nameValid = ModelState["Name"].Errors == null || !ModelState["Name"].Errors.Any();
                            bool cnValid = ModelState["CompanyName"].Errors == null || !ModelState["CompanyName"].Errors.Any();
                            bool addressValid = ModelState["Address"].Errors == null || !ModelState["Address"].Errors.Any();
                            bool urlValid = ModelState["Url"].Errors == null || !ModelState["Url"].Errors.Any();
                            bool titleValid = ModelState["Title"].Errors == null || !ModelState["Title"].Errors.Any();
                            if (!nameValid || !cnValid|| !addressValid || !urlValid || !titleValid)
                            {
                                return View(model);
                            }
                        }
                        string avator = string.Empty;
                        foreach (FileInfo fi in di.EnumerateFiles())
                        {
                            Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name), true);
                            avator = fi.Name;
                        }
                        Picture.DeleteDirectory(directory, true);
                        Doctor doctor = new Doctor()
                        {
                            Id = model.Id,
                            Name = model.Name,
                            CompanyName = model.CompanyName,
                            Address = model.Address,
                            Title = model.Title,
                            Url = model.Url,
                            UserId = userId,
                            Avator=avator
                        };
                        DoctorRepository.Update(doctor);

                        if (model.Id > 0)
                        {
                            Doctor d = DoctorRepository.GetDoctor(doctor.Id);
                            UserRepository.UpdateEmail(new User() { UserId = d.UserId, Email = model.Email });
                        }

                        return RedirectToAction("ManageDoctor");
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
            return View(model);
        }

        public ActionResult CancelDoctor()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManageDoctor");
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteDoctor()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    DoctorRepository.Delete(id);
                }
            }
            return RedirectToAction("ManageDoctor");
        }

        public ActionResult ManageCountry()
        {
            return View(new ManageCountryModel() { Countries=MasterDataRepository.GetCountries()});
        }

        public ActionResult EditCountry()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        Country country = MasterDataRepository.GetCountry(id);
                        if (country != null)
                        {
                            ViewBag.SubTitle = "Edit Country";
                            EditCountryModel model = new EditCountryModel() { Id=id,Name=country.Name,GUID=Guid.NewGuid().ToString(),Picture=country.Picture};
                            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                            string directory = Server.MapPath("~/" + uploadfolder);
                            string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                            Directory.CreateDirectory(todirectory);
                            if (!string.IsNullOrEmpty(model.Picture))
                            {
                                string name = model.Picture;
                                string path = Path.Combine(directory, name);
                                if (System.IO.File.Exists(path))
                                {
                                    Picture.Copy(path, Path.Combine(todirectory, name), true);
                                }
                            }
                            return View(model);
                        }
                    }
                    else
                    {
                        ViewBag.SubTitle = "Add Country";
                        EditCountryModel model = new EditCountryModel() { Id = 0, Name = "",GUID=Guid.NewGuid().ToString() };
                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        string todirectory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                        Directory.CreateDirectory(todirectory);
                        return View(model);
                    }
                }
            }
            return RedirectToAction("ManageCountry");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCountry(EditCountryModel model)
        {
            if (ModelState.IsValid)
            {
                Country cty = MasterDataRepository.GetCountry(model.Name);
                if (cty == null || cty.Id == model.Id)
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), model.GUID);
                    DirectoryInfo di = new DirectoryInfo(directory);
                    FileInfo[] files = di.GetFiles();

                    if (files == null || !files.Any())
                    {
                        ModelState.AddModelError("Picture", "Picture is required");
                    }
                    else
                    {
                        foreach (FileInfo fi in di.EnumerateFiles())
                        {
                            Picture.Copy(Path.Combine(directory, fi.Name), Path.Combine(Server.MapPath("~/" + uploadfolder), fi.Name), true);
                            model.Picture = fi.Name;
                        }
                        Picture.DeleteDirectory(directory, true);
                        Country country = new Country() { Id = model.Id, Name = model.Name, IsEnabled = false, Picture=model.Picture };
                        MasterDataRepository.UpdateCountry(country);
                        return RedirectToAction("ManageCountry");
                    }
                }
                else
                {
                    ModelState.AddModelError("Name", "Name already exists");
                }
            }
            return View(model);
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCountry()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    MasterDataRepository.DeleteCountry(id);
                }
            }
            return RedirectToAction("ManageCountry");
        }

        public ActionResult CancelCountry()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                string directory = Path.Combine(Server.MapPath("~/" + uploadfolder), RouteData.Values["id"].ToString());
                Picture.DeleteDirectory(directory, true);
            }
            return RedirectToAction("ManageCountry");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Hide()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        ProductRepository.Hide(id, true);
                    }
                }
            }
            return RedirectToAction("ManageProduct");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Unhide()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        ProductRepository.Hide(id, false);
                    }
                }
            }
            return RedirectToAction("ManageProduct");
        }

        #region helper methods

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public List<SelectListItem> GetOrderStatus(bool includeAll)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (includeAll)
            {
                items.Add(new SelectListItem() { Text = "All", Value = "All" });
            }
            items.Add(new SelectListItem() { Text = "Created", Value = "Created" });
            items.Add(new SelectListItem() { Text = "Paid", Value = "Paid" });
            items.Add(new SelectListItem() { Text = "Cancelled", Value = "Cancelled" });
            items.Add(new SelectListItem() { Text = "Preparing", Value = "Preparing" });
            items.Add(new SelectListItem() { Text = "Sent", Value = "Sent" });
            items.Add(new SelectListItem() { Text = "Delivered", Value = "Delivered" });
            return items;
        }

        public OrderStatus ConvertToEnum(string status)
        {
            OrderStatus os = OrderStatus.Created;
            switch (status)
            {
                case "Paid":
                    os = OrderStatus.Paid;
                    break;
                case "Sent":
                    os = OrderStatus.Sent;
                    break;
                case "Delivered":
                    os = OrderStatus.Delivered;
                    break;
                case "Created":
                    os = OrderStatus.Created;
                    break;
                case "Cancelled":
                    os = OrderStatus.Cancelled;
                    break;
                case "Preparing":
                    os = OrderStatus.Preparing;
                    break;
            }
            return os;
        }
        #endregion

        #region reports
        public ActionResult GetOrderStatusChart()
        {
            return Json(ReportRepository.GetOrderStatusChart(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBestSellerChart()
        {
            return Json(ReportRepository.GetBestSellerChart(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOrderStatisChart()
        {
            List<Order> orders = OrderRepository.GetAllOrders();
            List<PieChart> charts = new List<PieChart>();
            string[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
            Dictionary<string, decimal> data = new Dictionary<string, decimal>();
            foreach (string month in monthNames)
            {
                data.Add(month, 0);
            }
            if(orders!=null&&orders.Any(o=>o.Status!=OrderStatus.Created&&o.Status!=OrderStatus.Cancelled))
            {
                foreach (Order order in orders.Where(o=>o.Status!=OrderStatus.Created&&o.Status!=OrderStatus.Cancelled))
                {
                    data[monthNames[order.CreatedDate.Month - 1]] += order.Total;
                }
            }

            foreach (string key in data.Keys)
            {
                charts.Add(new PieChart() { label=key,data=Decimal.Round(data[key],2).ToString()});
            }

            return Json(charts, JsonRequestBehavior.AllowGet);
        }
        #endregion


    }

    #region helper class
    public class Picture
    {
        public string name { get; set; }
        public string uuid { get; set; }
        public string thumbnailUrl { get; set; }
        public long size { get; set; }

        public static void Delete(string file)
        {
            if (System.IO.File.Exists(file))
            {
                try
                {
                    System.IO.File.Delete(file);
                }
                catch { 
                    //log the exception later
                }
            }
        }

        public static void DeleteDirectory(string directory, bool recuive)
        {
            try
            {
                new DirectoryInfo(directory).Delete(recuive);
            }
            catch
            {
                //log the exception later
            }
        }

        public static void Copy(string from, string to, bool overwrite)
        {
            try
            {
                System.IO.File.Copy(from, to, overwrite);
            }
            catch
            {
                //log the exception later
            }
        }

    }

    public class PictureTitle
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class RelatedProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsRelated { get; set; }

        public static List<RelatedProduct> GetRelates(int id,List<Product> relates)
        {
            
            List<RelatedProduct> list = new List<RelatedProduct>();
            List<Product> products = ProductRepository.GetAllProducts();
            if (products != null)
            {
                foreach (Product p in products)
                {
                    if (p.Id != id)
                    {
                        list.Add(new RelatedProduct() { Id=p.Id,Name=p.Name,IsRelated=relates!=null&&relates.Any(r=>r.Id==p.Id)});
                    }
                }
            }
            return list;
        }
    }

    public class Accessory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAccessory { get; set; }

        public static List<Accessory> GetAccessories(int id, List<Product> relates)
        {

            List<Accessory> list = new List<Accessory>();
            List<Product> products = ProductRepository.GetAllProducts();
            if (products != null)
            {
                foreach (Product p in products)
                {
                    if (p.Id != id)
                    {
                        list.Add(new Accessory() { Id = p.Id, Name = p.Name, IsAccessory = relates != null && relates.Any(r => r.Id == p.Id) });
                    }
                }
            }
            return list;
        }
    }
    #endregion


}
