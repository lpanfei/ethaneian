﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TotoBobo.Mvc.Repository;

namespace TotoBobo.Mvc.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ValidationController : Controller
    {
        //
        // GET: /IsEmailAvailable/

        public JsonResult IsEmailAvailable(string email)
        {
            if (UserRepository.GetUserByEmail(email)!=null)
            {
                return Json(email+" already exists", JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DoesEmailExists(string email)
        {
            if (UserRepository.GetUserByEmail(email) != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(email + " doesn't exist in our system!", JsonRequestBehavior.AllowGet);
        }

    }
}
