﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using TotoBobo.Mvc.Models;
using System.Web.Security;
using System.Web.Routing;
using TotoBobo.Mvc.Helper;
using TotoBobo.Mvc.Repository;
using TotoBobo.Data.Objects;
using System.Text;

namespace TotoBobo.Mvc.Controllers
{ 
    [Authorize]
    public class AccountController : Controller
    {

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                //if (Roles.GetRolesForUser(model.UserName).Contains(ConstantHelper.AdminRole))
                //{
                //    return RedirectToAction("Index", "Admin");
                //}
                //else
                //{
                    return RedirectToLocal(returnUrl);
                //}
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }


        [AllowAnonymous]
        public ActionResult Register(string status)
        {
            ViewBag.RegisterStatus = status;
            CaptchaImage image = new CaptchaImage();
            CaptchaRepository.Create(image.UniqueId, image.Text);
            RegisterModel model = new RegisterModel()
            {
                Guid=image.UniqueId,
                CaptchaImage = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/captcha.ashx?guid=" + image.UniqueId + "&d=" + DateTime.Now.ToString("yyyyMMddhhmmssfffffff")
            };
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult UpdateCaptcha()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                Random _rand = new Random();
                StringBuilder sb = new StringBuilder(CaptchaImage.TextLength);
                int maxLength = CaptchaImage.TextChars.Length;
                for (int n = 0; n <= CaptchaImage.TextLength - 1; n++)
                    sb.Append(CaptchaImage.TextChars.Substring(_rand.Next(maxLength), 1));
                CaptchaRepository.Create(RouteData.Values["id"].ToString(), sb.ToString());
                return Json(new { success = true, image = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/captcha.ashx?guid=" + RouteData.Values["id"].ToString() + "&d=" + DateTime.Now.ToString("yyyyMMddhhmmssfffffff") }, "text/plain");
            }
            return Json(new { success = false }, "text/plain");
        }

        [AllowAnonymous]
        public ActionResult Resend()
        {
            if (TempData["EmailAddress"] != null && TempData["Token"] != null && TempData["UserName"] != null)
            {
                string relative_url = Url.Action("Confirm", "Account", new RouteValueDictionary(new { id = TempData["Token"].ToString() }));
                string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, relative_url);
                MailManager.Send(ConstantHelper.RepliedTo, TempData["EmailAddress"].ToString(), "Account Confirmation", "Hello " + TempData["UserName"].ToString() + ",<br/> Thank you for registering on Ethan &amp Eian. Please click below link or copy & paste it in your browser to complete your registeration: <br/>" + url + "<br/> Best Regards,<br/>Ethan &amp Eian", true);
                TempData["EmailAddress"] = TempData["EmailAddress"];
                TempData["Token"] = TempData["Token"];
                TempData["UserName"] = TempData["UserName"];
                return RedirectToAction("Register", new { Status = "Resend" });
            }
            else
            {
                return RedirectToAction("Register");
            }
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            model.CaptchaImage = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/captcha.ashx?guid=" + model.Guid + "&d=" + DateTime.Now.ToString("yyyyMMddhhmmssfffffff");
            if (!string.IsNullOrEmpty(model.CaptchaText) && !CaptchaRepository.Validate(model.Guid, model.CaptchaText))
            {
                ModelState.AddModelError("CaptchaText", "Captcha is wrong, please try it again!");
            }
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    if (UserRepository.GetUserByEmail(model.Email) != null)
                    {
                        ModelState.AddModelError("email", model.Email+" already exists");
                        return View(model);
                    }

                    string token = WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { Email = model.Email }, true);
                    string relative_url = Url.Action("Confirm", "Account", new RouteValueDictionary(new { id = token }));
                    string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, relative_url);
                    MailManager.Send(ConstantHelper.RepliedTo, model.Email, "Account Confirmation", "Hello " + model.UserName + ",<br/> Thank you for registering on Ethan &amp Eian. Please click below link or copy & paste it in your browser to complete your registeration: <br/>" + url + "<br/> Best Regards,<br/>Ethan &amp Eian", true);
                    
                    TempData["EmailAddress"] = model.Email;
                    TempData["Token"] = token;
                    TempData["UserName"] = model.UserName;
                    CaptchaRepository.Delete(model.Guid);
                    return RedirectToAction("Register", new { Status = "Success" });
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Confirm()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                try
                {
                    ViewBag.ConfirmSuccess = WebSecurity.ConfirmAccount(RouteData.Values["id"].ToString());
                }
                catch { ViewBag.ConfirmSuccess = false; }
            }
            else
            {
                ViewBag.ConfirmSuccess = false;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : "";
            ViewBag.ReturnUrl = Url.Action("Manage");
            Country country = MasterDataRepository.GetEnabledCountry();
            ViewData["Country"] = new List<SelectListItem>() { new SelectListItem() { Text = country.Name, Value = country.Name, Selected = true } };
            List<SelectListItem> stateitems = new List<SelectListItem>();
            List<SelectListItem> cityitems = new List<SelectListItem>();
            List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
            ViewData["StateId"] = stateitems;
            ViewData["CityId"] = cityitems;
            string selectedcity = "";
            if (states != null)
            {
                foreach (State state in states)
                {
                    stateitems.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString() });
                }
            }
            List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
            ManageModel model = new ManageModel() { Addresses = list };

            //
            model.Questions = new List<Question>();
                        
            if (Request.IsAuthenticated && Roles.GetRolesForUser(User.Identity.Name).Contains("Admin"))
            {
                model.Questions = QuestionRepository.GetQuestions(); ;
            }
            else
            {                
                Doctor currentDoctor = null;

                List<Doctor> doctorList = DoctorRepository.GetDoctors();
                foreach (Doctor doctor in doctorList)
                {
                    if (doctor.Name == User.Identity.Name)
                    {
                        currentDoctor = doctor;
                        break;
                    }
                }

                if (currentDoctor != null)
                {
                    List<Question> questions = QuestionRepository.GetQuestions();
                    foreach (Question q in questions)
                    {
                        if (q.Doctor != null && q.Doctor.Id == currentDoctor.Id)
                        {
                            model.Questions.Add(q);
                        }
                    }
                }
            }
            //

            ViewBag.ShowAddressForm = false;
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {

                    if (id != 0)
                    {
                        UserAddress ua = list.FirstOrDefault(l => l.Id == id);
                        if (ua!=null)
                        {
                            ViewBag.ShowAddressForm = true;
                            ViewBag.CurrentAddressId = id;
                            model.CurrentAddress = new ShippingAddressModel() { 
                                FirstName=ua.FirstName,
                                LastName=ua.LastName,
                                Country=ua.Country,
                                State=ua.State,
                                City=ua.City,
                                Address1=ua.Address1,
                                Address2=ua.Address2,
                                MobileNo=ua.MobileNo,
                                TelephoneNo=ua.TelephoneNo,
                                Id=ua.Id,
                                UserId=ua.UserId,
                                IsDefault=ua.IsDefault,
                                Postcode=ua.Postcode
                            };
                            selectedcity = ua.City;
                            if (stateitems.Any(s => s.Text.Equals(ua.State, StringComparison.InvariantCultureIgnoreCase)))
                            {
                                stateitems.FirstOrDefault(s => s.Text.Equals(ua.State, StringComparison.InvariantCultureIgnoreCase)).Selected = true;
                            }
                        }
                    }
                    else
                    {
                        ViewBag.ShowAddressForm = true;
                        ViewBag.CurrentAddressId = id;
                        model.CurrentAddress = new ShippingAddressModel();
                    }
                }
            }

            if (stateitems.Any())
            {
                int id = 0;
                if (stateitems.Any(s => s.Selected))
                {
                    State state = states.FirstOrDefault(s => s.Name.Equals(stateitems.FirstOrDefault(i => i.Selected).Text));
                    id = state.Id;
                }
                else
                {
                    id = Convert.ToInt32(stateitems.FirstOrDefault().Value);
                }
                State item = MasterDataRepository.GetState(id);
                if (item != null && item.Cities != null)
                {
                    
                    item.Cities.ForEach(c => cityitems.Add(new SelectListItem(){Text=c.Name,Value=c.Id.ToString(),Selected=c.Name.Equals(selectedcity,StringComparison.InvariantCultureIgnoreCase)}));
                }
            }

            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(ShippingAddressModel model)
        {
            List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
            
            if (ModelState.IsValid)
            {
                int id = 0;
                if (RouteData.Values.Any(v => v.Key == "id"))
                {
                    if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                    {
                        int userid = WebSecurity.GetUserId(User.Identity.Name);
                        bool isDefault = false;
                        UserAddress existing = list.FirstOrDefault(l => l.Id == id);
                        if (id == 0 || (id > 0 && existing != null&&existing.UserId==userid))
                        {
                            if (existing != null)
                                isDefault = existing.IsDefault;
                            UserAddress ua = new UserAddress()
                            {
                                Postcode = model.Postcode,
                                Country = model.Country,
                                State = model.State,
                                City = model.City,
                                Address2 = model.Address2,
                                Address1 = model.Address1,
                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                MobileNo = model.MobileNo,
                                TelephoneNo = model.TelephoneNo,
                                Id=id,
                                UserId=userid,
                                IsDefault=isDefault
                            };
                            try
                            {
                                UserRepository.UpdateUserAddress(ua);
                                return RedirectToAction("Manage", new { id = "" });
                            }
                            catch (Exception ex)
                            {
                                ModelState.AddModelError("", ex.Message);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Form is distorted");
                        }
                        
                    }
                    else
                    {
                        ModelState.AddModelError("", "Form is distorted");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Form is distorted");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.ShowAddressForm = true;
            ViewBag.CurrentAddressId = Convert.ToInt32(RouteData.Values["id"]);
            Country country = MasterDataRepository.GetEnabledCountry();
            List<SelectListItem> stateitems = new List<SelectListItem>();
            List<SelectListItem> cityitems = new List<SelectListItem>();
            List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
            ViewData["StateId"] = stateitems;
            ViewData["CityId"] = cityitems;

            if (states != null)
            {
                foreach (State state in states)
                {
                    stateitems.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString(),Selected=state.Name.Equals(model.State,StringComparison.InvariantCultureIgnoreCase) });
                }
            }
            if (stateitems.Any())
            {
                int sid = 0;
                if (stateitems.Any(s => s.Selected))
                {
                    State state = states.FirstOrDefault(s => s.Name.Equals(stateitems.FirstOrDefault(i => i.Selected).Text));
                    sid = state.Id;
                }
                else
                {
                    sid = Convert.ToInt32(stateitems.FirstOrDefault().Value);
                }
                State item = MasterDataRepository.GetState(sid);
                if (item != null && item.Cities != null)
                {

                    item.Cities.ForEach(c => cityitems.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = c.Name.Equals(model.City, StringComparison.InvariantCultureIgnoreCase) }));
                }
            }
            ViewData["Country"] = new List<SelectListItem>() { new SelectListItem() { Text = country.Name, Value = country.Name, Selected = true } };
            
            //ViewData["Addresses"] = list;
            return View(new ManageModel() { Addresses=list,CurrentAddress=model});
        }

        [Authorize]
        public ActionResult MarkDefault()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
                    if (list!=null&&list.Any(l => l.Id == id))
                    {
                        UserRepository.MarkAddressAsDefault(id);
                    }
                }
            }
            return RedirectToAction("Manage", new { id = "" });
        }

        [Authorize]
        [HttpDelete]
        public ActionResult DeleteAddress()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
                    if (list != null && list.Any(l => l.Id == id))
                    {
                        UserRepository.DeleteUserAddress(new UserAddress() { Id=id});
                    }
                }
            }
            return RedirectToAction("Manage", new { id = "" });
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }


            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            ViewBag.RecoveryStatus = "";
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                Data.Objects.User user=UserRepository.GetUserByEmail(model.Email);
                if ( user== null)
                {
                    ModelState.AddModelError("email", model.Email+" doesn't exist");
                    return View(model);
                }
                string username = user.UserName;
                if (!WebSecurity.IsConfirmed(username))
                {
                    ModelState.AddModelError("", "Your account has not been confirmed yet!");
                    return View(model);
                }
                try
                {
                    string token = WebSecurity.GeneratePasswordResetToken(username);
                    string relative_url = Url.Action("ResetPassword", "Account", new RouteValueDictionary(new { id = token }));
                    string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, relative_url);
                    MailManager.Send(ConstantHelper.RepliedTo, model.Email, "Password Reset", "Hello " + username + ",<br/> You got this email because you have requested to reset your password. Please click below link or copy & paste it in your browser to complete password reset in 24 hours: <br/>" + url + "<br/> Please ignore this email if this is not your operation.<br/> Best Regards,<br/>Ethan &amp Eian", true);
                    ViewBag.RecoveryStatus = "Email has been sent to you. Please follow the steps in email to reset your password in 24 hours";
                    return View();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(model);
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                string token = RouteData.Values["id"].ToString();
                try
                {
                    int id = WebSecurity.GetUserIdFromPasswordResetToken(token);
                    if (id > 0)
                    {
                        ViewBag.ResetToken = token;
                        ViewBag.ResetStatus = "Reset";
                    }
                    else
                    {
                        ViewBag.ResetStatus = "Invalid";
                    }
                }
                catch
                {
                    ViewBag.ResetStatus = "Invalid";
                }
            }
            else
            {
                ViewBag.ResetStatus = "Invalid";
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {

            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = WebSecurity.ResetPassword(RouteData.Values["id"].ToString(), model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    ViewBag.ResetStatus = "Success";
                    return View() ;
                }
                else
                {
                    ViewBag.ResetStatus = "Reset";
                    ModelState.AddModelError("", "The token is invalid or The new password is invalid.");
                }
            }


            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
