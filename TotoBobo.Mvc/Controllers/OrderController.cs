﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TotoBobo.Mvc.Models;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Repository;
using PayPal.Api.Payments;
using PayPal;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using WebMatrix.WebData;
using TotoBobo.Mvc.Helper;

namespace TotoBobo.Mvc.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        //
        // GET: /Order/

        public ActionResult Index()
        {
            UserOrderModel model = new UserOrderModel() { Orders=new List<OrderModel>()};
            List<Order> orders = OrderRepository.GetUserOrder(User.Identity.Name);
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
            if (orders != null)
            {
                Setting setting = SettingRepository.GetSetting();
                List<Order> userorders = OrderRepository.GetUserOrder(User.Identity.Name);
                decimal lastTotal = 0;
                if (userorders.Any(o => o.Status == OrderStatus.Paid))
                {
                    lastTotal = userorders.Where(o => o.Status == OrderStatus.Paid).OrderByDescending(o => o.CreatedDate).FirstOrDefault().Total;
                }
                foreach (Order order in orders)
                {
                    OrderModel om = new OrderModel() 
                    { 
                        Id=order.Id,
                        OrderNo=ConstantHelper.GetOrderNumber(order.Id),
                        Total=Decimal.Round(order.Total,2),
                        DeliveryFee=Decimal.Round(order.DeliveryFee,2),
                        CreatedDate=order.CreatedDate,
                        ShippingAddress=order.ShippingAddress,
                        Consignee=order.ShippingAddress.FirstName+" "+order.ShippingAddress.LastName,
                        Status=order.Status.ToString(),
                        Items=new List<OrderItemModel>(),
                        LastTotal=lastTotal,
                        RecordedInDiscount=order.RepeatCustomerDiscount
                    };
                    
                    if (setting != null)
                    {
                        om.TaxRate = setting.TaxRate;
                        om.RepeatCustomerDiscount = setting.RepeatCustomerDiscount;
                    }
                    foreach (OrderItem item in order.Items)
                    {
                        if (item.IsHamper)
                        {
                            OrderItemModel oim = new OrderItemModel()
                            {
                                ProductId = item.ProductSet.Id,
                                ProductName = item.ProductSet.Name
                            };

                            if (item.ProductSet.Pictures != null && item.ProductSet.Pictures.Any())
                            {

                                ProductSetPicture defPic = item.ProductSet.Pictures.FirstOrDefault(p => p.IsDefault);
                                if (defPic == null)
                                    defPic = item.ProductSet.Pictures.FirstOrDefault();
                                oim.Picture = url + "/" + defPic.Picture;

                            }
                            else
                            {
                                oim.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                            }
                            om.Items.Add(oim);
                        }
                        else
                        {
                            OrderItemModel oim = new OrderItemModel()
                            {
                                ProductId = item.Product.Id,
                                ProductName = item.Product.Name
                            };

                            if (item.Product.Pictures != null && item.Product.Pictures.Any())
                            {

                                ProductPicture defPic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                                if (defPic == null)
                                    defPic = item.Product.Pictures.FirstOrDefault();
                                oim.Picture = url + "/" + defPic.Picture;

                            }
                            else
                            {
                                oim.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                            }
                            om.Items.Add(oim);
                        }
                    }
                    model.Orders.Add(om);
                }
            }
            return View(model);
        }

        public ActionResult Pay()
        {
            PayModel pm = new PayModel() { };
            //List<Product> list = ProductRepository.GetAllProducts();
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    Order order = OrderRepository.GetOrder(id);
                    if (order != null&&order.Status==OrderStatus.Created)
                    {
                        foreach (OrderItem oi in order.Items)
                        {
                            if (oi.Tags != null&&oi.IsHamper)
                            {
                                foreach (ProductTag pt in oi.Tags)
                                {
                                    if (!oi.Products.Any(p => p.Id == pt.ProductId))
                                    {
                                        ProductSetItem psi = oi.ProductSet.Items.FirstOrDefault(i => i.ProductId == pt.ProductId);
                                        if (psi != null)
                                        {
                                            Product prod = new Product();
                                            prod.Name = psi.Product.Name;
                                            prod.Id = psi.ProductId;

                                            
                                            if (!string.IsNullOrEmpty(pt.Key))
                                            {
                                                prod.Tags = oi.Tags.Where(p => p.ProductId == pt.ProductId).ToList();
                                            }
                                            oi.Products.Add(prod);
                                        }
                                    }
                                }
                            }
                        }
                        pm.Order = order;
                    }
                }
            }
            if (pm.Order == null)
                return RedirectToAction("Index");
            
            return View(pm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Pay(PayModel model)
        {
            Order order = OrderRepository.GetOrder(model.Order.Id);
            if (ModelState.IsValid||model.CreditType=="paypal")
            {
                try
                {
                    var guid = Guid.NewGuid().ToString();
                    Payment pymnt = GetPayment(model, order,guid);
                    // ### Api Context
                    // Pass in a `APIContext` object to authenticate 
                    // the call and to send a unique request id 
                    // (that ensures idempotency). The SDK generates
                    // a request id if you do not pass one explicitly. 
                    // See [Configuration.cs](/Source/Configuration.html) to know more about APIContext..
                    APIContext apiContext = Configuration.GetAPIContext();

                    // Create a payment using a valid APIContext
                    Payment createdPayment = pymnt.Create(apiContext);

                    if (model.CreditType == "paypal")
                    {
                        var links = createdPayment.links;
                        if (links != null && links.Any(l => l.rel == "approval_url"))
                        {
                            OrderRepository.Pay(order.Id, createdPayment.id, guid,OrderStatus.Created);
                            Session.Add(guid, createdPayment.id);
                            return Redirect(links.FirstOrDefault(l => l.rel == "approval_url").href);
                        }
                    }
                    else
                    {
                        OrderRepository.Pay(order.Id, createdPayment.id, null,OrderStatus.Paid);
                        
                        TempData["Message"] = "Your order " + ConstantHelper.GetOrderNumber(order.Id) + " has been paid successfully";
                    }
                }
                catch (PayPal.Exception.PayPalException ex)
                {
                    TempData["Message"] = ex.Message;
                }



                return RedirectToAction("Paid");
            }
            
            model.Order = order;
            return View(model);
        }

        public ActionResult Paid()
        {
            ViewBag.Message = "";

            if (Request.Params["guid"] != null)
            {
                if (Request.Params["PayerID"] != null)
                {
                    string guid = Request.Params["guid"];
                    Order order = OrderRepository.GetOrder(guid);
                    if (order != null)
                    {
                        try
                        {
                            Payment pymnt = new Payment();
                            pymnt.id = (string)Session[guid];
                            APIContext apiContext = Configuration.GetAPIContext();
                            PaymentExecution pymntExecution = new PaymentExecution();
                            pymntExecution.payer_id = Request.Params["PayerID"];
                            Payment executedPayment = pymnt.Execute(apiContext, pymntExecution);
                            OrderRepository.Pay(order.Id, executedPayment.id, guid, OrderStatus.Paid);
                            ViewBag.Message = "Your order " + ConstantHelper.GetOrderNumber(order.Id) + " has been paid successfully";
                            if (order.CreatedBy != null && string.IsNullOrWhiteSpace(order.CreatedBy.UserName) == false && order.Items != null && order.Items.Count > 0)
                            {
                                string products = "";
                                for (int i = 0; i < order.Items.Count; i++)
                                {
                                    products += i + ". " + order.Items[i].Product.Name + "<br />";
                                }
                                MailManager.Send("noreply@ethaneian.com", ConstantHelper.RepliedTo, "Customer " + order.CreatedBy.UserName + " has purchased the products", "Dear Administrator, <br /> Customer " + order.CreatedBy.UserName + " has purchased the following products: <br /> " + products + " Please login to to update the order status at this URL http://www.ethaneian.com/Admin/ManageOrder. Thank you.", true);
                            }
                        }
                        catch (PayPal.Exception.PayPalException ex)
                        {
                            ViewBag.Message = ex.Message;
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "You have cancelled your payment, if you wanna pay it later, please go to Manage Orders and pay it there.";
                }
            }
            
            if (TempData.Any(v => v.Key == "Message"))
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cancel(OrderCancelModel model)
        {
            if (ModelState.IsValid)
            {
                OrderHistory orderhistory = new OrderHistory()
                {
                    OrderId = model.OrderId,
                    NewStatus = OrderStatus.Cancelled,
                    CreatedBy = new User() { UserName = User.Identity.Name },
                    Remark = model.Remark
                };
                OrderRepository.UpdateOrderStatus(orderhistory);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Detail()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                {
                    Order order = OrderRepository.GetOrder(id);
                    if (order != null)
                    {
                        OrderModel model = new OrderModel()
                        {
                            Id = order.Id,
                            Total = Decimal.Round(order.Total, 2),
                            ShippingAddress = order.ShippingAddress,
                            Status = order.Status.ToString(),
                            DeliveryFee = Decimal.Round(order.DeliveryFee, 2),
                            OrderNo = ConstantHelper.GetOrderNumber(id),
                            Items = new List<OrderItemModel>(),
                            NewAddress = new ShippingAddressModel(),
                            Guid = Guid.NewGuid().ToString(),
                            PaymentMethod = order.PaymentMethod,
                            DeliveryMethod = order.DeliveryMethod
                        };
                        Setting setting = SettingRepository.GetSetting();
                        if (setting != null)
                        {
                            model.TaxRate = setting.TaxRate;
                        }
                        string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        foreach (var item in order.Items)
                        {
                            if (item.IsHamper)
                            {
                                OrderItemModel oim = new OrderItemModel()
                                {
                                    ProductId = item.ProductSet.Id,
                                    ProductName = item.ProductSet.Name,
                                    Price = Decimal.Round(item.UnitPrice, 2),
                                    Quantity = item.Quantity,
                                    SubTotal = Decimal.Round(item.SubTotal, 2),
                                    Weight = item.UnitWeight,
                                    IsHamper=true,
                                    Products=new List<Product>()
                                };

                                if (item.Tags != null && item.IsHamper)
                                {
                                    foreach (ProductTag pt in item.Tags)
                                    {
                                        if (!oim.Products.Any(p => p.Id == pt.ProductId))
                                        {
                                            ProductSetItem psi = item.ProductSet.Items.FirstOrDefault(i => i.ProductId == pt.ProductId);
                                            if (psi != null)
                                            {
                                                Product prod = new Product();
                                                prod.Name = psi.Product.Name;
                                                prod.Id = psi.ProductId;


                                                if (!string.IsNullOrEmpty(pt.Key))
                                                {
                                                    prod.Tags = item.Tags.Where(p => p.ProductId == pt.ProductId).ToList();
                                                }
                                                oim.Products.Add(prod);
                                            }
                                        }
                                    }
                                }

                                if (item.ProductSet.Pictures != null && item.ProductSet.Pictures.Any())
                                {

                                    ProductSetPicture defPic = item.ProductSet.Pictures.FirstOrDefault(p => p.IsDefault);
                                    if (defPic == null)
                                        defPic = item.ProductSet.Pictures.FirstOrDefault();
                                    oim.Picture = uri + "/" + defPic.Picture;

                                }
                                else
                                {
                                    oim.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                                }
                                model.Items.Add(oim);
                            }
                            else
                            {
                                var orderitem = new OrderItemModel()
                                {
                                    Inventory = item.Product.Quantity,
                                    ProductId = item.Product.Id,
                                    Price = Decimal.Round(item.UnitPrice, 2),
                                    ProductName = item.Product.Name,
                                    Quantity = item.Quantity,
                                    SubTotal = Decimal.Round(item.SubTotal, 2),
                                    Weight = item.UnitWeight,
                                    IsHamper=false
                                };
                                if (item.Product.Pictures.Any())
                                {
                                    var defpic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                                    if (defpic == null)
                                        defpic = item.Product.Pictures.FirstOrDefault();
                                    orderitem.Picture = uri + "/" + defpic.Picture;
                                }
                                else
                                {
                                    orderitem.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                                }
                                model.Items.Add(orderitem);
                            }
                        }

                        return View(model);
                    }
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Modify()
        {
            ViewBag.Err = HttpContext.Request.QueryString["err"];
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id)&&id>0)
                {
                    if (HttpContext.Request.QueryString["guid"] != null)
                    {
                        if (Session[HttpContext.Request.QueryString["guid"]] != null)
                        {
                            OrderModel model = Session[HttpContext.Request.QueryString["guid"]] as OrderModel;
                            if (model != null && model.Id == id)
                            {
                                List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
                                model.Addresses = list == null ? new List<UserAddress>() : list;

                                Country country = MasterDataRepository.GetEnabledCountry();
                                ViewData["Country"] = new List<SelectListItem>() { new SelectListItem() { Text = country.Name, Value = country.Name, Selected = true } };
                                List<SelectListItem> stateitems = new List<SelectListItem>();
                                List<SelectListItem> cityitems = new List<SelectListItem>();
                                List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
                                ViewData["StateId"] = stateitems;
                                ViewData["CityId"] = cityitems;
                                if (states != null)
                                {
                                    foreach (State state in states)
                                    {
                                        stateitems.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString() });
                                    }
                                }
                                if (stateitems.Any())
                                {
                                    State item = MasterDataRepository.GetState(Convert.ToInt32(stateitems.FirstOrDefault().Value));
                                    if (item != null && item.Cities != null)
                                    {
                                        item.Cities.ForEach(c => cityitems.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }));
                                    }
                                }
                                UpdateDeliveryFee(model);
                                Session[HttpContext.Request.QueryString["guid"]] = model;
                                return View(model);
                            }
                        }
                    }
                    else
                    {
                        Order order = OrderRepository.GetOrder(id);
                        Setting setting = SettingRepository.GetSetting();
                        if (order != null)
                        {
                            List<Order> userorders = OrderRepository.GetUserOrder(User.Identity.Name);
                            decimal lastTotal = 0;
                            if (userorders.Any(o => o.Status == OrderStatus.Paid))
                            {
                                lastTotal = userorders.Where(o => o.Status == OrderStatus.Paid).OrderByDescending(o => o.CreatedDate).FirstOrDefault().Total;
                            }
                            OrderModel model = new OrderModel()
                            {
                                Id = order.Id,
                                Total = Decimal.Round(order.Total, 2),
                                ShippingAddress = order.ShippingAddress,
                                Status = order.Status.ToString(),
                                DeliveryFee = Decimal.Round(order.DeliveryFee, 2),
                                OrderNo = ConstantHelper.GetOrderNumber(id),
                                Items = new List<OrderItemModel>(),
                                NewAddress = new ShippingAddressModel(),
                                Guid = Guid.NewGuid().ToString(),
                                PaymentMethod=order.PaymentMethod,
                                DeliveryMethod=order.DeliveryMethod,
                                LastTotal=lastTotal
                            };
                            List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
                            model.Addresses = list == null ? new List<UserAddress>() : list;

                            Country country = MasterDataRepository.GetEnabledCountry();
                            ViewData["Country"] = new List<SelectListItem>() { new SelectListItem() { Text = country.Name, Value = country.Name, Selected = true } };
                            List<SelectListItem> stateitems = new List<SelectListItem>();
                            List<SelectListItem> cityitems = new List<SelectListItem>();
                            List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
                            ViewData["StateId"] = stateitems;
                            ViewData["CityId"] = cityitems;
                            if (states != null)
                            {
                                foreach (State state in states)
                                {
                                    stateitems.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString() });
                                }
                            }
                            if (stateitems.Any())
                            {
                                State item = MasterDataRepository.GetState(Convert.ToInt32(stateitems.FirstOrDefault().Value));
                                if (item != null && item.Cities != null)
                                {
                                    item.Cities.ForEach(c => cityitems.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }));
                                }
                            }

                            string uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority;
                            int orderitemid = 1;
                            foreach (var item in order.Items)
                            {
                                if (item.IsHamper)
                                {
                                    OrderItemModel oim = new OrderItemModel()
                                    {
                                        ProductId = item.ProductSet.Id,
                                        ProductName = item.ProductSet.Name,
                                        Inventory=item.ProductSet.Items.Where(p=>p.IsMandatory).Select(m=>m.Product.Quantity).Min(),
                                        Price = Decimal.Round(item.UnitPrice, 2),
                                        Quantity = item.Quantity,
                                        SubTotal = Decimal.Round(item.SubTotal, 2),
                                        Weight = item.UnitWeight,
                                        Tags = item.Tags,
                                        IsHamper=true,
                                        Products=new List<Product>(),
                                        OrderItemId=orderitemid++
                                    };

                                    if (item.Tags != null && item.IsHamper)
                                    {
                                        foreach (ProductTag pt in item.Tags)
                                        {
                                            if (!oim.Products.Any(p => p.Id == pt.ProductId))
                                            {
                                                ProductSetItem psi = item.ProductSet.Items.FirstOrDefault(i => i.ProductId == pt.ProductId);
                                                if (psi != null)
                                                {
                                                    Product prod = new Product();
                                                    prod.Name = psi.Product.Name;
                                                    prod.Id = psi.ProductId;


                                                    if (!string.IsNullOrEmpty(pt.Key))
                                                    {
                                                        prod.Tags = item.Tags.Where(p => p.ProductId == pt.ProductId).ToList();
                                                    }
                                                    oim.Products.Add(prod);
                                                }
                                            }
                                        }
                                    }

                                    if (item.ProductSet.Pictures != null && item.ProductSet.Pictures.Any())
                                    {

                                        ProductSetPicture defPic = item.ProductSet.Pictures.FirstOrDefault(p => p.IsDefault);
                                        if (defPic == null)
                                            defPic = item.ProductSet.Pictures.FirstOrDefault();
                                        oim.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defPic.Picture;

                                    }
                                    else
                                    {
                                        oim.Picture = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                                    }
                                    model.Items.Add(oim);
                                }
                                else
                                {
                                    var orderitem = new OrderItemModel()
                                    {
                                        Inventory = item.Product.Quantity,
                                        ProductId = item.Product.Id,
                                        Price = Decimal.Round(item.UnitPrice, 2),
                                        ProductName = item.Product.Name,
                                        Quantity = item.Quantity,
                                        SubTotal = Decimal.Round(item.SubTotal, 2),
                                        Weight = item.UnitWeight,
                                        Tags = item.Tags,
                                        IsHamper=false,
                                        OrderItemId = orderitemid++
                                    };
                                    if (item.Product.Pictures.Any())
                                    {
                                        var defpic = item.Product.Pictures.FirstOrDefault(p => p.IsDefault);
                                        if (defpic == null)
                                            defpic = item.Product.Pictures.FirstOrDefault();
                                        orderitem.Picture = uri + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + defpic.Picture;
                                    }
                                    else
                                    {
                                        orderitem.Picture = uri + "/images/assets/not_available-generic.png";
                                    }
                                    model.Items.Add(orderitem);
                                }
                            }
                            
                            if (setting != null)
                            {
                                model.TaxRate = setting.TaxRate;
                                model.RepeatCustomerDiscount = setting.RepeatCustomerDiscount;
                            }
                            Session[model.Guid] = model;
                            return View(model);
                        }
                    }
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddQty(int qty,string guid)
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0 && qty > 0&&!string.IsNullOrEmpty(guid))
                    {
                        if (Session[guid] == null)
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        OrderModel model = Session[guid] as OrderModel;
                        if (model == null)
                        {
                            return Json(new { success = false,message="Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        if (model.Items == null || !model.Items.Any(i => i.OrderItemId == id))
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        OrderItemModel itemmodel = model.Items.FirstOrDefault(i => i.OrderItemId == id);
                        if (itemmodel.Quantity + qty > itemmodel.Inventory)
                        {
                            return Json(new { success = false, message = "Sorry, no more product in stock!" }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        itemmodel.Quantity = itemmodel.Quantity + qty;
                        itemmodel.SubTotal = Decimal.Round(itemmodel.Price * itemmodel.Quantity,2);
                        model.Total = 0;
                        model.Items.ForEach(item => model.Total += item.SubTotal);
                        UpdateDeliveryFee(model);
                        Session[guid] = model;
                        return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubQty(int qty, string guid)
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0 && qty > 0 && !string.IsNullOrEmpty(guid))
                    {
                        if (Session[guid] == null)
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        OrderModel model = Session[guid] as OrderModel;
                        if (model == null)
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        if (model.Items == null || !model.Items.Any(i => i.OrderItemId == id))
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        OrderItemModel itemmodel = model.Items.FirstOrDefault(i => i.OrderItemId == id);
                        if (itemmodel.Quantity - qty <0)
                        {
                            return Json(new { success = false, message = "Sorry, invalid request! too many quantity to be deducted!" }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        itemmodel.Quantity = itemmodel.Quantity - qty;
                        if (itemmodel.Quantity == 0)
                        {
                            model.Items.Remove(itemmodel);
                        }
                        else
                        {
                            itemmodel.SubTotal = Decimal.Round(itemmodel.Price * itemmodel.Quantity, 2);
                        }
                        model.Total = 0;
                        model.Items.ForEach(item => model.Total += item.SubTotal);
                        UpdateDeliveryFee(model);
                        Session[guid] = model;
                        return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveItem(string guid)
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0  && !string.IsNullOrEmpty(guid))
                    {
                        if (Session[guid] == null)
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        OrderModel model = Session[guid] as OrderModel;
                        if (model == null)
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        if (model.Items == null || !model.Items.Any(i => i.OrderItemId == id))
                        {
                            return Json(new { success = false, message = "Invalid request, please try again later." }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        OrderItemModel itemmodel = model.Items.FirstOrDefault(i => i.OrderItemId == id);
                        model.Items.Remove(itemmodel);
                        model.Total = 0;
                        model.Items.ForEach(item => model.Total += item.SubTotal);
                        UpdateDeliveryFee(model);
                        Session[guid] = model;
                        return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAddress(OrderModel model)
        {
            if (Session[model.Guid] != null)
            {
                OrderModel om = Session[model.Guid] as OrderModel;
                if (om != null && om.Id == model.Id)
                {
                    List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
                    if (list != null && list.Any(l => l.Id == model.ShippingAddress.Id))
                    {
                        om.ShippingAddress = list.FirstOrDefault(l => l.Id == model.ShippingAddress.Id);
                        Session[model.Guid] = om;
                    }
                }

            }
            return RedirectToAction("Modify", new { id=model.Id,guid=model.Guid});
        }

        [HttpPost]
        public ActionResult NewAddress(OrderModel model)
        {
            OrderModel om = Session[model.Guid] as OrderModel;
            if (om != null && om.Id == model.Id)
            {
                List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
                om.NewAddress = model.NewAddress;
                om.ShippingAddress = new UserAddress() { Id=0};
                if (ModelState.IsValid)
                {
                    int userid = WebSecurity.GetUserId(User.Identity.Name);
                    UserAddress ua = new UserAddress()
                    {
                        Postcode = model.NewAddress.Postcode,
                        Country = model.NewAddress.Country,
                        State = model.NewAddress.State,
                        City = model.NewAddress.City,
                        Address2 = model.NewAddress.Address2,
                        Address1 = model.NewAddress.Address1,
                        FirstName = model.NewAddress.FirstName,
                        LastName = model.NewAddress.LastName,
                        MobileNo = model.NewAddress.MobileNo,
                        TelephoneNo = model.NewAddress.TelephoneNo,
                        Id = 0,
                        UserId = userid
                    };
                    try
                    {
                        om.ShippingAddress = UserRepository.UpdateUserAddress(ua);
                        Session[model.Guid] = om;
                        return RedirectToAction("Modify", new { id = model.Id, guid = model.Guid });
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                    }
                }
                Country country = MasterDataRepository.GetEnabledCountry();
                ViewData["Country"] = new List<SelectListItem>() { new SelectListItem() { Text = country.Name, Value = country.Name, Selected = true } };
                List<SelectListItem> stateitems = new List<SelectListItem>();
                List<SelectListItem> cityitems = new List<SelectListItem>();
                List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
                ViewData["StateId"] = stateitems;
                ViewData["CityId"] = cityitems;
                if (states != null)
                {
                    foreach (State state in states)
                    {
                        stateitems.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString() });
                    }
                }
                if (stateitems.Any())
                {
                    State item = MasterDataRepository.GetState(Convert.ToInt32(stateitems.FirstOrDefault().Value));
                    if (item != null && item.Cities != null)
                    {
                        item.Cities.ForEach(c => cityitems.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }));
                    }
                }
                return View("Modify", om);
            }

            return RedirectToAction("Index");
        }

        public ActionResult NewAddress()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                return RedirectToAction("Modify", new { id = RouteData.Values["id"] });
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(OrderModel model)
        {
            OrderModel om = Session[model.Guid] as OrderModel;
            if (om != null && om.Id == model.Id)
            {
                Order order = new Order()
                {
                    Id = om.Id,
                    Status = OrderStatus.Created,
                    CreatedBy = new User() { UserName = User.Identity.Name },
                    LastModifiedBy = new User() { UserName = User.Identity.Name },
                    DeliveryMethod = om.DeliveryMethod,
                    PaymentMethod = om.PaymentMethod,
                    ShippingAddress = om.ShippingAddress,
                    Total = om.Total,
                    DeliveryFee = om.DeliveryFee,
                    Items = new List<OrderItem>(),
                    TaxRate = om.TaxRate,
                    RepeatCustomerDiscount=om.Discount
                };

                foreach (OrderItemModel item in om.Items)
                {
                    OrderItem oi=new OrderItem()
                        {
                            Product = new Product() { Id = item.ProductId },
                            Quantity = item.Quantity,
                            UnitPrice = item.Price,
                            UnitWeight = item.Weight,
                            SubTotal = item.SubTotal,
                            Tags = item.Tags,
                            IsHamper = item.IsHamper,
                            Products = new List<Product>()
                        };
                    if (item.IsHamper&&item.Products!=null)
                    {
                        foreach (var prod in item.Products)
                        {
                            oi.Products.Add(new Product() { Id=prod.Id,Quantity=item.Quantity});
                        }
                    }
                    order.Items.Add(oi);
                }
                try
                {
                    OrderRepository.AddOrder(order);

                   

                    return RedirectToAction("Pay", "Order", new { id = om.Id });
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Modify", new { id = om.Id, err = ex.Message });
                }
            }
            return RedirectToAction("Index");
        }
        public Payment GetPayment(PayModel model, Order order,string guid)
        {
            Setting setting = SettingRepository.GetSetting();

            Payer payr = new Payer();
            Payment pymnt = new Payment();
            pymnt.intent = "sale";
            pymnt.payer = payr;

            // ###Items
            // Items within a transaction.
            List<Item> itms = new List<Item>();
            foreach (var oi in order.Items)
            {
                Item item = new Item();
                item.name = oi.Product.Name;
                item.currency = "SGD";
                item.price = Decimal.Round(oi.UnitPrice.ToDollarFromRupiah(setting.ExchangeRate), 2).ToString();
                item.quantity = oi.Quantity.ToString();
                item.sku = oi.Product.Name;
                itms.Add(item);
            }
            ItemList itemList = new ItemList();
            itemList.items = itms;

            // ###Details
            // Let's you specify details of a payment amount.
            Details details = new Details();
            details.shipping = Decimal.Round(order.DeliveryFee.ToDollarFromRupiah(setting.ExchangeRate), 2).ToString();
            details.subtotal = Decimal.Round((order.Total-order.RepeatCustomerDiscount).ToDollarFromRupiah(setting.ExchangeRate), 2).ToString();
            decimal tax=order.Total * order.TaxRate / 100;
            details.tax = Decimal.Round(tax.ToDollarFromRupiah(setting.ExchangeRate),2).ToString();

            // ###Amount
            // Let's you specify a payment amount.
            Amount amnt = new Amount();
            amnt.currency = "SGD";
            // Total must be equal to sum of shipping, tax and subtotal.
            amnt.total = (Decimal.Round(order.DeliveryFee.ToDollarFromRupiah(setting.ExchangeRate), 2) + Decimal.Round((order.Total - order.RepeatCustomerDiscount).ToDollarFromRupiah(setting.ExchangeRate), 2) + Decimal.Round(tax.ToDollarFromRupiah(setting.ExchangeRate), 2)).ToString();
            amnt.details = details;

            // ###Transaction
            // A transaction defines the contract of a
            // payment - what is the payment for and who
            // is fulfilling it. 
            Transaction tran = new Transaction();
            tran.amount = amnt;
            tran.description = "This is the payment transaction for "+ConstantHelper.GetOrderNumber(order.Id)+".";
            tran.item_list = itemList;

            // The Payment creation API requires a list of
            // Transaction; add the created `Transaction`
            // to a List
            List<Transaction> transactions = new List<Transaction>();
            transactions.Add(tran);

            pymnt.transactions = transactions;

            if (model.CreditType == "paypal")
            {
                payr.payment_method = "paypal";
                
                string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Order/Paid?";

                // # Redirect URLS
                RedirectUrls redirUrls = new RedirectUrls();
                redirUrls.cancel_url = baseURI + "guid=" + guid;
                redirUrls.return_url = baseURI + "guid=" + guid;
                pymnt.redirect_urls = redirUrls;
            }
            else
            {
                // ###Address
                // Base Address object used as shipping or billing
                // address in a payment.
                Address shippingAddress = new Address();
                shippingAddress.city = order.ShippingAddress.City;
                shippingAddress.country_code = "ID";
                shippingAddress.line1 = order.ShippingAddress.Address1;
                shippingAddress.postal_code = order.ShippingAddress.Postcode;
                shippingAddress.state = order.ShippingAddress.State.Length > 1 ? order.ShippingAddress.State.Substring(0, 2) : order.ShippingAddress.State;

                // ###CreditCard
                // A resource representing a credit card that can be
                // used to fund a payment.
                CreditCard crdtCard = new CreditCard();
                crdtCard.billing_address = shippingAddress;
                crdtCard.cvv2 = model.SecurityPin;
                crdtCard.expire_month = model.ExpireMonth;
                crdtCard.expire_year = model.ExpireYear;
                crdtCard.first_name = model.FirstName;
                crdtCard.last_name = model.LastName;
                crdtCard.number = model.CardNumber;
                crdtCard.type = model.CreditType;

                // ###FundingInstrument
                // A resource representing a Payer's funding instrument.
                // For direct credit card payments, set the CreditCard
                // field on this object.
                FundingInstrument fundInstrument = new FundingInstrument();
                fundInstrument.credit_card = crdtCard;

                // The Payment creation API requires a list of
                // FundingInstrument; add the created `FundingInstrument`
                // to a List
                List<FundingInstrument> fundingInstrumentList = new List<FundingInstrument>();
                fundingInstrumentList.Add(fundInstrument);

                // ###Payer
                // A resource representing a Payer that funds a payment
                // Use the List of `FundingInstrument` and the Payment Method
                // as `credit_card`
                payr.funding_instruments = fundingInstrumentList;
                payr.payment_method = "credit_card";
            }

            return pymnt;
        }

        public void UpdateDeliveryFee(OrderModel model)
        {
            //State state = MasterDataRepository.GetState(model.ShippingAddress.State);
            //decimal fee = 0;
            //DeliveryFee delfee = MasterDataRepository.GetDeliveryFee(model.DeliveryMethod.Id, state.Id);
            //if (delfee != null)
            //    fee = delfee.Fee;
            //decimal weight = 0;
            //foreach (OrderItemModel item in model.Items)
            //{
            //    weight += item.Quantity * item.Weight;
            //}
            model.DeliveryFee = model.Total >= ConstantHelper.PriceDivision ? Decimal.Round(0, 2) : Decimal.Round(ConstantHelper.ShippingFee, 2);
        }
    }
}
