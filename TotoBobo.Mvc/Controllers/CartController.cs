﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TotoBobo.Mvc.Helper;
using System.Security.Cryptography;
using TotoBobo.Mvc.Models;
using TotoBobo.Mvc.Repository;
using TotoBobo.Data.Objects;
using WebMatrix.WebData;

namespace TotoBobo.Mvc.Controllers
{
    public class CartController : Controller
    {
        //
        // GET: /Cart/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddToCart(CartItem item)
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0 && item!=null&&item.Quantity>0)
                    {
                        try
                        {
                            item.ProductId = id;
                            item.SessionId = Session.SessionID;
                            item.Id = 0;
                            if (HttpContext.Request.QueryString["type"] == "hamper")
                            {
                                item.IsHamper = true;
                            }
                            CartRepository.AddToCart(item);
                            return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = false, message = ex.Message }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Add(int qty)
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0 && qty > 0)
                    {
                        try
                        {
                            CartRepository.ChangeQty(id, qty);
                            return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = false, message = ex.Message }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        //string cart = "";
                        //HttpCookie existing = Request.Cookies[CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID)];
                        //if (existing != null)
                        //{
                        //    cart = CartDataProtector.DecryptStringFromBytes_Aes(existing.Value);
                        //}
                        //string[] items = cart.Split('|');
                        //Dictionary<int, int> dicItems = new Dictionary<int, int>();
                        //foreach (string item in items)
                        //{
                        //    if (string.IsNullOrEmpty(item))
                        //        continue;
                        //    string[] pair = item.Split(',');
                        //    if (pair.Length > 1)
                        //    {
                        //        int pid = 0;
                        //        int quantity = 0;
                        //        if (int.TryParse(pair[0], out pid) && int.TryParse(pair[1], out quantity) && pid > 0 && quantity > 0)
                        //        {
                        //            if (dicItems.Keys.Any(i => i == pid))
                        //            {
                        //                dicItems[pid] += quantity;
                        //            }
                        //            else
                        //            {
                        //                dicItems.Add(pid, quantity);
                        //            }
                        //        }
                        //    }
                        //}

                        //Product product = ProductRepository.GetProductById(id);
                        //int total = qty;
                        //if (dicItems.Keys.Any(i => i == id))
                        //{
                        //    total += dicItems[id];
                        //}
                        //if (product.Quantity < total)
                        //{
                        //    if (qty == 1)
                        //    {
                        //        return Json(new { success = false, message = "Sorry, no more product in stock!" }, "text/plain", JsonRequestBehavior.AllowGet);
                        //    }
                        //    else
                        //    {
                        //        return Json(new { success = false, message = "Sorry, the number you input is larger than what we have in stock!" }, "text/plain", JsonRequestBehavior.AllowGet);
                        //    }
                        //}

                        //if (dicItems.Keys.Any(i => i == id))
                        //{
                        //    dicItems[id] += qty;
                        //}
                        //else
                        //{
                        //    dicItems.Add(id, qty);
                        //}

                        //cart = "";
                        //foreach (var item in dicItems.Keys)
                        //{
                        //    cart = cart + item.ToString() + "," + dicItems[item].ToString() + "|";
                        //}
                        //if (cart.EndsWith("|"))
                        //{
                        //    cart=cart.Remove(cart.Length - 1);
                        //}

                        //cart = CartDataProtector.EncryptStringToBytes_Aes(cart);
                        //HttpCookie cookie = new HttpCookie(CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID), cart);
                        //cookie.Expires = DateTime.Now.AddHours(2);
                        //Response.Cookies.Set(cookie);
                        //return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Sub(int qty)
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0 && qty > 0)
                    {
                        try
                        {
                            CartRepository.ChangeQty(id, -qty);
                            return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = false, message = ex.Message }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        //string cart = "";
                        //HttpCookie existing = Request.Cookies[CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID)];
                        //if (existing != null)
                        //{
                        //    cart = CartDataProtector.DecryptStringFromBytes_Aes(existing.Value);
                        //}
                        //string[] items = cart.Split('|');
                        //Dictionary<int, int> dicItems = new Dictionary<int, int>();
                        //foreach (string item in items)
                        //{
                        //    if (string.IsNullOrEmpty(item))
                        //        continue;
                        //    string[] pair = item.Split(',');
                        //    if (pair.Length > 1)
                        //    {
                        //        int pid = 0;
                        //        int quantity = 0;
                        //        if (int.TryParse(pair[0], out pid) && int.TryParse(pair[1], out quantity) && pid > 0 && quantity > 0)
                        //        {
                        //            if (dicItems.Keys.Any(i => i == pid))
                        //            {
                        //                dicItems[pid] += quantity;
                        //            }
                        //            else
                        //            {
                        //                dicItems.Add(pid, quantity);
                        //            }
                        //        }
                        //    }
                        //}

                        //if (dicItems.Keys.Any(i => i == id))
                        //{
                        //    dicItems[id] -= qty;
                        //}

                        //cart = "";
                        //foreach (var item in dicItems.Keys)
                        //{
                        //    if (dicItems[item] == 0)
                        //        continue;
                        //    cart = cart + item.ToString() + "," + dicItems[item].ToString() + "|";
                        //}
                        //if (cart.EndsWith("|"))
                        //{
                        //    cart = cart.Remove(cart.Length - 1);
                        //}

                        //if (string.IsNullOrEmpty(cart))
                        //{
                        //    HttpCookie cookie = new HttpCookie(CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID))
                        //    {
                        //        Expires = DateTime.Now.AddDays(-1) // or any other time in the past
                        //    };
                        //    Response.Cookies.Set(cookie);
                        //    return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        //}
                        //else
                        //{
                        //    cart = CartDataProtector.EncryptStringToBytes_Aes(cart);
                        //    HttpCookie cookie = new HttpCookie(CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID), cart);
                        //    cookie.Expires = DateTime.Now.AddHours(2);
                        //    Response.Cookies.Set(cookie);
                        //    return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        //}
                    }
                }
            }
            return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Remove()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        try
                        {
                            CartRepository.RemoveCartItem(id);
                            return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = false, message = ex.Message }, "text/plain", JsonRequestBehavior.AllowGet);
                        }
                        //string cart = "";
                        //HttpCookie existing = Request.Cookies[CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID)];
                        //if (existing != null)
                        //{
                        //    cart = CartDataProtector.DecryptStringFromBytes_Aes(existing.Value);
                        //}
                        //string[] items = cart.Split('|');
                        //Dictionary<int, int> dicItems = new Dictionary<int, int>();
                        //foreach (string item in items)
                        //{
                        //    if (string.IsNullOrEmpty(item))
                        //        continue;
                        //    string[] pair = item.Split(',');
                        //    if (pair.Length > 1)
                        //    {
                        //        int pid = 0;
                        //        int quantity = 0;
                        //        if (int.TryParse(pair[0], out pid) && int.TryParse(pair[1], out quantity) && pid > 0 && quantity > 0)
                        //        {
                        //            if (dicItems.Keys.Any(i => i == pid))
                        //            {
                        //                dicItems[pid] += quantity;
                        //            }
                        //            else
                        //            {
                        //                dicItems.Add(pid, quantity);
                        //            }
                        //        }
                        //    }
                        //}

                        //if (dicItems.Keys.Any(i => i == id))
                        //{
                        //    dicItems.Remove(id);
                        //}

                        //cart = "";
                        //foreach (var item in dicItems.Keys)
                        //{
                        //    if (dicItems[item] == 0)
                        //        continue;
                        //    cart = cart + item.ToString() + "," + dicItems[item].ToString() + "|";
                        //}
                        //if (cart.EndsWith("|"))
                        //{
                        //    cart = cart.Remove(cart.Length - 1);
                        //}

                        //if (string.IsNullOrEmpty(cart))
                        //{
                        //    HttpCookie cookie = new HttpCookie(CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID))
                        //    {
                        //        Expires = DateTime.Now.AddDays(-1) // or any other time in the past
                        //    };
                        //    Response.Cookies.Set(cookie);
                        //    return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        //}
                        //else
                        //{
                        //    cart = CartDataProtector.EncryptStringToBytes_Aes(cart);
                        //    HttpCookie cookie = new HttpCookie(CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID), cart);
                        //    cookie.Expires = DateTime.Now.AddHours(2);
                        //    Response.Cookies.Set(cookie);
                        //    return Json(new { success = true }, "text/plain", JsonRequestBehavior.AllowGet);
                        //}
                    }
                }
            }
            return Json(new { success = false }, "text/plain", JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Checkout()
        {
            CartModel cart = GetCart();
            if (cart == null || !cart.Items.Any())
            {
                return RedirectToAction("ShoppingBag", new { Err="No items in cart"});
            }
            List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
            CheckoutModel model = new CheckoutModel() { Addresses = new List<UserAddress>(), NewAddress = new ShippingAddressModel() { Id = 0 }, StepNo = 1, SelectedAddress = new UserAddress(), Cart = new CartModel() { Items = new List<CartItemModel>() },Delivery=new DeliveryMethod(),Payment=new PaymentMethod() };
            Country country = MasterDataRepository.GetEnabledCountry();
            ViewData["Country"] = new List<SelectListItem>() { new SelectListItem() { Text = country.Name, Value = country.Name, Selected = true } };
            List<SelectListItem> stateitems = new List<SelectListItem>();
            List<SelectListItem> cityitems = new List<SelectListItem>();
            List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
            ViewData["StateId"] = stateitems;
            ViewData["CityId"] = cityitems;
            if (states != null)
            {
                foreach (State state in states)
                {
                    stateitems.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString() });
                }
            }
            if (stateitems.Any())
            {
                State item = MasterDataRepository.GetState(Convert.ToInt32(stateitems.FirstOrDefault().Value));
                if (item != null && item.Cities != null)
                {
                    item.Cities.ForEach(c => cityitems.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }));
                }
            }
            if (list != null&&list.Any())
            {
                model.Addresses = list;
                if (list.Any(l => l.IsDefault))
                {
                    model.ShippingAddressId = list.FirstOrDefault(l => l.IsDefault).Id;
                }
                else
                {
                    model.ShippingAddressId = list.FirstOrDefault().Id;
                }
            }
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Checkout(CheckoutModel model)
        {
            List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);

            if (model.ShippingAddressId > 0&&list!=null&&list.Any(l=>l.Id==model.ShippingAddressId))
            {
                List<PaymentMethod> payments = MasterDataRepository.GetPaymentMethods();
                PaymentMethod payment = payments.FirstOrDefault(p => p.IsEnabled);
                List<DeliveryMethod> deliveries = MasterDataRepository.GetDeliveryMethods();
                DeliveryMethod delivery = deliveries.FirstOrDefault(d => d.IsEnabled);
                Setting setting = SettingRepository.GetSetting();
                
                if (payment == null || delivery == null)
                    return RedirectToAction("Index", "Home");
                payment.Logo=HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + payment.Logo);
                delivery.Logo = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + delivery.Logo);
                model.Payment = payment;
                model.Delivery = delivery;
                model.Addresses = list;
                model.Cart = GetCart();
                if (setting != null)
                    model.TaxRate = setting.TaxRate;
                model.NewAddress = new ShippingAddressModel();
                model.SelectedAddress = list.FirstOrDefault(l => l.Id == model.ShippingAddressId);
                model.PaymentId = payment.Id;
                model.DeliveryId = delivery.Id;
                State state = MasterDataRepository.GetState(model.SelectedAddress.State);
                //decimal fee = 0;
                //DeliveryFee delfee = MasterDataRepository.GetDeliveryFee(delivery.Id, state.Id);
                //if (delfee != null)
                //    fee = delfee.Fee;
                //decimal weight = 0;
                //foreach (CartItemModel item in model.Cart.Items)
                //{
                //    weight += item.Quantity * item.Weight;
                //}
                model.ShippingFee = model.Cart.Total >= ConstantHelper.PriceDivision ? Decimal.Round(0, 2) : Decimal.Round(ConstantHelper.ShippingFee, 2);
                model.StepNo = 2;
                return View(model);
            }
            return RedirectToAction("Checkout");
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult PlaceOrder(CheckoutModel model)
        {
            CartModel cart = GetCart();
            List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
            Setting setting = SettingRepository.GetSetting();
            List<Product> actives = ProductRepository.GetActives();
            Order order = new Order()
            {
                Id = 0,
                Status = OrderStatus.Created,
                CreatedBy = new User() { UserName = User.Identity.Name },
                LastModifiedBy = new User() { UserName = User.Identity.Name },
                DeliveryMethod = new DeliveryMethod() { Id = model.DeliveryId },
                PaymentMethod = new PaymentMethod() { Id = model.PaymentId },
                ShippingAddress = list.FirstOrDefault(l => l.Id == model.ShippingAddressId),
                Total = cart.Total,
                DeliveryFee = model.ShippingFee,
                RepeatCustomerDiscount = cart.Discount,
                Items = new List<OrderItem>()
            };
            if (setting != null)
                order.TaxRate = setting.TaxRate;
            foreach (CartItemModel item in cart.Items)
            {
                OrderItem oi = new OrderItem()
                        {
                            Product = new Product() { Id = item.ProductId },
                            Quantity = item.Quantity,
                            UnitPrice = item.Price,
                            UnitWeight = item.Weight,
                            SubTotal = item.SubTotal,
                            Tags = item.Tags,
                            IsHamper = item.IsHamper,
                            Products=new List<Product>()
                        };
                if (actives != null && actives.Any(p => p.Id == item.ProductId)&&!item.IsHamper)
                {
                    oi.DiscountId = actives.FirstOrDefault(p => p.Id == item.ProductId).Discount.Id;
                }
                if (item.IsHamper&&item.Products!=null)
                {
                    foreach (var prod in item.Products)
                    {
                        oi.Products.Add(new Product() { Id=prod.Id,Quantity=item.Quantity});
                    }
                }
                order.Items.Add(oi);
            }
            try
            {
                int orderno = OrderRepository.AddOrder(order);
                CartRepository.ClearCart(Session.SessionID);
                //HttpCookie cookie = new HttpCookie(CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID))
                //{
                //    Expires = DateTime.Now.AddDays(-1) // or any other time in the past
                //};
                //Response.Cookies.Set(cookie);
                //PayModel pm = new PayModel() { Order=order};
                return RedirectToAction("Pay", "Order", new { id = orderno });
            }
            catch (Exception ex)
            {
                return RedirectToAction("ShoppingBag", new { err=ex.Message});
            }
        }

        [Authorize]
        public ActionResult CheckoutWithNA()
        {
            return RedirectToAction("Checkout");
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckoutWithNA(ShippingAddressModel model)
        {

            CheckoutModel cmodel = new CheckoutModel() { Addresses = new List<UserAddress>(), NewAddress = new ShippingAddressModel(), SelectedAddress = new UserAddress(), Cart = new CartModel() { Items=new List<CartItemModel>()} };
            if (ModelState.IsValid)
            {
                int uid = WebSecurity.GetUserId(User.Identity.Name);
                UserAddress ua = new UserAddress()
                {
                    Postcode = model.Postcode,
                    Country = model.Country,
                    State = model.State,
                    City = model.City,
                    Address2 = model.Address2,
                    Address1 = model.Address1,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    MobileNo = model.MobileNo,
                    TelephoneNo = model.TelephoneNo,
                    Id = 0,
                    UserId = uid,
                    IsDefault = false
                };
                try
                {
                    UserAddress newaddress = UserRepository.UpdateUserAddress(ua);
                    cmodel.ShippingAddressId = newaddress.Id;
                    cmodel.Addresses = UserRepository.GetAddressesForUser(User.Identity.Name);
                    cmodel.StepNo = 2;
                    cmodel.SelectedAddress = newaddress;
                    cmodel.Cart = GetCart();
                    List<PaymentMethod> payments = MasterDataRepository.GetPaymentMethods();
                    PaymentMethod payment = payments.FirstOrDefault(p => p.IsEnabled);
                    List<DeliveryMethod> deliveries = MasterDataRepository.GetDeliveryMethods();
                    DeliveryMethod delivery = deliveries.FirstOrDefault(d => d.IsEnabled);
                    Setting setting = SettingRepository.GetSetting();

                    if (payment == null || delivery == null)
                        return RedirectToAction("Index", "Home");
                    payment.Logo = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + payment.Logo);
                    delivery.Logo = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + delivery.Logo);
                    cmodel.Payment = payment;
                    cmodel.Delivery = delivery;
                    if (setting != null)
                        cmodel.TaxRate = setting.TaxRate;
                    cmodel.PaymentId = payment.Id;
                    cmodel.DeliveryId = delivery.Id;
                    //decimal fee = 0;
                    //DeliveryFee delfee = MasterDataRepository.GetDeliveryFee(delivery.Id, state.Id);
                    //if (delfee != null)
                    //    fee = delfee.Fee;
                    //decimal weight = 0;
                    //foreach (CartItemModel item in model.Cart.Items)
                    //{
                    //    weight += item.Quantity * item.Weight;
                    //}
                    cmodel.ShippingFee = cmodel.Cart.Total >= ConstantHelper.PriceDivision ? Decimal.Round(0, 2) : Decimal.Round(ConstantHelper.ShippingFee, 2);
                    return View("Checkout", cmodel);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            List<UserAddress> list = UserRepository.GetAddressesForUser(User.Identity.Name);
            if (list != null && list.Any())
            {
                cmodel.Addresses = list;
            }
            cmodel.NewAddress = model;
            cmodel.ShippingAddressId = 0;
            cmodel.StepNo = 1;
            Country country = MasterDataRepository.GetEnabledCountry();
            ViewData["Country"] = new List<SelectListItem>() { new SelectListItem() { Text = country.Name, Value = country.Name, Selected = true } };
            List<SelectListItem> stateitems = new List<SelectListItem>();
            List<SelectListItem> cityitems = new List<SelectListItem>();
            List<State> states = MasterDataRepository.GetStatesInCountry(country.Id);
            ViewData["StateId"] = stateitems;
            ViewData["CityId"] = cityitems;
            if (states != null)
            {
                foreach (State state in states)
                {
                    stateitems.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString(),Selected=state.Name.Equals(model.State,StringComparison.InvariantCultureIgnoreCase) });
                }
            }
            if (stateitems.Any())
            {
                int sid = 0;
                if (stateitems.Any(s => s.Selected))
                {
                    State state = states.FirstOrDefault(s => s.Name.Equals(stateitems.FirstOrDefault(i => i.Selected).Text));
                    sid = state.Id;
                }
                else
                {
                    sid = Convert.ToInt32(stateitems.FirstOrDefault().Value);
                }
                State item = MasterDataRepository.GetState(sid);
                if (item != null && item.Cities != null)
                {

                    item.Cities.ForEach(c => cityitems.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = c.Name.Equals(model.City, StringComparison.InvariantCultureIgnoreCase) }));
                }
            }
            return View("Checkout",cmodel);
        }

        [Authorize]
        public ActionResult ShoppingBag()
        {
            ViewBag.Err = HttpContext.Request.QueryString["err"];
           
            return View();
        }

        #region helper
        public CartModel GetCart()
        {
            //CartModel model = new CartModel() { Items = new List<CartItemModel>() };
            //HttpCookie existing = Request.Cookies[CartDataProtector.EncryptStringToBytes_Aes(Session.SessionID)];
            //string cart = "";
            //if (existing != null)
            //{
            //    cart = CartDataProtector.DecryptStringFromBytes_Aes(existing.Value);
            //    string[] items = cart.Split('|');
            //    Dictionary<int, int> dicItems = new Dictionary<int, int>();
            //    List<Product> list = ProductRepository.GetAllProducts();
            //    List<Product> actives = ProductRepository.GetActives();
            //    if (list == null)
            //        list = new List<Product>();
            //    foreach (string item in items)
            //    {
            //        if (string.IsNullOrEmpty(item))
            //            continue;
            //        string[] pair = item.Split(',');
            //        if (pair.Length > 1)
            //        {
            //            int pid = 0;
            //            int quantity = 0;

            //            if (int.TryParse(pair[0], out pid) && int.TryParse(pair[1], out quantity) && pid > 0 && quantity > 0)
            //            {
            //                Product product = list.FirstOrDefault(l => l.Id == pid);
            //                if (product != null)
            //                {
            //                    CartItemModel cartitem = new CartItemModel() { ProductId = pid, Quantity = quantity, Name = product.Name, Price = Decimal.Round(product.Price, 2),Weight=product.Weight };
            //                    if (actives != null && actives.Any(p => p.Id == pid))
            //                    {
            //                        cartitem.Price = decimal.Round(product.Price - actives.FirstOrDefault(p => p.Id == pid).DiscountAmount, 2);
            //                    }
            //                    if (product.Pictures != null && product.Pictures.Any())
            //                    {
            //                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            //                        string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
            //                        ProductPicture defPic = product.Pictures.FirstOrDefault(p => p.IsDefault);
            //                        if (defPic == null)
            //                            defPic = product.Pictures.FirstOrDefault();
            //                        cartitem.Url = url + "/" + defPic.Picture;
            //                    }
            //                    else
            //                    {
            //                        cartitem.Url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
            //                    }
            //                    model.Items.Add(cartitem);
            //                }
            //            }
            //        }
            //    }
            //}
            return CartRepository.GetCart(Session.SessionID);
        }
        #endregion
    }

    public class ProductItem
    {
        public string Key { get; set; }
    }
}
