﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Repository;
using TotoBobo.Mvc.Models;
using TotoBobo.Mvc.Helper;

namespace TotoBobo.Mvc.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public ActionResult Index()
        {
            return RedirectToAction("Sale");
        }

        public ActionResult Cat()
        {
            ViewBag.Menu = "";
            ViewBag.Title = TotoBobo.Mvc.ConstantHelper.ApplicationName + " - Product";
            ViewBag.CurrentCategory = "Products";
            ProductCatModel model = new ProductCatModel()
            {
                Categories=new List<CategoryModel>(),
                Products=new List<ProductModel>(),
                Brands=new List<Brand>()
            };
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                List<Category> categories = CategoryRepository.GetAllCategories();
                List<CategoryModel> cms = categories.GetParentHiarachy();
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority+"/"+uploadfolder;
                    model.CategoryId = id;
                    foreach (CategoryModel cm in cms)
                    {
                        if (cm.Id == id)
                        {
                            ViewBag.Menu = cm.Name;
                            ViewBag.Title = TotoBobo.Mvc.ConstantHelper.ApplicationName + " - " + cm.Name;
                            ViewBag.CurrentCategory = cm.Name;
                            model.Categories = cm.Children;
                            model.Categories.Insert(0, new CategoryModel() { Id = cm.Id, Name = cm.Name });
                            Category cat = categories.FirstOrDefault(c => c.Id == cm.Id);
                            if (!string.IsNullOrEmpty(cat.TopBanner))
                            {
                                model.TopBanner = url + "/" + cat.TopBanner;
                            }
                            if (!string.IsNullOrEmpty(cat.Banner1))
                            {
                                model.Banner1 = url + "/" + cat.Banner1;
                            }
                            model.Banner1Link = cat.Banner1Link;
                            break;
                        }
                        else if (cm.Children != null && cm.Children.Any(c => c.Id == id||(c.Children!=null&&c.Children.Any(cc=>cc.Id==id))))
                        {
                            ViewBag.Menu = cm.Name;
                            ViewBag.Title = TotoBobo.Mvc.ConstantHelper.ApplicationName + " - " + cm.Name + " " + categories.FirstOrDefault(c => c.Id == id).Name;
                            ViewBag.CurrentCategory = categories.FirstOrDefault(c => c.Id == id).Name;
                            model.Categories = cm.Children;
                            model.Categories.Insert(0, new CategoryModel() { Id = cm.Id, Name = cm.Name });
                            Category cat = categories.FirstOrDefault(c => c.Id == cm.Id);
                            if (!string.IsNullOrEmpty(cat.TopBanner))
                            {
                                model.TopBanner = url + "/" + cat.TopBanner;
                            }
                            if (!string.IsNullOrEmpty(cat.Banner1))
                            {
                                model.Banner1 = url + "/" + cat.Banner1;
                            }
                            model.Banner1Link = cat.Banner1Link;
                            break;
                        }
                    }

                    if (id > 0)
                    {
                        List<Product> products = ProductRepository.GetProductsByCategory(id);
                        List<Product> actives = ProductRepository.GetActives();
                        if (products != null && products.Any())
                        {
                            foreach (Product prod in products)
                            {
                                if (prod.Brand != null&&!model.Brands.Any(b=>b.Id==prod.Brand.Id))
                                {
                                    model.Brands.Add(prod.Brand);
                                }
                                ProductModel pm = new ProductModel()
                                {
                                    Id=prod.Id,
                                    Name=prod.Name,
                                    Price=Decimal.Round(prod.Price,2),
                                    IsFeatured=prod.IsFeatured,
                                    Inventory=prod.Quantity,
                                    Pictures=new List<string>(),
                                    Tags=prod.Tags
                                };
                                if (actives != null && actives.Any(p => p.Id == prod.Id))
                                {
                                    pm.Price = decimal.Round(prod.Price - actives.FirstOrDefault(p => p.Id == prod.Id).DiscountAmount, 2);
                                    pm.IsSale = true;
                                }
                                //string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                //string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
                                if (prod.Pictures != null && prod.Pictures.Any())
                                {
                                    
                                    ProductPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                                    if (defPic == null)
                                        defPic = prod.Pictures.FirstOrDefault();
                                    pm.Uri = url + "/" + defPic.Picture;
                                    if (prod.Pictures.Count > 1)
                                        pm.Uri2 = url + "/" + prod.Pictures.ElementAt(1).Picture;
                                    else
                                        pm.Uri2 = pm.Uri;
                                    foreach (ProductPicture pp in prod.Pictures)
                                    {
                                        pm.Pictures.Add(url + "/" + pp.Picture);
                                    }
                                }
                                else
                                {
                                    pm.Uri = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                                    pm.Uri2 = pm.Uri;
                                    pm.Pictures.Add(pm.Uri);
                                }
                                model.Products.Add(pm);
                            }
                        }
                    }
                }
            }
            return View(model);
        }

        public ActionResult Sale()
        {
            ProductSaleModel model = new ProductSaleModel()
            {
                Categories=new List<CategoryModel>(),
                Products=new List<ProductModel>()
            };
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + uploadfolder;
            SaleBanner sb = SettingRepository.GetSaleBanner();
            if (!string.IsNullOrEmpty(sb.TopBanner))
            {
                model.TopBanner = url +"/"+ sb.TopBanner;
            }
            if (!string.IsNullOrEmpty(sb.Banner1))
            {
                model.Banner1 = url + "/" + sb.Banner1;
            }
            model.Banner1Link = sb.Banner1Link;
            List<Category> categories = CategoryRepository.GetAllCategories();
            List<CategoryModel> cms = categories.GetParentHiarachy();
            foreach (CategoryModel cm in cms)
            {
                model.Categories.Add(new CategoryModel() { Id = cm.Id, Name = cm.Name });
            }
            List<Product> products = ProductRepository.GetActives();
            if (products != null && products.Any())
            {
                foreach (Product prod in products)
                {
                    ProductModel pm = new ProductModel()
                    {
                        Id = prod.Id,
                        Name = prod.Name,
                        Price = Decimal.Round(prod.Price, 2),
                        IsFeatured = prod.IsFeatured,
                        Inventory = prod.Quantity,
                        Pictures = new List<string>(),
                        Tags=prod.Tags
                    };

                    if (prod.Pictures != null && prod.Pictures.Any())
                    {

                        ProductPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                        if (defPic == null)
                            defPic = prod.Pictures.FirstOrDefault();
                        pm.Uri = url + "/" + defPic.Picture;
                        if (prod.Pictures.Count > 1)
                            pm.Uri2 = url + "/" + prod.Pictures.ElementAt(1).Picture;
                        else
                            pm.Uri2 = pm.Uri;
                        foreach (ProductPicture pp in prod.Pictures)
                        {
                            pm.Pictures.Add(url + "/" + pp.Picture);
                        }
                    }
                    else
                    {
                        pm.Uri = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                        pm.Uri2 = pm.Uri;
                        pm.Pictures.Add(pm.Uri);
                    }
                    model.Products.Add(pm);
                }
            }
            return View(model);
        }

        public ActionResult Detail()
        {
            ViewBag.Title = ConstantHelper.ApplicationName+" - Product Detail";
            ProductDetailModel model = new ProductDetailModel()
            {
                Categories = new List<CategoryModel>(),
                Product = new ProductModel()
                {
                    Pictures = new List<string>()
                    {
                        HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png")
                    },
                    ProductPictures = new List<ProductPicture>()
                    {
                        new ProductPicture(){Picture=HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png")}
                    }
                },

                Tags = new List<ProductTag>(),
                Recommends = GetModelFromProduct(ProductRepository.GetRecommends()),
                Relates = new List<ProductModel>(),
                Items = new List<ProductSetItem>(),
                Accessories=new List<ProductModel>()
            };


            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    if (id > 0)
                    {
                        if (HttpContext.Request.QueryString["type"] == "hamper")
                        {
                            model.ProductType = "hamper";
                            ProductSet product = ProductRepository.GetHamper(id);
                            if (product != null)
                            {
                                model.Product.Id = product.Id;
                                model.Product.Name = product.Name;
                                model.Product.Price = Decimal.Round(product.Price, 2);
                                model.Product.Quantity = 1;
                                model.Product.Description = product.Description;
                                model.Product.Inventory = product.Items.Where(p => p.IsMandatory).Select(m => m.Product.Quantity).Min();
                                model.Items = product.Items;
                                model.Tags = null;

                                string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);

                                if (product.Pictures != null && product.Pictures.Any())
                                {
                                    model.Product.Pictures = new List<string>();
                                    model.Product.ProductPictures = new List<ProductPicture>();
                                    
                                    /*
                                    if (product.Pictures.Count > 4)
                                    {
                                        ProductSetPicture defPic = product.Pictures.FirstOrDefault(p => p.IsDefault);
                                        if (defPic == null)
                                            defPic = product.Pictures.FirstOrDefault();
                                        model.Product.Pictures.Add(url + "/" + defPic.Picture);
                                        model.Product.ProductPictures.Add(new ProductPicture() { Picture = url + "/" + defPic.Picture });
                                        foreach (ProductSetPicture pp in product.Pictures.Where(p => !p.IsDefault).Take(3))
                                        {
                                            model.Product.Pictures.Add(url + "/" + pp.Picture);
                                            model.Product.ProductPictures.Add(new ProductPicture() { Picture = url + "/" + pp.Picture });
                                        }
                                    }
                                    else
                                    */
                                    {
                                        foreach (ProductSetPicture pp in product.Pictures)
                                        {
                                            model.Product.Pictures.Add(url + "/" + pp.Picture);
                                            model.Product.ProductPictures.Add(new ProductPicture() { Picture = url + "/" + pp.Picture });
                                        }
                                    }
                                }
                                List<Category> categories = CategoryRepository.GetAllCategories();
                                List<CategoryModel> cms = categories.GetParentHiarachy();
                                foreach (CategoryModel cm in cms)
                                {
                                    model.Categories.Add(new CategoryModel() { Id = cm.Id, Name = cm.Name });
                                }
                                ViewBag.Menu = "Gift Set";

                                SaleBanner sb = SettingRepository.GetSaleBanner();
                                if (!string.IsNullOrEmpty(sb.TopBanner))
                                {
                                    model.TopBanner = url + "/" + sb.TopBanner;
                                }
                                if (!string.IsNullOrEmpty(sb.Banner1))
                                {
                                    model.Banner1 = url + "/" + sb.Banner1;
                                }
                                model.Banner1Link = sb.Banner1Link;
                            }
                        }
                        else
                        {
                            Product product = ProductRepository.GetProductById(id);

                            product.RelatedItems = ProductRepository.GetRandomRelatedItems(product);

                            if (product != null && !product.IsHidden)
                            {
                                ViewBag.Title = ConstantHelper.ApplicationName + " - " + product.Name;
                                model.Product.Id = product.Id;
                                model.Product.Name = product.Name;
                                model.Product.Price = Decimal.Round(product.Price - product.DiscountAmount, 2);
                                model.Product.Quantity = 1;
                                model.Product.Description = product.Description;
                                model.Product.Inventory = product.Quantity;
                                model.Relates = GetModelFromProduct(product.RelatedItems);
                                model.Tags = product.Tags;
                                model.Product.CountryName = product.Country != null ? product.Country.Name : "";
                                model.Product.CountryPicture = product.Country != null ? product.Country.Picture : "";
                                model.Accessories = GetModelFromProduct(product.Accessories);
                                if (!string.IsNullOrEmpty(model.Product.CountryPicture))
                                {
                                    model.Product.CountryPicture = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + model.Product.CountryPicture;
                                }
                                if (product.Pictures != null && product.Pictures.Any())
                                {
                                    model.Product.Pictures = new List<string>();
                                    model.Product.ProductPictures = new List<ProductPicture>();
                                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                    string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
                                    //if (product.Pictures.Count > 4)
                                    //{
                                    //    ProductPicture defPic = product.Pictures.FirstOrDefault(p => p.IsDefault);
                                    //    if (defPic == null)
                                    //        defPic = product.Pictures.FirstOrDefault();
                                    //    model.Product.Pictures.Add(url + "/" + defPic.Picture);
                                    //    model.Product.ProductPictures.Add(new ProductPicture() { Picture = url + "/" + defPic.Picture,Title=defPic.Title });
                                    //    foreach (ProductPicture pp in product.Pictures.Where(p => !p.IsDefault).Take(3))
                                    //    {
                                    //        model.Product.Pictures.Add(url + "/" + pp.Picture);
                                    //        model.Product.ProductPictures.Add(new ProductPicture() { Picture = url + "/" + pp.Picture,Title=pp.Title });
                                    //    }
                                    //}
                                    //else
                                    //{
                                        foreach (ProductPicture pp in product.Pictures)
                                        {
                                            model.Product.Pictures.Add(url + "/" + pp.Picture);
                                            model.Product.ProductPictures.Add(new ProductPicture() { Picture = url + "/" + pp.Picture,Title=pp.Title });
                                        }
                                   // }
                                }
                                List<Category> categories = CategoryRepository.GetAllCategories();
                                List<CategoryModel> cms = categories.GetParentHiarachy();
                                int catId = product.Category.Id;
                                foreach (CategoryModel cm in cms)
                                {
                                    if (cm.Id == catId)
                                    {
                                        ViewBag.Menu = cm.Name;
                                        model.Categories = cm.Children;
                                        model.Categories.Insert(0, new CategoryModel() { Id = cm.Id, Name = cm.Name });

                                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                        string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);

                                        Category cat = categories.FirstOrDefault(c => c.Id == cm.Id);
                                        if (!string.IsNullOrEmpty(cat.TopBanner))
                                        {
                                            model.TopBanner = url + "/" + cat.TopBanner;
                                        }
                                        if (!string.IsNullOrEmpty(cat.Banner1))
                                        {
                                            model.Banner1 = url + "/" + cat.Banner1;
                                        }
                                        model.Banner1Link = cat.Banner1Link;

                                        break;
                                    }
                                    else if (FindCategoryInChildren(catId, cm))
                                    {
                                        ViewBag.Menu = cm.Name;
                                        model.Categories = cm.Children;
                                        model.Categories.Insert(0, new CategoryModel() { Id = cm.Id, Name = cm.Name });

                                        string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                                        string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);

                                        Category cat = categories.FirstOrDefault(c => c.Id == cm.Id);
                                        if (!string.IsNullOrEmpty(cat.TopBanner))
                                        {
                                            model.TopBanner = url + "/" + cat.TopBanner;
                                        }
                                        if (!string.IsNullOrEmpty(cat.Banner1))
                                        {
                                            model.Banner1 = url + "/" + cat.Banner1;
                                        }
                                        model.Banner1Link = cat.Banner1Link;

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
                    }
                }
            }
            return View(model);
        }

        private bool FindCategoryInChildren(int id, CategoryModel cm)
        {
            if (cm.Children != null)
            {
                if (cm.Children.Any(c => c.Id == id))
                {
                    return true;
                }
                else
                {
                    foreach (var item in cm.Children)
                    {
                        if (FindCategoryInChildren(id, item))
                            return true;
                    }
                }
            }
            return false;
        }

        public ActionResult List()
        {
            ViewBag.Menu = "";
            ViewBag.Title = TotoBobo.Mvc.ConstantHelper.ApplicationName + " - ";
            ViewBag.CurrentCategory = "";
            ViewBag.CurrentBrand = "";
            ProductCatModel model = new ProductCatModel()
            {
                Categories = new List<CategoryModel>(),
                Products = new List<ProductModel>(),
                Brands = new List<Brand>()
            };
            int bid = 0;
            int cid = 0;
            if (HttpContext.Request.QueryString["bid"] != null)
            {
                if (!int.TryParse(HttpContext.Request.QueryString["bid"], out bid))
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            if(bid<=0)
                return RedirectToAction("Index", "Home");

            if (HttpContext.Request.QueryString["cid"] != null)
            {
                if (!int.TryParse(HttpContext.Request.QueryString["cid"], out cid))
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            model.BrandId = bid;

            List<Category> categories = CategoryRepository.GetAllCategories();
            List<CategoryModel> cms = categories.GetParentHiarachy();

            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + uploadfolder;
            model.CategoryId = cid;

            if (cid > 0)
            {
                
                foreach (CategoryModel cm in cms)
                {
                    if (cm.Id == cid)
                    {
                        ViewBag.Menu = cm.Name;
                        ViewBag.Title = TotoBobo.Mvc.ConstantHelper.ApplicationName + " - " + cm.Name;
                        ViewBag.CurrentCategory = cm.Name;
                        model.Categories = cm.Children;
                        model.Categories.Insert(0, new CategoryModel() { Id = cm.Id, Name = cm.Name });
                        Category cat = categories.FirstOrDefault(c => c.Id == cm.Id);
                        if (!string.IsNullOrEmpty(cat.TopBanner))
                        {
                            model.TopBanner = url + "/" + cat.TopBanner;
                        }
                        if (!string.IsNullOrEmpty(cat.Banner1))
                        {
                            model.Banner1 = url + "/" + cat.Banner1;
                        }
                        model.Banner1Link = cat.Banner1Link;
                        break;
                    }
                    else if (cm.Children != null && cm.Children.Any(c => c.Id == cid))
                    {
                        ViewBag.Menu = cm.Name;
                        ViewBag.Title = TotoBobo.Mvc.ConstantHelper.ApplicationName + " - " + cm.Name + " " + cm.Children.FirstOrDefault(c => c.Id == cid).Name;
                        ViewBag.CurrentCategory = cm.Children.FirstOrDefault(c => c.Id == cid).Name;
                        model.Categories = cm.Children;
                        model.Categories.Insert(0, new CategoryModel() { Id = cm.Id, Name = cm.Name });
                        Category cat = categories.FirstOrDefault(c => c.Id == cm.Id);
                        if (!string.IsNullOrEmpty(cat.TopBanner))
                        {
                            model.TopBanner = url + "/" + cat.TopBanner;
                        }
                        if (!string.IsNullOrEmpty(cat.Banner1))
                        {
                            model.Banner1 = url + "/" + cat.Banner1;
                        }
                        model.Banner1Link = cat.Banner1Link;
                        break;
                    }
                }
                
            }
            else
            {
                model.Categories = cms;
            }


            Brand brand = BrandRepository.GetBrand(bid);
            ViewBag.CurrentBrand = brand.Name;
            ViewBag.Title = ViewBag.Title + " " + brand.Name;
            

            // List<Product> products = ProductRepository.GetProducts(bid,cid);

            List<Product> productListForBrand = ProductRepository.GetProducts(bid, 0);
            List<Product> productListForCategory = ProductRepository.GetProductsByCategory(cid);
            List<Product> products = new List<Product>(productListForBrand.Intersect(productListForCategory, new Product.ProductEqualityComparer()));

            List<Product> actives = ProductRepository.GetActives();
            if (products != null && products.Any())
            {
                foreach (Product prod in products)
                {
                    ProductModel pm = new ProductModel()
                    {
                        Id = prod.Id,
                        Name = prod.Name,
                        Price = Decimal.Round(prod.Price, 2),
                        IsFeatured = prod.IsFeatured,
                        Inventory = prod.Quantity,
                        Pictures = new List<string>(),
                        Tags=prod.Tags
                    };
                    
                    if (actives != null && actives.Any(p => p.Id == prod.Id))
                    {
                        pm.Price = decimal.Round(prod.Price - actives.FirstOrDefault(p => p.Id == prod.Id).DiscountAmount, 2);
                        pm.IsSale = true;
                    }
                    if (prod.Brand != null && !model.Brands.Any(b => b.Id == prod.Brand.Id))
                    {
                        model.Brands.Add(prod.Brand);
                    }
                    if (prod.Pictures != null && prod.Pictures.Any())
                    {

                        ProductPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                        if (defPic == null)
                            defPic = prod.Pictures.FirstOrDefault();
                        pm.Uri = url + "/" + defPic.Picture;
                        if (prod.Pictures.Count > 1)
                            pm.Uri2 = url + "/" + prod.Pictures.ElementAt(1).Picture;
                        else
                            pm.Uri2 = pm.Uri;
                        foreach (ProductPicture pp in prod.Pictures)
                        {
                            pm.Pictures.Add(url + "/" + pp.Picture);
                        }
                    }
                    else
                    {
                        pm.Uri = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                        pm.Uri2 = pm.Uri;
                        pm.Pictures.Add(pm.Uri);
                    }
                    model.Products.Add(pm);
                }
            }  
            
            return View(model);
        }

        public List<ProductModel> GetModelFromProduct(List<Product> products)
        {
            List<ProductModel> list = new List<ProductModel>();
            if (products != null)
            {
                string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                foreach (Product prod in products)
                {
                    ProductModel pm = new ProductModel()
                    {
                        Id = prod.Id,
                        Name = prod.Name,
                        Price = Decimal.Round(prod.Price, 2),
                        IsFeatured = prod.IsFeatured,
                        Inventory = prod.Quantity,
                        Pictures = new List<string>(),
                        Tags=prod.Tags
                    };
                    
                    if (prod.Pictures != null && prod.Pictures.Any())
                    {

                        ProductPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                        if (defPic == null)
                            defPic = prod.Pictures.FirstOrDefault();
                        pm.Uri = url + "/" + defPic.Picture;
                        if (prod.Pictures.Count > 1)
                            pm.Uri2 = url + "/" + prod.Pictures.ElementAt(1).Picture;
                        else
                            pm.Uri2 = pm.Uri;
                        foreach (ProductPicture pp in prod.Pictures)
                        {
                            pm.Pictures.Add(url + "/" + pp.Picture);
                        }
                    }
                    else
                    {
                        pm.Uri = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                        pm.Uri2 = pm.Uri;
                        pm.Pictures.Add(pm.Uri);
                    }
                    list.Add(pm);
                }
            }
            return list;
        }

        public ActionResult GiftSet()
        {
            GiftSetModel model = new GiftSetModel()
            {
                Categories = new List<CategoryModel>(),
                Products = new List<ProductModel>()
            };
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + uploadfolder;

            SaleBanner sb = SettingRepository.GetSaleBanner();
            if (!string.IsNullOrEmpty(sb.TopBanner))
            {
                model.TopBanner = url + "/" + sb.TopBanner;
            }
            if (!string.IsNullOrEmpty(sb.Banner1))
            {
                model.Banner1 = url + "/" + sb.Banner1;
            }
            model.Banner1Link = sb.Banner1Link;

            List<Category> categories = CategoryRepository.GetAllCategories();
            List<CategoryModel> cms = categories.GetParentHiarachy();
            foreach (CategoryModel cm in cms)
            {
                model.Categories.Add(new CategoryModel() { Id = cm.Id, Name = cm.Name });
            }
            List<ProductSet> products = ProductRepository.GetHampers();
            if (products != null && products.Any())
            {
                foreach (ProductSet prod in products)
                {
                    ProductModel pm = new ProductModel()
                    {
                        Id = prod.Id,
                        Name = prod.Name,
                        Price = Decimal.Round(prod.Price, 2),
                        Pictures = new List<string>(),
                        Inventory=prod.Items.Where(p=>p.IsMandatory).Select(m=>m.Product.Quantity).Min(),
                        ProductSetItems = prod.Items
                    };

                    if (prod.Pictures != null && prod.Pictures.Any())
                    {

                        ProductSetPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                        if (defPic == null)
                            defPic = prod.Pictures.FirstOrDefault();
                        pm.Uri = url + "/" + defPic.Picture;
                        if (prod.Pictures.Count > 1)
                            pm.Uri2 = url + "/" + prod.Pictures.ElementAt(1).Picture;
                        else
                            pm.Uri2 = pm.Uri;
                        foreach (ProductSetPicture pp in prod.Pictures)
                        {
                            pm.Pictures.Add(url + "/" + pp.Picture);
                        }
                    }
                    else
                    {
                        pm.Uri = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                        pm.Uri2 = pm.Uri;
                        pm.Pictures.Add(pm.Uri);
                    }
                    model.Products.Add(pm);
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(SearchProductModel model)
        {
            return RedirectToAction("Result", new { s=model.Name});
        }

        public ActionResult Result()
        {
            ProductCatModel model = new ProductCatModel()
            {
                Categories = new List<CategoryModel>(),
                Products = new List<ProductModel>(),
                Brands = new List<Brand>()
            };
            List<Category> categories = CategoryRepository.GetAllCategories();
            List<CategoryModel> cms = categories.GetParentHiarachy();
            foreach (CategoryModel cm in cms)
            {
                model.Categories.Add(new CategoryModel() { Id = cm.Id, Name = cm.Name });
            }
            string keyword = HttpContext.Request.QueryString["s"];
            if (keyword != null)
            {
                keyword=keyword.ToUpper();
                List<Product> products = ProductRepository.GetAllProducts();
                IEnumerable<Product> result = products.Where(p => p.Name.ToUpper().Contains(keyword)&&!p.IsHidden);
                if (result != null)
                {
                    string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                    string url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + uploadfolder;
                    List<Product> actives = ProductRepository.GetActives();
                    foreach (Product prod in result)
                    {
                        ProductModel pm = new ProductModel()
                        {
                            Id = prod.Id,
                            Name = prod.Name,
                            Price = Decimal.Round(prod.Price, 2),
                            IsFeatured = prod.IsFeatured,
                            Inventory = prod.Quantity,
                            Pictures = new List<string>(),
                            Tags = prod.Tags
                        };
                        if (actives != null && actives.Any(p => p.Id == prod.Id))
                        {
                            pm.Price = decimal.Round(prod.Price - actives.FirstOrDefault(p => p.Id == prod.Id).DiscountAmount, 2);
                            pm.IsSale = true;
                        }
                        //string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
                        //string url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
                        if (prod.Pictures != null && prod.Pictures.Any())
                        {

                            ProductPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                            if (defPic == null)
                                defPic = prod.Pictures.FirstOrDefault();
                            pm.Uri = url + "/" + defPic.Picture;
                            if (prod.Pictures.Count > 1)
                                pm.Uri2 = url + "/" + prod.Pictures.ElementAt(1).Picture;
                            else
                                pm.Uri2 = pm.Uri;
                            foreach (ProductPicture pp in prod.Pictures)
                            {
                                pm.Pictures.Add(url + "/" + pp.Picture);
                            }
                        }
                        else
                        {
                            pm.Uri = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/images/assets/not_available-generic.png");
                            pm.Uri2 = pm.Uri;
                            pm.Pictures.Add(pm.Uri);
                        }
                        model.Products.Add(pm);
                    }
                }
            }
            return View(model);
        }
    }
}
