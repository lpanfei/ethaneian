﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TotoBobo.Mvc.Models;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Repository;
using TotoBobo.Mvc.Helper;
using System.Web.Routing;

namespace TotoBobo.Mvc.Controllers
{
    public class HomeController : Controller
    {
        // GET: /Home/
        public ActionResult Index()
        {
            HomeModel model = new HomeModel() 
            { 
                HomeBanners=new List<HomeBanner>(),
                NewArraivals=new List<ProductModel>(),
                FeaturedProductRows=new List<FeatureProductRow>(),
                Brands=new List<Brand>()
            };
            string uploadfolder = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"];
            int newArrivalsLimit = 7;
            int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewArrivalDaysPeriod"], out newArrivalsLimit);
            string url = "";
            if (HttpContext.Request.Url.PathAndQuery == "/")
            {
                url = HttpContext.Request.Url.AbsoluteUri + uploadfolder;
            }
            else
            {
                url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.PathAndQuery, "/" + uploadfolder);
            }
            List<HomeBanner> hbs = BannerRepository.GetHomeBanners();
            if (hbs != null && hbs.Any())
            {
                foreach (HomeBanner hb in hbs)
                {
                    hb.TopBanner = url + "/" + hb.TopBanner;
                    hb.Banner1 = url + "/" + hb.Banner1;
                    hb.Banner2 = url + "/" + hb.Banner2;
                    hb.Banner3 = url + "/" + hb.Banner3;
                    model.HomeBanners.Add(hb);
                }
            }

            List<Brand> brands = BrandRepository.GetBrands();
            if (brands != null && brands.Any())
            {
                foreach (Brand brand in brands)
                {
                    brand.Logo = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + brand.Logo;
                    model.Brands.Add(brand);
                }
            }

            model.Brands = new List<Brand>(model.Brands.OrderBy(i => i.Name));

            List<Product> products = ProductRepository.GetAllProducts();
            List<Product> actives = ProductRepository.GetActives();
            if (products != null && products.Any())
            {
                foreach (Product prod in products)
                {
                    if (prod.IsHidden)
                        continue;
                    if (prod.IsFeatured || prod.CreatedDate > DateTime.Now.AddDays(-newArrivalsLimit))
                    {
                        ProductModel pm = new ProductModel()
                        {
                            Id = prod.Id,
                            Name = prod.Name,
                            Price = Decimal.Round(prod.Price, 2),
                            Pictures = new List<string>(),
                            Inventory=prod.Quantity,
                            Tags=prod.Tags
                        };
                        if (actives != null && actives.Any(p => p.Id == prod.Id))
                        {
                            pm.Price = decimal.Round(prod.Price - actives.FirstOrDefault(p => p.Id == prod.Id).DiscountAmount, 2);
                            pm.IsSale = true;
                        }
                        if (prod.Pictures != null && prod.Pictures.Any())
                        {

                            ProductPicture defPic = prod.Pictures.FirstOrDefault(p => p.IsDefault);
                            if (defPic == null)
                                defPic = prod.Pictures.FirstOrDefault();
                            pm.Uri = url + "/" + defPic.Picture;
                            if (prod.Pictures.Count > 1)
                                pm.Uri2 = url + "/" + prod.Pictures.ElementAt(1).Picture;
                            else
                                pm.Uri2 = pm.Uri;
                            foreach (ProductPicture pp in prod.Pictures)
                            {
                                pm.Pictures.Add(url + "/" + pp.Picture);
                            }
                        }
                        else
                        {
                            pm.Uri = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority+ "/images/assets/not_available-generic.png";
                            pm.Uri2 = pm.Uri;
                            pm.Pictures.Add(pm.Uri);
                        }
                        if (prod.IsFeatured)
                        {
                            if (model.FeaturedProductRows.Any(r => r.FeaturedProducts.Count < 4))
                            {
                                model.FeaturedProductRows.FirstOrDefault(r => r.FeaturedProducts.Count < 4).FeaturedProducts.Add(pm);
                            }
                            else
                            {
                                FeatureProductRow fpm = new FeatureProductRow() { FeaturedProducts=new List<ProductModel>()};
                                fpm.FeaturedProducts.Add(pm);
                                model.FeaturedProductRows.Add(fpm);
                            }
                        }

                        if (prod.CreatedDate > DateTime.Now.AddDays(-newArrivalsLimit))
                            model.NewArraivals.Add(pm);
                    }
                }
            }

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DeleteFAQ()
        {
            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id))
                {
                    QuestionRepository.DeleteQuestion(id);                    
                }
            }

            return RedirectToAction("Manage", "Account");
        }

        public ActionResult FAQ(int? questionTypeId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            List<QuestionType> types = QuestionRepository.GetTypes();
            if (types != null && types.Any())
            {
                foreach (QuestionType type in types)
                {
                    items.Add(new SelectListItem() { Value=type.Id.ToString(),Text=type.Name});
                }
            }
            ViewData["TypeId"] = items;
            FAQModel model = new FAQModel();

            if (questionTypeId == null)
            {
                model.Questions = QuestionRepository.GetQuestions();
            }
            else
            {
                List<Question> list = QuestionRepository.GetQuestions();
                
                model.Questions = new List<Question>();
                if (list != null && list.Count > 0)
                {
                    foreach (Question question in list)
                    {
                        if (question.QuestionType.Id == questionTypeId)
                        {
                            model.Questions.Add(question);
                        }
                    }
                }
            }

            model.Doctors = DoctorRepository.GetDoctors();
            List<SelectListItem> doctors = new List<SelectListItem>();
            doctors.Add(new SelectListItem() { Value="0",Selected=true,Text="None"});
            if (model.Doctors != null)
            {
                foreach (Doctor doc in model.Doctors)
                {
                    doctors.Add(new SelectListItem() { Value = doc.Id.ToString(),  Text = doc.Name+" | "+doc.Title });
                    doc.Avator = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + doc.Avator;
                }
            }
            ViewData["DoctorId"] = doctors;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FAQ(FAQModel model)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            List<QuestionType> types = QuestionRepository.GetTypes();
            if (types != null && types.Any())
            {
                foreach (QuestionType type in types)
                {
                    items.Add(new SelectListItem() { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            ViewData["TypeId"] = items;
            if (ModelState.IsValid)
            {
                Question question = new Question() { Doctor = new Doctor() { Id = model.DoctorId }, Id = 0, Title = model.Title, Description = model.Description, Email = model.Email, Phone = model.Phone, CreatedBy = new User() { UserName = User.Identity.Name }, LastModifiedBy = new User() { UserName = User.Identity.Name }, QuestionType = new QuestionType() { Id = model.TypeId } };
                QuestionRepository.AddQuestion(question);
                if (items.Any())
                {
                    items.FirstOrDefault().Selected = true;
                }

                if (string.IsNullOrWhiteSpace(ConstantHelper.RepliedTo) == false)
                {
                    Doctor doctor = DoctorRepository.GetDoctor(model.DoctorId);
                    if (doctor != null)
                    {
                        User doctorUser = UserRepository.GetUserById(doctor.UserId);
                        if (doctorUser != null)
                        {
                            if (string.IsNullOrWhiteSpace(doctorUser.Email) == false)
                            {
                                string url = Url.Action("FAQDetail", "Home", new RouteValueDictionary(new { id = question.Id }));
                                url = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, url);
                                MailManager.Send(ConstantHelper.RepliedTo, doctorUser.Email, "A new question is assigned to you", "A new question is assigned to you: " + url, true);
                            }
                        }
                    }
                }
            }
            List<SelectListItem> doctors = new List<SelectListItem>();
            doctors.Add(new SelectListItem() { Value = "0", Selected = true, Text = "None" });
            model.Questions = QuestionRepository.GetQuestions();
            model.Doctors = DoctorRepository.GetDoctors();
            if (model.Doctors != null)
            {
                foreach (Doctor doc in model.Doctors)
                {
                    doctors.Add(new SelectListItem() { Value = doc.Id.ToString(),  Text = doc.Name + " | " + doc.Title });
                    doc.Avator = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + doc.Avator;
                }
            }
            ViewData["DoctorId"] = doctors;
            return View(model);
        }

        public ActionResult FAQDetail(int? questionId)
        {
            if (questionId.HasValue)
            {
                RouteData.Values.Add("id", questionId.Value);
            }

            if (RouteData.Values.Any(v => v.Key == "id"))
            {
                int id = 0;
                if (int.TryParse(RouteData.Values["id"].ToString(), out id) && id > 0)
                {
                    Question q = QuestionRepository.GetQuestion(id);
                    if (q != null)
                    {
                        List<Answer> replies = new List<Answer>();
                        int creatorid = q.User != null ? q.User.UserId : 0;
                        if (q.Replies != null && q.Replies.Any())
                        {

                            int bestid = q.Replies.Any(r => r.IsBestAnswer) ? q.Replies.FirstOrDefault(r => r.IsBestAnswer).Id : 0;
                            if (bestid != 0)
                            {
                                var best = q.Replies.FirstOrDefault(r => r.IsBestAnswer);
                                best.QuestionCreatorId = creatorid;
                                best.BestId = bestid;
                                if (!string.IsNullOrEmpty(best.RepliedBy.Avator))
                                {
                                    best.RepliedBy.Avator = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + best.RepliedBy.Avator;
                                }
                                AddReplies(best, q.Replies);
                                replies.Add(best);
                            }
                            foreach (Answer item in q.Replies.OrderByDescending(r=>r.LikeCount))
                            {
                                if (item.RepliedTo == null&&!item.IsBestAnswer)
                                {
                                    item.QuestionCreatorId = creatorid;
                                    item.BestId=bestid;
                                    if (!string.IsNullOrEmpty(item.RepliedBy.Avator))
                                    {
                                        item.RepliedBy.Avator = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + item.RepliedBy.Avator;
                                    }
                                    replies.Add(item);
                                    AddReplies(item, q.Replies);
                                }
                            }
                        }
                        q.Replies = replies;
                        FAQModel model = new FAQModel() { Question = q };
                        model.Doctors = DoctorRepository.GetDoctors();
                        if (model.Doctors != null)
                        {
                            foreach (Doctor doc in model.Doctors)
                            {
                                doc.Avator = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + doc.Avator;
                            }
                        }
                        return View(model);
                    }
                }
            }
            return RedirectToAction("FAQ");
        }

        private void AddReplies(Answer parent, List<Answer> list)
        {
            if (parent.Replies == null)
                parent.Replies = new List<Answer>();
            foreach (Answer item in list.Where(c => c.RepliedTo!=null&&c.RepliedTo.Id == parent.Id))
            {
                item.RepliedBy.Avator = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + item.RepliedBy.Avator;
                parent.Replies.Add(item);
                AddReplies(item, list);
            }
        }

        public ActionResult SearchQA()
        {
            return RedirectToAction("FAQ");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchQA(FAQModel model)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            List<QuestionType> types = QuestionRepository.GetTypes();
            if (types != null && types.Any())
            {
                foreach (QuestionType type in types)
                {
                    items.Add(new SelectListItem() { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            ViewData["TypeId"] = items;
            FAQModel model2 = new FAQModel();
            List<Question> list= QuestionRepository.GetQuestions();
            if (string.IsNullOrEmpty(model.Keywords))
            {
                model2.Questions = list;
            }else if (list != null)
            {
                var result = list.Where(l => l.Title.ToUpper().Contains(model.Keywords.ToUpper()));
                if (result != null)
                {
                    model2.Questions = result.ToList();
                }
            }

            List<SelectListItem> doctors = new List<SelectListItem>();
            doctors.Add(new SelectListItem() { Value = "0", Selected = true, Text = "None" });
            model.Questions = QuestionRepository.GetQuestions();
            model.Doctors = DoctorRepository.GetDoctors();
            if (model.Doctors != null)
            {
                foreach (Doctor doc in model.Doctors)
                {
                    doctors.Add(new SelectListItem() { Value = doc.Id.ToString(), Text = doc.Name + " | " + doc.Title });
                    doc.Avator = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFolder"] + "/" + doc.Avator;
                }
            }
            ViewData["DoctorId"] = doctors;

            ModelState.Remove("Title");
            ModelState.Remove("Email");
            ModelState.Remove("Description");
            return View("FAQ",model2);
            
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        public ActionResult DisclaimerAndLegalNotice()
        {
            return View();
        }

        public ActionResult DataProtectionPolicy()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Like()
        {
            int id = 0;
            int.TryParse(RouteData.Values["id"].ToString(), out id);
            if (id > 0)
            {
                Answer ans = QuestionRepository.GetAnswer(id);
                if (ans != null)
                {
                    ans.LikeCount += 1;
                    QuestionRepository.Reply(ans);
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { Success=false}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Dislike()
        {
            int id = 0;
            int.TryParse(RouteData.Values["id"].ToString(), out id);
            if (id > 0)
            {
                Answer ans = QuestionRepository.GetAnswer(id);
                if (ans != null)
                {
                    ans.DislikeCount += 1;
                    QuestionRepository.Reply(ans);
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChooseBest()
        {
            int id = 0;
            int.TryParse(RouteData.Values["id"].ToString(), out id);
            int pid = 0;
            int.TryParse(Request.QueryString["pid"], out pid);
            if (id > 0)
            {
                Answer ans = QuestionRepository.GetAnswer(id);
                if (ans != null)
                {
                    if (pid > 0)
                    {
                        Answer prev = QuestionRepository.GetAnswer(pid);
                        if (prev == null)
                        {
                            return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                        }
                        prev.IsBestAnswer = false;
                        QuestionRepository.Reply(prev);
                    }
                    ans.IsBestAnswer=true;
                    QuestionRepository.Reply(ans);
                    
                    return Json(new { Success = true,Id=id }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
        }
    }
}