﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TotoBobo.Data;

namespace TotoBobo.Mvc
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            DBInitializer.Initialize(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            MasterDataConfig.InitializeMasterData();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["init"] = 0;
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (Response.Cookies.Count > 0)
            {
                foreach (string s in Response.Cookies.AllKeys)
                {
                    //if (s.ToLower() == Session.SessionID)
                    //{
                    //     Response.Cookies[s].Secure = true;
                    //}
                }
            }
        }
    }
}