﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Models;

namespace TotoBobo.Mvc.Helper
{
    public static class CategoryHelper
    {
        public static List<Category> GetHiarachy(this List<Category> cats)
        {
            List<Category> copies = new List<Category>();
            cats.OrderByDescending(cc => cc.Id).ToList().ForEach(c => { copies.Add(new Category() { Id = c.Id, Name = c.Name, ParentId = c.ParentId }); });

            List<Category> list = new List<Category>();
            foreach (Category cat in copies.Where(c => c.ParentId == 0))
            {
                list.Add(cat);
                AddSubCat(cat, list, copies, "├─");
            }
            return list;
        }

        public static void AddSubCat(Category parent, List<Category> hlist, List<Category> cats, string verb)
        {
            foreach (Category cat in cats.Where(c => c.ParentId == parent.Id))
            {
                cat.Name = verb + cat.Name;
                hlist.Add(cat);
                AddSubCat(cat, hlist, cats, verb + "──");
            }
        }

        public static List<CategoryModel> GetParentHiarachy(this List<Category> cats)
        {
            List<CategoryModel> list = new List<CategoryModel>();
            if (cats != null && cats.Any())
            {
                List<Category> copies = new List<Category>();
                cats.OrderBy(cc => cc.Id).ToList().ForEach(c => { copies.Add(new Category() { Id = c.Id, Name = c.Name, ParentId = c.ParentId }); });

                foreach (Category cat in copies.Where(c => c.ParentId == cats.Min(c2 => c2.ParentId)))
                {
                    CategoryModel cm = new CategoryModel() { Id = cat.Id, Name = cat.Name,Children=new List<CategoryModel>() };
                    AddChildren(cm, copies);
                    list.Add(cm);
                }

            }
            return list;
        }

        public static void AddChildren(CategoryModel parent, List<Category> cats)
        {
            if (cats.Any(c => c.ParentId == parent.Id))
            {
                foreach (Category cat in cats.Where(c => c.ParentId == parent.Id))
                {
                    CategoryModel cm = new CategoryModel() { Id = cat.Id, Name = cat.Name, Children = new List<CategoryModel>() };
                    parent.Children.Add(cm);
                    AddChildren(cm, cats);
                    
                }
            }
        }
    }
}