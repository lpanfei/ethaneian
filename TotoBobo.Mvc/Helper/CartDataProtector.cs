﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace TotoBobo.Mvc.Helper
{
    public static class CartDataProtector
    {
        private static byte[] key;
        private static byte[] Key
        {
            get
            {
                if (key == null || key.Length <= 0)
                {
                    SetKeyAndIV();
                }
                return key;
            }
        }

        private static byte[] iv;
        private static byte[] IV
        {
            get
            {
                if (iv == null || iv.Length <= 0)
                {
                    SetKeyAndIV();
                }
                return iv;
            }
        }

        private static void SetKeyAndIV()
        {
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                key = aesAlg.Key;
                iv = aesAlg.IV;
            }
        }

        public static string EncryptStringToBytes_Aes(string plainText)
        {
            // Check arguments. 
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException("plainText");
            
            byte[] encrypted;
            // Create an AesCryptoServiceProvider object 
            // with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < encrypted.Length; i++)
            {
                sBuilder.Append(encrypted[i].ToString("x2"));
            }
            // Return the encrypted bytes from the memory stream. 
            return sBuilder.ToString();

        }

        public static string DecryptStringFromBytes_Aes(string encrypted)
        {
            // Check arguments. 
            if(string.IsNullOrEmpty(encrypted))
                throw new ArgumentNullException("encrypted");
            byte[] cipherText = StringToByteArray(encrypted);
           
            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an AesCryptoServiceProvider object 
            // with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
    
}