﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotoBobo.Data.Objects;
using TotoBobo.Mvc.Repository;

namespace TotoBobo.Mvc
{
    public static class ConstantHelper
    {
        public static string AdminRole = "Admin";
        public static string SalesRole = "Sales";
        public static string CustomerRole = "Customers";
        public static string QAAdminRole = "QAAdmin";
        public static decimal ShippingFee = 5;
        public static decimal PriceDivision = 50;

        public static string GetOrderNumber(int id)
        {
            long seed = 1000000000;
            return (seed + id).ToString();
        }

        public static decimal ToDollarFromRupiah(this decimal amount,decimal exchangerate)
        {
            //if (exchangerate > 0)
            //    return amount / exchangerate;
            return amount;
        }

        public static string ApplicationName
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["ApplicationName"]; 
            }
        }

        public static string CurrencySymbol
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["CurrencySymbol"];
            }
        }

        public static string RepliedTo
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["RepliedToEmailAddress"];
            }
        }
    }
}