﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace TotoBobo.Mvc.Helper
{
    public class MailManager
    {
        public static void Send(string fromAddress, string toAddress, string subject, string body, bool isBodyHtml)
        {
            MailMessage message = new MailMessage(fromAddress, toAddress, subject, body);
            message.IsBodyHtml = isBodyHtml;
            Send(message);
        }

        public static void Send(string fromAddress, List<string> toAddressList, string subject, string body, bool isBodyHtml)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromAddress);
            foreach (string s in toAddressList)
            {
                message.To.Add(s);
            }
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = isBodyHtml;
            Send(message);
        }

        public static void Send(MailMessage message)
        {
            try
            {
                using (SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["MailServer"]))
                {
                    client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["MailCredentialUserName"], ConfigurationManager.AppSettings["MailCredentialPassword"]);
                    client.Send(message);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }

    public static class MailTemplates
    {
        public static string GetTemplates(MailType mt)
        {
            //right now, it's hardcoded. Go ahead we should make it configurable in web.config or somewhere else
            string template = "";
            switch (mt)
            {
                case MailType.AccountConfirm:
                    
                    break;
                case MailType.PasswordReset:
                    break;
            }
            return template;
        }
    }

    public enum MailType
    {
        AccountConfirm,
        PasswordReset
    }
}