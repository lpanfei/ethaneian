USE [Ethaneian]
GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Logo] [nvarchar](max) NOT NULL,
	[IsEnabled] [bit] NULL,
 CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[Picture] [nvarchar](255) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ParentId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[TopBanner] [nvarchar](255) NULL,
	[Banner1] [nvarchar](255) NULL,
	[Banner1Link] [nvarchar](max) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CartItem]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CartItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [nvarchar](255) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Tags] [xml] NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[IsHamper] [bit] NULL,
 CONSTRAINT [PK_CartItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Captcha]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Captcha](
	[Guid] [nvarchar](255) NOT NULL,
	[Text] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Brand]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Logo] [nvarchar](max) NOT NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Newsletter]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newsletter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[SentType] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[IsEnabled] [bit] NULL,
 CONSTRAINT [PK_Newsletter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HomeBanner]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeBanner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TopBanner] [nvarchar](255) NOT NULL,
	[TopBannerLink] [nvarchar](max) NULL,
	[Banner1] [nvarchar](255) NOT NULL,
	[Banner1Link] [nvarchar](max) NULL,
	[Banner2] [nvarchar](255) NOT NULL,
	[Banner2Link] [nvarchar](max) NULL,
	[Banner3] [nvarchar](255) NULL,
	[Banner3Link] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_HomeBanner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Doctor]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doctor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[Avator] [nvarchar](max) NOT NULL,
	[UserId] [int] NOT NULL,
	[Url] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Doctor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliveryMethod]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Logo] [nvarchar](max) NOT NULL,
	[IsEnabled] [bit] NULL,
 CONSTRAINT [PK_DeliveryMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSetPicture]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSetPicture](
	[ProductSetId] [int] NOT NULL,
	[Picture] [nvarchar](max) NOT NULL,
	[IsDefault] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSetItem]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSetItem](
	[ProductSetId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[IsMandatory] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSet]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Price] [money] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[IsDeleted] [bit] NULL,
	[OptionalCount] [int] NOT NULL,
 CONSTRAINT [PK_ProductSet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExchangeRate] [decimal](18, 10) NOT NULL,
	[TaxRate] [decimal](18, 10) NULL,
	[GiftSetDiscount] [decimal](18, 10) NULL,
	[RepeatCustomerDiscount] [decimal](18, 10) NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SaleBanner]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaleBanner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TopBanner] [nvarchar](255) NULL,
	[Banner1] [nvarchar](255) NULL,
	[Banner1Link] [nvarchar](max) NULL,
 CONSTRAINT [PK_SaleBanner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RelatedProduct]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelatedProduct](
	[ProductId] [int] NOT NULL,
	[RelatedId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionType]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_QuestionType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spSelHomeBanners]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelHomeBanners]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.HomeBanner
	ORDER BY [Order]
	FOR XML PATH('HomeBanner'), ROOT('HomeBannerData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelHomeBannerById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelHomeBannerById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.HomeBanner
	WHERE Id=@id
	FOR XML PATH('HomeBanner'), ROOT('HomeBannerData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelDoctors]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelDoctors]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Doctor
	FOR XML PATH('Doctor'), ROOT('DoctorData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelDoctorById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelDoctorById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Doctor WHERE Id=@id
	FOR XML PATH('Doctor'), ROOT('DoctorData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelDeliveryMethods]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelDeliveryMethods]  
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.DeliveryMethod
	FOR XML PATH('DeliveryMethod'), ROOT('DeliveryMethodData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelDeliveryMethodByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelDeliveryMethodByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.DeliveryMethod
	WHERE Name=@name
	FOR XML PATH('DeliveryMethod'), ROOT('DeliveryMethodData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelDeliveryMethodById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spSelDeliveryMethodById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.DeliveryMethod
	WHERE Id=@id
	FOR XML PATH('DeliveryMethod'), ROOT('DeliveryMethodData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCountryByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCountryByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Country
	WHERE Name=@name AND ISNULL(IsDeleted,0)=0
	FOR XML PATH('Country'), ROOT('CountryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCountry]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCountry]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Country
	WHERE Id=@id AND ISNULL(IsDeleted,0)=0
	FOR XML PATH('Country'), ROOT('CountryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCountries]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCountries] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Country
	WHERE ISNULL(IsDeleted,0)=0
	FOR XML PATH('Country'), ROOT('CountryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCategoryByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCategoryByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	Id,Name,ParentId,TopBanner,Banner1,Banner1Link,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE)
	,CreatedDate,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	LastModifiedDate
	FROM dbo.Category
	WHERE Name like '%'+@name+'%' and IsNULL(IsDeleted,0)<>1
	FOR XML PATH('Category'), ROOT('CategoryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCategoryById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCategoryById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	Id,Name,ParentId,TopBanner,Banner1,Banner1Link,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE)
	,CreatedDate,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	LastModifiedDate
	FROM dbo.Category
	WHERE Id=@id AND IsNULL(IsDeleted,0)<>1
	FOR XML PATH('Category'), ROOT('CategoryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelAllCategories]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelAllCategories]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	Id,Name,ParentId,TopBanner,Banner1,Banner1Link,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE)
	,CreatedDate,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	LastModifiedDate
	FROM dbo.Category
	WHERE IsNULL(IsDeleted,0)<>1
	FOR XML PATH('Category'), ROOT('CategoryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelPaymentMethods]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelPaymentMethods] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.PaymentMethod
	FOR XML PATH('PaymentMethod'), ROOT('PaymentMethodData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelPaymentMethodByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelPaymentMethodByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.PaymentMethod
	WHERE Name=@name
	FOR XML PATH('PaymentMethod'), ROOT('PaymentMethodData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelPaymentMethodById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spSelPaymentMethodById] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.PaymentMethod
	WHERE Id=@id
	FOR XML PATH('PaymentMethod'), ROOT('PaymentMethodData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelProductSetByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductSetByName]
	-- Add the parameters for the stored procedure here
	@name NVARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id,Name FROM dbo.ProductSet WHERE Name=@name
	FOR XML PATH('ProductSet'), ROOT('ProductSetData')

END
GO
/****** Object:  StoredProcedure [dbo].[spSelQuestionTypes]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelQuestionTypes]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.QuestionType
	FOR XML PATH('QuestionType'), ROOT('QuestionTypeData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelQuestionTypeById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelQuestionTypeById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.QuestionType
	WHERE Id=@id
	FOR XML PATH('QuestionType'), ROOT('QuestionTypeData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelUserByEmail]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelUserByEmail]
	-- Add the parameters for the stored procedure here
	@email nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.[User] WHERE Email=@email
	FOR XML PATH('User'), ROOT('UserData')
END
GO
/****** Object:  StoredProcedure [dbo].[spUpdCategoryBanner]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdCategoryBanner]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@TopBanner NVARCHAR(255),
	@Banner1 NVARCHAR(255),
	@Banner1Link NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    BEGIN TRANSACTION
	BEGIN TRY
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Category
		SET
			TopBanner=@TopBanner,
			Banner1=@Banner1,
			Banner1Link=@Banner1Link
		WHERE
			Id=@Id
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  Table [dbo].[UserAddress]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAddress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[TelephoneNo] [nvarchar](50) NULL,
	[Postcode] [nvarchar](50) NOT NULL,
	[Country] [nvarchar](255) NOT NULL,
	[State] [nvarchar](255) NOT NULL,
	[City] [nvarchar](255) NOT NULL,
	[Address1] [nvarchar](max) NOT NULL,
	[Address2] [nvarchar](max) NULL,
	[IsDefault] [bit] NULL,
 CONSTRAINT [PK_UserAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[CountryId] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spSelSetting]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelSetting]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Setting
	FOR XML PATH('Setting'), ROOT('SettingData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelSaleBanner]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelSaleBanner]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.SaleBanner
	FOR XML PATH('SaleBanner'), ROOT('SaleBannerData')
END
GO
/****** Object:  Table [dbo].[Question]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [int] NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[Phone] [nvarchar](50) NULL,
	[Answer] [nvarchar](max) NULL,
	[DoctorId] [int] NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spDelCountry]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelCountry]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Country SET IsDeleted=1 WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spDelHomeBanner]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelHomeBanner]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
    DECLARE @order INT
    SELECT @order=[Order] FROM dbo.HomeBanner WHERE Id=@id
	DELETE FROM dbo.HomeBanner WHERE Id=@id
	UPDATE dbo.HomeBanner SET [Order]=[Order]-1 WHERE [Order]>@order
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spDelDoctor]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelDoctor]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.Doctor WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spDelProductSet]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelProductSet]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.ProductSet SET IsDeleted=1 WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spClearCartItems]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spClearCartItems]
	-- Add the parameters for the stored procedure here
	@sessionId NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.CartItem WHERE SessionId=@sessionId
END
GO
/****** Object:  StoredProcedure [dbo].[spDelCartItem]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelCartItem] 
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.CartItem WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spDelCaptcha]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelCaptcha]
	-- Add the parameters for the stored procedure here
	@Guid NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.Captcha WHERE [Guid]=@Guid
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdHomeBanner]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdHomeBanner]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @TopBanner NVARCHAR(255)
	DECLARE @Banner1 NVARCHAR(255)
	DECLARE @Banner2 NVARCHAR(255)
	DECLARE @Banner3 NVARCHAR(255)
	DECLARE @TopBannerLink NVARCHAR(max)
	DECLARE @Banner1Link NVARCHAR(max)
	DECLARE @Banner2Link NVARCHAR(max)
	DECLARE @Banner3Link NVARCHAR(max)
	DECLARE @Order INT
	
	SET @Id =  @xml.value('(/HomeBanner/Id)[1]', 'int' )
	SET @TopBanner =  @xml.value('(/HomeBanner/TopBanner)[1]', 'nvarchar(255)' )
	SET @Banner1 =  @xml.value('(/HomeBanner/Banner1)[1]', 'nvarchar(255)' )
	SET @Banner2 =  @xml.value('(/HomeBanner/Banner2)[1]', 'nvarchar(255)' )
	SET @Banner3 =  @xml.value('(/HomeBanner/Banner3)[1]', 'nvarchar(255)' )
	SET @TopBannerLink =  @xml.value('(/HomeBanner/TopBannerLink)[1]', 'nvarchar(max)' )
	SET @Banner1Link =  @xml.value('(/HomeBanner/Banner1Link)[1]', 'nvarchar(max)' )
	SET @Banner2Link =  @xml.value('(/HomeBanner/Banner2Link)[1]', 'nvarchar(max)' )
	SET @Banner3Link =  @xml.value('(/HomeBanner/Banner3Link)[1]', 'nvarchar(max)' )
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.HomeBanner
		SET
			TopBanner=@TopBanner,
			TopBannerLink=@TopBannerLink,
			Banner1=@Banner1,
			Banner1Link=@Banner1Link,
			Banner2=@Banner2,
			Banner3=@Banner3,
			Banner2Link=@Banner2Link,
			Banner3Link=@Banner3Link
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT 1 FROM dbo.HomeBanner)
		BEGIN
			SELECT @Order=MAX([Order])+1 FROM dbo.HomeBanner
		END
		ELSE
		BEGIN
			SELECT @Order=1
		END
		
		INSERT INTO dbo.HomeBanner
		(TopBanner,TopBannerLink,Banner1,Banner1Link,Banner2,Banner2Link,Banner3,Banner3Link,[Order])
		VALUES
		(@TopBanner,@TopBannerLink,@Banner1,@Banner1Link,@Banner2,@Banner2Link,@Banner3,@Banner3Link,@Order)
		SET @Id = SCOPE_IDENTITY()
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdDoctor]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdDoctor]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @CompanyName NVARCHAR(255)
	DECLARE @Title NVARCHAR(255)
	DECLARE @Address NVARCHAR(max)
	DECLARE @Avator NVARCHAR(max)
	DECLARE @UserId INT
	DECLARE @Url NVARCHAR(max)
	SET @Id =  @xml.value('(/Doctor/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Doctor/Name)[1]', 'nvarchar(255)' )
	SET @CompanyName =  @xml.value('(/Doctor/CompanyName)[1]', 'nvarchar(255)' )
	SET @Title =  @xml.value('(/Doctor/Title)[1]', 'nvarchar(255)' )
	SET @Address =  @xml.value('(/Doctor/Address)[1]', 'nvarchar(max)' )
	SET @Avator =  @xml.value('(/Doctor/Avator)[1]', 'nvarchar(max)' )
	SET @UserId =  @xml.value('(/Doctor/UserId)[1]', 'nvarchar(255)' )
	SET @Url =  @xml.value('(/Doctor/Url)[1]', 'nvarchar(max)' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.Doctor
		SET Name=@Name,CompanyName=@CompanyName,Title=@Title,[Address]=@Address,Avator=@Avator,Url=@Url
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Doctor (Name,CompanyName,Title,[Address],Avator,UserId,Url)
		VALUES(@Name,@CompanyName,@Title,@Address,@Avator,@UserId,@Url)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdDeliveryMethod]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdDeliveryMethod]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Logo NVARCHAR(MAX)
	DECLARE @IsEnabled BIT
	
	SET @Id =  @xml.value('(/DeliveryMethod/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/DeliveryMethod/Name)[1]', 'nvarchar(255)' )
	SET @Logo =  @xml.value('(/DeliveryMethod/Logo)[1]', 'NVARCHAR(MAX)' )
	SET @IsEnabled = @xml.value('(/DeliveryMethod/IsEnabled)[1]', 'BIT' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.DeliveryMethod
		SET Name=@Name,Logo=@Logo,IsEnabled=@IsEnabled
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.DeliveryMethod (Name,Logo,IsEnabled)
		VALUES(@Name,@Logo,@IsEnabled)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdCountry]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdCountry]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Picture NVARCHAR(255)
	DECLARE @IsEnabled BIT
	SET @Id =  @xml.value('(/Country/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Country/Name)[1]', 'nvarchar(255)' )
	SET @IsEnabled =  @xml.value('(/Country/IsEnabled)[1]', 'bit' )
	SET @Picture =  @xml.value('(/Country/Picture)[1]', 'nvarchar(255)' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.Country
		SET Name=@Name, IsEnabled=@IsEnabled,Picture=@Picture
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT 1 FROM dbo.Country WHERE Name=@Name AND ISNULL(IsDeleted,0)=1)
		BEGIN
			UPDATE dbo.Country SET IsDeleted=0,IsEnabled=@IsEnabled,Picture=@Picture
		END
		ELSE
		BEGIN
			INSERT INTO dbo.Country (Name,IsEnabled,Picture)
			VALUES(@Name,@IsEnabled,@Picture)
			SET @Id = SCOPE_IDENTITY()
		END
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdCaptcha]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdCaptcha]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Guid NVARCHAR(255)
	DECLARE @Text NVARCHAR(50)
	
	SET @Guid =  @xml.value('(/Captcha/Guid)[1]', 'nvarchar(255)' )
	SET @Text =  @xml.value('(/Captcha/Text)[1]', 'NVARCHAR(50)' )
	IF EXISTS (SELECT 1 FROM dbo.Captcha WHERE [Guid]=@Guid)
	BEGIN
		UPDATE dbo.Captcha
		SET Text=@Text
		WHERE [Guid]=@Guid
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Captcha ([Guid],[Text])
		VALUES(@Guid,@Text)
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT 0 
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdBrand]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdBrand] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Logo NVARCHAR(MAX)
	
	SET @Id =  @xml.value('(/Brand/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Brand/Name)[1]', 'nvarchar(255)' )
	SET @Logo =  @xml.value('(/Brand/Logo)[1]', 'NVARCHAR(MAX)' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.Brand
		SET Name=@Name,Logo=@Logo
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Brand (Name,Logo)
		VALUES(@Name,@Logo)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdCategory]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdCategory]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @ParentId INT
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	
	SET @Id =  @xml.value('(/Category/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Category/Name)[1]', 'nvarchar(255)' )
	SET @ParentId =  @xml.value('(/Category/ParentId)[1]', 'int' )
	SET @UserName =  @xml.value('(/Category/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Category/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Category
		SET
			Name=@Name,
			ParentId=@ParentId,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE()
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Category
		(Name,ParentId,CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate)
		VALUES
		(@Name,@ParentId,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE())
		SET @Id = SCOPE_IDENTITY()
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdPaymentMethod]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdPaymentMethod]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Logo NVARCHAR(MAX)
	DECLARE @IsEnabled BIT
	
	SET @Id =  @xml.value('(/PaymentMethod/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/PaymentMethod/Name)[1]', 'nvarchar(255)' )
	SET @Logo =  @xml.value('(/PaymentMethod/Logo)[1]', 'NVARCHAR(MAX)' )
	SET @IsEnabled = @xml.value('(/PaymentMethod/IsEnabled)[1]', 'BIT' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.PaymentMethod
		SET Name=@Name,Logo=@Logo,IsEnabled=@IsEnabled
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.PaymentMethod (Name,Logo,IsEnabled)
		VALUES(@Name,@Logo,@IsEnabled)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdProductSet]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdProductSet]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Description NVARCHAR(MAX)
	DECLARE @Price money
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	DECLARE @OptionalCount INT
	
	SET @Id =  @xml.value('(/ProductSet/Id)[1]', 'int' )
	SET @OptionalCount =  @xml.value('(/ProductSet/OptionalCount)[1]', 'int' )
	SET @Name =  @xml.value('(/ProductSet/Name)[1]', 'nvarchar(255)' )
	SET @Description =  @xml.value('(/ProductSet/Description)[1]', 'nvarchar(max)' )
	SET @Price =  @xml.value('(/ProductSet/Price)[1]', 'money' )
	SET @UserName =  @xml.value('(/ProductSet/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/ProductSet/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.ProductSet
		SET
			Name=@Name,
			OptionalCount=@OptionalCount,
			[Description]=@Description,
			Price=@Price,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE()
		WHERE
			Id=@Id
		DELETE  FROM dbo.ProductSetPicture WHERE ProductSetId=@Id
		INSERT INTO dbo.ProductSetPicture (ProductSetId,Picture,IsDefault)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault
		FROM @xml.nodes('(/*/Pictures)[1]/ProductSetPicture') as x(y)
		DELETE  FROM dbo.ProductSetItem WHERE ProductSetId=@Id
		INSERT INTO dbo.ProductSetItem (ProductSetId,ProductId,IsMandatory)
		SELECT
			@Id,
			y.value('ProductId[1]', 'INT') as ProductId,
			y.value('IsMandatory[1]', 'bit') as IsMandatory
		FROM @xml.nodes('(/*/Items)[1]/ProductSetItem') as x(y)
	END
	ELSE
	BEGIN
		INSERT INTO dbo.ProductSet
		(Name,[Description],Price,CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate,OptionalCount)
		VALUES
		(@Name,@Description,@Price,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE(),@OptionalCount)
		SET @Id = SCOPE_IDENTITY()
		
		INSERT INTO dbo.ProductSetPicture (ProductSetId,Picture,IsDefault)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault
		FROM @xml.nodes('(/*/Pictures)[1]/ProductSetPicture') as x(y)
		
		INSERT INTO dbo.ProductSetItem (ProductSetId,ProductId,IsMandatory)
		SELECT
			@Id,
			y.value('ProductId[1]', 'INT') as ProductId,
			y.value('IsMandatory[1]', 'bit') as IsMandatory
		FROM @xml.nodes('(/*/Items)[1]/ProductSetItem') as x(y)
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdQuestionType]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdQuestionType]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	
	
	SET @Id =  @xml.value('(/QuestionType/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/QuestionType/Name)[1]', 'nvarchar(255)' )
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.QuestionType
		SET
			Name=@Name
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.QuestionType
		(Name)
		VALUES
		(@Name)
		SET @Id = SCOPE_IDENTITY()
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdSetting]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdSetting]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id int
	DECLARE @ExchangeRate decimal(18,10)
	DECLARE @TaxRate decimal(18,10)
	DECLARE @GiftSetDiscount decimal(18,10)
	DECLARE @RepeatCustomerDiscount decimal(18,10)
	
	SET @Id =  @xml.value('(/Setting/Id)[1]', 'int' )
	SET @ExchangeRate =  @xml.value('(/Setting/ExchangeRate)[1]', 'decimal(18,10)')
	SET @TaxRate =  @xml.value('(/Setting/TaxRate)[1]', 'decimal(18,10)')
	SET @GiftSetDiscount =  @xml.value('(/Setting/GiftSetDiscount)[1]', 'decimal(18,10)')
	SET @RepeatCustomerDiscount =  @xml.value('(/Setting/RepeatCustomerDiscount)[1]', 'decimal(18,10)')
	IF(@Id>0)
	BEGIN
		UPDATE dbo.Setting SET ExchangeRate=@ExchangeRate,TaxRate=@TaxRate,GiftSetDiscount=@GiftSetDiscount,RepeatCustomerDiscount=@RepeatCustomerDiscount WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Setting (ExchangeRate,TaxRate,GiftSetDiscount,RepeatCustomerDiscount)
		VALUES (@ExchangeRate,@TaxRate,@GiftSetDiscount,@RepeatCustomerDiscount)
		SET @Id = SCOPE_IDENTITY()
	END 
	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdSaleBanner]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdSaleBanner]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @TopBanner NVARCHAR(255)
	DECLARE @Banner1 NVARCHAR(255)
	DECLARE @Banner1Link NVARCHAR(MAX)
	
	SET @Id =  @xml.value('(/SaleBanner/Id)[1]', 'int' )
	SET @TopBanner =  @xml.value('(/SaleBanner/TopBanner)[1]', 'nvarchar(255)' )
	SET @Banner1 =  @xml.value('(/SaleBanner/Banner1)[1]', 'nvarchar(255)' )
	SET @Banner1Link =  @xml.value('(/SaleBanner/Banner1Link)[1]', 'nvarchar(max)' )
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.SaleBanner
		SET
			TopBanner=@TopBanner,
			Banner1=@Banner1,
			Banner1Link=@Banner1Link
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.SaleBanner(TopBanner,Banner1,Banner1Link)
		VALUES
		(@TopBanner,@Banner1,@Banner1Link)
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCaptcha]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCaptcha]
	-- Add the parameters for the stored procedure here
	@Guid NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Captcha
	WHERE [Guid]=@Guid
	FOR XML PATH('Captcha'), ROOT('CaptchaData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelBrands]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelBrands]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Brand
	WHERE ISNULL(IsDeleted,0)<>1
	FOR XML PATH('Brand'), ROOT('BrandData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelBrandByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelBrandByName]
	-- Add the parameters for the stored procedure here
	@name NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Brand
	WHERE ISNULL(IsDeleted,0)<>1 AND Name=@name
	FOR XML PATH('Brand'), ROOT('BrandData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelBrandById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelBrandById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Brand
	WHERE ISNULL(IsDeleted,0)<>1 AND Id=@id
	FOR XML PATH('Brand'), ROOT('BrandData')
END
GO
/****** Object:  Table [dbo].[Product]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Price] [money] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Weight] [decimal](18, 10) NULL,
	[IsDeleted] [bit] NULL,
	[IsFeatured] [bit] NULL,
	[BrandId] [int] NULL,
	[Tags] [xml] NULL,
	[IsRecommended] [bit] NULL,
	[CountryId] [int] NULL,
	[IsHidden] [bit] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductPromotion]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPromotion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Picture] [nvarchar](max) NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ProductPromotion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductPicture]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPicture](
	[ProductId] [int] NOT NULL,
	[Picture] [nvarchar](max) NOT NULL,
	[IsDefault] [bit] NULL,
	[Title] [nvarchar](max) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductDiscount]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDiscount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Discount] [decimal](18, 10) NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[ByValue] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[LastModifiedBy] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ProductDiscount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spMarkAddressAsDefault]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spMarkAddressAsDefault] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @userid int
	
	SELECT @userid=UserId FROM dbo.UserAddress WHERE Id=@id
	
	UPDATE dbo.UserAddress
	SET IsDefault=0
	WHERE UserId=@userid
	
	UPDATE dbo.UserAddress
	SET IsDefault=1
	WHERE Id=@id 
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdUserAddress]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdUserAddress]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @Id int
		DECLARE @UserId int
		DECLARE @FirstName NVARCHAR(255)
		DECLARE @LastName NVARCHAR(255)
		DECLARE @MobileNo NVARCHAR(50)
		DECLARE @TelephoneNo NVARCHAR(50)
		DECLARE @Postcode NVARCHAR(50)
		DECLARE @Country NVARCHAR(255)
		DECLARE @State NVARCHAR(255)
		DECLARE @City NVARCHAR(255)
		DECLARE @Address1 NVARCHAR(MAX)
		DECLARE @Address2 NVARCHAR(MAX)
		DECLARE @IsDefault BIT
		
		SET @Id =  @xml.value('(/UserAddress/Id)[1]', 'int' )
		SET @UserId =  @xml.value('(/UserAddress/UserId)[1]', 'int' )
		SET @FirstName =  @xml.value('(/UserAddress/FirstName)[1]', 'NVARCHAR(255)' )
		SET @LastName =  @xml.value('(/UserAddress/LastName)[1]', 'NVARCHAR(255)' )
		SET @MobileNo =  @xml.value('(/UserAddress/MobileNo)[1]', 'NVARCHAR(50)' )
		SET @TelephoneNo =  @xml.value('(/UserAddress/TelephoneNo)[1]', 'NVARCHAR(50)' )
		SET @Postcode =  @xml.value('(/UserAddress/Postcode)[1]', 'NVARCHAR(50)' )
		SET @Country =  @xml.value('(/UserAddress/Country)[1]', 'NVARCHAR(255)' )
		SET @State =  @xml.value('(/UserAddress/State)[1]', 'NVARCHAR(255)' )
		SET @City =  @xml.value('(/UserAddress/City)[1]', 'NVARCHAR(255)' )
		SET @Address1 =  @xml.value('(/UserAddress/Address1)[1]', 'NVARCHAR(MAX)' )
		SET @Address2 =  @xml.value('(/UserAddress/Address2)[1]', 'NVARCHAR(MAX)' )
		SET @IsDefault =  @xml.value('(/UserAddress/IsDefault)[1]', 'bit' )
		
		IF(@Id>0)
		BEGIN
			UPDATE dbo.UserAddress
			SET 
				UserId=@UserId,
				FirstName=@FirstName,
				LastName=@LastName,
				MobileNo=@MobileNo,
				TelephoneNo=@TelephoneNo,
				Postcode=@Postcode,
				Country=@Country,
				[State]=@State,
				City=@City,
				Address1=@Address1,
				Address2=@Address2,
				IsDefault=@IsDefault
			WHERE Id=@Id
		END
		ELSE
		BEGIN
			INSERT INTO dbo.UserAddress
			(UserId,FirstName,LastName,MobileNo,TelephoneNo,Postcode,Country,[State],City,Address1,Address2,IsDefault)
			VALUES
			(@UserId,@FirstName,@LastName,@MobileNo,@TelephoneNo,@Postcode,@Country,@State,@City,@Address1,@Address2,@IsDefault)
			SET @Id = SCOPE_IDENTITY()
		END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdState]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdState]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @CountryId INT
	
	SET @Id =  @xml.value('(/State/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/State/Name)[1]', 'nvarchar(255)' )
	SET @CountryId =  @xml.value('(/State/Country/Id)[1]', 'int' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.[State]
		SET Name=@Name, IsEnabled=1
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.[State] (Name,CountryId,IsEnabled)
		VALUES(@Name,@CountryId,1)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spRecommendProduct]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRecommendProduct]
	-- Add the parameters for the stored procedure here
	@id int,
	@isRecommended bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Product SET IsRecommended=@isRecommended WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spSelAllUsers]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelAllUsers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	*,
	(
		SELECT r.RoleName FROM  dbo.webpages_Roles r
		INNER JOIN dbo.webpages_UsersInRoles uir ON uir.RoleId=r.RoleId
		WHERE uir.UserId=[User].UserId
		FOR XML PATH('UserRole'), ROOT('Roles'), TYPE
	)
	FROM dbo.[User]
	FOR XML PATH('User'), ROOT('UserData')
END
GO
/****** Object:  StoredProcedure [dbo].[spHideProduct]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spHideProduct] 
	-- Add the parameters for the stored procedure here
	@id INT,
	@isHidden BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Product SET IsHidden=@isHidden WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spFeatureProduct]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFeatureProduct]
	-- Add the parameters for the stored procedure here
	@id int,
	@isFeatured bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Product SET IsFeatured=@isFeatured WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spDelUserAddress]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelUserAddress]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.UserAddress WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spDelState]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelState]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE dbo.[State] SET IsEnabled=0 WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdCartItem]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdCartItem]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @SessionId NVARCHAR(255)
	DECLARE @ProductId INT
	DECLARE @Quantity INT
	DECLARE @Price MONEY
	DECLARE @Inventory INT
	DECLARE @IsHamper BIT
	DECLARE @GiftSetDiscount DECIMAL(18,10)
	
	SET @SessionId =  @xml.value('(/CartItem/SessionId)[1]', 'NVARCHAR(255)' )
	SET @ProductId =  @xml.value('(/CartItem/ProductId)[1]', 'INT' )
	SET @Quantity =  @xml.value('(/CartItem/Quantity)[1]', 'INT' )
	SET @IsHamper =  @xml.value('(/CartItem/IsHamper)[1]', 'BIT' )
	
	IF ISNULL(@IsHamper,0)=1
	BEGIN
		DECLARE @FixedPrice MONEY
		SELECT @FixedPrice = Price FROM dbo.ProductSet WHERE Id=@ProductId
		DECLARE @OptionalPrice MONEY
		SELECT  @OptionalPrice = SUM(Price) FROM dbo.Product WHERE Id IN
		(SELECT Id FROM (SELECT
			y.value('Id[1]', 'INT') as Id
		FROM @xml.nodes('(/*/Products)[1]/Product') as x(y)) AS Products  WHERE Products.Id NOT IN 
		(SELECT ProductId FROM dbo.ProductSetItem WHERE ProductSetId=@ProductId AND ISNULL(IsMandatory,0)=1)
		)
		SELECT TOP 1 @GiftSetDiscount=GiftSetDiscount FROM dbo.Setting
		SELECT @OptionalPrice=@OptionalPrice-@OptionalPrice*@GiftSetDiscount/100
		SELECT @Price=ISNULL(@OptionalPrice,0)+@FixedPrice
		SELECT @Inventory=MIN(Quantity) FROM dbo.Product WHERE Id IN (SELECT Id FROM (SELECT
			y.value('Id[1]', 'INT') as Id
		FROM @xml.nodes('(/*/Products)[1]/Product') as x(y)) AS Products)
	END
	ELSE
	BEGIN
		SELECT @Price=Price,@Inventory=Quantity FROM dbo.Product WHERE Id=@ProductId
	END
	
	IF EXISTS (SELECT 1 FROM dbo.CartItem WHERE SessionId=@SessionId AND ProductId=@ProductId AND CAST(Tags AS NVARCHAR(MAX))=CAST(@xml.query('/CartItem/Tags/ProductTag') AS NVARCHAR(MAX)))
	BEGIN
		DECLARE @Qty INT
		SELECT @Qty=Quantity+@Quantity FROM dbo.CartItem WHERE SessionId=@SessionId AND ProductId=@ProductId AND CAST(Tags AS NVARCHAR(MAX))=CAST(@xml.query('/CartItem/Tags/ProductTag') AS NVARCHAR(MAX))
		
		IF(@Qty>@Inventory)
		BEGIN
			RAISERROR('The quantity in cart is larger than inventory',16,16)
		END
		
		IF(@Qty<=0)
		BEGIN
			DELETE FROM CartItem WHERE SessionId=@SessionId AND ProductId=@ProductId AND CAST(Tags AS NVARCHAR(MAX))=CAST(@xml.query('/CartItem/Tags/ProductTag') AS NVARCHAR(MAX))
		END
		ELSE
		BEGIN
			UPDATE dbo.CartItem
			SET Quantity=@Qty,Price=@Price,Tags=@xml.query('/CartItem/Tags/ProductTag'),LastModifiedDate=GETDATE()
			WHERE SessionId=@SessionId AND ProductId=@ProductId AND CAST(Tags AS NVARCHAR(MAX))=CAST(@xml.query('/CartItem/Tags/ProductTag') AS NVARCHAR(MAX))
		END
	END
	ELSE
	BEGIN
		IF(@Quantity>@Inventory)
		BEGIN
			RAISERROR('The quantity in cart is larger than inventory',16,16)
		END
		
		INSERT INTO dbo.CartItem (SessionId,ProductId,Quantity,Price,Tags,LastModifiedDate,IsHamper)
		VALUES(@SessionId,@ProductId,@Quantity,@Price,@xml.query('/CartItem/Tags/ProductTag'),GETDATE(),@IsHamper)

	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT 0 
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[spDelCategoryById]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelCategoryById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
    UPDATE dbo.Category
    SET ParentId=0
    WHERE ParentId=@id
	UPDATE dbo.Category SET IsDeleted=1 WHERE Id=@id
	UPDATE dbo.Product SET IsDeleted=1 WHERE CategoryId=@id
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spChangeCartItemQuantity]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spChangeCartItemQuantity]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@Quantity INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @SessionId NVARCHAR(255)
	DECLARE @ProductId INT
	DECLARE @ExQty INT
	DECLARE @IsHamper BIT
	DECLARE @Inventory INT
	
	SELECT @SessionId=SessionId,@ProductId=ProductId,@ExQty=Quantity,@IsHamper=IsHamper FROM dbo.CartItem WHERE Id=@Id
	IF(ISNULL(@IsHamper,0)=1)
	BEGIN
		DECLARE @xml XML
		SELECT @xml=Tags
		FROM [Ethaneian].[dbo].[CartItem] where Id=@Id
	  
		
		SELECT @Inventory=MIN(Quantity) FROM dbo.Product WHERE Id IN (SELECT
				DISTINCT y.value('ProductId[1]', 'INT') as ProductId
			FROM @xml.nodes('/ProductTag') as x(y))
	END
	ELSE
	BEGIN
		SELECT @Inventory=Quantity FROM dbo.Product WHERE Id=@ProductId
	END
	
	IF(@ExQty+@Quantity>@Inventory)
	BEGIN
		RAISERROR('No more product in inventory',16,16)
	END
	
	IF(@ExQty+@Quantity<=0)
	BEGIN
		DELETE FROM dbo.CartItem WHERE Id=@Id
	END
	ELSE
	BEGIN
		UPDATE dbo.CartItem
		SET Quantity=@ExQty+@Quantity
		WHERE Id=@Id
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spDelBrand]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelBrand]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
  
	UPDATE dbo.Brand SET IsDeleted=1 WHERE Id=@id
	UPDATE dbo.Product SET IsDeleted=1 WHERE BrandId=@id
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spDelQuestionType]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelQuestionType]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DELETE FROM dbo.Answer WHERE dbo.Answer.Id IN (SELECT dbo.Answer.Id FROM dbo.Answer, dbo.Question WHERE dbo.Answer.QuestionId = dbo.Question.Id AND dbo.Question.TypeId = @id)
	DELETE FROM dbo.Question WHERE TypeId=@id
	DELETE FROM dbo.QuestionType WHERE Id=@id
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spDelQuestion]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelQuestion]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.Answer WHERE dbo.Answer.Id IN (SELECT dbo.Answer.Id FROM dbo.Answer, dbo.Question WHERE dbo.Answer.QuestionId = dbo.Question.Id AND dbo.Question.Id = @id)
	DELETE FROM dbo.Question WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spDelProductById]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelProductById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE dbo.Product
    SET IsDeleted=1
    WHERE Id=@id
END
GO
/****** Object:  Table [dbo].[DeliveryFee]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryFee](
	[DeliveryMethodId] [int] NOT NULL,
	[StateId] [int] NOT NULL,
	[Fee] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[RepliedBy] [int] NOT NULL,
	[RepliedDate] [datetime] NOT NULL,
	[QuestionId] [int] NOT NULL,
	[RepliedTo] [int] NOT NULL,
	[IsAdminReply] [bit] NULL,
 CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ShippingAddress] [int] NULL,
	[Status] [int] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[Total] [money] NOT NULL,
	[DeliveryFee] [money] NOT NULL,
	[DeliveryMethodId] [int] NOT NULL,
	[PaymentMethodId] [int] NOT NULL,
	[Country] [nvarchar](255) NOT NULL,
	[State] [nvarchar](255) NOT NULL,
	[City] [nvarchar](255) NOT NULL,
	[Address1] [nvarchar](max) NOT NULL,
	[Address2] [nvarchar](max) NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[TelephoneNo] [nvarchar](50) NULL,
	[Postcode] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[PaymentId] [nvarchar](255) NULL,
	[PaymentGuid] [nvarchar](255) NULL,
	[ShippingNo] [nvarchar](255) NULL,
	[TaxRate] [decimal](18, 10) NULL,
	[RepeatCustomerDiscount] [money] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsletterProduct]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsletterProduct](
	[NewsletterId] [int] NOT NULL,
	[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[StateId] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spSelProductByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id, Name FROM dbo.Product
	WHERE Name=@name
	FOR XML PATH('Product'), ROOT('ProductData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelStates]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelStates]
	-- Add the parameters for the stored procedure here
	@countryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[Country] WHERE Id=[State].CountryId
	FOR XML PATH(''), ROOT('Country'), TYPE)
	 FROM dbo.[State]
	WHERE CountryId=@countryId  AND IsEnabled=1
	FOR XML PATH('State'), ROOT('StateData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelStateByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelStateByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[Country] WHERE Id=[State].CountryId
	FOR XML PATH(''), ROOT('Country'), TYPE),
	IsEnabled
	 FROM dbo.[State]
	WHERE Name=@name
	FOR XML PATH('State'), ROOT('StateData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelUserById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelUserById]
	-- Add the parameters for the stored procedure here
	@userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	*,
	(
		SELECT r.RoleName FROM  dbo.webpages_Roles r
		INNER JOIN dbo.webpages_UsersInRoles uir ON uir.RoleId=r.RoleId
		WHERE uir.UserId=[User].UserId
		FOR XML PATH('UserRole'), ROOT('Roles'), TYPE
	)
	FROM dbo.[User]
	WHERE UserId=@userid
	FOR XML PATH('User'), ROOT('UserData')
END
GO
/****** Object:  Table [dbo].[StockHistory]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockHistory](
	[ProductId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spSelQuestions]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelQuestions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *, (SELECT * FROM dbo.[User] WHERE UserId=Question.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Question.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.QuestionType WHERE Id=Question.TypeId
	FOR XML PATH(''), ROOT('QuestionType'), TYPE),
	(SELECT * FROM dbo.Doctor WHERE Id=Question.DoctorId
	FOR XML PATH(''), ROOT('Doctor'), TYPE)
	 FROM dbo.Question
	FOR XML PATH('Question'), ROOT('QuestionData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelAddressesByUser]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelAddressesByUser]
	-- Add the parameters for the stored procedure here
	@username nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ua.* FROM dbo.UserAddress ua
	INNER JOIN dbo.[User] usr ON ua.UserId=usr.UserId
	WHERE usr.UserName=@username
	FOR XML PATH('UserAddress'), ROOT('UserAddressData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelDeliveryFeeByState]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelDeliveryFeeByState]
	-- Add the parameters for the stored procedure here
	@deliveryMethodId int,
	@stateId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Fee,
	(SELECT * FROM dbo.DeliveryMethod WHERE Id=DeliveryFee.DeliveryMethodId
	FOR XML PATH(''), ROOT('DeliveryMethod'), TYPE),
	(SELECT * FROM dbo.[State]  WHERE Id=DeliveryFee.StateId
	FOR XML PATH(''), ROOT('State'), TYPE)
	 FROM dbo.DeliveryFee
	WHERE DeliveryMethodId=@deliveryMethodId AND StateId=@stateId
	FOR XML PATH('DeliveryFee'), ROOT('DeliveryFeeData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelDeliveryFee]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spSelDeliveryFee]
	-- Add the parameters for the stored procedure here
	@deliveryMethodId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Fee,
	(SELECT *  FROM dbo.DeliveryMethod  WHERE Id=DeliveryFee.DeliveryMethodId
	FOR XML PATH(''), ROOT('DeliveryMethod'), TYPE),
	(SELECT *  FROM dbo.[State]  WHERE Id=DeliveryFee.StateId
	FOR XML PATH(''), ROOT('State'), TYPE)
	 FROM dbo.DeliveryFee
	WHERE DeliveryMethodId=@deliveryMethodId
	FOR XML PATH('DeliveryFee'), ROOT('DeliveryFeeData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCityByName]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCityByName] 
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[State] WHERE Id=[City].StateId
	FOR XML PATH(''), ROOT('State'), TYPE),
	IsEnabled
	 FROM dbo.[City]
	WHERE Name=@name
	FOR XML PATH('City'), ROOT('CityData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCityById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCityById] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[State] WHERE Id=[City].StateId
	FOR XML PATH(''), ROOT('State'), TYPE)
	 FROM dbo.[City]
	WHERE Id=@id AND IsEnabled=1
	FOR XML PATH('City'), ROOT('CityData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCities]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCities]
	-- Add the parameters for the stored procedure here
	@stateId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[State] WHERE Id=[City].StateId
	FOR XML PATH(''), ROOT('State'), TYPE)
	 FROM dbo.[City]
	WHERE StateId=@stateId AND IsEnabled=1
	FOR XML PATH('City'), ROOT('CityData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelCartItems]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCartItems] 
	-- Add the parameters for the stored procedure here
	@sessionId NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=CartItem.ProductId AND ISNULL(IsDeleted,0)<>1 AND ISNULL(CartItem.IsHamper,0)=0
	FOR XML PATH(''), ROOT('Product'), TYPE
	),(SELECT *,
	(SELECT * FROM dbo.ProductSetPicture  WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetPicture'), ROOT('Pictures'), TYPE),
	(SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=ProductSetItem.ProductId AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH(''), ROOT('Product'), TYPE
	) FROM  dbo.ProductSetItem WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetItem'), ROOT('Items'), TYPE)
	
	 FROM dbo.ProductSet
	WHERE Id=CartItem.ProductId AND ISNULL(IsDeleted,0)<>1 AND ISNULL(CartItem.IsHamper,0)=1
	FOR XML PATH(''), ROOT('ProductSet'), TYPE
	)
	 FROM dbo.CartItem
	WHERE SessionId=@sessionId
	FOR XML PATH('CartItem'), ROOT('CartItemData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelActiveSales]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelActiveSales]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 prod.Id,prod.Name,prod.IsHidden,prod.[Description],prod.Price,prod.Quantity,prod.CreatedDate,prod.LastModifiedDate,
	 prod.[Weight],ISNULL(prod.IsFeatured,0) AS IsFeatured,
	 (SELECT pd.FromDate,pd.EndDate, pd.Discount,pd.ByValue,pd.Id
	FOR XML PATH(''), ROOT('Discount'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=prod.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod
	INNER JOIN dbo.ProductDiscount pd ON prod.Id=pd.ProductId
	WHERE ISNULL(prod.IsDeleted,0)<>1 AND ISNULL(pd.IsDeleted,0)<>1 AND GETDATE()>=pd.FromDate AND GETDATE()<=DATEADD(dd, 1,pd.EndDate)
	FOR XML PATH('Product'), ROOT('ProductData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelQuestionById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelQuestionById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *, (SELECT * FROM dbo.[User] WHERE UserId=Question.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Question.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.QuestionType WHERE Id=Question.TypeId
	FOR XML PATH(''), ROOT('QuestionType'), TYPE),
	(SELECT Id,Body,RepliedDate,QuestionId,IsAdminReply,
	(SELECT *,(SELECT Avator FROM dbo.Doctor WHERE UserId=[User].UserId) AS Avator FROM dbo.[User] WHERE UserId=anws.RepliedBy
	FOR XML PATH(''), ROOT('RepliedBy'), TYPE),
	(SELECT * FROM dbo.Answer WHERE Id=anws.RepliedTo
	FOR XML PATH(''), ROOT('RepliedTo'), TYPE)
	 FROM dbo.Answer anws WHERE QuestionId=Question.Id
	FOR XML PATH('Answer'), ROOT('Replies'), TYPE),
	(SELECT * FROM dbo.Doctor WHERE Id=Question.DoctorId
	FOR XML PATH(''), ROOT('Doctor'), TYPE)
	 FROM dbo.Question
	 WHERE Id=@id
	FOR XML PATH('Question'), ROOT('QuestionData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelProductSetById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductSetById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],Price,CreatedDate,LastModifiedDate,OptionalCount,
	 (SELECT * FROM dbo.[User] WHERE UserId=ProductSet.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ProductSet.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.ProductSetPicture  WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetPicture'), ROOT('Pictures'), TYPE),
	(SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=ProductSetItem.ProductId AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH(''), ROOT('Product'), TYPE
	) FROM  dbo.ProductSetItem WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetItem'), ROOT('Items'), TYPE)
	FROM dbo.ProductSet
	WHERE ISNULL(IsDeleted,0)<>1 AND Id=@id
	ORDER BY CreatedDate DESC
	FOR XML PATH('ProductSet'), ROOT('ProductSetData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelProductSet]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductSet] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],Price,CreatedDate,LastModifiedDate,OptionalCount,
	 (SELECT * FROM dbo.[User] WHERE UserId=ProductSet.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ProductSet.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.ProductSetPicture  WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetPicture'), ROOT('Pictures'), TYPE),
	(SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=ProductSetItem.ProductId AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH(''), ROOT('Product'), TYPE
	) FROM  dbo.ProductSetItem WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetItem'), ROOT('Items'), TYPE)
	FROM dbo.ProductSet
	WHERE ISNULL(IsDeleted,0)<>1
	ORDER BY CreatedDate DESC
	FOR XML PATH('ProductSet'), ROOT('ProductSetData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelProductList]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductList]
	-- Add the parameters for the stored procedure here
	@bid int,
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,ISNULL(IsRecommended,0) AS IsRecommended,
	 (SELECT * FROM dbo.[User] WHERE UserId=Product.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Product.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=Product.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.Brand WHERE Id=Product.BrandId
	FOR XML PATH(''), ROOT('Brand'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE),
	(SELECT RelatedId AS Id FROM  dbo.RelatedProduct WHERE ProductId=Product.Id
	FOR XML PATH('Product'), ROOT('RelatedItems'), TYPE)
	FROM dbo.Product
	WHERE (ISNULL(@bid,0)=0 OR BrandId=@bid) AND (ISNULL(@cid,0)=0 OR CategoryId=@cid) AND ISNULL(IsDeleted,0)<>1  AND ISNULL(IsHidden,0)<>1
	FOR XML PATH('Product'), ROOT('ProductData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelNewsletters]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelNewsletters]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id,Title,Body,SentType,CreatedDate,IsEnabled,StartDate,
	(
	SELECT Id,Name, [Description], (SELECT * FROM dbo.ProductPicture  WHERE ProductId=prod.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.[Product] prod
	INNER JOIN dbo.NewsletterProduct np ON prod.Id=np.ProductId
	WHERE np.NewsletterId=Newsletter.Id
	FOR XML PATH('Product'), ROOT('Products'), TYPE
	)
	FROM dbo.Newsletter
	FOR XML PATH('Newsletter'), ROOT('NewsletterData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelNewsletterById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelNewsletterById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id,Title,Body,SentType,CreatedDate,IsEnabled,StartDate,
	(
	SELECT Id,Name, [Description], (SELECT * FROM dbo.ProductPicture  WHERE ProductId=prod.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.[Product] prod
	INNER JOIN dbo.NewsletterProduct np ON prod.Id=np.ProductId
	WHERE np.NewsletterId=Newsletter.Id
	FOR XML PATH('Product'), ROOT('Products'), TYPE
	)
	FROM dbo.Newsletter
	WHERE Id=@id
	FOR XML PATH('Newsletter'), ROOT('NewsletterData')
END
GO
/****** Object:  StoredProcedure [dbo].[spStockToInventory]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spStockToInventory]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @ProductId INT
	DECLARE @Quantity INT
	DECLARE @UserId INT
	DECLARE @Username NVARCHAR(50)
	
	SET @ProductId =  @xml.value('(/StockHistory/ProductId)[1]', 'int' )
	SET @Quantity =  @xml.value('(/StockHistory/Quantity)[1]', 'int' )
	SET @Username =  @xml.value('(/StockHistory/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @UserId =UserId FROM dbo.[User] WHERE UserName=@Username
	
	UPDATE dbo.Product SET Quantity=Quantity+@Quantity WHERE Id=@ProductId
	
	INSERT INTO dbo.StockHistory
	(ProductId,Quantity,CreatedBy,CreatedDate)
	VALUES
	(@ProductId,@Quantity,@UserId,GETDATE())
	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spSelStockHistoryById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelStockHistoryById]
	-- Add the parameters for the stored procedure here
	@productid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ProductId,Quantity,CreatedDate,
	(SELECT * FROM dbo.[User] WHERE UserId=StockHistory.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE)
	FROM dbo.StockHistory
	WHERE ProductId=@productid
	ORDER BY CreatedDate DESC
	FOR XML PATH('StockHistory'), ROOT('StockHistoryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelStateById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelStateById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[Country] WHERE Id=[State].CountryId
	FOR XML PATH(''), ROOT('Country'), TYPE),
	(SELECT * FROM dbo.[City] WHERE StateId=[State].Id AND IsEnabled=1
	FOR XML PATH('City'), ROOT('Cities'), TYPE)
	 FROM dbo.[State]
	WHERE Id=@id  AND IsEnabled=1
	FOR XML PATH('State'), ROOT('StateData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelSales]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelSales]
	-- Add the parameters for the stored procedure here
	@pid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.ProductDiscount
	WHERE ProductId=@pid AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH('ProductDiscount'), ROOT('ProductDiscountData')
	
END
GO
/****** Object:  StoredProcedure [dbo].[spSelRecommendedProducts]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelRecommendedProducts] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,ISNULL(IsRecommended,0) AS IsRecommended,
	 (SELECT * FROM dbo.[User] WHERE UserId=Product.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Product.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=Product.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.Brand WHERE Id=Product.BrandId
	FOR XML PATH(''), ROOT('Brand'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE),
	(SELECT * FROM dbo.ProductDiscount pd WHERE ISNULL(pd.IsDeleted,0)<>1 AND GETDATE()>=pd.FromDate AND GETDATE()<=DATEADD(dd, 1,pd.EndDate) AND pd.ProductId=Product.Id
	FOR XML PATH(''), ROOT('Discount'), TYPE),
	(SELECT RelatedId AS Id FROM  dbo.RelatedProduct WHERE ProductId=Product.Id
	FOR XML PATH('Product'), ROOT('RelatedItems'), TYPE)
	FROM dbo.Product
	WHERE  ISNULL(IsDeleted,0)<>1 AND ISNULL(IsRecommended,0)=1
	FOR XML PATH('Product'), ROOT('ProductData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelProductById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],IsHidden,Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,ISNULL(IsRecommended,0) AS IsRecommended,
	 (SELECT * FROM dbo.[User] WHERE UserId=Product.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Product.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=Product.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.Country WHERE Id=Product.CountryId
	FOR XML PATH(''), ROOT('Country'), TYPE),
	(SELECT * FROM dbo.Brand WHERE Id=Product.BrandId
	FOR XML PATH(''), ROOT('Brand'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE),
	(SELECT * FROM dbo.ProductDiscount pd WHERE ISNULL(pd.IsDeleted,0)<>1 AND GETDATE()>=pd.FromDate AND GETDATE()<=DATEADD(dd, 1,pd.EndDate) AND pd.ProductId=Product.Id
	FOR XML PATH(''), ROOT('Discount'), TYPE),
	(SELECT p.Id,p.Name,p.Price,(SELECT * FROM dbo.ProductPicture  WHERE ProductId=p.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM  dbo.RelatedProduct rp 
	INNER JOIN Product p ON rp.RelatedId=p.Id
	 WHERE rp.ProductId=Product.Id
	FOR XML PATH('Product'), ROOT('RelatedItems'), TYPE)
	FROM dbo.Product
	WHERE Id=@id AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH('Product'), ROOT('ProductData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelProductByCategory]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductByCategory]
	-- Add the parameters for the stored procedure here
	@categoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 prod.Id,prod.Name,prod.[Description],prod.Price,prod.Quantity,prod.CreatedDate,prod.LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,ISNULL(IsRecommended,0) AS IsRecommended,
	 (SELECT * FROM dbo.[User] WHERE UserId=prod.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=prod.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=prod.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.Brand WHERE Id=prod.BrandId
	FOR XML PATH(''), ROOT('Brand'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=prod.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE),
	(SELECT RelatedId AS Id FROM  dbo.RelatedProduct WHERE ProductId=prod.Id
	FOR XML PATH('Product'), ROOT('RelatedItems'), TYPE)
	FROM dbo.Product prod 
	INNER JOIN Category cat ON prod.CategoryId=cat.Id
	WHERE (cat.Id=@categoryId OR cat.ParentId=@categoryId) AND ISNULL(prod.IsDeleted,0)<>1 AND ISNULL(prod.IsHidden,0)<>1
	FOR XML PATH('Product'), ROOT('ProductData')
END
GO
/****** Object:  Table [dbo].[OrderItem]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItem](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[SubTotal] [money] NOT NULL,
	[DiscountId] [int] NULL,
	[UnitWeight] [decimal](18, 10) NOT NULL,
	[Tags] [xml] NULL,
	[IsHamper] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderHistory]    Script Date: 06/23/2014 11:02:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderHistory](
	[OrderId] [int] NOT NULL,
	[OldStatus] [int] NOT NULL,
	[NewStatus] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[Xml] [xml] NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spDelNewsletter]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelNewsletter]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
    DELETE FROM dbo.NewsletterProduct WHERE NewsletterId=@id
    DELETE FROM dbo.Newsletter WHERE Id=@id
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spDelDeliveryFee]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelDeliveryFee] 
	-- Add the parameters for the stored procedure here
	@deliveryMethodId int,
	@stateId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.DeliveryFee WHERE DeliveryMethodId=@deliveryMethodId AND StateId=@stateId
END
GO
/****** Object:  StoredProcedure [dbo].[spDelCity]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelCity]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.City SET IsEnabled=0 WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spChartOrderStatus]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spChartOrderStatus] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select os.Name AS label,Count(*) as data from [Order] ord
	inner join dbo.OrderStatus os ON os.Id=ord.[Status]
	group by os.Name
	FOR XML PATH('PieChart'), ROOT('PieChartData')
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdNewsletter]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdNewsletter]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Body NVARCHAR(MAX)
	DECLARE @Title NVARCHAR(MAX)
	DECLARE @Type NVARCHAR(50)
	DECLARE @StartDate DATETIME
	DECLARE @IsEnabled BIT

	
	SET @Id =  @xml.value('(/Newsletter/Id)[1]', 'int' )
	SET @Body =  @xml.value('(/Newsletter/Body)[1]', 'nvarchar(max)' )
	SET @Title =  @xml.value('(/Newsletter/Title)[1]', 'nvarchar(max)' )
	SET @Type =  @xml.value('(/Newsletter/SentType)[1]', 'nvarchar(50)' )
	SET @StartDate =  @xml.value('(/Newsletter/StartDate)[1]', 'datetime' )
	SET @IsEnabled =  @xml.value('(/Newsletter/IsEnabled)[1]', 'bit' )
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Newsletter
		SET
			Body=@Body,
			Title=@Title,
			SentType=@Type,
			StartDate=@StartDate,
			IsEnabled=@IsEnabled
		WHERE
			Id=@Id
		DELETE  FROM dbo.NewsletterProduct  WHERE NewsletterId=@Id
		INSERT INTO dbo.NewsletterProduct (NewsletterId,ProductId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as ProductId
		FROM @xml.nodes('(/*/Products)[1]/Product') as x(y)
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Newsletter
		(Title,Body,SentType,CreatedDate,StartDate,IsEnabled)
		VALUES
		(@Title,@Body,@Type,GETDATE(),@StartDate,@IsEnabled)
		SET @Id = SCOPE_IDENTITY()
		
		DELETE  FROM dbo.NewsletterProduct  WHERE NewsletterId=@Id
		INSERT INTO dbo.NewsletterProduct (NewsletterId,ProductId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as ProductId
		FROM @xml.nodes('(/*/Products)[1]/Product') as x(y)
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdCity]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdCity]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @StateId INT
	
	SET @Id =  @xml.value('(/City/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/City/Name)[1]', 'nvarchar(255)' )
	SET @StateId =  @xml.value('(/City/State/Id)[1]', 'int' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.[City]
		SET Name=@Name, IsEnabled=1,StateId=@StateId
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.[City] (Name,StateId,IsEnabled)
		VALUES(@Name,@StateId,1)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdDeliveryFee]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdDeliveryFee]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @DeliveryMethodId INT
	DECLARE @StateId INT
	DECLARE @Fee MONEY
	
	SET @DeliveryMethodId =  @xml.value('(/DeliveryFee/DeliveryMethod/Id)[1]', 'int' )
	SET @StateId =  @xml.value('(/DeliveryFee/State/Id)[1]', 'int' )
	SET @Fee =  @xml.value('(/DeliveryFee/Fee)[1]', 'money' )
	
	IF EXISTS (SELECT 1 FROM dbo.DeliveryFee WHERE DeliveryMethodId=@DeliveryMethodId AND StateId=@StateId)
	BEGIN
		UPDATE dbo.DeliveryFee
		SET Fee=@Fee
		WHERE DeliveryMethodId=@DeliveryMethodId AND StateId=@StateId
	END
	ELSE
	BEGIN
		INSERT INTO dbo.DeliveryFee (DeliveryMethodId,StateId,Fee)
		VALUES(@DeliveryMethodId,@StateId,@Fee)
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @StateId 
	RETURN @StateId
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdAnswer]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdAnswer] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Body NVARCHAR(MAX)
	DECLARE @UserName NVARCHAR(50)
	DECLARE @QuestionId INT
	DECLARE @RepliedTo INT
	DECLARE @UserId INT

	SET @Id =  @xml.value('(/Answer/Id)[1]', 'int' )
	SET @Body =  @xml.value('(/Answer/Body)[1]', 'NVARCHAR(MAX)' )
	SET @UserName =  @xml.value('(/Answer/RepliedBy/UserName)[1]', 'NVARCHAR(50)' )
	SET @QuestionId =  @xml.value('(/Answer/QuestionId)[1]', 'int' )
	SET @RepliedTo =  @xml.value('(/Answer/RepliedTo/Id)[1]', 'int' )
	
	IF (@Id>0)
	BEGIN
		UPDATE dbo.Answer
		SET Body=@Body
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		SELECT @UserId=UserId FROM dbo.[User] WHERE UserName=@UserName
		INSERT INTO dbo.Answer(Body,RepliedBy,RepliedDate,QuestionId,RepliedTo,IsAdminReply)
		VALUES(@Body,@UserId,GETDATE(),@QuestionId,@RepliedTo,0)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spDelSale]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelSale]
	-- Add the parameters for the stored procedure here
	@id INT,
	@username NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @CreatedBy INT
    SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@username
	UPDATE dbo.ProductDiscount SET IsDeleted=1,LastModifiedBy=@CreatedBy,LastModifiedDate=GETDATE()
	WHERE Id=@id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdSale]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdSale] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @Id INT
	DECLARE @Discount DECIMAL(18,10)
	DECLARE @FromDate DATETIME
	DECLARE @EndDate DATETIME
	DECLARE @Description NVARCHAR(MAX)
	DECLARE @ByValue BIT
	DECLARE @ProductId INT
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	
	SET @Id =  @xml.value('(/ProductDiscount/Id)[1]', 'int' )
	SET @Discount =  @xml.value('(/ProductDiscount/Discount)[1]', 'DECIMAL(18,10)' )
	SET @ProductId =  @xml.value('(/ProductDiscount/ProductId)[1]', 'int' )
	
	SET @FromDate =  @xml.value('(/ProductDiscount/FromDate)[1]', 'DATETIME' )
	SET @EndDate =  @xml.value('(/ProductDiscount/EndDate)[1]', 'DATETIME' )
	SET @Description =  @xml.value('(/ProductDiscount/Description)[1]', 'NVARCHAR(MAX)' )
	SET @ByValue =  @xml.value('(/ProductDiscount/ByValue)[1]', 'BIT' )
	
	SET @UserName =  @xml.value('(/ProductDiscount/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/ProductDiscount/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	IF (@Id=0)
	BEGIN
		INSERT INTO dbo.ProductDiscount
		(ProductId,Discount,FromDate,EndDate,[Description],ByValue,CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate)
		VALUES
		(@ProductId,@Discount,@FromDate,@EndDate,@Description,@ByValue,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE())
		SET @Id = SCOPE_IDENTITY()
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdQuestion]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdQuestion]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @Id INT
	DECLARE @TypeId INT
	
	DECLARE @Title NVARCHAR(MAX)
	DECLARE @Description NVARCHAR(MAX)
	DECLARE @Email NVARCHAR(255)
	DECLARE @Answer NVARCHAR(MAX)
	DECLARE @Phone NVARCHAR(50)
	DECLARE @DoctorId INT
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	
	SET @Id =  @xml.value('(/Question/Id)[1]', 'int' )
	SET @TypeId =  @xml.value('(/Question/QuestionType/Id)[1]', 'INT' )
	
	SET @Title =  @xml.value('(/Question/Title)[1]', 'NVARCHAR(MAX)' )
	SET @Description =  @xml.value('(/Question/Description)[1]', 'NVARCHAR(MAX)' )
	SET @Email =  @xml.value('(/Question/Email)[1]', 'NVARCHAR(255)' )
	SET @Answer =  @xml.value('(/Question/Answer)[1]', 'NVARCHAR(MAX)' )
	SET @Phone =  @xml.value('(/Question/Phone)[1]', 'NVARCHAR(50)' )
	SET @DoctorId =  @xml.value('(/Question/Doctor/Id)[1]', 'INT' )
	
	SET @UserName =  @xml.value('(/Question/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Question/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	IF (@Id>0)
	BEGIN
		UPDATE
			dbo.Question
		SET
			Answer=@Answer,
			LastModifiedDate=GETDATE(),
			LastModifiedBy=@LastModifiedBy
		WHERE
			Id=@Id
		
		IF EXISTS(SELECT 1 FROM dbo.Answer WHERE QuestionId=@Id AND ISNULL(IsAdminReply,0)=1)
		BEGIN
			UPDATE dbo.Answer
			SET Body=@Answer
			WHERE QuestionId=@Id AND ISNULL(IsAdminReply,0)=1
		END
		ELSE
		BEGIN
			INSERT INTO dbo.Answer(Body,RepliedBy,RepliedDate,QuestionId,RepliedTo,IsAdminReply)
			VALUES(@Answer,@LastModifiedBy,GETDATE(),@Id,NULL,1)
		END
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Question
		(TypeId,Title,[Description],CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate,Email,Phone,DoctorId)
		VALUES
		(@TypeId,@Title,@Description,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE(),@Email,@Phone,@DoctorId)
		SET @Id = SCOPE_IDENTITY()
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdProduct]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdProduct] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @CategoryId INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Description NVARCHAR(MAX)
	DECLARE @Price money
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	DECLARE @Weight DECIMAL(18,2)
	DECLARE @BrandId INT
	DECLARE @CountryId INT
	
	SET @Id =  @xml.value('(/Product/Id)[1]', 'int' )
	SET @CategoryId =  @xml.value('(/Product/Category/Id)[1]', 'int' )
	SET @BrandId =  @xml.value('(/Product/Brand/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Product/Name)[1]', 'nvarchar(255)' )
	SET @Description =  @xml.value('(/Product/Description)[1]', 'nvarchar(max)' )
	SET @Price =  @xml.value('(/Product/Price)[1]', 'money' )
	SET @Weight =  @xml.value('(/Product/Weight)[1]', 'DECIMAL(18,2)' )
	SET @UserName =  @xml.value('(/Product/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Product/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	SET @CountryId =  @xml.value('(/Product/Country/Id)[1]', 'int' )
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Product
		SET
			Name=@Name,
			CategoryId=@CategoryId,
			BrandId=@BrandId,
			[Description]=@Description,
			Price=@Price,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE(),
			[Weight]=@Weight,
			CountryId=@CountryId,
			Tags=@xml.query('/Product/Tags/ProductTag')
		WHERE
			Id=@Id
		DELETE  FROM dbo.ProductPicture WHERE ProductId=@Id
		INSERT INTO dbo.ProductPicture (ProductId,Picture,IsDefault,Title)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault,
			y.value('Title[1]', 'nvarchar(max)') as Title
		FROM @xml.nodes('(/*/Pictures)[1]/ProductPicture') as x(y)
		
		INSERT INTO dbo.RelatedProduct (ProductId,RelatedId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as RelatedId
		FROM @xml.nodes('(/*/RelatedItems)[1]/Product') as x(y)
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Product
		(CategoryId,BrandId,Name,[Description],Price,CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate,Quantity,[Weight],CountryId,Tags)
		VALUES
		(@CategoryId,@BrandId,@Name,@Description,@Price,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE(),0,@Weight,@CountryId,@xml.query('/Product/Tags/ProductTag'))
		SET @Id = SCOPE_IDENTITY()
		
		INSERT INTO dbo.ProductPicture (ProductId,Picture,IsDefault,Title)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault,
			y.value('Title[1]', 'nvarchar(max)') as Title
		FROM @xml.nodes('(/*/Pictures)[1]/ProductPicture') as x(y)
		
		INSERT INTO dbo.RelatedProduct (ProductId,RelatedId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as RelatedId
		FROM @xml.nodes('(/*/RelatedItems)[1]/Product') as x(y)
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spSelAllProducts]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelAllProducts]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],IsHidden,Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,ISNULL(IsRecommended,0) AS IsRecommended,
	 (SELECT * FROM dbo.[User] WHERE UserId=Product.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Product.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=Product.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.Brand WHERE Id=Product.BrandId
	FOR XML PATH(''), ROOT('Brand'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE),
	(SELECT RelatedId AS Id FROM  dbo.RelatedProduct WHERE ProductId=Product.Id
	FOR XML PATH('Product'), ROOT('RelatedItems'), TYPE)
	FROM dbo.Product
	WHERE ISNULL(IsDeleted,0)<>1
	ORDER BY CreatedDate DESC
	FOR XML PATH('Product'), ROOT('ProductData')
END
GO
/****** Object:  StoredProcedure [dbo].[spPayOrder]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spPayOrder]
	-- Add the parameters for the stored procedure here
	@id INT,
	@paymentid NVARCHAR(255),
	@paymentguid NVARCHAR(255),
	@status NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @OldStatusId INT
	DECLARE @NewStatusId INT
	DECLARE @userId INT
	SELECT @OldStatusId = [Status],@userId=CreatedBy FROM dbo.[Order] WHERE Id=@Id
	SELECT @NewStatusId = Id FROM dbo.OrderStatus WHERE Name=@status
	UPDATE dbo.[Order] SET [Status]=@NewStatusId,PaymentId=@paymentid,PaymentGuid=@paymentguid WHERE Id=@Id
	
	INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
	VALUES
	(@id,@OldStatusId,@NewStatusId,@userId,GETDATE(),'')
END
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdOrder]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdOrder] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	DECLARE @Id INT
	DECLARE @Status NVARCHAR(50)
	DECLARE @Total MONEY
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @StatusId INT
	DECLARE @AddressId INT
	DECLARE @OldStatusId INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	DECLARE @FirstName NVARCHAR(255)
	DECLARE @LastName NVARCHAR(255)
	DECLARE @MobileNo NVARCHAR(50)
	DECLARE @TelephoneNo NVARCHAR(50)
	DECLARE @Postcode NVARCHAR(50)
	DECLARE @Country NVARCHAR(255)
	DECLARE @State NVARCHAR(255)
	DECLARE @City NVARCHAR(255)
	DECLARE @Address1 NVARCHAR(MAX)
	DECLARE @Address2 NVARCHAR(MAX)
	DECLARE @DeliveryId INT
	DECLARE @PaymentId INT
	DECLARE @DeliveryFee money
	DECLARE @TaxRate decimal(18,10)
	DECLARE @RepeatCustomerDiscount money
	
	--validate quantity
	IF EXISTS (SELECT 1 FROM dbo.Product prod 
	INNER JOIN (
	SELECT
		y.value('Product[1]/Id[1]', 'int') as ProductId,
		y.value('Quantity[1]', 'int') as Quantity
	FROM @xml.nodes('(/*/Items)[1]/OrderItem') as x(y)) cart ON prod.Id=cart.ProductId
	WHERE prod.Quantity<cart.Quantity)
	BEGIN
		RAISERROR('The quantity in cart is larger than inventory',16,16)
	END
	
	SET @Id =  @xml.value('(/Order/Id)[1]', 'int' )
	SET @Status =  @xml.value('(/Order/Status)[1]', 'nvarchar(50)' )
	SELECT TOP 1 @StatusId = Id FROM dbo.[OrderStatus] WHERE Name=@Status
	SET @Total =  @xml.value('(/Order/Total)[1]', 'money' )
	SET @DeliveryId =  @xml.value('(/Order/DeliveryMethod/Id)[1]', 'int') 
	SET @PaymentId =  @xml.value('(/Order/PaymentMethod/Id)[1]', 'int')
	SET @DeliveryFee =  @xml.value('(/Order/DeliveryFee)[1]', 'money' )
	SET @TaxRate =  @xml.value('(/Order/TaxRate)[1]', 'decimal(18,10)' )
	SET @UserName =  @xml.value('(/Order/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Order/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	SET @FirstName =  @xml.value('(/Order/ShippingAddress/FirstName)[1]', 'NVARCHAR(255)' )
	SET @LastName =  @xml.value('(/Order/ShippingAddress/LastName)[1]', 'NVARCHAR(255)' )
	SET @MobileNo =  @xml.value('(/Order/ShippingAddress/MobileNo)[1]', 'NVARCHAR(50)' )
	SET @TelephoneNo =  @xml.value('(/Order/ShippingAddress/TelephoneNo)[1]', 'NVARCHAR(50)' )
	SET @Postcode =  @xml.value('(/Order/ShippingAddress/Postcode)[1]', 'NVARCHAR(50)' )
	SET @Country =  @xml.value('(/Order/ShippingAddress/Country)[1]', 'NVARCHAR(255)' )
	SET @State =  @xml.value('(/Order/ShippingAddress/State)[1]', 'NVARCHAR(255)' )
	SET @City =  @xml.value('(/Order/ShippingAddress/City)[1]', 'NVARCHAR(255)' )
	SET @Address1 =  @xml.value('(/Order/ShippingAddress/Address1)[1]', 'NVARCHAR(MAX)' )
	SET @Address2 =  @xml.value('(/Order/ShippingAddress/Address2)[1]', 'NVARCHAR(MAX)' )
		
	SET @AddressId =  @xml.value('(/Order/ShippingAddress/Id)[1]', 'INT' )
	SET @RepeatCustomerDiscount =  @xml.value('(/Order/RepeatCustomerDiscount)[1]', 'money' )
	
	
	IF (@Id>0)
	BEGIN
		SELECT @OldStatusId = [Status] FROM dbo.[Order] WHERE Id=@Id
		UPDATE
			dbo.[Order]
		SET
			[Status]=@StatusId,
			Total=@Total,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE(),
			ShippingAddress=@AddressId,
			DeliveryFee=@DeliveryFee,
			Country=@Country,
			[State]=@State,
			City=@City,
			Address1=@Address1,
			Address2=@Address2,
			MobileNo=@MobileNo,
			TelephoneNo=@TelephoneNo,
			Postcode=@Postcode,
			FirstName=@FirstName,
			LastName=@LastName,
			TaxRate=@TaxRate,
			RepeatCustomerDiscount=@RepeatCustomerDiscount
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		SET @OldStatusId = @StatusId
		INSERT INTO dbo.[Order]
		(CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate,ShippingAddress,[Status],Total,DeliveryFee,DeliveryMethodId,
		PaymentMethodId,Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName,TaxRate,RepeatCustomerDiscount)
		VALUES
		(@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE(),@AddressId,@StatusId,@Total,@DeliveryFee,@DeliveryId,
		@PaymentId,@Country,@State,@City,@Address1,@Address2,@MobileNo,@TelephoneNo,@Postcode,@FirstName,@LastName,@TaxRate,@RepeatCustomerDiscount)
		SET @Id = SCOPE_IDENTITY()
	END
	
	DELETE FROM dbo.OrderItem WHERE OrderId=@Id
	INSERT INTO dbo.OrderItem (OrderId,ProductId,Quantity,UnitPrice,SubTotal,DiscountId,UnitWeight,IsHamper,Tags)
	SELECT
		@Id,
		y.value('Product[1]/Id[1]', 'int') as ProductId,
		y.value('Quantity[1]', 'int') as Quantity,
		y.value('UnitPrice[1]', 'money') as UnitPrice,
		y.value('SubTotal[1]', 'money') as SubTotal,
		y.value('DiscountId[1]', 'int') as DiscountId,
		y.value('UnitWeight[1]', 'decimal(18,10)') as UnitWeight,
		y.value('IsHamper[1]', 'BIT') as IsHamper,
		y.query('Tags/ProductTag') as Tags
		
	FROM @xml.nodes('(/*/Items)[1]/OrderItem') as x(y)
	
	--update inventory
	UPDATE prod
	SET prod.Quantity=prod.Quantity-cart.Quantity
	FROM dbo.Product AS prod
	INNER JOIN (
	SELECT
		y.value('Product[1]/Id[1]', 'int') as ProductId,
		y.value('Quantity[1]', 'int') as Quantity,
		y.value('IsHamper[1]', 'BIT') as IsHamper
	FROM @xml.nodes('(/*/Items)[1]/OrderItem') as x(y)) AS cart ON prod.Id=cart.ProductId AND ISNULL(cart.IsHamper,0)=0
	
	UPDATE prod
	SET prod.Quantity=prod.Quantity-cart.Quantity
	FROM dbo.Product AS prod
	INNER JOIN (
	SELECT
		y.value('Id[1]', 'int') as ProductId,
		y.value('Quantity[1]', 'int') as Quantity
	FROM @xml.nodes('(/*/Items)[1]/OrderItem/Products/Product') as x(y)) AS cart ON prod.Id=cart.ProductId
	
	INSERT INTO dbo.OrderHistory (OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark,[Xml])
	VALUES
	(@Id,@OldStatusId,@StatusId,@CreatedBy,GETDATE(),NULL,@xml)
	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = ERROR_SEVERITY()
		SET @msg = ERROR_MESSAGE()
		SET @errstate = ERROR_STATE()
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spChartBestSeller]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spChartBestSeller]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top 5 prod.Name as label,Sum(oi.Quantity) as data from [Order] ord
	inner join dbo.OrderItem oi ON oi.OrderId=ord.Id
	INNER JOIN dbo.Product prod ON oi.ProductId=prod.Id
	inner join dbo.OrderStatus os ON os.Id=ord.[Status]
	WHERE os.Name<>'Cancelled'
	group by prod.Name
	order by data desc
	FOR XML PATH('PieChart'), ROOT('PieChartData')
END
GO
/****** Object:  StoredProcedure [dbo].[spCancelOverdueOrders]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCancelOverdueOrders]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
    DECLARE @sid INT
    DECLARE @cid INT
    SELECT @sid=Id FROM dbo.OrderStatus WHERE Name='Cancelled'
    SELECT @cid=Id FROM dbo.OrderStatus WHERE Name='Created'

    INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
    SELECT Id,@cid,@sid,CreatedBy,GETDATE(),'Cancelled automatically!' FROM [order] where createddate<= DATEADD(day, -1, GETDATE()) AND [Status]=@cid
    
    --restock if cancel
    UPDATE prod
    SET prod.Quantity=prod.Quantity+cords.Quantity
    FROM dbo.Product prod INNER JOIN
    (SELECT oi.ProductId AS ProductId, SUM(oi.Quantity) AS Quantity FROM
    dbo.OrderItem oi
    INNER JOIN dbo.[Order] ord ON ord.Id=oi.OrderId
    WHERE ord.createddate<= DATEADD(day, -1, GETDATE()) AND ord.[Status]=@cid
    GROUP BY oi.ProductId
    ) cords ON prod.Id=cords.ProductId
    
    DECLARE @tagxml XML
	DECLARE @qty INT
	DECLARE hamper_tags CURSOR FOR
	SELECT Tags,Quantity FROM dbo.OrderItem oi
    INNER JOIN dbo.[Order] ord ON ord.Id=oi.OrderId
    WHERE ord.createddate<= DATEADD(day, -1, GETDATE()) AND ord.[Status]=@cid AND ISNULL(oi.IsHamper,0)=1

	OPEN hamper_tags

	FETCH NEXT FROM hamper_tags 
	INTO @tagxml,@qty

	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE prod
			SET prod.Quantity=prod.Quantity+@qty
			FROM dbo.Product prod
			INNER JOIN (SELECT DISTINCT y.value('ProductId[1]', 'INT') as ProductId
				FROM @tagxml.nodes('/ProductTag') as x(y)) oi ON prod.Id=oi.ProductId
		
		FETCH NEXT FROM hamper_tags 
		INTO @tagxml, @qty
	END

	CLOSE hamper_tags
	DEALLOCATE hamper_tags
    
	 UPDATE [order] SET [Status]=@sid 
	 where createddate<= DATEADD(day, -1, GETDATE()) AND [Status]=@cid
	 END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spCancelOrverdueOrdersByUser]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCancelOrverdueOrdersByUser] 
	-- Add the parameters for the stored procedure here
	@uid INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
    DECLARE @sid INT
    DECLARE @cid INT
    SELECT @sid=Id FROM dbo.OrderStatus WHERE Name='Cancelled'
    SELECT @cid=Id FROM dbo.OrderStatus WHERE Name='Created'
    
    INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
    SELECT Id,@cid,@sid,@uid,GETDATE(),'Cancelled automatically!' FROM [order] where createddate<= DATEADD(day, -1, GETDATE()) AND [Status]=@cid AND CreatedBy=@uid
    
    --restock if cancel
    UPDATE prod
    SET prod.Quantity=prod.Quantity+cords.Quantity
    FROM dbo.Product prod INNER JOIN
    (SELECT oi.ProductId AS ProductId, SUM(oi.Quantity) AS Quantity FROM
    dbo.OrderItem oi
    INNER JOIN dbo.[Order] ord ON ord.Id=oi.OrderId
    WHERE ord.createddate<= DATEADD(day, -1, GETDATE()) AND ord.[Status]=@cid AND ord.CreatedBy=@uid
    GROUP BY oi.ProductId
    ) cords ON prod.Id=cords.ProductId
    
    DECLARE @tagxml XML
	DECLARE @qty INT
	DECLARE hamper_tags CURSOR FOR
	SELECT Tags,Quantity FROM dbo.OrderItem oi
    INNER JOIN dbo.[Order] ord ON ord.Id=oi.OrderId
    WHERE ord.createddate<= DATEADD(day, -1, GETDATE()) AND ord.[Status]=@cid AND ord.CreatedBy=@uid AND ISNULL(oi.IsHamper,0)=1

	OPEN hamper_tags

	FETCH NEXT FROM hamper_tags 
	INTO @tagxml,@qty

	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE prod
			SET prod.Quantity=prod.Quantity+@qty
			FROM dbo.Product prod
			INNER JOIN (SELECT DISTINCT y.value('ProductId[1]', 'INT') as ProductId
				FROM @tagxml.nodes('/ProductTag') as x(y)) oi ON prod.Id=oi.ProductId
		
		FETCH NEXT FROM hamper_tags 
		INTO @tagxml, @qty
	END

	CLOSE hamper_tags
	DEALLOCATE hamper_tags
    
	 UPDATE [order] SET [Status]=@sid 
	 where createddate<= DATEADD(day, -1, GETDATE()) AND [Status]=@cid AND CreatedBy=@uid
	 END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[spUpdOrderStatus]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdOrderStatus]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
    DECLARE @id int
	DECLARE @newStatus NVARCHAR(50)
	DECLARE @username NVARCHAR(50)
	DECLARE @remark NVARCHAR(max)
	DECLARE @shippingno NVARCHAR(255)
	
	SET @id =  @xml.value('(/OrderHistory/OrderId)[1]', 'int' )
	SET @newStatus =  @xml.value('(/OrderHistory/NewStatus)[1]', 'nvarchar(50)' )
	SET @username =  @xml.value('(/OrderHistory/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @remark =  @xml.value('(/OrderHistory/Remark)[1]', 'nvarchar(max)' )
	SET @shippingno =  @xml.value('(/OrderHistory/ShippingNo)[1]', 'nvarchar(255)' )
	
	DECLARE @OldStatusId INT
	DECLARE @NewStatusId INT
	DECLARE @userId INT
	SELECT @OldStatusId = [Status] FROM dbo.[Order] WHERE Id=@Id
	SELECT @NewStatusId = Id FROM dbo.OrderStatus WHERE Name=@newStatus
	SELECT @userId=UserId FROM dbo.[User] WHERE UserName=@username
	
	--restock if cancelled
	IF(@newStatus='Cancelled')
	BEGIN
		UPDATE prod
		SET prod.Quantity=prod.Quantity+oi.Quantity
		FROM dbo.Product prod
		INNER JOIN dbo.OrderItem oi ON prod.Id=oi.ProductId AND ISNULL(oi.IsHamper,0)=0
		WHERE oi.OrderId=@Id
		
		DECLARE @tagxml XML
		DECLARE @qty INT
		DECLARE hamper_tags CURSOR FOR
		SELECT Tags,Quantity FROM dbo.OrderItem WHERE OrderId=@Id AND ISNULL(IsHamper,0)=1

		OPEN hamper_tags

		FETCH NEXT FROM hamper_tags 
		INTO @tagxml,@qty

		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE prod
				SET prod.Quantity=prod.Quantity+@qty
				FROM dbo.Product prod
				INNER JOIN (SELECT DISTINCT y.value('ProductId[1]', 'INT') as ProductId
					FROM @tagxml.nodes('/ProductTag') as x(y)) oi ON prod.Id=oi.ProductId
			
			FETCH NEXT FROM hamper_tags 
			INTO @tagxml, @qty
		END

		CLOSE hamper_tags
		DEALLOCATE hamper_tags
	END
	
	--update shipping no 
	IF(@newStatus='Sent')
	BEGIN
		UPDATE dbo.[Order] SET ShippingNo=@shippingno WHERE Id=@Id
	END
	
	UPDATE dbo.[Order] SET [Status]=@NewStatusId WHERE Id=@Id
	
	INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
	VALUES
	(@id,@OldStatusId,@NewStatusId,@userId,GETDATE(),@remark)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

END
GO
/****** Object:  StoredProcedure [dbo].[spSelOrderHistory]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrderHistory]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT OrderId,CreatedDate,Remark,
	(SELECT Name FROM dbo.OrderStatus WHERE Id=OrderHistory.OldStatus) OldStatus,
	(SELECT Name FROM dbo.OrderStatus WHERE Id=OrderHistory.NewStatus) NewStatus,
	(SELECT * FROM dbo.[User] WHERE UserId=OrderHistory.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE)
	FROM dbo.OrderHistory
	WHERE OrderId=@Id
	ORDER BY CreatedDate DESC
	FOR XML PATH('OrderHistory'), ROOT('OrderHistoryData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelOrderByPaymentGuid]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrderByPaymentGuid]
	-- Add the parameters for the stored procedure here
	@guid nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Insert statements for procedure here
	SELECT ord.Id, ord.CreatedDate,ord.LastModifiedDate,ord.Total,os.Name AS [Status],DeliveryFee,
	 (SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT ShippingAddress AS Id, Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName FROM dbo.[Order] WHERE Id=ord.Id
	FOR XML PATH(''), ROOT('ShippingAddress'), TYPE),
	(SELECT Quantity, UnitPrice,SubTotal,UnitWeight,Tags,(SELECT Name,ID, (SELECT * FROM dbo.ProductPicture WHERE ProductId=prod.Id FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod WHERE prod.Id=orditem.ProductId FOR XML PATH(''), ROOT('Product'), TYPE),
	(SELECT *,
	(SELECT * FROM dbo.ProductSetPicture  WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetPicture'), ROOT('Pictures'), TYPE),
	(SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=ProductSetItem.ProductId AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH(''), ROOT('Product'), TYPE
	) FROM  dbo.ProductSetItem WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetItem'), ROOT('Items'), TYPE)
	
	 FROM dbo.ProductSet
	WHERE Id=orditem.ProductId AND ISNULL(IsDeleted,0)<>1 AND ISNULL(orditem.IsHamper,0)=1
	FOR XML PATH(''), ROOT('ProductSet'), TYPE
	) 
	FROM dbo.OrderItem orditem WHERE orditem.OrderId=ord.Id
	FOR XML PATH('OrderItem'), ROOT('Items'), TYPE)
	FROM
	dbo.[Order] ord
	INNER JOIN dbo.OrderStatus os ON ord.[Status]=os.Id
	WHERE ord.PaymentGuid=@guid AND os.Name='Created'
	ORDER BY CreatedDate DESC
	FOR XML PATH('Order'), ROOT('OrderData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelOrderById]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrderById]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ord.Id, ord.CreatedDate,ord.LastModifiedDate,ord.Total,os.Name AS [Status],DeliveryFee,TaxRate,ShippingNo,RepeatCustomerDiscount,
	(SELECT * FROM dbo.DeliveryMethod WHERE Id=ord.DeliveryMethodId
	FOR XML PATH(''), ROOT('DeliveryMethod'), TYPE),
	(SELECT * FROM dbo.PaymentMethod WHERE Id=ord.PaymentMethodId
	FOR XML PATH(''), ROOT('PaymentMethod'), TYPE),
	 (SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT ShippingAddress AS Id, Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName FROM dbo.[Order] WHERE Id=ord.Id
	FOR XML PATH(''), ROOT('ShippingAddress'), TYPE),
	(SELECT Quantity, UnitPrice,SubTotal,UnitWeight, Tags,IsHamper,(SELECT Name,Id,Quantity,[Weight], 
	
	(SELECT * FROM dbo.ProductPicture WHERE ProductId=prod.Id FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod WHERE prod.Id=orditem.ProductId AND ISNULL(orditem.IsHamper,0)=0 FOR XML PATH(''), ROOT('Product'), TYPE), 
	
	(SELECT *,
	(SELECT * FROM dbo.ProductSetPicture  WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetPicture'), ROOT('Pictures'), TYPE),
	(SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=ProductSetItem.ProductId AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH(''), ROOT('Product'), TYPE
	) FROM  dbo.ProductSetItem WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetItem'), ROOT('Items'), TYPE)
	
	 FROM dbo.ProductSet
	WHERE Id=orditem.ProductId AND ISNULL(IsDeleted,0)<>1 AND ISNULL(orditem.IsHamper,0)=1
	FOR XML PATH(''), ROOT('ProductSet'), TYPE
	)
	
	FROM dbo.OrderItem orditem WHERE orditem.OrderId=ord.Id
	FOR XML PATH('OrderItem'), ROOT('Items'), TYPE)
	FROM
	dbo.[Order] ord
	INNER JOIN dbo.OrderStatus os ON ord.[Status]=os.Id
	WHERE ord.Id=@id
	ORDER BY CreatedDate DESC
	FOR XML PATH('Order'), ROOT('OrderData')
END
GO
/****** Object:  StoredProcedure [dbo].[spSelOrdersByUser]    Script Date: 06/23/2014 11:02:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrdersByUser]
	-- Add the parameters for the stored procedure here
	@username NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    DECLARE @UserId INT
    SELECT @UserId = UserId FROM dbo.[User] WHERE UserName=@username
    EXEC spCancelOrverdueOrdersByUser @UserId
    
	SELECT ord.Id, ord.CreatedDate,ord.LastModifiedDate,ord.Total,os.Name AS [Status],DeliveryFee,
	 (SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT ShippingAddress AS Id, Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName FROM dbo.[Order] WHERE Id=ord.Id
	FOR XML PATH(''), ROOT('ShippingAddress'), TYPE),
	(SELECT IsHamper, Quantity, UnitPrice,SubTotal,UnitWeight,Tags,
	(SELECT Name,Id, (SELECT * FROM dbo.ProductPicture WHERE ProductId=prod.Id FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod WHERE prod.Id=orditem.ProductId  AND  ISNULL(orditem.IsHamper,0)=0 FOR XML PATH(''), ROOT('Product'), TYPE), 
	
	(SELECT *,
	(SELECT * FROM dbo.ProductSetPicture  WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetPicture'), ROOT('Pictures'), TYPE),
	(SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=ProductSetItem.ProductId AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH(''), ROOT('Product'), TYPE
	) FROM  dbo.ProductSetItem WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetItem'), ROOT('Items'), TYPE)
	
	 FROM dbo.ProductSet
	WHERE Id=orditem.ProductId AND ISNULL(IsDeleted,0)<>1 AND ISNULL(orditem.IsHamper,0)=1
	FOR XML PATH(''), ROOT('ProductSet'), TYPE
	)
	
	FROM dbo.OrderItem orditem WHERE orditem.OrderId=ord.Id
	FOR XML PATH('OrderItem'), ROOT('Items'), TYPE)
	FROM
	dbo.[Order] ord
	INNER JOIN dbo.OrderStatus os ON ord.[Status]=os.Id
	WHERE CreatedBy=@UserId
	ORDER BY CreatedDate DESC
	FOR XML PATH('Order'), ROOT('OrderData')
END
GO
/****** Object:  StoredProcedure [dbo].[spAllOrders]    Script Date: 06/23/2014 11:02:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAllOrders]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
exec spCancelOverdueOrders
    -- Insert statements for procedure here
	SELECT ord.Id, ord.CreatedDate,ord.LastModifiedDate,ord.Total,os.Name AS [Status],DeliveryFee,TaxRate,
	(SELECT * FROM dbo.DeliveryMethod WHERE Id=ord.DeliveryMethodId
	FOR XML PATH(''), ROOT('DeliveryMethod'), TYPE),
	(SELECT * FROM dbo.PaymentMethod WHERE Id=ord.PaymentMethodId
	FOR XML PATH(''), ROOT('PaymentMethod'), TYPE),
	 (SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT ShippingAddress AS Id, Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName FROM dbo.[Order] WHERE Id=ord.Id
	FOR XML PATH(''), ROOT('ShippingAddress'), TYPE),
	(SELECT Quantity, UnitPrice,SubTotal,UnitWeight,Tags,IsHamper,
	(SELECT Name,Id,Quantity,[Weight], (SELECT * FROM dbo.ProductPicture WHERE ProductId=prod.Id FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod WHERE prod.Id=orditem.ProductId FOR XML PATH(''), ROOT('Product'), TYPE), 
	
	(SELECT *,
	(SELECT * FROM dbo.ProductSetPicture  WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetPicture'), ROOT('Pictures'), TYPE),
	(SELECT *,
	(SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,
	 (SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM dbo.Product
	WHERE Id=ProductSetItem.ProductId AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH(''), ROOT('Product'), TYPE
	) FROM  dbo.ProductSetItem WHERE ProductSetId=ProductSet.Id
	FOR XML PATH('ProductSetItem'), ROOT('Items'), TYPE)
	
	 FROM dbo.ProductSet
	WHERE Id=orditem.ProductId AND ISNULL(IsDeleted,0)<>1 AND ISNULL(orditem.IsHamper,0)=1
	FOR XML PATH(''), ROOT('ProductSet'), TYPE
	)
	
	FROM dbo.OrderItem orditem WHERE orditem.OrderId=ord.Id
	FOR XML PATH('OrderItem'), ROOT('Items'), TYPE)
	FROM
	dbo.[Order] ord
	INNER JOIN dbo.OrderStatus os ON ord.[Status]=os.Id
	ORDER BY CreatedDate DESC
	FOR XML PATH('Order'), ROOT('OrderData')
END
GO
/****** Object:  Default [DF_Product_Weight]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_Weight]  DEFAULT ((0)) FOR [Weight]
GO
/****** Object:  ForeignKey [FK_Answer_Question]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Question] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([Id])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Question]
GO
/****** Object:  ForeignKey [FK_Answer_User]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_User] FOREIGN KEY([RepliedBy])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_User]
GO
/****** Object:  ForeignKey [FK_City_State]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_State]
GO
/****** Object:  ForeignKey [FK_DeliveryFee_DeliveryMethod]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[DeliveryFee]  WITH CHECK ADD  CONSTRAINT [FK_DeliveryFee_DeliveryMethod] FOREIGN KEY([DeliveryMethodId])
REFERENCES [dbo].[DeliveryMethod] ([Id])
GO
ALTER TABLE [dbo].[DeliveryFee] CHECK CONSTRAINT [FK_DeliveryFee_DeliveryMethod]
GO
/****** Object:  ForeignKey [FK_DeliveryFee_State]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[DeliveryFee]  WITH CHECK ADD  CONSTRAINT [FK_DeliveryFee_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
ALTER TABLE [dbo].[DeliveryFee] CHECK CONSTRAINT [FK_DeliveryFee_State]
GO
/****** Object:  ForeignKey [FK_NewsletterProduct_Newsletter]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[NewsletterProduct]  WITH CHECK ADD  CONSTRAINT [FK_NewsletterProduct_Newsletter] FOREIGN KEY([NewsletterId])
REFERENCES [dbo].[Newsletter] ([Id])
GO
ALTER TABLE [dbo].[NewsletterProduct] CHECK CONSTRAINT [FK_NewsletterProduct_Newsletter]
GO
/****** Object:  ForeignKey [FK_NewsletterProduct_Product]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[NewsletterProduct]  WITH CHECK ADD  CONSTRAINT [FK_NewsletterProduct_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[NewsletterProduct] CHECK CONSTRAINT [FK_NewsletterProduct_Product]
GO
/****** Object:  ForeignKey [FK_Order_DeliveryMethod]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_DeliveryMethod] FOREIGN KEY([DeliveryMethodId])
REFERENCES [dbo].[DeliveryMethod] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_DeliveryMethod]
GO
/****** Object:  ForeignKey [FK_Order_OrderStatus]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderStatus]
GO
/****** Object:  ForeignKey [FK_Order_PaymentMethod]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_PaymentMethod] FOREIGN KEY([PaymentMethodId])
REFERENCES [dbo].[PaymentMethod] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_PaymentMethod]
GO
/****** Object:  ForeignKey [FK_Order_User]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_User]
GO
/****** Object:  ForeignKey [FK_Order_User1]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_User1] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_User1]
GO
/****** Object:  ForeignKey [FK_Order_UserAddress]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_UserAddress] FOREIGN KEY([ShippingAddress])
REFERENCES [dbo].[UserAddress] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_UserAddress]
GO
/****** Object:  ForeignKey [FK_OrderHistory_Order]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_Order]
GO
/****** Object:  ForeignKey [FK_OrderHistory_OrderStatus]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_OrderStatus] FOREIGN KEY([OldStatus])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_OrderStatus]
GO
/****** Object:  ForeignKey [FK_OrderHistory_OrderStatus1]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_OrderStatus1] FOREIGN KEY([NewStatus])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_OrderStatus1]
GO
/****** Object:  ForeignKey [FK_OrderHistory_User]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_User]
GO
/****** Object:  ForeignKey [FK_OrderItem_Order]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[OrderItem]  WITH CHECK ADD  CONSTRAINT [FK_OrderItem_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[OrderItem] CHECK CONSTRAINT [FK_OrderItem_Order]
GO
/****** Object:  ForeignKey [FK_Product_Brand]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Brand] FOREIGN KEY([BrandId])
REFERENCES [dbo].[Brand] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Brand]
GO
/****** Object:  ForeignKey [FK_Product_Category]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Category]
GO
/****** Object:  ForeignKey [FK_ProductDiscount_Product]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[ProductDiscount]  WITH CHECK ADD  CONSTRAINT [FK_ProductDiscount_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[ProductDiscount] CHECK CONSTRAINT [FK_ProductDiscount_Product]
GO
/****** Object:  ForeignKey [FK_ProductPicture_Product]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[ProductPicture]  WITH CHECK ADD  CONSTRAINT [FK_ProductPicture_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[ProductPicture] CHECK CONSTRAINT [FK_ProductPicture_Product]
GO
/****** Object:  ForeignKey [FK_ProductPromotion_Product]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[ProductPromotion]  WITH CHECK ADD  CONSTRAINT [FK_ProductPromotion_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[ProductPromotion] CHECK CONSTRAINT [FK_ProductPromotion_Product]
GO
/****** Object:  ForeignKey [FK_Question_QuestionType]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_QuestionType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[QuestionType] ([Id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_QuestionType]
GO
/****** Object:  ForeignKey [FK_State_Country]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[State]  WITH CHECK ADD  CONSTRAINT [FK_State_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[State] CHECK CONSTRAINT [FK_State_Country]
GO
/****** Object:  ForeignKey [FK_StockHistory_Product]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[StockHistory]  WITH CHECK ADD  CONSTRAINT [FK_StockHistory_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[StockHistory] CHECK CONSTRAINT [FK_StockHistory_Product]
GO
/****** Object:  ForeignKey [FK_UserAddress_User]    Script Date: 06/23/2014 11:02:40 ******/
ALTER TABLE [dbo].[UserAddress]  WITH CHECK ADD  CONSTRAINT [FK_UserAddress_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserAddress] CHECK CONSTRAINT [FK_UserAddress_User]
GO
INSERT INTO dbo.OrderStatus(Name)VALUES('Created')
INSERT INTO dbo.OrderStatus(Name)VALUES('Paid')
INSERT INTO dbo.OrderStatus(Name)VALUES('Cancelled')
INSERT INTO dbo.OrderStatus(Name)VALUES('Preparing')
INSERT INTO dbo.OrderStatus(Name)VALUES('Sent')
INSERT INTO dbo.OrderStatus(Name)VALUES('Delivered')
GO