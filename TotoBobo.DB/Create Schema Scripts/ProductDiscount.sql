﻿-- =============================================
-- Script Template
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDiscount_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDiscount]'))
ALTER TABLE [dbo].[ProductDiscount] DROP CONSTRAINT [FK_ProductDiscount_Product]
GO

/****** Object:  Table [dbo].[ProductDiscount]    Script Date: 04/17/2014 15:22:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductDiscount]') AND type in (N'U'))
DROP TABLE [dbo].[ProductDiscount]
GO

/****** Object:  Table [dbo].[ProductDiscount]    Script Date: 04/17/2014 15:22:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProductDiscount](
	[Id] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Discount] [decimal](18, 0) NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[ByValue] [bit] NOT NULL,
 CONSTRAINT [PK_ProductDiscount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProductDiscount]  WITH CHECK ADD  CONSTRAINT [FK_ProductDiscount_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO

ALTER TABLE [dbo].[ProductDiscount] CHECK CONSTRAINT [FK_ProductDiscount_Product]
GO


