﻿-- =============================================
-- Script Template
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StockHistory_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[StockHistory]'))
ALTER TABLE [dbo].[StockHistory] DROP CONSTRAINT [FK_StockHistory_Product]
GO

/****** Object:  Table [dbo].[StockHistory]    Script Date: 04/17/2014 15:23:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StockHistory]') AND type in (N'U'))
DROP TABLE [dbo].[StockHistory]
GO


/****** Object:  Table [dbo].[StockHistory]    Script Date: 04/17/2014 15:23:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StockHistory](
	[ProductId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StockHistory]  WITH CHECK ADD  CONSTRAINT [FK_StockHistory_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO

ALTER TABLE [dbo].[StockHistory] CHECK CONSTRAINT [FK_StockHistory_Product]
GO


