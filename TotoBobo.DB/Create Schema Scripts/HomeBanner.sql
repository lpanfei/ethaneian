﻿-- =============================================
-- Script Template
-- =============================================
/****** Object:  Table [dbo].[HomeBanner]    Script Date: 04/30/2014 18:23:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HomeBanner]') AND type in (N'U'))
DROP TABLE [dbo].[HomeBanner]
GO

/****** Object:  Table [dbo].[HomeBanner]    Script Date: 04/30/2014 18:23:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HomeBanner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TopBanner] [nvarchar](255) NOT NULL,
	[TopBannerLink] [nvarchar](max) NULL,
	[Banner1] [nvarchar](255) NOT NULL,
	[Banner1Link] [nvarchar](max) NULL,
	[Banner2] [nvarchar](255) NOT NULL,
	[Banner2Link] [nvarchar](max) NULL,
	[Banner3] [nvarchar](255) NULL,
	[Banner3Link] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_HomeBanner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


