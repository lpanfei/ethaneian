﻿-- =============================================
-- Script Template
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DeliveryFee_DeliveryMethod]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeliveryFee]'))
ALTER TABLE [dbo].[DeliveryFee] DROP CONSTRAINT [FK_DeliveryFee_DeliveryMethod]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DeliveryFee_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeliveryFee]'))
ALTER TABLE [dbo].[DeliveryFee] DROP CONSTRAINT [FK_DeliveryFee_State]
GO


/****** Object:  Table [dbo].[DeliveryFee]    Script Date: 04/25/2014 18:17:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeliveryFee]') AND type in (N'U'))
DROP TABLE [dbo].[DeliveryFee]
GO


/****** Object:  Table [dbo].[DeliveryFee]    Script Date: 04/25/2014 18:17:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DeliveryFee](
	[DeliveryMethodId] [int] NOT NULL,
	[StateId] [int] NOT NULL,
	[Fee] [money] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DeliveryFee]  WITH CHECK ADD  CONSTRAINT [FK_DeliveryFee_DeliveryMethod] FOREIGN KEY([DeliveryMethodId])
REFERENCES [dbo].[DeliveryMethod] ([Id])
GO

ALTER TABLE [dbo].[DeliveryFee] CHECK CONSTRAINT [FK_DeliveryFee_DeliveryMethod]
GO

ALTER TABLE [dbo].[DeliveryFee]  WITH CHECK ADD  CONSTRAINT [FK_DeliveryFee_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO

ALTER TABLE [dbo].[DeliveryFee] CHECK CONSTRAINT [FK_DeliveryFee_State]
GO


