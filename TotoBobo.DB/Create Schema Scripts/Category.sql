﻿-- =============================================
-- Script Template
-- =============================================

/****** Object:  Table [dbo].[Category]    Script Date: 04/17/2014 15:20:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Category]') AND type in (N'U'))
DROP TABLE [dbo].[Category]
GO

/****** Object:  Table [dbo].[Category]    Script Date: 04/17/2014 15:20:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ParentId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF NOT EXISTS(SELECT 1 FROM sys.columns
WHERE Name = N'TopBanner' AND OBJECT_ID = OBJECT_ID(N'Category'))
BEGIN
	ALTER TABLE dbo.Category
	ADD [TopBanner] NVARCHAR(255)
END 
GO
IF NOT EXISTS(SELECT 1 FROM sys.columns
WHERE Name = N'Banner1' AND OBJECT_ID = OBJECT_ID(N'Category'))
BEGIN
	ALTER TABLE dbo.Category
	ADD [Banner1] NVARCHAR(255)
END 
GO
IF NOT EXISTS(SELECT 1 FROM sys.columns
WHERE Name = N'Banner1Link' AND OBJECT_ID = OBJECT_ID(N'Category'))
BEGIN
	ALTER TABLE dbo.Category
	ADD [Banner1Link] NVARCHAR(MAX)
END 
GO