﻿-- =============================================
-- Script Template
-- =============================================

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductPicture_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductPicture]'))
ALTER TABLE [dbo].[ProductPicture] DROP CONSTRAINT [FK_ProductPicture_Product]
GO

/****** Object:  Table [dbo].[ProductPicture]    Script Date: 04/17/2014 15:23:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductPicture]') AND type in (N'U'))
DROP TABLE [dbo].[ProductPicture]
GO

/****** Object:  Table [dbo].[ProductPicture]    Script Date: 04/17/2014 15:23:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProductPicture](
	[ProductId] [int] NOT NULL,
	[Picture] [nvarchar](max) NOT NULL,
	[IsDefault] [bit] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProductPicture]  WITH CHECK ADD  CONSTRAINT [FK_ProductPicture_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO

ALTER TABLE [dbo].[ProductPicture] CHECK CONSTRAINT [FK_ProductPicture_Product]
GO


