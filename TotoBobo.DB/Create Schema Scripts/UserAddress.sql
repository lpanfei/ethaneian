﻿-- =============================================
-- Script Template
-- =============================================


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserAddress_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserAddress]'))
ALTER TABLE [dbo].[UserAddress] DROP CONSTRAINT [FK_UserAddress_User]
GO

/****** Object:  Table [dbo].[UserAddress]    Script Date: 04/17/2014 15:25:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAddress]') AND type in (N'U'))
DROP TABLE [dbo].[UserAddress]
GO


/****** Object:  Table [dbo].[UserAddress]    Script Date: 04/17/2014 15:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserAddress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[TelephoneNo] [nvarchar](50) NULL,
	[Postcode] [nvarchar](50) NOT NULL,
	[Country] [nvarchar](255) NOT NULL,
	[State] [nvarchar](255) NOT NULL,
	[City] [nvarchar](255) NOT NULL,
	[Address1] [nvarchar](max) NOT NULL,
	[Address2] [nvarchar](max) NULL,
	[IsDefault] [bit] NULL,
 CONSTRAINT [PK_UserAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserAddress]  WITH CHECK ADD  CONSTRAINT [FK_UserAddress_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO

ALTER TABLE [dbo].[UserAddress] CHECK CONSTRAINT [FK_UserAddress_User]
GO


