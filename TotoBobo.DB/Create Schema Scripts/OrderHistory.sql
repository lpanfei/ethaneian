﻿-- =============================================
-- Script Template
-- =============================================
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrderHistory_Order]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrderHistory]'))
ALTER TABLE [dbo].[OrderHistory] DROP CONSTRAINT [FK_OrderHistory_Order]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrderHistory_OrderStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrderHistory]'))
ALTER TABLE [dbo].[OrderHistory] DROP CONSTRAINT [FK_OrderHistory_OrderStatus]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrderHistory_OrderStatus1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrderHistory]'))
ALTER TABLE [dbo].[OrderHistory] DROP CONSTRAINT [FK_OrderHistory_OrderStatus1]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrderHistory_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrderHistory]'))
ALTER TABLE [dbo].[OrderHistory] DROP CONSTRAINT [FK_OrderHistory_User]
GO


/****** Object:  Table [dbo].[OrderHistory]    Script Date: 04/25/2014 18:25:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrderHistory]') AND type in (N'U'))
DROP TABLE [dbo].[OrderHistory]
GO

/****** Object:  Table [dbo].[OrderHistory]    Script Date: 04/25/2014 18:25:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrderHistory](
	[OrderId] [int] NOT NULL,
	[OldStatus] [int] NOT NULL,
	[NewStatus] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[Xml] [xml] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO

ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_Order]
GO

ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_OrderStatus] FOREIGN KEY([OldStatus])
REFERENCES [dbo].[OrderStatus] ([Id])
GO

ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_OrderStatus]
GO

ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_OrderStatus1] FOREIGN KEY([NewStatus])
REFERENCES [dbo].[OrderStatus] ([Id])
GO

ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_OrderStatus1]
GO

ALTER TABLE [dbo].[OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_OrderHistory_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserId])
GO

ALTER TABLE [dbo].[OrderHistory] CHECK CONSTRAINT [FK_OrderHistory_User]
GO


