USE [Ethaneian]
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdProduct]    Script Date: 07/06/2014 21:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInsUpdProduct] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @CategoryId INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Description NVARCHAR(MAX)
	DECLARE @Price money
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	DECLARE @Weight DECIMAL(18,2)
	DECLARE @BrandId INT
	DECLARE @CountryId INT
	
	SET @Id =  @xml.value('(/Product/Id)[1]', 'int' )
	SET @CategoryId =  @xml.value('(/Product/Category/Id)[1]', 'int' )
	SET @BrandId =  @xml.value('(/Product/Brand/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Product/Name)[1]', 'nvarchar(255)' )
	SET @Description =  @xml.value('(/Product/Description)[1]', 'nvarchar(max)' )
	SET @Price =  @xml.value('(/Product/Price)[1]', 'money' )
	SET @Weight =  @xml.value('(/Product/Weight)[1]', 'DECIMAL(18,2)' )
	SET @UserName =  @xml.value('(/Product/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Product/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	SET @CountryId =  @xml.value('(/Product/Country/Id)[1]', 'int' )
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Product
		SET
			Name=@Name,
			CategoryId=@CategoryId,
			BrandId=@BrandId,
			[Description]=@Description,
			Price=@Price,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE(),
			[Weight]=@Weight,
			CountryId=@CountryId,
			Tags=@xml.query('/Product/Tags/ProductTag')
		WHERE
			Id=@Id
		DELETE  FROM dbo.ProductPicture WHERE ProductId=@Id
		INSERT INTO dbo.ProductPicture (ProductId,Picture,IsDefault,Title)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault,
			y.value('Title[1]', 'nvarchar(max)') as Title
		FROM @xml.nodes('(/*/Pictures)[1]/ProductPicture') as x(y)
		
		DELETE  FROM dbo.RelatedProduct WHERE ProductId=@Id
		INSERT INTO dbo.RelatedProduct (ProductId,RelatedId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as RelatedId
		FROM @xml.nodes('(/*/RelatedItems)[1]/Product') as x(y)
		
		DELETE  FROM dbo.Accessory WHERE ProductId=@Id
		INSERT INTO dbo.Accessory (ProductId,AccessoryId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as AccessoryId
		FROM @xml.nodes('(/*/Accessories)[1]/Product') as x(y)
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Product
		(CategoryId,BrandId,Name,[Description],Price,CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate,Quantity,[Weight],CountryId,Tags)
		VALUES
		(@CategoryId,@BrandId,@Name,@Description,@Price,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE(),0,@Weight,@CountryId,@xml.query('/Product/Tags/ProductTag'))
		SET @Id = SCOPE_IDENTITY()
		
		INSERT INTO dbo.ProductPicture (ProductId,Picture,IsDefault,Title)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault,
			y.value('Title[1]', 'nvarchar(max)') as Title
		FROM @xml.nodes('(/*/Pictures)[1]/ProductPicture') as x(y)
		
		INSERT INTO dbo.RelatedProduct (ProductId,RelatedId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as RelatedId
		FROM @xml.nodes('(/*/RelatedItems)[1]/Product') as x(y)
		
		INSERT INTO dbo.Accessory (ProductId,AccessoryId)
		SELECT
			@Id,
			y.value('Id[1]', 'INT') as AccessoryId
		FROM @xml.nodes('(/*/Accessories)[1]/Product') as x(y)
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END
