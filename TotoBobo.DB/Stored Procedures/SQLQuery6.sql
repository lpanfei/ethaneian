USE [Ethaneian]
GO
/****** Object:  StoredProcedure [dbo].[spInsUpdAnswer]    Script Date: 07/06/2014 22:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInsUpdAnswer] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Body NVARCHAR(MAX)
	DECLARE @UserName NVARCHAR(50)
	DECLARE @QuestionId INT
	DECLARE @RepliedTo INT
	DECLARE @UserId INT
	DECLARE @IsDoctorReply BIT
	DECLARE @IsBestAnswer BIT
	DECLARE @LikeCount INT
	DECLARE @DislikeCount INT

	SET @Id =  @xml.value('(/Answer/Id)[1]', 'int' )
	SET @Body =  @xml.value('(/Answer/Body)[1]', 'NVARCHAR(MAX)' )
	SET @UserName =  @xml.value('(/Answer/RepliedBy/UserName)[1]', 'NVARCHAR(50)' )
	SET @QuestionId =  @xml.value('(/Answer/QuestionId)[1]', 'int' )
	SET @RepliedTo =  @xml.value('(/Answer/RepliedTo/Id)[1]', 'int' )
	SET @LikeCount =  @xml.value('(/Answer/LikeCount)[1]', 'int' )
	SET @DislikeCount =  @xml.value('(/Answer/DislikeCount)[1]', 'int' )
	SET @IsDoctorReply =  @xml.value('(/Answer/IsDoctorReply)[1]', 'BIT' )
	SET @IsBestAnswer =  @xml.value('(/Answer/IsBestAnswer)[1]', 'BIT' )
	
	IF (@Id>0)
	BEGIN
		UPDATE dbo.Answer
		SET LikeCount=@LikeCount,DislikeCount=@DislikeCount,IsBestAnswer=@IsBestAnswer
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		SELECT @UserId=UserId FROM dbo.[User] WHERE UserName=@UserName
		INSERT INTO dbo.Answer(Body,RepliedBy,RepliedDate,QuestionId,RepliedTo,IsAdminReply,IsDoctorReply)
		VALUES(@Body,@UserId,GETDATE(),@QuestionId,@RepliedTo,0,@IsDoctorReply)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END


