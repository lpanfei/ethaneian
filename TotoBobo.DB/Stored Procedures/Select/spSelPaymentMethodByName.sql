/****** Object:  StoredProcedure [dbo].[spSelPaymentMethodByName]    Script Date: 04/25/2014 18:46:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelPaymentMethodByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelPaymentMethodByName]
GO

/****** Object:  StoredProcedure [dbo].[spSelPaymentMethodByName]    Script Date: 04/25/2014 18:46:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelPaymentMethodByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.PaymentMethod
	WHERE Name=@name
	FOR XML PATH('PaymentMethod'), ROOT('PaymentMethodData')
END

GO


