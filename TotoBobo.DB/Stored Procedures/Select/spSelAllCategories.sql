/****** Object:  StoredProcedure [dbo].[spSelAllCategories]    Script Date: 04/15/2014 21:30:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelAllCategories]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelAllCategories]
GO

/****** Object:  StoredProcedure [dbo].[spSelAllCategories]    Script Date: 04/15/2014 21:30:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelAllCategories]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	Id,Name,ParentId,TopBanner,Banner1,Banner1Link,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.CreatedBy
	FOR XML PATH('User'), ROOT('CreatedBy'), TYPE)
	,CreatedDate,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.CreatedBy
	FOR XML PATH('User'), ROOT('LastModifiedBy'), TYPE),
	LastModifiedDate
	FROM dbo.Category
	FOR XML PATH('Category'), ROOT('CategoryData')
END

GO


