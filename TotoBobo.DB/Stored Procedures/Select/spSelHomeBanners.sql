/****** Object:  StoredProcedure [dbo].[spSelHomeBanners]    Script Date: 04/30/2014 19:57:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelHomeBanners]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelHomeBanners]
GO

/****** Object:  StoredProcedure [dbo].[spSelHomeBanners]    Script Date: 04/30/2014 19:57:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelHomeBanners]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.HomeBanner
	ORDER BY [Order]
	FOR XML PATH('HomeBanner'), ROOT('HomeBannerData')
END

GO


