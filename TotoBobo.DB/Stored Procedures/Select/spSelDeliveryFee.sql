/****** Object:  StoredProcedure [dbo].[spSelDeliveryFee]    Script Date: 04/25/2014 18:42:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelDeliveryFee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelDeliveryFee]
GO

/****** Object:  StoredProcedure [dbo].[spSelDeliveryFee]    Script Date: 04/25/2014 18:42:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spSelDeliveryFee]
	-- Add the parameters for the stored procedure here
	@deliveryMethodId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Fee,
	(SELECT *  FROM dbo.DeliveryMethod  WHERE Id=DeliveryFee.DeliveryMethodId
	FOR XML PATH(''), ROOT('DeliveryMethod'), TYPE),
	(SELECT *  FROM dbo.[State]  WHERE Id=DeliveryFee.StateId
	FOR XML PATH(''), ROOT('State'), TYPE)
	 FROM dbo.DeliveryFee
	WHERE DeliveryMethodId=@deliveryMethodId
	FOR XML PATH('DeliveryFee'), ROOT('DeliveryFeeData')
END

GO


