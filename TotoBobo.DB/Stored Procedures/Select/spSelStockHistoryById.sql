/****** Object:  StoredProcedure [dbo].[spSelStockHistoryById]    Script Date: 04/25/2014 18:49:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelStockHistoryById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelStockHistoryById]
GO

/****** Object:  StoredProcedure [dbo].[spSelStockHistoryById]    Script Date: 04/25/2014 18:49:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelStockHistoryById]
	-- Add the parameters for the stored procedure here
	@productid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ProductId,Quantity,CreatedDate,
	(SELECT * FROM dbo.[User] WHERE UserId=StockHistory.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE)
	FROM dbo.StockHistory
	WHERE ProductId=@productid
	ORDER BY CreatedDate DESC
	FOR XML PATH('StockHistory'), ROOT('StockHistoryData')
END

GO


