/****** Object:  StoredProcedure [dbo].[spSelDeliveryMethods]    Script Date: 04/25/2014 18:43:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelDeliveryMethods]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelDeliveryMethods]
GO

/****** Object:  StoredProcedure [dbo].[spSelDeliveryMethods]    Script Date: 04/25/2014 18:43:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelDeliveryMethods]  
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.DeliveryMethod
	FOR XML PATH('DeliveryMethod'), ROOT('DeliveryMethodData')
END

GO


