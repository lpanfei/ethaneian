/****** Object:  StoredProcedure [dbo].[spSelCityById]    Script Date: 04/25/2014 18:40:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelCityById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelCityById]
GO


/****** Object:  StoredProcedure [dbo].[spSelCityById]    Script Date: 04/25/2014 18:40:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCityById] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[State] WHERE Id=[City].StateId
	FOR XML PATH(''), ROOT('State'), TYPE)
	 FROM dbo.[City]
	WHERE Id=@id AND IsEnabled=1
	FOR XML PATH('City'), ROOT('CityData')
END

GO


