/****** Object:  StoredProcedure [dbo].[spSelPaymentMethods]    Script Date: 04/25/2014 18:46:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelPaymentMethods]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelPaymentMethods]
GO

/****** Object:  StoredProcedure [dbo].[spSelPaymentMethods]    Script Date: 04/25/2014 18:46:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelPaymentMethods] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.PaymentMethod
	FOR XML PATH('PaymentMethod'), ROOT('PaymentMethodData')
END

GO


