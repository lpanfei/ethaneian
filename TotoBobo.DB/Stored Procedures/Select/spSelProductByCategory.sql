/****** Object:  StoredProcedure [dbo].[spSelProductByCategory]    Script Date: 04/25/2014 18:47:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelProductByCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelProductByCategory]
GO

/****** Object:  StoredProcedure [dbo].[spSelProductByCategory]    Script Date: 04/25/2014 18:47:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductByCategory]
	-- Add the parameters for the stored procedure here
	@categoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 prod.Id,prod.Name,prod.[Description],prod.Price,prod.Quantity,prod.CreatedDate,prod.LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,
	 (SELECT * FROM dbo.[User] WHERE UserId=prod.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=prod.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=prod.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=prod.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod 
	INNER JOIN Category cat ON prod.CategoryId=cat.Id
	WHERE (cat.Id=@categoryId OR cat.ParentId=@categoryId) AND ISNULL(prod.IsDeleted,0)<>1
	FOR XML PATH('Product'), ROOT('ProductData')
END

GO


