/****** Object:  StoredProcedure [dbo].[spSelOrderHistory]    Script Date: 04/25/2014 18:44:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelOrderHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelOrderHistory]
GO

/****** Object:  StoredProcedure [dbo].[spSelOrderHistory]    Script Date: 04/25/2014 18:44:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrderHistory]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT OrderId,CreatedDate,Remark,
	(SELECT Name FROM dbo.OrderStatus WHERE Id=OrderHistory.OldStatus) OldStatus,
	(SELECT Name FROM dbo.OrderStatus WHERE Id=OrderHistory.NewStatus) NewStatus,
	(SELECT * FROM dbo.[User] WHERE UserId=OrderHistory.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE)
	FROM dbo.OrderHistory
	WHERE OrderId=@Id
	ORDER BY CreatedDate DESC
	FOR XML PATH('OrderHistory'), ROOT('OrderHistoryData')
END

GO


