/****** Object:  StoredProcedure [dbo].[spSelPaymentMethodById]    Script Date: 04/25/2014 18:45:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelPaymentMethodById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelPaymentMethodById]
GO

/****** Object:  StoredProcedure [dbo].[spSelPaymentMethodById]    Script Date: 04/25/2014 18:45:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spSelPaymentMethodById] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.PaymentMethod
	WHERE Id=@id
	FOR XML PATH('PaymentMethod'), ROOT('PaymentMethodData')
END

GO


