/****** Object:  StoredProcedure [dbo].[spSelHomeBannerById]    Script Date: 04/30/2014 19:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelHomeBannerById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelHomeBannerById]
GO

/****** Object:  StoredProcedure [dbo].[spSelHomeBannerById]    Script Date: 04/30/2014 19:56:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelHomeBannerById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.HomeBanner
	WHERE Id=@id
	FOR XML PATH('HomeBanner'), ROOT('HomeBannerData')
END

GO


