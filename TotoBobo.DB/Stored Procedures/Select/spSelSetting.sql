/****** Object:  StoredProcedure [dbo].[spSelSetting]    Script Date: 04/30/2014 20:03:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelSetting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelSetting]
GO

/****** Object:  StoredProcedure [dbo].[spSelSetting]    Script Date: 04/30/2014 20:03:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelSetting]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Setting
	FOR XML PATH('Setting'), ROOT('SettingData')
END

GO


