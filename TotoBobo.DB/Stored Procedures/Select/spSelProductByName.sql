/****** Object:  StoredProcedure [dbo].[spSelProductByName]    Script Date: 04/25/2014 18:47:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelProductByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelProductByName]
GO

/****** Object:  StoredProcedure [dbo].[spSelProductByName]    Script Date: 04/25/2014 18:47:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id, Name FROM dbo.Product
	WHERE Name=@name
	FOR XML PATH('Product'), ROOT('ProductData')
END

GO


