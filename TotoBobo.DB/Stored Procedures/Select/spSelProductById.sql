/****** Object:  StoredProcedure [dbo].[spSelProductById]    Script Date: 04/25/2014 18:47:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelProductById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelProductById]
GO

/****** Object:  StoredProcedure [dbo].[spSelProductById]    Script Date: 04/25/2014 18:47:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelProductById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,
	 (SELECT * FROM dbo.[User] WHERE UserId=Product.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Product.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=Product.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product
	WHERE Id=@id AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH('Product'), ROOT('ProductData')
END

GO


