
/****** Object:  StoredProcedure [dbo].[spSelAllUsers]    Script Date: 04/15/2014 21:31:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelAllUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelAllUsers]
GO

/****** Object:  StoredProcedure [dbo].[spSelAllUsers]    Script Date: 04/15/2014 21:31:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelAllUsers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	*,
	(
		SELECT r.RoleName FROM  dbo.webpages_Roles r
		INNER JOIN dbo.webpages_UsersInRoles uir ON uir.RoleId=r.RoleId
		WHERE uir.UserId=[User].UserId
		FOR XML PATH('UserRole'), ROOT('Roles'), TYPE
	)
	FROM dbo.[User]
	FOR XML PATH('User'), ROOT('UserData')
END

GO


