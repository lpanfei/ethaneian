/****** Object:  StoredProcedure [dbo].[spSelUserByEmail]    Script Date: 04/15/2014 21:33:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelUserByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelUserByEmail]
GO

/****** Object:  StoredProcedure [dbo].[spSelUserByEmail]    Script Date: 04/15/2014 21:33:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelUserByEmail]
	-- Add the parameters for the stored procedure here
	@email nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.[User] WHERE Email=@email
	FOR XML PATH('User'), ROOT('UserData')
END

GO


