/****** Object:  StoredProcedure [dbo].[spUpdOrderStatus]    Script Date: 04/25/2014 18:49:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdOrderStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdOrderStatus]
GO

/****** Object:  StoredProcedure [dbo].[spUpdOrderStatus]    Script Date: 04/25/2014 18:49:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdOrderStatus]
	-- Add the parameters for the stored procedure here
	@id int,
	@newStatus NVARCHAR(50),
	@username NVARCHAR(50),
	@remark NVARCHAR(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @OldStatusId INT
	DECLARE @NewStatusId INT
	DECLARE @userId INT
	SELECT @OldStatusId = [Status] FROM dbo.[Order] WHERE Id=@Id
	SELECT @NewStatusId = Id FROM dbo.OrderStatus WHERE Name=@newStatus
	SELECT @userId=UserId FROM dbo.[User] WHERE UserName=@username
	
	INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
	VALUES
	(@id,@OldStatusId,@NewStatusId,@userId,GETDATE(),@remark)
	
END

GO


