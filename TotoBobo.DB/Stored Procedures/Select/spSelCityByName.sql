/****** Object:  StoredProcedure [dbo].[spSelCityByName]    Script Date: 04/25/2014 18:41:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelCityByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelCityByName]
GO

/****** Object:  StoredProcedure [dbo].[spSelCityByName]    Script Date: 04/25/2014 18:41:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCityByName] 
	-- Add the parameters for the stored procedure here
	@name nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[State] WHERE Id=[City].StateId
	FOR XML PATH(''), ROOT('State'), TYPE),
	IsEnabled
	 FROM dbo.[City]
	WHERE Name=@name
	FOR XML PATH('City'), ROOT('CityData')
END

GO


