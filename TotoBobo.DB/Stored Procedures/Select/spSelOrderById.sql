/****** Object:  StoredProcedure [dbo].[spSelOrderById]    Script Date: 04/25/2014 18:44:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelOrderById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelOrderById]
GO

/****** Object:  StoredProcedure [dbo].[spSelOrderById]    Script Date: 04/25/2014 18:44:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrderById]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ord.Id, ord.CreatedDate,ord.LastModifiedDate,ord.Total,os.Name AS [Status],DeliveryFee,
	 (SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT ShippingAddress AS Id, Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName FROM dbo.[Order] WHERE Id=ord.Id
	FOR XML PATH(''), ROOT('ShippingAddress'), TYPE),
	(SELECT Quantity, UnitPrice,SubTotal,UnitWeight,(SELECT Name,ID, (SELECT * FROM dbo.ProductPicture WHERE ProductId=prod.Id FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod WHERE prod.Id=orditem.ProductId FOR XML PATH(''), ROOT('Product'), TYPE) FROM dbo.OrderItem orditem WHERE orditem.OrderId=ord.Id
	FOR XML PATH('OrderItem'), ROOT('Items'), TYPE)
	FROM
	dbo.[Order] ord
	INNER JOIN dbo.OrderStatus os ON ord.[Status]=os.Id
	WHERE ord.Id=@id
	ORDER BY CreatedDate DESC
	FOR XML PATH('Order'), ROOT('OrderData')
END

GO


