/****** Object:  StoredProcedure [dbo].[spSelOrdersByUser]    Script Date: 04/25/2014 18:45:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelOrdersByUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelOrdersByUser]
GO

/****** Object:  StoredProcedure [dbo].[spSelOrdersByUser]    Script Date: 04/25/2014 18:45:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrdersByUser]
	-- Add the parameters for the stored procedure here
	@username NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    DECLARE @UserId INT
    SELECT @UserId = UserId FROM dbo.[User] WHERE UserName=@username
    EXEC spCancelOverdueOrders @UserId
    
	SELECT ord.Id, ord.CreatedDate,ord.LastModifiedDate,ord.Total,os.Name AS [Status],DeliveryFee,
	 (SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT ShippingAddress AS Id, Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName FROM dbo.[Order] WHERE Id=ord.Id
	FOR XML PATH(''), ROOT('ShippingAddress'), TYPE),
	(SELECT Quantity, UnitPrice,SubTotal,UnitWeight,(SELECT Name,Id, (SELECT * FROM dbo.ProductPicture WHERE ProductId=prod.Id FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod WHERE prod.Id=orditem.ProductId FOR XML PATH(''), ROOT('Product'), TYPE) FROM dbo.OrderItem orditem WHERE orditem.OrderId=ord.Id
	FOR XML PATH('OrderItem'), ROOT('Items'), TYPE)
	FROM
	dbo.[Order] ord
	INNER JOIN dbo.OrderStatus os ON ord.[Status]=os.Id
	WHERE CreatedBy=@UserId
	ORDER BY CreatedDate DESC
	FOR XML PATH('Order'), ROOT('OrderData')
END

GO


