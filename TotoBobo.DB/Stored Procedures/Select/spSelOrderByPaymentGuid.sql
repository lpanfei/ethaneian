/****** Object:  StoredProcedure [dbo].[spSelOrderByPaymentGuid]    Script Date: 04/30/2014 20:01:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelOrderByPaymentGuid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelOrderByPaymentGuid]
GO

/****** Object:  StoredProcedure [dbo].[spSelOrderByPaymentGuid]    Script Date: 04/30/2014 20:01:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelOrderByPaymentGuid]
	-- Add the parameters for the stored procedure here
	@guid nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Insert statements for procedure here
	SELECT ord.Id, ord.CreatedDate,ord.LastModifiedDate,ord.Total,os.Name AS [Status],DeliveryFee,
	 (SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=ord.CreatedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT ShippingAddress AS Id, Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName FROM dbo.[Order] WHERE Id=ord.Id
	FOR XML PATH(''), ROOT('ShippingAddress'), TYPE),
	(SELECT Quantity, UnitPrice,SubTotal,UnitWeight,(SELECT Name,ID, (SELECT * FROM dbo.ProductPicture WHERE ProductId=prod.Id FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE)
	FROM dbo.Product prod WHERE prod.Id=orditem.ProductId FOR XML PATH(''), ROOT('Product'), TYPE) FROM dbo.OrderItem orditem WHERE orditem.OrderId=ord.Id
	FOR XML PATH('OrderItem'), ROOT('Items'), TYPE)
	FROM
	dbo.[Order] ord
	INNER JOIN dbo.OrderStatus os ON ord.[Status]=os.Id
	WHERE ord.PaymentGuid=@guid AND os.Name='Created'
	ORDER BY CreatedDate DESC
	FOR XML PATH('Order'), ROOT('OrderData')
END

GO


