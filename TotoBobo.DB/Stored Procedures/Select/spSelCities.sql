
/****** Object:  StoredProcedure [dbo].[spSelCities]    Script Date: 04/25/2014 18:40:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelCities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelCities]
GO


/****** Object:  StoredProcedure [dbo].[spSelCities]    Script Date: 04/25/2014 18:40:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCities]
	-- Add the parameters for the stored procedure here
	@stateId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[State] WHERE Id=[City].StateId
	FOR XML PATH(''), ROOT('State'), TYPE)
	 FROM dbo.[City]
	WHERE StateId=@stateId AND IsEnabled=1
	FOR XML PATH('City'), ROOT('CityData')
END

GO


