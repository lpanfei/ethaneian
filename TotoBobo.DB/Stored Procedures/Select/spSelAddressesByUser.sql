/****** Object:  StoredProcedure [dbo].[spSelAddressesByUser]    Script Date: 04/15/2014 21:30:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelAddressesByUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelAddressesByUser]
GO

/****** Object:  StoredProcedure [dbo].[spSelAddressesByUser]    Script Date: 04/15/2014 21:30:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelAddressesByUser]
	-- Add the parameters for the stored procedure here
	@username nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ua.* FROM dbo.UserAddress ua
	INNER JOIN dbo.[User] usr ON ua.UserId=usr.UserId
	WHERE usr.UserName=@username
	FOR XML PATH('UserAddress'), ROOT('UserAddressData')
END

GO


