/****** Object:  StoredProcedure [dbo].[spSelStateById]    Script Date: 04/25/2014 18:48:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelStateById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelStateById]
GO

/****** Object:  StoredProcedure [dbo].[spSelStateById]    Script Date: 04/25/2014 18:48:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelStateById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Id, 
		Name,
		(SELECT * FROM dbo.[Country] WHERE Id=[State].CountryId
	FOR XML PATH(''), ROOT('Country'), TYPE),
	(SELECT * FROM dbo.[City] WHERE StateId=[State].Id AND IsEnabled=1
	FOR XML PATH('City'), ROOT('Cities'), TYPE)
	 FROM dbo.[State]
	WHERE Id=@id  AND IsEnabled=1
	FOR XML PATH('State'), ROOT('StateData')
END

GO


