
/****** Object:  StoredProcedure [dbo].[spSelCategoryByName]    Script Date: 04/15/2014 21:32:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSelCategoryByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSelCategoryByName]
GO

/****** Object:  StoredProcedure [dbo].[spSelCategoryByName]    Script Date: 04/15/2014 21:32:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCategoryByName]
	-- Add the parameters for the stored procedure here
	@name nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	Id,Name,ParentId,TopBanner,Banner1,Banner1Link,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.CreatedBy
	FOR XML PATH('User'), ROOT('CreatedBy'), TYPE)
	,CreatedDate,
	(SELECT * FROM dbo.[User] WHERE UserId=Category.CreatedBy
	FOR XML PATH('User'), ROOT('LastModifiedBy'), TYPE),
	LastModifiedDate
	FROM dbo.Category
	WHERE Name like '%'+@name+'%'
	FOR XML PATH('Category'), ROOT('CategoryData')
END

GO


