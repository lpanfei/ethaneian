/****** Object:  StoredProcedure [dbo].[spPayOrder]    Script Date: 04/30/2014 18:40:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPayOrder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPayOrder]
GO

/****** Object:  StoredProcedure [dbo].[spPayOrder]    Script Date: 04/30/2014 18:40:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spPayOrder]
	-- Add the parameters for the stored procedure here
	@id INT,
	@paymentid NVARCHAR(255),
	@paymentguid NVARCHAR(255),
	@status NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @OldStatusId INT
	DECLARE @NewStatusId INT
	DECLARE @userId INT
	SELECT @OldStatusId = [Status],@userId=CreatedBy FROM dbo.[Order] WHERE Id=@Id
	SELECT @NewStatusId = Id FROM dbo.OrderStatus WHERE Name=@status
	UPDATE dbo.[Order] SET [Status]=@NewStatusId,PaymentId=@paymentid,PaymentGuid=@paymentguid WHERE Id=@Id
	
	INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
	VALUES
	(@id,@OldStatusId,@NewStatusId,@userId,GETDATE(),'')
END

GO


