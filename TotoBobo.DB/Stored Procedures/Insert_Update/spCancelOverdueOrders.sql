/****** Object:  StoredProcedure [dbo].[spCancelOverdueOrders]    Script Date: 04/30/2014 18:32:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCancelOverdueOrders]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCancelOverdueOrders]
GO

/****** Object:  StoredProcedure [dbo].[spCancelOverdueOrders]    Script Date: 04/30/2014 18:32:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCancelOverdueOrders]
	-- Add the parameters for the stored procedure here
	@uid INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @sid INT
    DECLARE @cid INT
    SELECT @sid=Id FROM dbo.OrderStatus WHERE Name='Cancelled'
    SELECT @cid=Id FROM dbo.OrderStatus WHERE Name='Created'
    
    INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
    SELECT Id,@cid,@sid,@uid,GETDATE(),'Cancelled automatically!' FROM [order] where createddate<= DATEADD(day, -1, GETDATE()) AND [Status]=@cid AND CreatedBy=@uid
    
	 UPDATE [order] SET [Status]=@sid 
	 where createddate<= DATEADD(day, -1, GETDATE()) AND [Status]=@cid AND CreatedBy=@uid
END

GO


