/****** Object:  StoredProcedure [dbo].[spMarkAddressAsDefault]    Script Date: 04/15/2014 21:29:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMarkAddressAsDefault]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMarkAddressAsDefault]
GO

/****** Object:  StoredProcedure [dbo].[spMarkAddressAsDefault]    Script Date: 04/15/2014 21:29:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spMarkAddressAsDefault] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @userid int
	
	SELECT @userid=UserId FROM dbo.UserAddress WHERE Id=@id
	
	UPDATE dbo.UserAddress
	SET IsDefault=0
	WHERE UserId=@userid
	
	UPDATE dbo.UserAddress
	SET IsDefault=1
	WHERE Id=@id 
END

GO


