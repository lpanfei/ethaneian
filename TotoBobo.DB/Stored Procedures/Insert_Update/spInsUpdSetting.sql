/****** Object:  StoredProcedure [dbo].[spInsUpdSetting]    Script Date: 04/30/2014 18:39:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdSetting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdSetting]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdSetting]    Script Date: 04/30/2014 18:39:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdSetting]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id int
	DECLARE @ExchangeRate decimal(18,0)
	
	SET @Id =  @xml.value('(/Setting/Id)[1]', 'int' )
	SET @ExchangeRate =  @xml.value('(/Setting/ExchangeRate)[1]', 'decimal(18,0)')
	
	IF(@Id>0)
	BEGIN
		UPDATE dbo.Setting SET ExchangeRate=@ExchangeRate WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Setting (ExchangeRate)
		VALUES (@ExchangeRate)
		SET @Id = SCOPE_IDENTITY()
	END 
	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END

GO


