/****** Object:  StoredProcedure [dbo].[spInsUpdDeliveryFee]    Script Date: 04/25/2014 18:35:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdDeliveryFee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdDeliveryFee]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdDeliveryFee]    Script Date: 04/25/2014 18:35:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdDeliveryFee]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @DeliveryMethodId INT
	DECLARE @StateId INT
	DECLARE @Fee MONEY
	
	SET @DeliveryMethodId =  @xml.value('(/DeliveryFee/DeliveryMethod/Id)[1]', 'int' )
	SET @StateId =  @xml.value('(/DeliveryFee/State/Id)[1]', 'int' )
	SET @Fee =  @xml.value('(/DeliveryFee/Fee)[1]', 'money' )
	
	IF EXISTS (SELECT 1 FROM dbo.DeliveryFee WHERE DeliveryMethodId=@DeliveryMethodId AND StateId=@StateId)
	BEGIN
		UPDATE dbo.DeliveryFee
		SET Fee=@Fee
		WHERE DeliveryMethodId=@DeliveryMethodId AND StateId=@StateId
	END
	ELSE
	BEGIN
		INSERT INTO dbo.DeliveryFee (DeliveryMethodId,StateId,Fee)
		VALUES(@DeliveryMethodId,@StateId,@Fee)
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @StateId 
	RETURN @StateId
END

GO


