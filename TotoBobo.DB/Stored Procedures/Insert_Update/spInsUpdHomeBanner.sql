/****** Object:  StoredProcedure [dbo].[spInsUpdHomeBanner]    Script Date: 04/30/2014 18:37:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdHomeBanner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdHomeBanner]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdHomeBanner]    Script Date: 04/30/2014 18:37:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdHomeBanner]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @TopBanner NVARCHAR(255)
	DECLARE @Banner1 NVARCHAR(255)
	DECLARE @Banner2 NVARCHAR(255)
	DECLARE @Banner3 NVARCHAR(255)
	DECLARE @TopBannerLink NVARCHAR(max)
	DECLARE @Banner1Link NVARCHAR(max)
	DECLARE @Banner2Link NVARCHAR(max)
	DECLARE @Banner3Link NVARCHAR(max)
	DECLARE @Order INT
	
	SET @Id =  @xml.value('(/HomeBanner/Id)[1]', 'int' )
	SET @TopBanner =  @xml.value('(/HomeBanner/TopBanner)[1]', 'nvarchar(255)' )
	SET @Banner1 =  @xml.value('(/HomeBanner/Banner1)[1]', 'nvarchar(255)' )
	SET @Banner2 =  @xml.value('(/HomeBanner/Banner2)[1]', 'nvarchar(255)' )
	SET @Banner3 =  @xml.value('(/HomeBanner/Banner3)[1]', 'nvarchar(255)' )
	SET @TopBannerLink =  @xml.value('(/HomeBanner/TopBannerLink)[1]', 'nvarchar(max)' )
	SET @Banner1Link =  @xml.value('(/HomeBanner/Banner1Link)[1]', 'nvarchar(max)' )
	SET @Banner2Link =  @xml.value('(/HomeBanner/Banner2Link)[1]', 'nvarchar(max)' )
	SET @Banner3Link =  @xml.value('(/HomeBanner/Banner3Link)[1]', 'nvarchar(max)' )
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.HomeBanner
		SET
			TopBanner=@TopBanner,
			TopBannerLink=@TopBannerLink,
			Banner1=@Banner1,
			Banner1Link=@Banner1Link,
			Banner2=@Banner2,
			Banner3=@Banner3,
			Banner2Link=@Banner2Link,
			Banner3Link=@Banner3Link
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT 1 FROM dbo.HomeBanner)
		BEGIN
			SELECT @Order=MAX([Order])+1 FROM dbo.HomeBanner
		END
		ELSE
		BEGIN
			SELECT @Order=1
		END
		
		INSERT INTO dbo.HomeBanner
		(TopBanner,TopBannerLink,Banner1,Banner1Link,Banner2,Banner2Link,Banner3,Banner3Link,[Order])
		VALUES
		(@TopBanner,@TopBannerLink,@Banner1,@Banner1Link,@Banner2,@Banner2Link,@Banner3,@Banner3Link,@Order)
		SET @Id = SCOPE_IDENTITY()
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END


GO


