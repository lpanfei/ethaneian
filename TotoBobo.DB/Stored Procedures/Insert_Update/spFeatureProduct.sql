/****** Object:  StoredProcedure [dbo].[spFeatureProduct]    Script Date: 04/30/2014 18:36:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFeatureProduct]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFeatureProduct]
GO

/****** Object:  StoredProcedure [dbo].[spFeatureProduct]    Script Date: 04/30/2014 18:36:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFeatureProduct]
	-- Add the parameters for the stored procedure here
	@id int,
	@isFeatured bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Product SET IsFeatured=@isFeatured WHERE Id=@id
END

GO


