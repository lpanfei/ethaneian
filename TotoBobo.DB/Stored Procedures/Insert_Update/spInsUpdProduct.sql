/****** Object:  StoredProcedure [dbo].[spInsUpdProduct]    Script Date: 04/25/2014 18:37:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdProduct]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdProduct]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdProduct]    Script Date: 04/25/2014 18:37:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdProduct] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @CategoryId INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @Description NVARCHAR(MAX)
	DECLARE @Price money
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	DECLARE @Weight DECIMAL(18,0)
	
	SET @Id =  @xml.value('(/Product/Id)[1]', 'int' )
	SET @CategoryId =  @xml.value('(/Product/Category/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Product/Name)[1]', 'nvarchar(255)' )
	SET @Description =  @xml.value('(/Product/Description)[1]', 'nvarchar(max)' )
	SET @Price =  @xml.value('(/Product/Price)[1]', 'money' )
	SET @Weight =  @xml.value('(/Product/Weight)[1]', 'DECIMAL(18,0)' )
	SET @UserName =  @xml.value('(/Product/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Product/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Product
		SET
			Name=@Name,
			CategoryId=@CategoryId,
			[Description]=@Description,
			Price=@Price,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE(),
			[Weight]=@Weight
		WHERE
			Id=@Id
		DELETE  FROM dbo.ProductPicture WHERE ProductId=@Id
		INSERT INTO dbo.ProductPicture (ProductId,Picture,IsDefault)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault
		FROM @xml.nodes('(/*/Pictures)[1]/ProductPicture') as x(y)
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Product
		(CategoryId,Name,[Description],Price,CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate,Quantity,[Weight])
		VALUES
		(@CategoryId,@Name,@Description,@Price,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE(),0,@Weight)
		SET @Id = SCOPE_IDENTITY()
		
		INSERT INTO dbo.ProductPicture (ProductId,Picture,IsDefault)
		SELECT
			@Id,
			y.value('Picture[1]', 'nvarchar(max)') as Picture,
			y.value('IsDefault[1]', 'bit') as IsDefault
		FROM @xml.nodes('(/*/Pictures)[1]/ProductPicture') as x(y)
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END

GO


