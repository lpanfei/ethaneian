/****** Object:  StoredProcedure [dbo].[spInsUpdOrder]    Script Date: 04/25/2014 18:36:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdOrder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdOrder]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdOrder]    Script Date: 04/25/2014 18:36:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdOrder] 
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Status NVARCHAR(50)
	DECLARE @Total MONEY
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @StatusId INT
	DECLARE @AddressId INT
	DECLARE @OldStatusId INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	DECLARE @FirstName NVARCHAR(255)
	DECLARE @LastName NVARCHAR(255)
	DECLARE @MobileNo NVARCHAR(50)
	DECLARE @TelephoneNo NVARCHAR(50)
	DECLARE @Postcode NVARCHAR(50)
	DECLARE @Country NVARCHAR(255)
	DECLARE @State NVARCHAR(255)
	DECLARE @City NVARCHAR(255)
	DECLARE @Address1 NVARCHAR(MAX)
	DECLARE @Address2 NVARCHAR(MAX)
	DECLARE @DeliveryId INT
	DECLARE @PaymentId INT
	DECLARE @DeliveryFee money
	
	SET @Id =  @xml.value('(/Order/Id)[1]', 'int' )
	SET @Status =  @xml.value('(/Order/Status)[1]', 'nvarchar(50)' )
	SELECT TOP 1 @StatusId = Id FROM dbo.[OrderStatus] WHERE Name=@Status
	SET @Total =  @xml.value('(/Order/Total)[1]', 'money' )
	SET @DeliveryId =  @xml.value('(/Order/DeliveryMethod/Id)[1]', 'int') 
	SET @PaymentId =  @xml.value('(/Order/PaymentMethod/Id)[1]', 'int')
	SET @DeliveryFee =  @xml.value('(/Order/DeliveryFee)[1]', 'money' )
	SET @UserName =  @xml.value('(/Order/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Order/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	SET @FirstName =  @xml.value('(/Order/ShippingAddress/FirstName)[1]', 'NVARCHAR(255)' )
	SET @LastName =  @xml.value('(/Order/ShippingAddress/LastName)[1]', 'NVARCHAR(255)' )
	SET @MobileNo =  @xml.value('(/Order/ShippingAddress/MobileNo)[1]', 'NVARCHAR(50)' )
	SET @TelephoneNo =  @xml.value('(/Order/ShippingAddress/TelephoneNo)[1]', 'NVARCHAR(50)' )
	SET @Postcode =  @xml.value('(/Order/ShippingAddress/Postcode)[1]', 'NVARCHAR(50)' )
	SET @Country =  @xml.value('(/Order/ShippingAddress/Country)[1]', 'NVARCHAR(255)' )
	SET @State =  @xml.value('(/Order/ShippingAddress/State)[1]', 'NVARCHAR(255)' )
	SET @City =  @xml.value('(/Order/ShippingAddress/City)[1]', 'NVARCHAR(255)' )
	SET @Address1 =  @xml.value('(/Order/ShippingAddress/Address1)[1]', 'NVARCHAR(MAX)' )
	SET @Address2 =  @xml.value('(/Order/ShippingAddress/Address2)[1]', 'NVARCHAR(MAX)' )
		
	SET @AddressId =  @xml.value('(/Order/ShippingAddress/Id)[1]', 'INT' )
	
	
	
	IF (@Id>0)
	BEGIN
		SELECT @OldStatusId = [Status] FROM dbo.[Order] WHERE Id=@Id
		UPDATE
			dbo.[Order]
		SET
			[Status]=@StatusId,
			Total=@Total,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE(),
			ShippingAddress=@AddressId,
			DeliveryFee=@DeliveryFee,
			Country=@Country,
			[State]=@State,
			City=@City,
			Address1=@Address1,
			Address2=@Address2,
			MobileNo=@MobileNo,
			TelephoneNo=@TelephoneNo,
			Postcode=@Postcode,
			FirstName=@FirstName,
			LastName=@LastName
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		SET @OldStatusId = @StatusId
		INSERT INTO dbo.[Order]
		(CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate,ShippingAddress,[Status],Total,DeliveryFee,DeliveryMethodId,
		PaymentMethodId,Country,[State],City,Address1,Address2,MobileNo,TelephoneNo,Postcode,FirstName,LastName)
		VALUES
		(@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE(),@AddressId,@StatusId,@Total,@DeliveryFee,@DeliveryId,
		@PaymentId,@Country,@State,@City,@Address1,@Address2,@MobileNo,@TelephoneNo,@Postcode,@FirstName,@LastName)
		SET @Id = SCOPE_IDENTITY()
	END
	
	DELETE FROM dbo.OrderItem WHERE OrderId=@Id
	INSERT INTO dbo.OrderItem (OrderId,ProductId,Quantity,UnitPrice,SubTotal,DiscountId,UnitWeight)
	SELECT
		@Id,
		y.value('Product[1]/Id[1]', 'int') as ProductId,
		y.value('Quantity[1]', 'int') as Quantity,
		y.value('UnitPrice[1]', 'money') as UnitPrice,
		y.value('SubTotal[1]', 'money') as SubTotal,
		y.value('DiscountId[1]', 'int') as DiscountId,
		y.value('UnitWeight[1]', 'decimal(18,0)') as UnitWeight
	FROM @xml.nodes('(/*/Items)[1]/OrderItem') as x(y)
	
	INSERT INTO dbo.OrderHistory (OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark,[Xml])
	VALUES
	(@Id,@OldStatusId,@StatusId,@CreatedBy,GETDATE(),NULL,@xml)
	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END

GO


