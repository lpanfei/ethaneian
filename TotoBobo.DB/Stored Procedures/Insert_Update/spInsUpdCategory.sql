
/****** Object:  StoredProcedure [dbo].[spInsUpdCategory]    Script Date: 04/15/2014 21:28:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdCategory]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdCategory]    Script Date: 04/15/2014 21:28:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdCategory]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @ParentId INT
	DECLARE @CreatedBy INT
	DECLARE @LastModifiedBy INT
	DECLARE @UserName NVARCHAR(50)
	DECLARE @UserName2 NVARCHAR(50)
	
	SET @Id =  @xml.value('(/Category/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/Category/Name)[1]', 'nvarchar(255)' )
	SET @ParentId =  @xml.value('(/Category/ParentId)[1]', 'int' )
	SET @UserName =  @xml.value('(/Category/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @UserName2 =  @xml.value('(/Category/LastModifiedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @CreatedBy= UserId FROM dbo.[User] WHERE UserName=@UserName
	SELECT @LastModifiedBy= UserId FROM dbo.[User] WHERE UserName=@UserName2
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Category
		SET
			Name=@Name,
			ParentId=@ParentId,
			LastModifiedBy=@LastModifiedBy,
			LastModifiedDate=GETDATE()
		WHERE
			Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.Category
		(Name,ParentId,CreatedBy,CreatedDate,LastModifiedBy,LastModifiedDate)
		VALUES
		(@Name,@ParentId,@CreatedBy,GETDATE(),@LastModifiedBy,GETDATE())
		SET @Id = SCOPE_IDENTITY()
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END

GO


