/****** Object:  StoredProcedure [dbo].[spStockToInventory]    Script Date: 04/17/2014 15:40:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStockToInventory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStockToInventory]
GO


/****** Object:  StoredProcedure [dbo].[spStockToInventory]    Script Date: 04/17/2014 15:40:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spStockToInventory]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @ProductId INT
	DECLARE @Quantity INT
	DECLARE @UserId INT
	DECLARE @Username NVARCHAR(50)
	
	SET @ProductId =  @xml.value('(/StockHistory/ProductId)[1]', 'int' )
	SET @Quantity =  @xml.value('(/StockHistory/Quantity)[1]', 'int' )
	SET @Username =  @xml.value('(/StockHistory/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SELECT @UserId =UserId FROM dbo.[User] WHERE UserName=@Username
	
	UPDATE dbo.Product SET Quantity=Quantity+@Quantity WHERE Id=@ProductId
	
	INSERT INTO dbo.StockHistory
	(ProductId,Quantity,CreatedBy,CreatedDate)
	VALUES
	(@ProductId,@Quantity,@UserId,GETDATE())
	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END

GO


