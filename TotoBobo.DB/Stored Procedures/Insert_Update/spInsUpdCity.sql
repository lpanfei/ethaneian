/****** Object:  StoredProcedure [dbo].[spInsUpdCity]    Script Date: 04/25/2014 18:33:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdCity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdCity]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdCity]    Script Date: 04/25/2014 18:33:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdCity]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @StateId INT
	
	SET @Id =  @xml.value('(/City/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/City/Name)[1]', 'nvarchar(255)' )
	SET @StateId =  @xml.value('(/City/State/Id)[1]', 'int' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.[City]
		SET Name=@Name, IsEnabled=1,StateId=@StateId
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.[City] (Name,StateId,IsEnabled)
		VALUES(@Name,@StateId,1)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END

GO


