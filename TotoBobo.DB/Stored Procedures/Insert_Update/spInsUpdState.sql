/****** Object:  StoredProcedure [dbo].[spInsUpdState]    Script Date: 04/25/2014 18:38:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdState]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdState]    Script Date: 04/25/2014 18:38:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdState]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @Id INT
	DECLARE @Name NVARCHAR(255)
	DECLARE @CountryId INT
	
	SET @Id =  @xml.value('(/State/Id)[1]', 'int' )
	SET @Name =  @xml.value('(/State/Name)[1]', 'nvarchar(255)' )
	SET @CountryId =  @xml.value('(/State/Country/Id)[1]', 'int' )
	IF (@Id>0)
	BEGIN
		UPDATE dbo.[State]
		SET Name=@Name, IsEnabled=1
		WHERE Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO dbo.[State] (Name,CountryId,IsEnabled)
		VALUES(@Name,@CountryId,1)
		SET @Id = SCOPE_IDENTITY()
	END	
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END

GO


