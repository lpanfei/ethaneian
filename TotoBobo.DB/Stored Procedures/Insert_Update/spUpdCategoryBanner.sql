/****** Object:  StoredProcedure [dbo].[spUpdCategoryBanner]    Script Date: 05/04/2014 20:40:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdCategoryBanner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdCategoryBanner]
GO

/****** Object:  StoredProcedure [dbo].[spUpdCategoryBanner]    Script Date: 05/04/2014 20:40:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdCategoryBanner]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@TopBanner NVARCHAR(255),
	@Banner1 NVARCHAR(255),
	@Banner1Link NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    BEGIN TRANSACTION
	BEGIN TRY
	
	IF (@Id>0)
	BEGIN
		UPDATE
		dbo.Category
		SET
			TopBanner=@TopBanner,
			Banner1=@Banner1,
			Banner1Link=@Banner1Link
		WHERE
			Id=@Id
	END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @Id 
	RETURN @Id
END

GO


