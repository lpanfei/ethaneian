/****** Object:  StoredProcedure [dbo].[spInsUpdUserAddress]    Script Date: 04/25/2014 18:38:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsUpdUserAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsUpdUserAddress]
GO

/****** Object:  StoredProcedure [dbo].[spInsUpdUserAddress]    Script Date: 04/25/2014 18:38:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUpdUserAddress]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @Id int
		DECLARE @UserId int
		DECLARE @FirstName NVARCHAR(255)
		DECLARE @LastName NVARCHAR(255)
		DECLARE @MobileNo NVARCHAR(50)
		DECLARE @TelephoneNo NVARCHAR(50)
		DECLARE @Postcode NVARCHAR(50)
		DECLARE @Country NVARCHAR(255)
		DECLARE @State NVARCHAR(255)
		DECLARE @City NVARCHAR(255)
		DECLARE @Address1 NVARCHAR(MAX)
		DECLARE @Address2 NVARCHAR(MAX)
		DECLARE @IsDefault BIT
		
		SET @Id =  @xml.value('(/UserAddress/Id)[1]', 'int' )
		SET @UserId =  @xml.value('(/UserAddress/UserId)[1]', 'int' )
		SET @FirstName =  @xml.value('(/UserAddress/FirstName)[1]', 'NVARCHAR(255)' )
		SET @LastName =  @xml.value('(/UserAddress/LastName)[1]', 'NVARCHAR(255)' )
		SET @MobileNo =  @xml.value('(/UserAddress/MobileNo)[1]', 'NVARCHAR(50)' )
		SET @TelephoneNo =  @xml.value('(/UserAddress/TelephoneNo)[1]', 'NVARCHAR(50)' )
		SET @Postcode =  @xml.value('(/UserAddress/Postcode)[1]', 'NVARCHAR(50)' )
		SET @Country =  @xml.value('(/UserAddress/Country)[1]', 'NVARCHAR(255)' )
		SET @State =  @xml.value('(/UserAddress/State)[1]', 'NVARCHAR(255)' )
		SET @City =  @xml.value('(/UserAddress/City)[1]', 'NVARCHAR(255)' )
		SET @Address1 =  @xml.value('(/UserAddress/Address1)[1]', 'NVARCHAR(MAX)' )
		SET @Address2 =  @xml.value('(/UserAddress/Address2)[1]', 'NVARCHAR(MAX)' )
		SET @IsDefault =  @xml.value('(/UserAddress/IsDefault)[1]', 'bit' )
		
		IF(@Id>0)
		BEGIN
			UPDATE dbo.UserAddress
			SET 
				UserId=@UserId,
				FirstName=@FirstName,
				LastName=@LastName,
				MobileNo=@MobileNo,
				TelephoneNo=@TelephoneNo,
				Postcode=@Postcode,
				Country=@Country,
				[State]=@State,
				City=@City,
				Address1=@Address1,
				Address2=@Address2,
				IsDefault=@IsDefault
			WHERE Id=@Id
		END
		ELSE
		BEGIN
			INSERT INTO dbo.UserAddress
			(UserId,FirstName,LastName,MobileNo,TelephoneNo,Postcode,Country,[State],City,Address1,Address2,IsDefault)
			VALUES
			(@UserId,@FirstName,@LastName,@MobileNo,@TelephoneNo,@Postcode,@Country,@State,@City,@Address1,@Address2,@IsDefault)
			SET @Id = SCOPE_IDENTITY()
		END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
	SELECT @Id 
	RETURN @Id
END

GO


