
/****** Object:  StoredProcedure [dbo].[spUpdOrderStatus]    Script Date: 04/30/2014 20:04:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdOrderStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdOrderStatus]
GO

/****** Object:  StoredProcedure [dbo].[spUpdOrderStatus]    Script Date: 04/30/2014 20:04:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdOrderStatus]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @id int
	DECLARE @newStatus NVARCHAR(50)
	DECLARE @username NVARCHAR(50)
	DECLARE @remark NVARCHAR(max)
	SET @id =  @xml.value('(/OrderHistory/OrderId)[1]', 'int' )
	SET @newStatus =  @xml.value('(/OrderHistory/NewStatus)[1]', 'nvarchar(50)' )
	SET @username =  @xml.value('(/OrderHistory/CreatedBy/UserName)[1]', 'nvarchar(50)' )
	SET @remark =  @xml.value('(/OrderHistory/Remark)[1]', 'nvarchar(max)' )
	
	DECLARE @OldStatusId INT
	DECLARE @NewStatusId INT
	DECLARE @userId INT
	SELECT @OldStatusId = [Status] FROM dbo.[Order] WHERE Id=@Id
	SELECT @NewStatusId = Id FROM dbo.OrderStatus WHERE Name=@newStatus
	SELECT @userId=UserId FROM dbo.[User] WHERE UserName=@username
	
	UPDATE dbo.[Order] SET [Status]=@NewStatusId WHERE Id=@Id
	
	INSERT INTO dbo.OrderHistory(OrderId,OldStatus,NewStatus,CreatedBy,CreatedDate,Remark)
	VALUES
	(@id,@OldStatusId,@NewStatusId,@userId,GETDATE(),@remark)
	
END

GO


