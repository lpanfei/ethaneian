IF NOT EXISTS(SELECT 1 FROM sys.columns
WHERE Name = N'IsDoctorReply' AND OBJECT_ID = OBJECT_ID(N'Answer'))
BEGIN
	ALTER TABLE dbo.[Answer]
	ADD [IsDoctorReply] BIT
END 
GO
IF NOT EXISTS(SELECT 1 FROM sys.columns
WHERE Name = N'IsBestAnswer' AND OBJECT_ID = OBJECT_ID(N'Answer'))
BEGIN
	ALTER TABLE dbo.[Answer]
	ADD [IsBestAnswer] BIT
END 
GO
IF NOT EXISTS(SELECT 1 FROM sys.columns
WHERE Name = N'LikeCount' AND OBJECT_ID = OBJECT_ID(N'Answer'))
BEGIN
	ALTER TABLE dbo.[Answer]
	ADD [LikeCount] int NOT NULL default(0)
END 
GO
IF NOT EXISTS(SELECT 1 FROM sys.columns
WHERE Name = N'DislikeCount' AND OBJECT_ID = OBJECT_ID(N'Answer'))
BEGIN
	ALTER TABLE dbo.[Answer]
	ADD [DislikeCount] BIT
END 
GO