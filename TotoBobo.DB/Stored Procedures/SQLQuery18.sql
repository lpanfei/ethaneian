USE [Ethaneian]
GO
/****** Object:  StoredProcedure [dbo].[spSelQuestions]    Script Date: 07/07/2014 00:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spSelQuestions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *, (SELECT * FROM dbo.[User] WHERE UserId=Question.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Question.CreatedBy
	FOR XML PATH(''), ROOT('User'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Question.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.QuestionType WHERE Id=Question.TypeId
	FOR XML PATH(''), ROOT('QuestionType'), TYPE),
	(SELECT * FROM dbo.Doctor WHERE Id=Question.DoctorId
	FOR XML PATH(''), ROOT('Doctor'), TYPE),
	(SELECT Id,Body,RepliedDate,QuestionId,IsAdminReply,IsDoctorReply, IsBestAnswer, LikeCount, DislikeCount,
	(SELECT *,(SELECT Avator FROM dbo.Doctor WHERE UserId=[User].UserId) AS Avator FROM dbo.[User] WHERE UserId=anws.RepliedBy
	FOR XML PATH(''), ROOT('RepliedBy'), TYPE),
	(SELECT * FROM dbo.Answer WHERE Id=anws.RepliedTo
	FOR XML PATH(''), ROOT('RepliedTo'), TYPE)
	 FROM dbo.Answer anws WHERE QuestionId=Question.Id
	FOR XML PATH('Answer'), ROOT('Replies'), TYPE)
	 FROM dbo.Question
	FOR XML PATH('Question'), ROOT('QuestionData')
END
