USE [Ethaneian]
GO
/****** Object:  StoredProcedure [dbo].[spSelCaptcha]    Script Date: 07/06/2014 23:00:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spSelAnswer]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.Answer
	WHERE Id=@id
	FOR XML PATH('Answer'), ROOT('AnswerData')
END
