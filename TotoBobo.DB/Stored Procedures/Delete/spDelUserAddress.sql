/****** Object:  StoredProcedure [dbo].[spDelUserAddress]    Script Date: 04/25/2014 18:33:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDelUserAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDelUserAddress]
GO

/****** Object:  StoredProcedure [dbo].[spDelUserAddress]    Script Date: 04/25/2014 18:33:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelUserAddress]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.UserAddress WHERE Id=@id
END

GO


