/****** Object:  StoredProcedure [dbo].[spDelDeliveryFee]    Script Date: 04/25/2014 18:32:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDelDeliveryFee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDelDeliveryFee]
GO

/****** Object:  StoredProcedure [dbo].[spDelDeliveryFee]    Script Date: 04/25/2014 18:32:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelDeliveryFee] 
	-- Add the parameters for the stored procedure here
	@deliveryMethodId int,
	@stateId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.DeliveryFee WHERE DeliveryMethodId=@deliveryMethodId AND StateId=@stateId
END

GO


