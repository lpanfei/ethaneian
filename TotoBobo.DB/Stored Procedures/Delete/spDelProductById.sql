/****** Object:  StoredProcedure [dbo].[spDelProductById]    Script Date: 04/25/2014 18:32:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDelProductById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDelProductById]
GO

/****** Object:  StoredProcedure [dbo].[spDelProductById]    Script Date: 04/25/2014 18:32:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelProductById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE dbo.Product
    SET IsDeleted=1
    WHERE Id=@id
END

GO


