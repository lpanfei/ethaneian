/****** Object:  StoredProcedure [dbo].[spDelState]    Script Date: 04/25/2014 18:32:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDelState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDelState]
GO

/****** Object:  StoredProcedure [dbo].[spDelState]    Script Date: 04/25/2014 18:32:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelState]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE dbo.[State] SET IsEnabled=0 WHERE Id=@id
END

GO


