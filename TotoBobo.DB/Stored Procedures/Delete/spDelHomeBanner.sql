/****** Object:  StoredProcedure [dbo].[spDelHomeBanner]    Script Date: 04/30/2014 18:35:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDelHomeBanner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDelHomeBanner]
GO

/****** Object:  StoredProcedure [dbo].[spDelHomeBanner]    Script Date: 04/30/2014 18:35:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelHomeBanner]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
    DECLARE @order INT
    SELECT @order=[Order] FROM dbo.HomeBanner WHERE Id=@id
	DELETE FROM dbo.HomeBanner WHERE Id=@id
	UPDATE dbo.HomeBanner SET [Order]=[Order]-1 WHERE [Order]>@order
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END

GO


