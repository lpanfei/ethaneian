/****** Object:  StoredProcedure [dbo].[spDelCategoryById]    Script Date: 04/15/2014 21:27:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDelCategoryById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDelCategoryById]
GO

/****** Object:  StoredProcedure [dbo].[spDelCategoryById]    Script Date: 04/15/2014 21:27:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelCategoryById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE dbo.Category
    SET ParentId=0
    WHERE ParentId=@id
	DELETE FROM dbo.Category WHERE Id=@id
END

GO


