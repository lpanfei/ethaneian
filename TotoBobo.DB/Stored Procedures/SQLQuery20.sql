USE [Ethaneian]
GO
/****** Object:  StoredProcedure [dbo].[spSelUserById]    Script Date: 08/04/2014 13:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdUser]
	-- Add the parameters for the stored procedure here
	@userid int,
	@email nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY	
		UPDATE dbo.[User]
		SET Email = @email
		WHERE UserId = @userid
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION

	SELECT @userid
	RETURN @userid
END