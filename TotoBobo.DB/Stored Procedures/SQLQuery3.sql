USE [Ethaneian]
GO
/****** Object:  StoredProcedure [dbo].[spSelProductById]    Script Date: 07/06/2014 21:11:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spSelProductById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	 Id,Name,[Description],IsHidden,Price,Quantity,CreatedDate,LastModifiedDate,[Weight],ISNULL(IsFeatured,0) AS IsFeatured,Tags,ISNULL(IsRecommended,0) AS IsRecommended,
	 (SELECT * FROM dbo.[User] WHERE UserId=Product.CreatedBy
	FOR XML PATH(''), ROOT('CreatedBy'), TYPE),
	(SELECT * FROM dbo.[User] WHERE UserId=Product.LastModifiedBy
	FOR XML PATH(''), ROOT('LastModifiedBy'), TYPE),
	(SELECT * FROM dbo.Category WHERE Id=Product.CategoryId
	FOR XML PATH(''), ROOT('Category'), TYPE),
	(SELECT * FROM dbo.Country WHERE Id=Product.CountryId
	FOR XML PATH(''), ROOT('Country'), TYPE),
	(SELECT * FROM dbo.Brand WHERE Id=Product.BrandId
	FOR XML PATH(''), ROOT('Brand'), TYPE),
	(SELECT * FROM dbo.ProductPicture  WHERE ProductId=Product.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE),
	(SELECT * FROM dbo.ProductDiscount pd WHERE ISNULL(pd.IsDeleted,0)<>1 AND GETDATE()>=pd.FromDate AND GETDATE()<=DATEADD(dd, 1,pd.EndDate) AND pd.ProductId=Product.Id
	FOR XML PATH(''), ROOT('Discount'), TYPE),
	(SELECT p.Id,p.Name,p.Price,(SELECT * FROM dbo.ProductPicture  WHERE ProductId=p.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM  dbo.RelatedProduct rp 
	INNER JOIN Product p ON rp.RelatedId=p.Id
	 WHERE rp.ProductId=Product.Id
	FOR XML PATH('Product'), ROOT('RelatedItems'), TYPE),
	(SELECT p.Id,p.Name,p.Price,p.Quantity,(SELECT * FROM dbo.ProductPicture  WHERE ProductId=p.Id
	FOR XML PATH('ProductPicture'), ROOT('Pictures'), TYPE) FROM  dbo.Accessory acc 
	INNER JOIN Product p ON acc.AccessoryId=p.Id
	 WHERE acc.ProductId=Product.Id
	FOR XML PATH('Product'), ROOT('Accessories'), TYPE)
	FROM dbo.Product
	WHERE Id=@id AND ISNULL(IsDeleted,0)<>1
	FOR XML PATH('Product'), ROOT('ProductData')
END
