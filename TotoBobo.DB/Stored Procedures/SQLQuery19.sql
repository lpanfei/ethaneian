USE [Ethaneian]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDelUserById]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRANSACTION
	BEGIN TRY
	DELETE [dbo].[Doctor] WHERE UserId = @id
	DELETE [dbo].[webpages_Membership] WHERE UserId = @id
	DELETE [dbo].[webpages_OAuthMembership] WHERE UserId = @id
	DELETE [dbo].[webpages_UsersInRoles] WHERE UserId = @id
	DELETE [dbo].[User] WHERE UserId = @id
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @msg nvarchar(200)
		DECLARE @errnum int
		DECLARE @errstate int
		SET @errnum = 16
		SET @msg = ERROR_MESSAGE()
		SET @errstate = 16
		RAISERROR(@msg, @errnum, @errstate)
		RETURN	
	END CATCH
	COMMIT TRANSACTION
END
