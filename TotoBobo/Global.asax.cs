﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using TotoBobo.Data;
using System.Web.Routing;

namespace TotoBobo
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            DBInitializer.Initialize(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);
            RegisterRoutes();
        }

        void RegisterRoutes()
        {
            RouteTable.Routes.Ignore("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.MapPageRoute("account-login","login","~/Account/Login.aspx");
            RouteTable.Routes.MapPageRoute("account-register", "register", "~/Account/Register.aspx");
            RouteTable.Routes.MapPageRoute("account-verifyname", "register/verifyname", "~/Account/Register.aspx");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            //log the error to txt file

            //show error page
            //Server.Transfer("~/error", true);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}