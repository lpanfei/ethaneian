﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="TotoBobo.Account.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
<!-- MY ACCOUNT PAGE -->
		<section class="my_account parallax">
			<!-- CONTAINER -->
			<div class="container">
				
				<div class="my_account_block clearfix">
					<div class="register">
						<h2>CREATE NEW ACCOUNT</h2>
						<div class="register_form" >
                            <div>
                            <label>Username <span class="color_red">*</span><span style="margin-left:5px;"><img src="../images/preloader.gif"/><span  class="error"></span></span></label>
                            <input type="text" name="username" placeholder="USERNAME" />
                            </div>
							<div>
                            <label>EMAIL <span class="color_red">*</span><span style="margin-left:5px;"><img src="../images/preloader.gif"/><span  class="error"></span></span></label>
							<input type="text" name="email" placeholder="EMAIL"/>
                            </div>
                            <div>
                            <label>PASSWORD <span class="color_red">*</span><span class="error"  style="margin-left:5px;"></span></label>
                            <input type="password" name="password" placeholder="PASSWORD" />
                            </div>
                            <div>
                            <label>CONFIRM PASSWORD <span class="color_red">*</span><span class="error"  style="margin-left:5px;"></span></label>
							<input class="last" type="password" name="confirmpassword" placeholder="CONFIRM PASSWORD"/>
                            </div>
							<div class="center"><input type="submit" value="Register"/></div>
						</div>
					</div>
					<div class="already_member">
						<h2>ALREADY A MEMBER?</h2>
						
						<div ><a class="btn active" href="login" style="margin:0px" >login</a></div>
					</div>
				</div>
			</div><!-- //CONTAINER -->
		</section><!-- //MY ACCOUNT PAGE -->
        <script type="text/javascript">
            $(function () {
                $("input[name='username']").blur(function () {
                    $(this).parent().find(".error").text("");
                    $(this).parent().find("img").css("display", "none");
                    if ($(this).val() == "") {
                        $(this).parent().find(".error").text("Username is required!");
                        return;
                    }
                    var username = $(this).val();
                    $(this).parent().find("img").css("display", "inline");
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "register/verifyname",
                        data: "{'username':'" + $("input[name='username']").val() + "}",
                        dataType: "json",
                        success: function (msg) {
                            alert(msg.d);
                            $(this).parent().find("img").css("display", "none");
                        }
                    });
                });
                $("input[name='email']").blur(function () {
                    $(this).parent().find(".error").text("");
                    $(this).parent().find("img").css("display", "none");
                    if ($(this).val() == "") {
                        $(this).parent().find(".error").text("Email is required!");
                        return;
                    }
                    $(this).parent().find("img").css("display", "inline");
                });
                $("input[name='password']").blur(function () {
                    $(this).parent().find(".error").text("");
                    if ($(this).val() == "") {
                        $(this).parent().find(".error").text("Password is required!");
                        return;
                    }
                });
                $("input[name='confirmpassword']").blur(function () {
                    $(this).parent().find(".error").text("");
                    if ($(this).val() == "") {
                        $(this).parent().find(".error").text("Please confirm password!");
                        return;
                    }
                    if ($(this).val() != $("input[name='password']").val()) {
                        $(this).parent().find(".error").text("Passwor doesn't match!");
                        return;
                    }
                });
            });
        </script>
</asp:Content>
