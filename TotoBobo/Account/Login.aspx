﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TotoBobo.Account.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- MY ACCOUNT PAGE -->
		<section class="my_account parallax">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<div class="my_account_block clearfix">
					<div class="login">
						<h2>I'M ALREADY REGISTERED</h2>
						<div class="login_form" >
							<input type="text" name="username" value="Username" onFocus="if (this.value == 'Username') this.value = '';" onBlur="if (this.value == '') this.value = 'Username';" />
							<input class="last" type="text" name="password" value="Password" onFocus="if (this.value == 'Password') this.value = '';" onBlur="if (this.value == '') this.value = 'Password';" />
							<div class="clearfix">
								<div class="pull-left"><input type="checkbox" id="categorymanufacturer1" /><label for="categorymanufacturer1">Keep me signed</label></div>
								<div class="pull-right"><a class="forgot_pass" href="javascript:void(0);" >Forgot password?</a></div>
							</div>
							<div class="center"><input type="submit" value="Login"></div>
						</div>
					</div>
					<div class="new_customers">
						<h2>NEW CUSTOMERS</h2>
						<p>Register with Glammy Shop to enjoy personalized services, including:</p>
						<ul>
							<li><a href="javascript:void(0);" >—  Online Order Status</a></li>
							<li><a href="javascript:void(0);" >—  Love List</a></li>
							<li><a href="javascript:void(0);" >—  Sign up to receive exclusive news and private sales</a></li>
							<li><a href="javascript:void(0);" >—  Place Test Orders</a></li>
							<li><a href="javascript:void(0);" >—  Quick and easy checkout</a></li>
						</ul>
						<div class="center"><a class="btn active" href="register" >create new account</a></div>
					</div>
				</div>
				
				<div class="my_account_note center">HAVE A QUESTION? <b>1 800 888 02828</b></div>
			</div><!-- //CONTAINER -->
		</section><!-- //MY ACCOUNT PAGE -->
</asp:Content>
