using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Eidolon.Web.DA
{
    /// <summary>
    /// Acts as a central class to store the connection string.
    /// </summary>
	public class DBTools
	{
		private static string _connectionString;
		/// <summary>
		/// The global connection string (usually set in Global.asax)
		/// </summary>
		public static string ConnectionString
		{
			get { return _connectionString; }
			set { _connectionString = value; }
		}
		/// <summary>
		/// The globally available connection
		/// </summary>
		public SqlConnection GetConnection()
		{
			return new SqlConnection(_connectionString);
		}
	}
}
