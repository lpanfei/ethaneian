using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Eidolon.Web.DA
{
    /// <summary>
    /// Holds the connection and transaction for managing atomic
    /// transactions for SQL database.
    /// </summary>
    public struct DBLockHolder
    {
        SqlConnection _connection;
        SqlTransaction _transaction;

        /// <summary>
        /// A database version of a LockHolder which is simply a connection and transaction
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        public DBLockHolder(SqlConnection connection, SqlTransaction transaction)
        {
            _connection = connection;
            _transaction = transaction;
        }

        /// <summary>
        /// The connection for the lock holder
        /// </summary>
        public SqlConnection Connection
        {
            get { return _connection; }
        }

        /// <summary>
        /// The transaction
        /// </summary>
        public SqlTransaction Transaction
        {
            get { return _transaction; }
        }
    }

    /// <summary>
    /// Supplies the basic operations for supporting a SQL database store.
    /// </summary>
    /// <typeparam name="T">The data class to operate on</typeparam>
    /// <typeparam name="Tparms">A set of parameters to select with</typeparam>
    [Serializable]
    public abstract class DataProviderDBBase<T, Tparms> : DBTools
    {
        /// <summary>
        /// This string should be set during application initialization (Application_Start)
        /// </summary>
        protected Tparms _parms = default(Tparms);
        /// <summary>
        /// The List of objects which may be used for persistence of data to avoid
        /// multiple trips to the database. It is not required that it be used.
        /// </summary>
        protected List<T> _list = null;

        /// <summary>
        /// Exists only to provide an object to lock on
        /// </summary>
        protected string LockObject = "";

        /// <summary>
        /// Provides access to _list. Use this for caching data.
        /// </summary>
        protected List<T> CachedList { get { return _list; } set { _list = value; } }

        /// <summary>
        /// Clears the cache (if any)
        /// </summary>
        public void Invalidate()
        {
            _list = null;
            _parms = default(Tparms);
        }

        /// <summary>
        /// Executes and deserializes a prepared command
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        protected List<T> LoadXml(SqlCommand sqlCmd)
        {
            XmlReader rdr = null;
            try
            {
                sqlCmd.Connection.Open();
                rdr = sqlCmd.ExecuteXmlReader();
                if (rdr.Read())
                {
                    return Deserialize(rdr);
                }
                else
                {
                    return new List<T>(0);
                }
            }
            finally
            {
                if (rdr != null) rdr.Close();
                rdr = null;
                sqlCmd.Connection.Close();
                sqlCmd.Dispose();
                sqlCmd = null;
            }
        }

        /// <summary>
        /// gets xml from the reader and serializes a list of objects of type T
        /// </summary>
        XmlSerializer _serializer = new XmlSerializer(typeof(T[]), new System.Xml.Serialization.XmlRootAttribute(SetRoot()));
        private List<T> Deserialize(XmlReader rdr)
        {
            //Helpful debug code
            //if ( rdr.Name.Contains("StringValueData")) 
            //{
            //    String xml = rdr.ReadOuterXml();
            //    return null;
            //}

            List<T> list = new List<T>((T[])_serializer.Deserialize(rdr));
            return list;
        }

        /// <summary>
        /// Sets the root name of the XML data
        /// </summary>
        /// <returns></returns>
        protected static string SetRoot()
        {
            return typeof(T).Name + "Data";
        }

        private List<T> ExecuteCommand(SqlCommand sqlCommand)
        {
            XmlReader rdr = null;
            try
            {
                sqlCommand.CommandTimeout = 180;
                sqlCommand.Connection.Open();
                try
                {
                    rdr = sqlCommand.ExecuteXmlReader();
                    rdr.Read();
                    if (rdr.NodeType != XmlNodeType.None)
                    {
                        return Deserialize(rdr);
                    }
                    else
                    {
                        return new List<T>(0);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException is System.Data.SqlTypes.SqlNullValueException)
                    {
                        return new List<T>(0);
                    }
                    throw;
                }
            }
            finally
            {
                sqlCommand.Connection.Close();
                if (rdr != null) rdr.Close();
                sqlCommand.Dispose();
                sqlCommand = null;
                rdr = null;
            }
        }

        /// <summary>
        /// Executes a command on the database to return data
        /// </summary>
        /// <param name="procedureName">name of the stored procedure</param>
        /// <param name="parms">A set of SQLParameters to operate with</param>
        /// <param name="lockHolder">A LockHolder object. Null if no locking needed.</param>
        /// <returns></returns>
        protected List<T> ExecuteCommand(string procedureName, SqlParameter[] parms, object lockHolder)
        {
            SqlCommand cmd = GetCommand(procedureName, lockHolder);
            cmd.Parameters.AddRange(parms);
            return ExecuteCommand(cmd, lockHolder);
        }

        /// <summary>
        /// Executes a command without parameters
        /// </summary>
        /// <param name="cmd">name of the stored procedure</param>
        /// <param name="lockHolder">A LockHolder object. Null if no locking needed.</param>
        /// <returns></returns>
        protected List<T> ExecuteCommand(SqlCommand cmd, object lockHolder)
        {
            if (lockHolder is DBLockHolder)
            {
                cmd.Transaction = ((DBLockHolder)lockHolder).Transaction;
            }
            return ExecuteCommand(cmd);
        }

        /// <summary>
        /// Executes the procedure, no parameters.
        /// </summary>
        /// <param name="procedureName"></param>
        /// <returns></returns>
        protected List<T> ExecuteCommand(string procedureName)
        {
            return ExecuteCommand(procedureName, new SqlParameter[0], null);
        }

        /// <summary>
        /// Prepares and returns a SqlCommand
        /// </summary>
        /// <param name="spName">the name of a stored procedure</param>
        /// <param name="lockHolder">a DBLockHolder object or null</param>
        /// <returns></returns>
        protected SqlCommand GetCommand(string spName, object lockHolder)
        {
            SqlConnection sqlConn = null;
            if (lockHolder == null) sqlConn = GetConnection();
            else sqlConn = ((DBLockHolder)lockHolder).Connection;
            SqlCommand sqlCmd = sqlConn.CreateCommand();
            if (lockHolder != null) sqlCmd.Transaction = ((DBLockHolder)lockHolder).Transaction;
            sqlCmd.CommandText = spName;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            return sqlCmd;
        }

        XmlSerializer _xmlS = new XmlSerializer(typeof(T));
        /// <summary>
        /// Serializes the item, and adds a @xml parameter to the command
        /// </summary>
        /// <param name="spName">Stored procedure name</param>
        /// <param name="item">what is to be stored</param>
        /// <param name="lockHolder">optional transaction object</param>
        /// <returns></returns>
        protected SqlCommand PrepareUpdateCommand(string spName, T item, object lockHolder)
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            try
            {
                _xmlS.Serialize(sw, item);
                SqlCommand sqlCmd = GetCommand(spName, lockHolder);
                string xml = sw.GetStringBuilder().ToString();
                // remove the <?xml...> tag
                xml = System.Text.RegularExpressions.Regex.Replace(xml, "<[?]xml.*?>\r\n", "");
                sqlCmd.Parameters.AddWithValue("@xml", xml);
                return sqlCmd;
            }
            finally
            {
                sw.Close();
            }
        }

        /// <summary>
        /// Updates the item using the command with a lock holder to provide transaction
        /// management.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="sqlCommand"></param>
        /// <param name="lockHolder"></param>
        /// <returns></returns>
        protected object Update(T item, string sqlCommand, object lockHolder)
        {
            SqlCommand sqlCmd = PrepareUpdateCommand(sqlCommand, item, lockHolder);
            sqlCmd.CommandTimeout = 120; // Increase from default of 30 seconds
            sqlCmd.Parameters.Add("@RETVAL", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            object ID = null;
            if (sqlCmd.Connection.State == ConnectionState.Closed) sqlCmd.Connection.Open();
            try
            {
                sqlCmd.ExecuteNonQuery();
                ID = sqlCmd.Parameters["@RETVAL"].Value;
            }
            catch (Exception ex)
            {
                throw ex;	// a convenient debugging point
            }
            finally
            {
                if (lockHolder == null) sqlCmd.Connection.Close();
                sqlCmd.Dispose();
                sqlCmd = null;
            }
            return ID;
        }

        /// <summary>
        /// Delete an item
        /// </summary>
        /// <param name="sqlCommand">The SQL command string (stored procedure) to use</param>
        /// <param name="lockHolder">A DBLockHolder object or null if no lock needed</param>
        protected void Delete(string sqlCommand, object lockHolder)
        {
            SqlCommand sqlCmd = GetCommand(sqlCommand, lockHolder);
            if (sqlCmd.Connection.State == ConnectionState.Closed) sqlCmd.Connection.Open();
            try
            {
                sqlCmd.ExecuteNonQuery();
            }
            finally
            {
                sqlCmd.Connection.Close();
                sqlCmd.Dispose();
                sqlCmd = null;
            }
        }

        /// <summary>
        /// Gets a database transaction
        /// </summary>
        /// <returns></returns>
        public object GetLock()
        {
            SqlConnection sqlConn = new SqlConnection(DBTools.ConnectionString);
            sqlConn.Open();
            SqlTransaction sqlTran = sqlConn.BeginTransaction();
            return new DBLockHolder(sqlConn, sqlTran);
        }

        /// <summary>
        /// Commits a database transaction
        /// </summary>
        /// <param name="lockHolder"></param>
        public void CommitLock(object lockHolder)
        {
            DBLockHolder lh = (DBLockHolder)lockHolder;
            if (lh.Transaction != null && lh.Transaction.Connection != null) lh.Transaction.Commit();
            if (lh.Connection != null && lh.Connection.State != System.Data.ConnectionState.Closed) lh.Connection.Close();
        }

        /// <summary>
        /// Rolls back a database transaction
        /// </summary>
        /// <param name="lockHolder"></param>
        public void RollbackLock(object lockHolder)
        {
            DBLockHolder lh = (DBLockHolder)lockHolder;
            if (lh.Transaction != null && lh.Transaction.Connection != null) lh.Transaction.Rollback();
            if (lh.Connection != null && lh.Connection.State != System.Data.ConnectionState.Closed) lh.Connection.Close();
        }

    }
}
