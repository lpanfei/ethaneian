using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Eidolon.Web.DA
{
    /// <summary>
    /// Inherit from this class to create 
    /// </summary>
	[Serializable]
	public abstract class DataBaseBase
	{
		/// <summary>
		/// This string should be set during application initialization (Application_Start)
		/// </summary>
		private static string _connectionString;
		private static SqlConnection _connection = null;

		public static string ConnectionString
		{
			get { return _connectionString; }
			set { _connectionString = value; }
		}

		public static SqlConnection Connection
		{
			get
			{
				if (_connection == null)
				{
					_connection = new SqlConnection(_connectionString);
				}
				return _connection;
			}
		}

		/// <summary>
		/// Decides if a data item in a row is valid and not null or
		/// returns an alternate value
		/// </summary>
		/// <typeparam name="T">A System.Type to cast the item to and return</typeparam>
		/// <param name="name">the name of the column</param>
		/// <param name="row">a DataRow</param>
		/// <param name="alternate">the value to return if the column is not available</param>
		/// <returns></returns>
		public static T Get<T>(string name, DataRow row, T alternate)
		{
			if (row.Table.Columns[name] != null &&
				row[name] != null &&
				row[name] != System.DBNull.Value)
			{
				return (T)row[name];
			}
			else
			{
				return alternate;
			}
		}

		/// <summary>
		/// Decides if a data item in a row is valid and not null or
		/// returns an alternate value
		/// </summary>
		/// <typeparam name="T">A System.Type to cast the item to and return</typeparam>
		/// <param name="name">the name of the column</param>
		/// <param name="row">a DataRow</param>
		/// <param name="alternate">the value to return if the column is not available</param>
		/// <returns></returns>
		public T Get<T>(string name, SqlDataReader rdr, T alternate)
		{
			if (rdr[name] != null &&
				rdr[name] != System.DBNull.Value)
			{
				return (T)rdr[name];
			}
			else
			{
				return alternate;
			}
		}
	}
}
