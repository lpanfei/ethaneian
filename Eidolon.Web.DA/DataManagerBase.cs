using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Eidolon.Web.DA
{
    /// <summary>
    /// DataManagerBase forms the connection between an object 
    /// and its data provider. This class provides the base implementation for
    /// ObjectBase.
    /// </summary>
    /// <typeparam name="T">A data object class</typeparam>
    /// <typeparam name="Tparms">A parameter class for the object controlling the Select method</typeparam>
	[Serializable]
	public abstract class DataManagerBase<T, Tparms>
	{
		
        /// <summary>
        /// Gets or sets the provider that supports T
        /// </summary>
		public static IDataProvider<T, Tparms> Provider
		{
			get { return _provider; }
			set { _provider = value; }
		} 
		private static IDataProvider<T, Tparms> _provider;

		/// <summary>
		/// Select a list of items according the the provided parameters
		/// </summary>
		/// <param name="parms"></param>
		/// <returns></returns>
		public static List<T> Select(Tparms parms)
		{
			if (_provider != null)
			{
#if DEBUG
				if (!string.IsNullOrEmpty(parms.ToString()))
				{
					DateTime dt = DateTime.Now;
					System.Diagnostics.Debug.WriteLine(string.Format("{0:00}.{1:000} {2} - {3}", dt.Second, dt.Millisecond
						, parms.GetType().ReflectedType != null ? parms.GetType().ReflectedType.Name : parms.ToString(), parms));
				}
#endif
// lock removed 10/13/2011 by LL
//				lock (_provider)
//				{
					List<T> list= _provider.Select(parms);
                    if (list == null)
                    {
                        list = new List<T>(0);
                    }
                    return list;
//				}
			}
			return new List<T>(0);
		}

		/// <summary>
		/// Updates the item given. If lockHolder is not null, it
		/// uses it as a transaction manager.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="lockHolder"></param>
		/// <returns></returns>
        public static T Update(T item, object lockHolder)
        {
            lock (_provider)
            {
                return _provider.Update(item, lockHolder);
            }
        }

		/// <summary>
		/// Update the given item. No transaction management.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
        public static T Update(T item)
        {
            return Update(item, null);
        }
		/// <summary>
		/// Inserts the given item. If lockholder is not null, it
		/// uses it a s a transaction manager.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="lockHolder"></param>
		/// <returns></returns>
        public static T Insert(T item, object lockHolder)
        {
            lock (_provider)
            {
                return _provider.Insert(item, lockHolder);
            }
        }
		/// <summary>
		/// Inserts the given item. No transaction management.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
        public static T Insert(T item)
        {
            return Insert(item, null);
        }

		/// <summary>
		/// Deletes the given item. If lockHolder is not null, it
		/// uses it as a transaction manager.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="lockHolder"></param>
		/// <returns></returns>
        public static T Delete(T item, object lockHolder)
		{
			lock (_provider)
			{
				return _provider.Delete(item, lockHolder);
			}
		}
		/// <summary>
		/// Deletest the given item. No transaction management.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
        public static T Delete(T item)
        {
            return Delete(item, null);
        }
		/// <summary>
		/// Gets a lock object to be used as a transaction manager.
		/// </summary>
		/// <returns></returns>
        public static object GetLock()
        {
            return _provider.GetLock();
        }
		/// <summary>
		/// Commits a transaction.
		/// </summary>
		/// <param name="lockHolder"></param>
        public static void CommitLock(object lockHolder)
        {
            _provider.CommitLock(lockHolder);
        }
		/// <summary>
		/// Rolls back a transaction.
		/// </summary>
		/// <param name="lockHolder"></param>
        public static void RollbackLock(object lockHolder)
        {
            _provider.RollbackLock(lockHolder);
        }

		/// <summary>
		/// Nulls the _list object, and clears the remembered parameters.
		/// </summary>
		public static void Invalidate()
		{
			lock (_provider)
			{
				_provider.Invalidate();
			}
		}
	}
}
