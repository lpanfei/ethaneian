﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class QuestionDB : DataProviderDBBase<Question, Question.Parms>, IDataProvider<Question, Question.Parms>
    {
        public List<Question> Select(Question.Parms parms)
        {
            if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selQuestionById, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.selQuestions, new SqlParameter[]
							{
							}, null);
            }
        }

        public Question Update(Question item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdQuestion, lockHolder);
            item.Id = id;
            return item;
        }

        public Question Insert(Question item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdQuestion, lockHolder);
            item.Id = id;
            return item;
        }

        public Question Delete(Question item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delQuestion;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
