﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class CountryDB : DataProviderDBBase<Country, Country.Parms>, IDataProvider<Country, Country.Parms>
    {
        public List<Country> Select(Country.Parms parms)
        {
            if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selCountryByName, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selCountryById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.allCountries, new SqlParameter[]
							{
								
							}, null);
            }
        }

        public Country Update(Country item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdCountry, lockHolder);
            item.Id = id;
            return item;
        }

        public Country Insert(Country item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdCountry, lockHolder);
            item.Id = id;
            return item;
        }

        public Country Delete(Country item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delCountry;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }

    public class StateDB : DataProviderDBBase<State, State.Parms>, IDataProvider<State, State.Parms>
    {
        public List<State> Select(State.Parms parms)
        {
            if (parms.ByCountry)
            {
                return ExecuteCommand(StoredProcedures.allStates, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@countryId", parms.CountryId)
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selStateById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selStateByName, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            return null;
        }

        public State Update(State item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdState, lockHolder);
            item.Id = id;
            return item;
        }

        public State Insert(State item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdState, lockHolder);
            item.Id = id;
            return item;
        }

        public State Delete(State item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delState;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }

    public class CityDB : DataProviderDBBase<City, City.Parms>, IDataProvider<City, City.Parms>
    {
        public List<City> Select(City.Parms parms)
        {
            if (parms.ByState)
            {
                return ExecuteCommand(StoredProcedures.allCities, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@stateId", parms.StateId)
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selCityById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selCityByName, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            return null;
        }

        public City Update(City item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdCity, lockHolder);
            item.Id = id;
            return item;
        }

        public City Insert(City item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdCity, lockHolder);
            item.Id = id;
            return item;
        }

        public City Delete(City item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delCity;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }

    public class PaymentMethodDB : DataProviderDBBase<PaymentMethod, PaymentMethod.Parms>, IDataProvider<PaymentMethod, PaymentMethod.Parms>
    {
        public List<PaymentMethod> Select(PaymentMethod.Parms parms)
        {
            if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selPaymentMethodByName, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selPaymentMethodById, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.allPaymentMethods, new SqlParameter[]
							{
							}, null);
            }
        }

        public PaymentMethod Update(PaymentMethod item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdPaymentMethod, lockHolder);
            item.Id = id;
            return item;
        }

        public PaymentMethod Insert(PaymentMethod item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdPaymentMethod, lockHolder);
            item.Id = id;
            return item;
        }

        public PaymentMethod Delete(PaymentMethod item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }

    public class DeliveryMethodDB : DataProviderDBBase<DeliveryMethod, DeliveryMethod.Parms>, IDataProvider<DeliveryMethod, DeliveryMethod.Parms>
    {
        public List<DeliveryMethod> Select(DeliveryMethod.Parms parms)
        {
            if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selDeliveryMethodByName, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selDeliveryMethodById, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.allDeliveryMethods, new SqlParameter[]
							{
							}, null);
            }
        }

        public DeliveryMethod Update(DeliveryMethod item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdDeliveryMethod, lockHolder);
            item.Id = id;
            return item;
        }

        public DeliveryMethod Insert(DeliveryMethod item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdDeliveryMethod, lockHolder);
            item.Id = id;
            return item;
        }

        public DeliveryMethod Delete(DeliveryMethod item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }

    public class DeliveryFeeDB : DataProviderDBBase<DeliveryFee, DeliveryFee.Parms>, IDataProvider<DeliveryFee, DeliveryFee.Parms>
    {
        public List<DeliveryFee> Select(DeliveryFee.Parms parms)
        {
            if (parms.ByState)
            {
                return ExecuteCommand(StoredProcedures.selDeliveryFeeByState, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@deliveryMethodId", parms.DeliveryMethodId),
								new System.Data.SqlClient.SqlParameter("@stateId", parms.StateId)
							}, null);
            }
            else if (parms.ByDeliveryMethod)
            {
                return ExecuteCommand(StoredProcedures.allDeliveryFee, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@deliveryMethodId", parms.DeliveryMethodId)
							}, null);
            }

            return null;
        }

        public DeliveryFee Update(DeliveryFee item, object lockHolder)
        {
            Update(item, StoredProcedures.insUpdDeliveryFee, lockHolder);
            return item;
        }

        public DeliveryFee Insert(DeliveryFee item, object lockHolder)
        {
            Update(item, StoredProcedures.insUpdDeliveryFee, lockHolder);
            return item;
        }

        public DeliveryFee Delete(DeliveryFee item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delDeliveryFee;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@deliveryMethodId", item.DeliveryMethod.Id));
            cmd.Parameters.Add(new SqlParameter("@stateId", item.State.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
