﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;

namespace TotoBobo.Data.DBProviders
{
    public class OrderHistoryDB : DataProviderDBBase<OrderHistory, OrderHistory.Parms>, IDataProvider<OrderHistory, OrderHistory.Parms>
    {
        public List<OrderHistory> Select(OrderHistory.Parms parms)
        {
            throw new NotImplementedException();
        }

        public OrderHistory Update(OrderHistory item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.changeStatus, lockHolder);
            item.OrderId = id;
            return item;
        }

        public OrderHistory Insert(OrderHistory item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.changeStatus, lockHolder);
            item.OrderId = id;
            return item;
        }

        public OrderHistory Delete(OrderHistory item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
