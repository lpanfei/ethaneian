﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class CaptchaDB : DataProviderDBBase<Captcha, Captcha.Parms>, IDataProvider<Captcha, Captcha.Parms>
    {
        public List<Captcha> Select(Captcha.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selCaptcha, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@Guid", parms.Guid)
							}, null);
        }

        public Captcha Update(Captcha item, object lockHolder)
        {
            Update(item, StoredProcedures.insUpdCaptcha, lockHolder);
            return item;
        }

        public Captcha Insert(Captcha item, object lockHolder)
        {
            Update(item, StoredProcedures.insUpdCaptcha, lockHolder);
            return item;
        }

        public Captcha Delete(Captcha item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delCaptcha;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Guid", item.Guid));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
