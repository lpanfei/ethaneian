﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class AnswerDB : DataProviderDBBase<Answer, Answer.Parms>, IDataProvider<Answer, Answer.Parms>
    {
        public List<Answer> Select(Answer.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selAnswer, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
        }

        public Answer Update(Answer item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdAnswer, lockHolder);
            item.Id = id;
            return item;
        }

        public Answer Insert(Answer item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdAnswer, lockHolder);
            item.Id = id;
            return item;
        }

        public Answer Delete(Answer item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
