﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class CategoryDB : DataProviderDBBase<Category, Category.Parms>, IDataProvider<Category, Category.Parms>,ICategoryProvider
    {
        public List<Category> Select(Category.Parms parms)
        {
            if (parms.AllCategories)
            {
                return ExecuteCommand(StoredProcedures.allCategories, new SqlParameter[]
							{
								
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selCategoryById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selCategoryByName, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            return null;
        }

        public Category Update(Category item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdCategory, lockHolder);
            item.Id = id;
            return item;
        }

        public Category Insert(Category item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdCategory, lockHolder);
            item.Id = id;
            return item;
        }

        public Category Delete(Category item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delCategoryById;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }

        public void CreateBanner(Category category)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.updCategoryBanner;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Id", category.Id));
            cmd.Parameters.Add(new SqlParameter("@TopBanner", category.TopBanner));
            cmd.Parameters.Add(new SqlParameter("@Banner1", category.Banner1));
            cmd.Parameters.Add(new SqlParameter("@Banner1Link", category.Banner1Link));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
