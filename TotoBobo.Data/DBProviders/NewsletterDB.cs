﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class NewsletterDB : DataProviderDBBase<Newsletter, Newsletter.Parms>, IDataProvider<Newsletter, Newsletter.Parms>
    {
        public List<Newsletter> Select(Newsletter.Parms parms)
        {
             if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selNewsletterById, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.allNewsletters, new SqlParameter[]
							{
							}, null);
            }
        }

        public Newsletter Update(Newsletter item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdNewsletter, lockHolder);
            item.Id = id;
            return item;
        }

        public Newsletter Insert(Newsletter item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdNewsletter, lockHolder);
            item.Id = id;
            return item;
        }

        public Newsletter Delete(Newsletter item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delNewsletter;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
