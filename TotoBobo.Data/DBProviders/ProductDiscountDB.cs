﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class ProductDiscountDB : DataProviderDBBase<ProductDiscount, ProductDiscount.Parms>, IDataProvider<ProductDiscount, ProductDiscount.Parms>
    {
        public List<ProductDiscount> Select(ProductDiscount.Parms parms)
        {
            if (parms.ByProduct)
            {
                return ExecuteCommand(StoredProcedures.selSalesByProduct, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@pid", parms.ProductId)
							}, null);
            }
            return null;
        }

        public ProductDiscount Update(ProductDiscount item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdSale, lockHolder);
            item.Id = id;
            return item;
        }

        public ProductDiscount Insert(ProductDiscount item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdSale, lockHolder);
            item.Id = id;
            return item;
        }

        public ProductDiscount Delete(ProductDiscount item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delSale;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            cmd.Parameters.Add(new SqlParameter("@username", item.CreatedBy.UserName));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
