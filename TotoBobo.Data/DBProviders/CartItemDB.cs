﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class CartItemDB : DataProviderDBBase<CartItem, CartItem.Parms>, IDataProvider<CartItem, CartItem.Parms>,ICartItemProdiver
    {
        public List<CartItem> Select(CartItem.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selCartItems, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@sessionId", parms.SessionId)
							}, null);
        }

        public CartItem Update(CartItem item, object lockHolder)
        {
            Update(item, StoredProcedures.insUpdCartItem, lockHolder);
            return item;
        }

        public CartItem Insert(CartItem item, object lockHolder)
        {
            Update(item, StoredProcedures.insUpdCartItem, lockHolder);
            return item;
        }

        public CartItem Delete(CartItem item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.clearCart;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@sessionId", item.SessionId));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }

        public void ChangeQuantity(int Id, int Quantity)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.changeQty;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Id", Id));
            cmd.Parameters.Add(new SqlParameter("@Quantity",Quantity));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }


        public void RemoveItem(int id)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delCartItem;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
