﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class UserDB : DataProviderDBBase<User, User.Parms>, IDataProvider<User, User.Parms>
    {

        public List<User> Select(User.Parms parms)
        {
            if (parms.AllUsers)
            {
                return ExecuteCommand(StoredProcedures.allUsers, new SqlParameter[]
							{
								
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selUserById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@userid", parms.UserId)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.selUserByEmail, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@email", parms.Email)
							}, null);
            }
        }

        public User Update(User item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.updUser;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@userid", item.UserId));
            cmd.Parameters.Add(new SqlParameter("@email", item.Email));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }

        public User Insert(User item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public User Delete(User item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delUser;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.UserId));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
