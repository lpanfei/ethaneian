﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class HomeBannerDB : DataProviderDBBase<HomeBanner, HomeBanner.Parms>, IDataProvider<HomeBanner, HomeBanner.Parms>
    {
        public List<HomeBanner> Select(HomeBanner.Parms parms)
        {
            if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selHomeBannerById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.selHomeBanners, new SqlParameter[]
							{
								
							}, null);
            }
        }

        public HomeBanner Update(HomeBanner item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdHomeBanner, lockHolder);
            item.Id = id;
            return item;
        }

        public HomeBanner Insert(HomeBanner item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdHomeBanner, lockHolder);
            item.Id = id;
            return item;
        }

        public HomeBanner Delete(HomeBanner item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delHomeBanner;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
