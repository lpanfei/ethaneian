﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class SettingDB : DataProviderDBBase<Setting, Setting.Parms>, IDataProvider<Setting, Setting.Parms>
    {
        public List<Setting> Select(Setting.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selSetting, new SqlParameter[]
							{
								
							}, null);
        }

        public Setting Update(Setting item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdSetting, lockHolder);
            item.Id = id;
            return item;
        }

        public Setting Insert(Setting item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdSetting, lockHolder);
            item.Id = id;
            return item;
        }

        public Setting Delete(Setting item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }

    public class SaleBannerDB : DataProviderDBBase<SaleBanner, SaleBanner.Parms>, IDataProvider<SaleBanner, SaleBanner.Parms>
    {

        public List<SaleBanner> Select(SaleBanner.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selSaleBanner, new SqlParameter[]
							{
								
							}, null);
        }

        public SaleBanner Update(SaleBanner item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.createSaleBanner, lockHolder);
            item.Id = id;
            return item;
        }

        public SaleBanner Insert(SaleBanner item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.createSaleBanner, lockHolder);
            item.Id = id;
            return item;
        }

        public SaleBanner Delete(SaleBanner item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
