﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class QuestionTypeDB : DataProviderDBBase<QuestionType, QuestionType.Parms>, IDataProvider<QuestionType, QuestionType.Parms>
    {
        public List<QuestionType> Select(QuestionType.Parms parms)
        {
            if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selQuestionTypeById, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.selQuestionTypes, new SqlParameter[]
							{
							}, null);
            }
        }

        public QuestionType Update(QuestionType item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdQuestionType, lockHolder);
            item.Id = id;
            return item;
        }

        public QuestionType Insert(QuestionType item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdQuestionType, lockHolder);
            item.Id = id;
            return item;
        }

        public QuestionType Delete(QuestionType item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delQuestionType;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
