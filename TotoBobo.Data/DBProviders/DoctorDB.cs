﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class DoctorDB : DataProviderDBBase<Doctor, Doctor.Parms>, IDataProvider<Doctor, Doctor.Parms>
    {
        public List<Doctor> Select(Doctor.Parms parms)
        {
            if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selDoctorById, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.selDoctors, new SqlParameter[]
							{
                               
							}, null);
            }
        }

        public Doctor Update(Doctor item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdDoctor, lockHolder);
            item.Id = id;
            return item;
        }

        public Doctor Insert(Doctor item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdDoctor, lockHolder);
            item.Id = id;
            return item;
        }

        public Doctor Delete(Doctor item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delDoctor;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
