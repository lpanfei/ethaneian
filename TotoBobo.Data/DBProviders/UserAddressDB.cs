﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class UserAddressDB : DataProviderDBBase<UserAddress, UserAddress.Parms>, IDataProvider<UserAddress, UserAddress.Parms>, IUserAddressProvider
    {
        public List<UserAddress> Select(UserAddress.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selAddressesByUser, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@username", parms.UserName)
							}, null);
        }

        public UserAddress Update(UserAddress item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdAddresses, lockHolder);
            item.Id = id;
            return item;
        }

        public UserAddress Insert(UserAddress item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdAddresses, lockHolder);
            item.Id = id;
            return item;
        }

        public UserAddress Delete(UserAddress item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delAddress;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }

        public void MarkAsDefault(int id)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.markDefault;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
