﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class PieChartDB : DataProviderDBBase<PieChart, PieChart.Parms>, IDataProvider<PieChart, PieChart.Parms>
    {
        public List<PieChart> Select(PieChart.Parms parms)
        {
            if (parms.ChartType == PieChartType.OrderStatusChart)
            {
                return ExecuteCommand(StoredProcedures.chartOrderStatus, new SqlParameter[]
							{
								
							}, null);
            }
            else if (parms.ChartType == PieChartType.BestSellerChart)
            {
                return ExecuteCommand(StoredProcedures.chartBestSeller, new SqlParameter[]
							{
								
							}, null);
            }
            return null;
        }

        public PieChart Update(PieChart item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public PieChart Insert(PieChart item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public PieChart Delete(PieChart item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
