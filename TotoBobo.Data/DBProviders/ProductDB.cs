﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TotoBobo.Data.Objects;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class ProductDB : DataProviderDBBase<Product, Product.Parms>, IDataProvider<Product, Product.Parms>, IProductProvider
    {
        public List<Product> Select(Product.Parms parms)
        {
            if (parms.AllProducts)
            {
                return ExecuteCommand(StoredProcedures.allProducts, new SqlParameter[]
							{
								
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selProductById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selProductByName, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            else if (parms.ByCategory)
            {
                return ExecuteCommand(StoredProcedures.selProductByCategory, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@categoryId", parms.CategoryId)
							}, null);
            }
            else if (parms.ProductList)
            {
                return ExecuteCommand(StoredProcedures.selProductList, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@bid", parms.BrandId),
								new System.Data.SqlClient.SqlParameter("@cid", parms.CategoryId)
							}, null);
            }
            else if (parms.ActiveSales)
            {
                return ExecuteCommand(StoredProcedures.selActiveSales, new SqlParameter[]
							{
                               
							}, null);

            }
            else if (parms.Recommended)
            {
                return ExecuteCommand(StoredProcedures.selRecommended, new SqlParameter[]
							{
                               
							}, null);

            }

            return null;
        }

        public Product Update(Product item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdProduct, lockHolder);
            item.Id = id;
            return item;
        }

        public Product Insert(Product item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdProduct, lockHolder);
            item.Id = id;
            return item;
        }

        public Product Delete(Product item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delProductById;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }

        public void Feature(int id, bool isFeatured)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.featureProduct;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));
            cmd.Parameters.Add(new SqlParameter("@isFeatured", isFeatured));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }


        public void Recommend(int id, bool isRecommended)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.recommendProduct;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));
            cmd.Parameters.Add(new SqlParameter("@isRecommended", isRecommended));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }


        public void Hide(int id, bool isHidden)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.hideProduct;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));
            cmd.Parameters.Add(new SqlParameter("@isHidden", isHidden));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
