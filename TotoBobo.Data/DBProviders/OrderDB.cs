﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class OrderDB : DataProviderDBBase<Order, Order.Parms>, IDataProvider<Order, Order.Parms>, IOrderProvider
    {
        public List<Order> Select(Order.Parms parms)
        {
            if (parms.ByUser)
            {
                return ExecuteCommand(StoredProcedures.SelOrdersByUser, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@username", parms.UserName)
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.SelOrdersById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else if (parms.ByGuid)
            {
                return ExecuteCommand(StoredProcedures.SelOrdersByGuid, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@guid", parms.Guid)
							}, null);
            }
            else if (parms.AllOrders)
            {
                return ExecuteCommand(StoredProcedures.allOrders, new SqlParameter[]
							{
								
							}, null);
            }
            return null;
        }

        public Order Update(Order item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdOrder, lockHolder);
            item.Id = id;
            return item;
        }

        public Order Insert(Order item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdOrder, lockHolder);
            item.Id = id;
            return item;
        }

        public Order Delete(Order item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public void Pay(int orderId, string paymentId, string paymentGuid, OrderStatus status)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.payOrder;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", orderId));
            cmd.Parameters.Add(new SqlParameter("@paymentid", paymentId));
            cmd.Parameters.Add(new SqlParameter("@paymentguid", paymentGuid==null?"null":paymentGuid));
            cmd.Parameters.Add(new SqlParameter("@status", status.ToString()));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
