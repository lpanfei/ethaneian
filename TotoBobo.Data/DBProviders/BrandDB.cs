﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class BrandDB : DataProviderDBBase<Brand, Brand.Parms>, IDataProvider<Brand, Brand.Parms>
    {
        public List<Brand> Select(Brand.Parms parms)
        {
            if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selBrandByName, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selBrandById, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.allBrands, new SqlParameter[]
							{
							}, null);
            }
        }

        public Brand Update(Brand item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdBrand, lockHolder);
            item.Id = id;
            return item;
        }

        public Brand Insert(Brand item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdBrand, lockHolder);
            item.Id = id;
            return item;
        }

        public Brand Delete(Brand item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delBrand;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
