﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class StockHistoryDB : DataProviderDBBase<StockHistory, StockHistory.Parms>, IDataProvider<StockHistory, StockHistory.Parms>
    {
        public List<StockHistory> Select(StockHistory.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selStockHistoryById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@productid", parms.ProductId)
							}, null);
        }

        public StockHistory Update(StockHistory item, object lockHolder)
        {
           
            throw new NotImplementedException();
        }

        public StockHistory Insert(StockHistory item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.stockToInventory, lockHolder);
            item.ProductId = id;
            return item;
        }

        public StockHistory Delete(StockHistory item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
