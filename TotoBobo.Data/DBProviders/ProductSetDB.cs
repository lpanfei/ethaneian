﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using System.Data.SqlClient;

namespace TotoBobo.Data.DBProviders
{
    public class ProductSetDB : DataProviderDBBase<ProductSet, ProductSet.Parms>, IDataProvider<ProductSet, ProductSet.Parms>
    {
        public List<ProductSet> Select(ProductSet.Parms parms)
        {
            if (parms.ByAll)
            {
                return ExecuteCommand(StoredProcedures.allProductSet, new SqlParameter[]
							{
								
							}, null);
            }
            else if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selProductSetById, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.Id)
							}, null);
            }
            else if (parms.ByName)
            {
                return ExecuteCommand(StoredProcedures.selProductSetByName, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@name", parms.Name)
							}, null);
            }
            return null;
        }

        public ProductSet Update(ProductSet item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdProductSet, lockHolder);
            item.Id = id;
            return item;
        }

        public ProductSet Insert(ProductSet item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdProductSet, lockHolder);
            item.Id = id;
            return item;
        }

        public ProductSet Delete(ProductSet item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delProductSet;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
