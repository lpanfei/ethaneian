﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Captcha : DataManagerBase<Captcha, Captcha.Parms>
    {
        public class Parms
        {
            public string Guid { get; set; }
        }

        public string Guid { get; set; }
        public string Text { get; set; }
    }
}
