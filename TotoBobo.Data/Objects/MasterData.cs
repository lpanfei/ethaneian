﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Country : DataManagerBase<Country, Country.Parms>
    {
        public class Parms 
        {
            public bool ByName { get; set; }
            public string Name { get; set; }
            public bool ById { get; set; }
            public int Id { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public string Picture { get; set; }
    }

    public class State : DataManagerBase<State, State.Parms>
    {
        public class Parms
        {
            public bool ById { get; set; }
            public bool ByCountry { get; set; }
            public bool ByName { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public int CountryId { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }
        public List<City> Cities { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class City : DataManagerBase<City, City.Parms>
    {
        public class Parms
        {
            public bool ById { get; set; }
            public bool ByState { get; set; }
            public bool ByName { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public int StateId { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public State State { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class PaymentMethod : DataManagerBase<PaymentMethod, PaymentMethod.Parms>
    {
        public class Parms
        {
            public bool ByName { get; set; }
            public string Name { get; set; }
            public bool ById { get; set; }
            public int Id { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class DeliveryMethod : DataManagerBase<DeliveryMethod, DeliveryMethod.Parms>
    {
        public class Parms
        {
            public bool ByName { get; set; }
            public string Name { get; set; }
            public bool ById { get; set; }
            public int Id { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class DeliveryFee : DataManagerBase<DeliveryFee, DeliveryFee.Parms>
    {
        public class Parms
        {
            public int DeliveryMethodId { get; set; }
            public int StateId { get; set; }
            public bool ByDeliveryMethod { get; set; }
            public bool ByState { get; set; }
        }
        public DeliveryMethod DeliveryMethod { get; set; }
        public State State { get; set; }
        public decimal Fee { get; set; }
    }
}
