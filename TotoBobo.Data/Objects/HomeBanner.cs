﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class HomeBanner : DataManagerBase<HomeBanner, HomeBanner.Parms>
    {
        public class Parms
        {
            public bool ById { get; set; }
            public int Id { get; set; }
        }

        public int Id { get; set; }
        public string TopBanner { get; set; }
        public string Banner1 { get; set; }
        public string Banner2 { get; set; }
        public string Banner3 { get; set; }
        public string TopBannerLink { get; set; }
        public string Banner1Link { get; set; }
        public string Banner2Link { get; set; }
        public string Banner3Link { get; set; }
        public int Order { get; set; }
    }
}
