﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Answer : DataManagerBase<Answer, Answer.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
        }

        public int Id { get; set; }
        public string Body { get; set; }
        public User RepliedBy { get; set; }
        public DateTime RepliedDate { get; set; }
        public int QuestionId { get; set; }
        public Answer RepliedTo { get; set; }
        public bool IsAdminReply { get; set; }
        public bool IsDoctorReply { get; set; }
        public bool IsBestAnswer { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }
        public int BestId { get; set; }
        public int QuestionCreatorId { get; set; }
        public List<Answer> Replies { get; set; }
    }
}
