﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Newsletter : DataManagerBase<Newsletter, Newsletter.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
            public bool ById { get; set; }
        }

        public int Id { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public bool IsEnabled { get; set; }
        public NewsletterType SentType { get;set;}
        public List<Product> Products { get; set; }
    }

    public enum NewsletterType
    {
        Daily,
        Weekly,
        Monthly
    }
}
