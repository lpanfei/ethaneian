﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class PieChart : DataManagerBase<PieChart, PieChart.Parms>
    {
        public class Parms
        {
            public PieChartType ChartType { get; set; }
        }

        public string label { get; set; }
        public string data { get; set; }
    }

    public enum PieChartType
    {
        OrderStatusChart,
        BestSellerChart
    }
}
