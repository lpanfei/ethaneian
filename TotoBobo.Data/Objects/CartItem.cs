﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class CartItem : DataManagerBase<CartItem, CartItem.Parms>
    {
        public class Parms
        {
            public string SessionId { get; set; }
        }

        public int Id { get; set; }
        public string SessionId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public List<ProductTag> Tags { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public Product Product { get; set; }
        public bool IsHamper { get; set; }
        public List<Product> Products { get; set; }
        public ProductSet ProductSet { get; set; }

        public static void ChangeQty(int id, int qty)
        {
            ((ICartItemProdiver)Provider).ChangeQuantity(id, qty);
        }

        public static void RemoveItem(int id)
        {
            ((ICartItemProdiver)Provider).RemoveItem(id);
        }
    }

    public interface ICartItemProdiver : IDataProvider<CartItem, CartItem.Parms>
    {
        void ChangeQuantity(int Id, int Quantity);
        void RemoveItem(int id);
    }
}
