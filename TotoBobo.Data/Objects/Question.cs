﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Question : DataManagerBase<Question, Question.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
            public bool ById { get; set; }
        }

        public int Id { get; set; }
        public QuestionType QuestionType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TotoBobo.Data.Objects.User CreatedBy { get; set; }
        public User User { get; set; }
        public DateTime CreatedDate { get; set; }
        public User LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Answer { get; set; }
        public List<Answer> Replies { get; set; }
        public Doctor Doctor { get; set; }
        public string Status
        {
            get
            {
                if (Replies==null||!Replies.Any())
                    return "Unreplied";
                else
                    return "Replied";
            }
        }
        public string CreatedUserName
        {
            get{
                return User == null ? "Anonymous" : User.UserName;
            }
        }

    }

    public class QuestionType : DataManagerBase<QuestionType, QuestionType.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
            public bool ById { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
