﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Category : DataManagerBase<Category, Category.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
            public bool AllCategories { get; set; }
            public bool ById { get; set; }
            public bool ByName { get; set; }
            public string Name { get; set; }
        }

        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public User LastModifiedBy { get; set; }
        public string TopBanner { get; set; }
        public string Banner1 { get; set; }
        public string Banner1Link { get; set; }

        public static void CreateBanner(Category category)
        {
            ((ICategoryProvider)Provider).CreateBanner(category);
        }
    }

    public interface ICategoryProvider : IDataProvider<Category, Category.Parms>
    {
        void CreateBanner(Category category);
    }
}
