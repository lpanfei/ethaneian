﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Order : DataManagerBase<Order, Order.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
            public string UserName { get; set; }
            public string Guid { get; set; }
            public bool ById { get; set; }
            public bool ByUser { get; set; }
            public bool ByGuid { get; set; }
            public bool AllOrders { get; set; }
        }

        public int Id { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public UserAddress ShippingAddress { get; set; }
        public OrderStatus Status { get; set; }
        public User LastModifiedBy { get; set; }
        public DateTime LastModifedDate { get; set; }
        public decimal Total { get; set; }
        public List<OrderItem> Items { get; set; }
        public decimal DeliveryFee { get; set; }
        public DeliveryMethod DeliveryMethod { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string PaymentId { get; set; }
        public string PaymentGuid { get; set; }
        public decimal TaxRate { get; set; }
        public string ShippingNo { get; set; }
        public decimal RepeatCustomerDiscount { get; set; }
        public static void Pay(int orderId, string paymentId, string paymentGuid, OrderStatus status)
        {
            ((IOrderProvider)Provider).Pay(orderId,paymentId,paymentGuid,status);
        }

    }

    [Serializable]
    public class OrderItem
    {
        public int OrderId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal SubTotal { get; set; }
        public ProductDiscount Discount { get; set; }
        public int DiscountId { get; set; }
        public decimal UnitWeight { get; set; }
        public List<ProductTag> Tags { get; set; }
        public bool IsHamper { get; set; }
        public List<Product> Products { get; set; }
        public ProductSet ProductSet { get; set; }
    }

    public class OrderHistory : DataManagerBase<OrderHistory, OrderHistory.Parms>
    {
        public class Parms
        {
            public int OrderId { get; set; }
        }
        public int OrderId { get; set; }
        public OrderStatus OldStatus { get; set; }
        public OrderStatus NewStatus { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Remark { get; set; }
        public string ShippingNo { get; set; }
    }

    public enum OrderStatus
    {
        Created,
        Paid,
        Cancelled,
        Preparing,
        Sent,
        Delivered
    }

    public interface IOrderProvider : IDataProvider<Order, Order.Parms>
    {
        void Pay(int orderId, string paymentId, string paymentGuid, OrderStatus status);
    }
}
