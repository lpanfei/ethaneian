﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Product : DataManagerBase<Product, Product.Parms>
    {
        public class ProductEqualityComparer : IEqualityComparer<Product>
        {
            public bool Equals(Product x, Product y)
            {
                return x.Id == y.Id;
            }

            public int GetHashCode(Product obj)
            {
                return obj.Id.GetHashCode();
            }
        }

        public class Parms
        {
            public int Id { get; set; }
            public bool ById { get; set; }
            public bool AllProducts { get; set; }
            public bool ByName { get; set; }
            public bool ByCategory { get; set; }
            public int CategoryId { get; set; }
            public string Name { get; set; }
            public int BrandId { get; set; }
            public bool ProductList { get; set; }
            public bool ActiveSales { get; set; }
            public bool Recommended { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public User LastModifiedBy { get; set; }
        public int Quantity { get; set; }
        public Category Category { get; set; }
        public List<ProductPicture> Pictures { get; set; }
        public ProductDiscount Discount { get; set; }
        public ProductPromotion Promotion { get; set; }
        public decimal Weight { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsFeatured { get; set; }
        public Brand Brand { get; set; }
        public Country Country { get; set; }
        public bool IsHidden { get; set; }
        public string BrandName
        {
            get
            {
                if (Brand == null)
                    return "";
                else
                    return Brand.Name;
            }
        }
        public decimal DiscountAmount
        {
            get
            {
                if (Discount != null)
                {
                    return Discount.ByValue ? Discount.Discount : Discount.Discount * Price / 100;
                }
                return 0;
            }
        }
        public List<ProductTag> Tags { get; set; }
        public bool IsRecommended { get; set; }
        public List<Product> RelatedItems { get; set; }
        public List<Product> Accessories { get; set; }

        public static void Feature(int id, bool isFeatured)
        {
            ((IProductProvider)Provider).Feature(id,isFeatured);
        }
        public static void Recommend(int id, bool isRecommended)
        {
            ((IProductProvider)Provider).Recommend(id, isRecommended);
        }
        public static void Hide(int id, bool isHidden)
        {
            ((IProductProvider)Provider).Hide(id, isHidden);
        }
    }

    public class ProductTag
    {
        public int ProductId { get; set; }
        public string Key { get; set; }
        public List<ProductTagValue> Values { get; set; }
    }
    public class ProductTagValue
    {
        public string Value { get; set; }
        public decimal Price { get; set; }
    }

    public interface IProductProvider : IDataProvider<Product, Product.Parms>
    {
        void Feature(int id, bool isFeatured);
        void Recommend(int id, bool isRecommended);
        void Hide(int id, bool isHidden);
    }

    public class ProductPicture : DataManagerBase<ProductPicture, ProductPicture.Parms>
    {
        public class Parms
        {
        }

        public int ProductId { get; set; }
        public string Picture { get; set; }
        public bool IsDefault { get; set; }
        public string Title { get; set; }
    }

    public class ProductDiscount : DataManagerBase<ProductDiscount, ProductDiscount.Parms>
    {
        public class Parms
        {
            public int ProductId { get; set; }
            public bool ByProduct { get; set; }
        }

        public int Id { get; set; }
        public int ProductId { get; set; } 
        public decimal Discount { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime EndDate { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public bool ByValue { get; set; }
        public bool IsDeleted { get; set; }
        public User LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }

    public class ProductPromotion : DataManagerBase<ProductPromotion, ProductPromotion.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime EndDate { get; set; }
        }
        public int Id { get; set; }
        public Product Product { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime EndDate { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class StockHistory : DataManagerBase<StockHistory, StockHistory.Parms>
    {
        public class Parms
        {
            public int ProductId { get; set; }
        }

        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
