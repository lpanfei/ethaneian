﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    [Serializable]
    public class User : DataManagerBase<User, User.Parms>
    {
        public class Parms
        {
            public string Email { get; set; }
            public bool AllUsers { get; set; }
            public bool ById { get; set; }
            public int UserId { get; set; }
        }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Avator { get; set; }
        public List<UserAddress> Addresses { get; set; }
        public List<UserRole> Roles { get; set; }
    }

    public class UserAddress : DataManagerBase<UserAddress, UserAddress.Parms>
    {
        public class Parms
        {
            public string UserName { get; set; }
        }
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string TelephoneNo { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public bool IsDefault { get; set; }

        public static void MarkDefault(int id)
        {
            ((IUserAddressProvider)Provider).MarkAsDefault(id);
        }
    }

    public class UserRole : DataManagerBase<UserRole, UserRole.Parms>
    {
        public class Parms
        {
        }
        public string RoleName { get; set; }
    }

    public interface IUserAddressProvider : IDataProvider<UserAddress, UserAddress.Parms>
    {
        void MarkAsDefault(int id);
    }
}
