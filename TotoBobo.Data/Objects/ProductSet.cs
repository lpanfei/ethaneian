﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class ProductSet : DataManagerBase<ProductSet, ProductSet.Parms>
    {
        public class Parms
        {
            public int Id { get; set; }
            public bool ById { get; set; }
            public bool ByAll { get; set; }
            public string Name { get; set; }
            public bool ByName { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public User CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public User LastModifiedBy { get; set; }
        public List<ProductSetPicture> Pictures { get; set; }
        public List<ProductSetItem> Items { get; set; }
        public bool IsDeleted { get; set; }
        public int OptionalCount { get; set; }
    }

    public class ProductSetPicture : DataManagerBase<ProductSetPicture, ProductSetPicture.Parms>
    {
        public class Parms
        {
        }

        public int ProductSetId { get; set; }
        public string Picture { get; set; }
        public bool IsDefault { get; set; }
    }

    public class ProductSetItem : DataManagerBase<ProductSetItem, ProductSetItem.Parms>
    {
        public class Parms
        {
        }

        public int ProductSetId { get; set; }
        public int ProductId { get; set; }
        public bool IsMandatory { get; set; }
        public Product Product { get; set; }
    }
}
