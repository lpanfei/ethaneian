﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Setting : DataManagerBase<Setting, Setting.Parms>
    {
        public class Parms
        {
        }

        public int Id { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal TaxRate { get; set; }
        public decimal GiftSetDiscount { get; set; }
        public decimal RepeatCustomerDiscount { get; set; }
    }

    public class SaleBanner : DataManagerBase<SaleBanner, SaleBanner.Parms>
    {
        public class Parms
        {
        }
        public int Id { get; set; }
        public string TopBanner { get; set; }
        public string Banner1 { get; set; }
        public string Banner1Link { get; set; }
    }
}
