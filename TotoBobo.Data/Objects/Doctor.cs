﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Doctor : DataManagerBase<Doctor, Doctor.Parms>
    {
        public class Parms
        {
            public bool ById { get; set; }
            public int Id { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Address { get; set; }
        public string Avator { get; set; }
        public int UserId { get; set; }
    }
}
