﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace TotoBobo.Data.Objects
{
    public class Brand : DataManagerBase<Brand, Brand.Parms>
    {
        public class Parms
        {
            public bool ByName { get; set; }
            public string Name { get; set; }
            public bool ById { get; set; }
            public int Id { get; set; }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public bool IsDeleted { get; set; }
    }
}
