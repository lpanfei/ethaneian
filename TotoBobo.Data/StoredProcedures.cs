﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TotoBobo.Data
{
    public static class StoredProcedures
    {
        #region account
        public static string selUserByEmail = "spSelUserByEmail";
        public static string insUpdAddresses = "spInsUpdUserAddress";
        public static string selAddressesByUser = "spSelAddressesByUser";
        public static string delAddress = "spDelUserAddress";
        public static string markDefault = "spMarkAddressAsDefault";
        public static string allUsers = "spSelAllUsers";
        public static string selUserById = "spSelUserById";
        #endregion

        #region category
        public static string selCategoryById = "spSelCategoryById";
        public static string allCategories = "spSelAllCategories";
        public static string insUpdCategory = "spInsUpdCategory";
        public static string delCategoryById = "spDelCategoryById";
        public static string selCategoryByName = "spSelCategoryByName";
        public static string updCategoryBanner = "spUpdCategoryBanner";
        #endregion

        #region product
        public static string allProducts = "spSelAllProducts";
        public static string selProductById = "spSelProductById";
        public static string insUpdProduct = "spInsUpdProduct";
        public static string delProductById = "spDelProductById";
        public static string selProductByName = "spSelProductByName";
        public static string selStockHistoryById = "spSelStockHistoryById";
        public static string stockToInventory = "spStockToInventory";
        public static string selProductByCategory = "spSelProductByCategory";
        public static string featureProduct = "spFeatureProduct";
        public static string selProductList = "spSelProductList";
        public static string recommendProduct = "spRecommendProduct";
        public static string selRecommended = "spSelRecommendedProducts";
        public static string hideProduct = "spHideProduct";
        #endregion

        #region master data
        public static string allCountries = "spSelCountries";
        public static string selCountryByName = "spSelCountryByName";
        public static string selCountryById = "spSelCountry";
        public static string insUpdCountry = "spInsUpdCountry";
        public static string delCountry = "spDelCountry";

        public static string allStates = "spSelStates";
        public static string selStateById = "spSelStateById";
        public static string selStateByName = "spSelStateByName";
        public static string insUpdState = "spInsUpdState";
        public static string delState = "spDelState";

        public static string allCities = "spSelCities";
        public static string selCityById = "spSelCityById";
        public static string selCityByName = "spSelCityByName";
        public static string insUpdCity = "spInsUpdCity";
        public static string delCity = "spDelCity";

        public static string allPaymentMethods = "spSelPaymentMethods";
        public static string selPaymentMethodByName = "spSelPaymentMethodByName";
        public static string selPaymentMethodById = "spSelPaymentMethodById";
        public static string insUpdPaymentMethod = "spInsUpdPaymentMethod";

        public static string allDeliveryMethods = "spSelDeliveryMethods";
        public static string selDeliveryMethodByName = "spSelDeliveryMethodByName";
        public static string selDeliveryMethodById = "spSelDeliveryMethodById";
        public static string insUpdDeliveryMethod = "spInsUpdDeliveryMethod";

        public static string allDeliveryFee = "spSelDeliveryFee";
        public static string selDeliveryFeeByState = "spSelDeliveryFeeByState";
        public static string insUpdDeliveryFee = "spInsUpdDeliveryFee";
        public static string delDeliveryFee = "spDelDeliveryFee";
        #endregion

        #region order
        public static string insUpdOrder = "spInsUpdOrder";
        public static string SelOrdersByUser = "spSelOrdersByUser";
        public static string SelOrdersById = "spSelOrderById";
        public static string SelOrdersByGuid = "spSelOrderByPaymentGuid";
        public static string changeStatus = "spUpdOrderStatus";
        public static string payOrder = "spPayOrder";
        public static string allOrders = "spAllOrders";
        #endregion

        #region setting
        public static string selSetting = "spSelSetting";
        public static string insUpdSetting = "spInsUpdSetting";
        #endregion

        #region sale banner
        public static string selSaleBanner = "spSelSaleBanner";
        public static string createSaleBanner = "spInsUpdSaleBanner";
        #endregion

        #region banners
        public static string insUpdHomeBanner = "spInsUpdHomeBanner";
        public static string selHomeBanners = "spSelHomeBanners";
        public static string delHomeBanner = "spDelHomeBanner";
        public static string selHomeBannerById = "spSelHomeBannerById";
        #endregion

        #region brand
        public static string allBrands = "spSelBrands";
        public static string selBrandByName = "spSelBrandByName";
        public static string selBrandById = "spSelBrandById";
        public static string insUpdBrand = "spInsUpdBrand";
        public static string delBrand = "spDelBrand";
        #endregion

        #region newsletter
        public static string allNewsletters = "spSelNewsletters";
        public static string selNewsletterById = "spSelNewsletterById";
        public static string insUpdNewsletter = "spInsUpdNewsletter";
        public static string delNewsletter = "spDelNewsletter";
        #endregion

        #region reports
        public static string chartOrderStatus = "spChartOrderStatus";
        public static string chartBestSeller = "spChartBestSeller";
        #endregion

        #region captcha
        public static string selCaptcha = "spSelCaptcha";
        public static string insUpdCaptcha = "spInsUpdCaptcha";
        public static string delCaptcha = "spDelCaptcha";
        #endregion

        #region sales
        public static string selSalesByProduct = "spSelSales";
        public static string selActiveSales = "spSelActiveSales";
        public static string insUpdSale = "spInsUpdSale";
        public static string delSale = "spDelSale";
        #endregion

        #region cart
        public static string selCartItems = "spSelCartItems";
        public static string insUpdCartItem = "spInsUpdCartItem";
        public static string delCartItem = "spDelCartItem";
        public static string changeQty = "spChangeCartItemQuantity";
        public static string clearCart = "spClearCartItems";
        #endregion

        #region question
        public static string selQuestions = "spSelQuestions";
        public static string selQuestionById = "spSelQuestionById";
        public static string selQuestionTypes = "spSelQuestionTypes";
        public static string delQuestion = "spDelQuestion";
        public static string delQuestionType = "spDelQuestionType";
        public static string selQuestionTypeById = "spSelQuestionTypeById";
        public static string insUpdQuestion = "spInsUpdQuestion";
        public static string insUpdQuestionType = "spInsUpdQuestionType";
        public static string insUpdAnswer = "spInsUpdAnswer";
        public static string selAnswer = "spSelAnswer";
        #endregion

        #region product set
        public static string allProductSet = "spSelProductSet";
        public static string selProductSetById = "spSelProductSetById";
        public static string insUpdProductSet = "spInsUpdProductSet";
        public static string delProductSet = "spDelProductSet";
        public static string selProductSetByName = "spSelProductSetByName";
        #endregion

        #region doctor
        public static string selDoctors = "spSelDoctors";
        public static string selDoctorById = "spSelDoctorById";
        public static string insUpdDoctor = "spInsUpdDoctor";
        public static string delDoctor = "spDelDoctor";
        #endregion

        #region user        
        public static string delUser = "spDelUserById";
        public static string updUser = "spUpdUser";
        #endregion
    }
}
