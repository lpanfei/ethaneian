﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using TotoBobo.Data.Objects;
using TotoBobo.Data.DBProviders;

namespace TotoBobo.Data
{
    public static class DBInitializer
    {
        public static void Initialize(string connectionString)
        {
            DBTools.ConnectionString = connectionString;
            DataManagerBase<User, User.Parms>.Provider = new UserDB();
            DataManagerBase<UserAddress, UserAddress.Parms>.Provider = new UserAddressDB();
            DataManagerBase<Category, Category.Parms>.Provider = new CategoryDB();
            DataManagerBase<Product, Product.Parms>.Provider = new ProductDB();
            DataManagerBase<StockHistory, StockHistory.Parms>.Provider = new StockHistoryDB();

            DataManagerBase<Country, Country.Parms>.Provider = new CountryDB();
            DataManagerBase<State, State.Parms>.Provider = new StateDB();
            DataManagerBase<City, City.Parms>.Provider = new CityDB();
            DataManagerBase<PaymentMethod, PaymentMethod.Parms>.Provider = new PaymentMethodDB();
            DataManagerBase<DeliveryMethod, DeliveryMethod.Parms>.Provider = new DeliveryMethodDB();
            DataManagerBase<DeliveryFee, DeliveryFee.Parms>.Provider = new DeliveryFeeDB();

            DataManagerBase<Order, Order.Parms>.Provider = new OrderDB();
            DataManagerBase<OrderHistory, OrderHistory.Parms>.Provider = new OrderHistoryDB();

            DataManagerBase<Setting, Setting.Parms>.Provider = new SettingDB();

            DataManagerBase<HomeBanner, HomeBanner.Parms>.Provider = new HomeBannerDB();

            DataManagerBase<Brand, Brand.Parms>.Provider = new BrandDB();

            DataManagerBase<Newsletter, Newsletter.Parms>.Provider = new NewsletterDB();

            DataManagerBase<PieChart, PieChart.Parms>.Provider = new PieChartDB();

            DataManagerBase<Captcha, Captcha.Parms>.Provider = new CaptchaDB();

            DataManagerBase<ProductDiscount, ProductDiscount.Parms>.Provider = new ProductDiscountDB();

            DataManagerBase<SaleBanner, SaleBanner.Parms>.Provider = new SaleBannerDB();

            DataManagerBase<CartItem, CartItem.Parms>.Provider = new CartItemDB();

            DataManagerBase<Question, Question.Parms>.Provider = new QuestionDB();
            DataManagerBase<QuestionType, QuestionType.Parms>.Provider = new QuestionTypeDB();

            DataManagerBase<ProductSet, ProductSet.Parms>.Provider = new ProductSetDB();

            DataManagerBase<Doctor, Doctor.Parms>.Provider = new DoctorDB();

            DataManagerBase<Answer, Answer.Parms>.Provider = new AnswerDB();
        }
    }
}
